"""ombazar URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.conf.urls.static import static
from django.views.static import serve as static_serve
from django.conf import settings
urlpatterns = [
    path('admin/', admin.site.urls),
    path('control/', include("OmbazarBackEnd.urls")),
    path('', include("OmbazarWeb.urls")),
]


def serve(request, path, document_root=None, show_indexes=False):
    """
    An override to `django.views.static.serve` that will allow us to add our
    own headers for development.

    Like `django.views.static.serve`, this should only ever be used in
    development, and never in production.
    """
    response = static_serve(request, path, document_root=document_root, show_indexes=show_indexes)

    # response['Access-Control-Allow-Origin'] = '*'
    response['Cache-Control'] = 'max-age=31536000'
    return response


urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT, view=serve)
urlpatterns += static(settings.BASE_URL, document_root=settings.BASE_ROOT, view=serve)

urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT, view=serve)
