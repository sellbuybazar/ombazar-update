from django.shortcuts import render
from django.http import JsonResponse, HttpResponse
from .retrieve import WebRetrieve
from .modify import WebModify
from .passed import WebPassed
from .common import WebCommon
Retrieve = WebRetrieve()
Common = WebCommon()
Passed = WebPassed()
Modify = WebModify()

# Create your views here.


def index(request):
    if request.session:
        return render(request, "OmbazarWeb/pages/home.html")


def request(request):
        errors = {}
        messages = {}
        return_object = {
            "status": 0,
            "message": messages,
            "error": errors
        }
        if request.method == "POST":
            try:
                result = ''
                class_name = ''
                method_name = request.POST['request_name']
                request_type = request.POST['request_type']

                if request_type == 'get':
                    class_name = "WebRetrieve()"
                elif request_type == 'update':
                    class_name = "WebModify()"
                elif request_type == "post":
                    class_name = "WebPassed()"
                elif request_type == "delete":
                    class_name = "WebDelete()"
                elif request_type == "asset":
                    class_name = "WebCommon()"

                method = class_name+'.'+method_name
                result = eval(method)
                if not result:
                    not_found_object = {"not_found": "Data not found", "length": 0}
                    result = not_found_object
                elif 'status' in result:
                    if not result['status']:
                        result['form_data'] = request.POST

            except Exception as e:
                error_message = "Method not found"
                return_object['status'] = 0
                errors['error'] = error_message
                errors['error_details'] = str(Common.error_info())
                return_object['post_data'] = request.POST
                return_object['method'] = method
                return_object['result'] = str(result)
            else:
                return JsonResponse(result)
        else:
            error_message = "Post/Form data empty"
            errors['error'] = error_message
            return_object['status'] = 0
        return JsonResponse(return_object)


