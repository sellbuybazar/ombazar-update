<?php require_once "helper.php";?>
<?php

?>
<div class="uk-modal-dialog">
    <div class="uk-modal-header">
        <h3 class="uk-modal-title">Barcode generator</h3>
    </div>
    <form class="barcode-generate-form" id="customer-form">
        <div class="modal-body">
            <div class="uk-grid">
                <div class="uk-width-medium-5-10 parsley-row">
                    <select class="bar-code-selectize" required name="bar_code">
                        <option value="">Select barcode type</option>
                    </select>
                </div>
                <div class="uk-width-medium-5-10 parsley-row">
                    <label>Barcode</label>
                    <input type="text" class="md-input" required name="text">
                </div>
                <p class="bar-code-info">

                </p>
                <div class="print-button-area hidden">
                    <button class="md-btn md-btn-success md-btn-mini"><i class="material-icons">print</i></button>
                </div>
                <div class=" bar-code-image-layout">

                </div>
            </div>


        </div>

        <div class="uk-modal-footer uk-text-right">
            <button type="button" class="md-btn md-btn-flat uk-modal-close">Close</button>
            <button type="button" class="md-btn md-btn-flat md-btn-flat-success generate-done-button hidden" >Done</button>
            <button class="md-btn md-btn-flat md-btn-flat-primary generate-button" >Generate</button>
        </div>
    </form>
</div>

<script>
    altair_form_validation.init();
</script>

