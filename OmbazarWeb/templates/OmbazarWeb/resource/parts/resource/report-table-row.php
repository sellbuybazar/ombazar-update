<?php require_once "support.php"?>
{{#each rows}}
<tr id="row_id_{{id}}" data-id="{{id}}">
    <td><input type="checkbox" data-md-icheck class="ts_checkbox"></td>
    <td>
        {{report_type}}
    </td>
    <td class="active-colum">
        {{generator}}
    </td>
    <td>{{date_range}}</td>
    <td>
        {{time}}
    </td>
    <td class="uk-text-center icon-relative">
        <div class="uk-button-dropdown" data-uk-dropdown="{pos:'left-top',mode:'click'}">
            <i class="md-icon material-icons">&#xE5D4;</i>
            <div class="uk-dropdown uk-dropdown-small">
                <ul class="uk-nav uk-text-left">
                    <li><a href="{{view_path}}" class="uk-text-info"><i class="material-icons">create</i> View</a></li>
                </ul>
            </div>
        </div>
    </td>
</tr>
{{/each}}