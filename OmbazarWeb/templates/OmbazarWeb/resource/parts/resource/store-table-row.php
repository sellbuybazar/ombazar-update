<?php require_once "support.php"?>
{{#each rows}}
<tr id="row_id_{{id}}" data-id="{{id}}">
    <td><input type="checkbox" data-md-icheck class="ts_checkbox"></td>
    <td><img class="md-user-image" src="{{image_path}}" alt=""/></td>
    <td>
        <a href="{{activate_link}}">{{store_name}}</a>
    </td>
    <td class="active-colum">
        {{#if active}}
            <span class="uk-badge uk-badge-success">Active</span>
        {{else}}
            <span class="uk-badge uk-badge-danger">Deactive</span>
        {{/if}}
    </td>
    <td>{{post_code}}</td>
    <td>
        {{time}}

    </td>
    <td class="uk-text-center icon-relative">
        <div class="uk-button-dropdown" data-uk-dropdown="{pos:'left-top',mode:'click'}">
            <i class="md-icon material-icons">&#xE5D4;</i>
            <div class="uk-dropdown uk-dropdown-small">
                <ul class="uk-nav uk-text-left">
                    <li><a href="{{edit_path}}" class="uk-text-info"><i class="material-icons">create</i> Edit</a></li>
<!--                        {{#if active}}-->
<!--                            <li><a href="#"  class="un_active" data-table="store" data-id="{{id}}"   class="uk-text-danger"><i class="material-icons ">settings_power</i> Deactive </a></li>-->
<!--                        {{else}}-->
<!--                            <li><a href="#"  class="active" data-table="store" data-id="{{id}}"   class="uk-text-danger"><i class="material-icons ">settings_power</i> Active</a></li>-->
<!--                        {{/if}}-->
                </ul>
            </div>
        </div>
    </td>
</tr>
{{/each}}