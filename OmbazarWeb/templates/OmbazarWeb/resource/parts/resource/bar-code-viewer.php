<?php require_once "helper.php";?>
<?php

?>
<div class="uk-modal-dialog">
    <div class="uk-modal-header">
        <h3 class="uk-modal-title">Barcode preview</h3>
    </div>
        <div class="modal-body">
            <div class="uk-grid">

                <p class="bar-code-info">

                </p>
                <div class="area-print-button print-button-area hidden">
                    <button class="md-btn md-btn-success md-btn-mini"><i class="material-icons">print</i></button>
                </div>
                <div class=" bar-code-image-layout print-visible">

                </div>
            </div>


        </div>

        <div class="uk-modal-footer uk-text-right">
            <button type="button" class="md-btn md-btn-flat uk-modal-close">Close</button>
        </div>

</div>



