<?php require_once "support.php"?>
{{#each rows}}

<tr id="row_id_{{id}}" data-id="{{id}}">
    <td>
        <a href="{{view_path}}"  class="no_load"><img class="img_thumb" src="{{image_path}}" alt=""/></a>
    </td>
    <td>
        <a href="{{view_path}}"  class="no_load">
            <p class="uk-text-bold">
                <span class="uk-text-bold">{{name}}</span>
            </p>
            <p class="corporate-address">{{address}}</p>
        </a>
    </td>

</tr>
{{/each}}