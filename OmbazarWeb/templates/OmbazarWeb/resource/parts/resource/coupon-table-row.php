<?php require_once "support.php"?>
{{#each rows}}
<tr id="row_id_{{id}}" data-id="{{id}}">
    <td class="hidden-print"><input type="checkbox" data-md-icheck class="ts_checkbox"></td>
    <td>
        <div class="coupon-image">
            <img src="{{coupon_image}}">
            <span>{{coupon_readable_number}}</span>
            <button class="copy-button md-btn md-btn-mini md-btn-default"  data-clipboard-text="{{coupon_number}}"><i class="fa fa-clipboard" aria-hidden="true"></i></button>
        </div>

    </td>

    <td class="active-colum hidden-print">
        <span class="uk-badge uk-badge-success">{{type}}</span>
    </td>
    <td class="active-colum hidden-print">
        {{batch}}
    </td>

    <td class="hidden-print">
        {{time}}

    </td>

</tr>
{{/each}}
<script>
    altair_forms.switches();
    copy_init();
</script>
