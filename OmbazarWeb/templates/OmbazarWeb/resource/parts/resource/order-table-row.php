<?php require_once "support.php"?>
{{#each rows}}
<tr id="row_id_{{row_id}}" data-id="{{row_id}}">
    <td><input type="checkbox" data-md-icheck class="ts_checkbox"></td>
    <td>
        {{order_id}}
    </td>
    <td class="active-colum">
        {{#if status}}
            <span class="uk-badge uk-badge-success">Done</span>
        {{else}}
            <span class="uk-badge uk-badge-danger">Pending</span>
        {{/if}}
    </td>
    <td>{{mobile}}</td>
    <td>{{email}}</td>
    <td>
        {{time}}

    </td>
    <td class="uk-text-center icon-relative">
        <div class="uk-button-dropdown" data-uk-dropdown="{pos:'left-top',mode:'click'}">
            <i class="md-icon material-icons">&#xE5D4;</i>
            <div class="uk-dropdown uk-dropdown-small">
                <ul class="uk-nav uk-text-left">
                    <li><a href="{{view_path}}" class="uk-text-info"><i class="material-icons">create</i> View</a></li>

<!--                        {{#if active}}-->
<!--                            <li><a href="#"  class="un_active" data-table="controller_short_info" data-id="{{row_id}}"   class="uk-text-danger"><i class="material-icons ">settings_power</i> Deactive </a></li>-->
<!--                        {{else}}-->
<!--                            <li><a href="#"  class="active" data-table="controller_short_info" data-id="{{row_id}}"   class="uk-text-danger"><i class="material-icons ">settings_power</i> Active</a></li>-->
<!--                        {{/if}}-->
                </ul>
            </div>
        </div>
    </td>
</tr>
{{/each}}