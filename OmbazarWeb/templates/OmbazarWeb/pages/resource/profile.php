    <?php
        if(!isset($data)){
            $pars_url=parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
        }
        else{
            $pars_url=$data;
        }
        $divi=explode("/",$pars_url);
        if(isset($divi[2])){
        $referel_id=$divi[2];
        $id_info=$retrive->user_info_by_referel_id($referel_id);
        foreach($id_info as $info){

    ?>

    <div id="page_content">
        <div id="top_bar">
            <ul id="breadcrumbs">
                <li><a href="<?php echo SITE_URL;?>home" class="no_load">Home</a></li>
                <li><a href="<?php echo $pars_url;?>" class="no_load">Profile</a></li>
                <li><span>Details</span></li>
            </ul>
        </div>
        <div id="page_content_inner">
            <?php

                $user_id=$info['user_id'];
                $user_info=$retrive->controller_info($user_id);
                $referrel_info=$retrive->referrel_info($user_id);
                foreach ($user_info as  $result){
            ?>
            <div class="uk-grid" data-uk-grid-margin data-uk-grid-match id="user_profile">
                <div class="uk-width-large-10-10">
                    <div class="md-card">
                        <div class="user_heading">
                            <div class="user_heading_avatar">
                                <div class="thumbnail">
                                    <img src="<?php echo SITE_URL;?>assets/img/images/<?php echo $result['image']?>" alt="user avatar"/>
                                </div>
                            </div>
                            <div class="user_heading_content">
                                <h2 class="heading_b uk-margin-bottom"><span class="uk-text-truncate"><?php echo $result['name'];?></span><span class="sub-heading"></span></h2>
                                <ul class="user_stats">
                                    <li>
                                        <h4 class="heading_a">
                                        <?php
                                            $d=$retrive->user_order_list("","","<",true);
                                            echo $d['data'];
                                        ?>
                                        <span class="sub-heading">Order</span></h4>
                                    </li>
                                    <li>
                                        <h4 class="heading_a">
                                            <?php
                                            echo $retrive->user_balance();
                                            ?>
                                            <span class="sub-heading">Balance</span></h4>
                                    </li>
                                </ul>
                            </div>
                            <?php if(isset($_SESSION['user_id'])){
                                if($_SESSION['user_id']==$user_id){
                                ?>
                            <a class="md-fab md-fab-small md-fab-accent hidden-print no_load" href="<?php echo SITE_URL;?>edit-profile/?user=<?php echo $user_id;?>">
                                <i class="material-icons">&#xE150;</i>
                            </a>
                            <?php } } ?>
                        </div>
                        <div class="user_content">
                            <ul id="user_profile_tabs" class="uk-tab" data-uk-tab="{connect:'#user_profile_tabs_content', animation:'slide-horizontal'}" data-uk-sticky="{ top: 48, media: 960 }">
                                <li class="uk-active"><a href="#">About</a></li>
                            </ul>
                            <ul id="user_profile_tabs_content" class="uk-switcher uk-margin">
                                <li>
                                    <div class="uk-grid uk-margin-medium-top uk-margin-large-bottom" data-uk-grid-margin>
                                        <div class="uk-width-large-1-2">
                                            <h4 class="heading_c uk-margin-small-bottom">Contact Info</h4>
                                            <ul class="md-list md-list-addon">
                                                <li>
                                                    <div class="md-list-addon-element">
                                                        <i class="md-list-addon-icon material-icons">&#xE158;</i>
                                                    </div>
                                                    <div class="md-list-content">
                                                        <span class="md-list-heading"><?php echo $result['email']?></span>
                                                        <span class="uk-text-small uk-text-muted">Email</span>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="md-list-addon-element">
                                                        <i class="md-list-addon-icon material-icons">&#xE0CD;</i>
                                                    </div>
                                                    <div class="md-list-content">
                                                        <span class="md-list-heading"><?php echo $result['calling_code'].$result['mobile'];?></span>
                                                        <span class="uk-text-small uk-text-muted">Phone</span>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="md-list-addon-element">
                                                        <i class="md-list-addon-icon material-icons">location_on</i>
                                                    </div>
                                                    <div class="md-list-content">
                                                        <span class="md-list-heading">
                                                            <?php echo $result['address_line_1'];?><br>
                                                            <?php echo $result['address_line_2'];?>
                                                        </span>
                                                        <span class="uk-text-small uk-text-muted">Address</span>
                                                    </div>
                                                </li>
                                                <?php if($referrel_info){?>
                                                <li>
                                                    <div class="md-list-addon-element">
                                                        <i class="md-list-addon-icon material-icons">insert_link</i>
                                                    </div>
                                                    <div class="md-list-content">
                                                        <?php
                                                            $url=SITE_URL."profile/".$referrel_info;
                                                        ?>
                                                        <span class="md-list-heading"><a href="<?php echo $url;?>"><?php echo $url;?></a></span>
                                                        <span class="uk-text-small uk-text-muted">Profile link</span>
                                                    </div>
                                                </li>
                                                <?php } ?>
                                            </ul>
                                        </div>

                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>

            </div>
            <?php } } }?>
        </div>
    </div>