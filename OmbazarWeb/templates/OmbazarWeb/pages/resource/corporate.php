<?php
    $id=$_REQUEST['id'];
    $info=$retrive->corporate_info($id);
?>
<div id="page_content">
    <div id="top_bar">
        <ul id="breadcrumbs">
            <li><a href="<?php echo SITE_URL;?>home" class="no_load">Home</a></li>
            <li><a href="<?php echo SITE_URL;?>corporates" class="no_load">Corporates</a></li>
            <li><span>Details</span></li>
        </ul>
    </div>
    <div id="page_content_inner">
        <?php
            foreach($info as $result){
        ?>
        <div class="uk-grid">
            <div class="uk-width-medium-1-10"></div>
            <div class="uk-width-medium-8-10 corporate-slide">
                <div class="md-card">
                    <div class="md-card-content">
                        <h2><?php echo $result['name']?></h2>
                        <div class="uk-slidenav-position uk-margin-large-bottom" data-uk-slider="{autoplayInterval: '3000',infinite:false}">
                            <div class="uk-slider-container">
                                <ul class="uk-slider uk-grid " data-uk-grid-margin>
                                    <?php
                                    $images=$result['images'];
                                    foreach($images as $image){
                                        ?>
                                        <li><img src="<?php echo SITE_URL;?>assets/img/images/<?php echo $image['image']?>" </li>
                                    <?php } ?>
                                </ul>
                            </div>
                            <a href="#" class="uk-slidenav uk-slidenav-previous" data-uk-slider-item="previous"></a>
                            <a href="#" class="uk-slidenav uk-slidenav-next" data-uk-slider-item="next"></a>
                        </div>
                        <h3 class="not-found"></h3>
                        <hr class="hr"/>
                        <div class="uk-grid" data-uk-grid-margin>
                            <div class="uk-width-medium-5-10">
                                <h4>Location</h4>
                                <?php echo $result['location'];?>
                                <h4>Address</h4>
                                <?php echo $result['address'];?>

                            </div>
                            <div class="uk-width-medium-5-10">
                                <h4>Tags</h4>
                                <?php
                                    $tags=explode(",",$result['tags']);
                                    foreach($tags as $tag){

                                ?>
                                        <span class="uk-badge"><?php echo $tag;?></span>
                                <?php } ?>
                            </div>

                        </div>
                        <?php
                            if(isset($_SESSION['user_id'])){
                                $id_type_name=$common->id_type_name($common->user_id);

                                if($id_type_name=="admin" or $id_type_name=="cs"){
                        ?>
                        <div class="uk-grid" data-uk-grid-margin>
                            <div class="uk-width-medium-5-10">
                                <h4>Phone</h4>
                                <?php echo $result['phone'];?>
                                <h4>Email</h4>
                                <?php echo $result['email'];?>
                            </div>
                            <div class="uk-width-medium-5-10">
                                <h3 class="">Notes</h3>
                                <div class="uk-grid " data-uk-grid="{gutter:12}">
                                    <?php
                                        $notes=$result['notes'];
                                        foreach($notes as $note){
                                    ?>
                                    <div class="uk-width-medium-7-10">
                                        <div class="md-card ">
                                            <div class="md-card-toolbar">
                                                <div class="md-card-toolbar-actions">
                                                    <i class="md-icon material-icons md-card-fullscreen-activate">&#xE5D0;</i>
<!--                                                    <i class="md-icon material-icons md-card-toggle">&#xE316;</i>-->
                                                </div>
                                                <h3 class="md-card-toolbar-heading-text">
                                                    Note <?php
                                                        $time=$note['time'];
                                                        echo $common->simple_date($time,"D M H:i:s A");
                                                    ?>
                                                </h3>
                                            </div>
                                            <div class="md-card-content note-content">
                                                <?php echo $note['val']?>
                                            </div>
                                        </div>
                                    </div>
                                    <?php  } ?>

                                </div>
                            </div>
                        </div>
                        <?php } } ?>

                    </div>
                </div>
            </div>
            <div class="uk-width-medium-1-10"></div>
        </div>
        <?php } ?>
    </div>
</div>
