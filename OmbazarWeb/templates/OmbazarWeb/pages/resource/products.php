    <?php 
        if(!isset($data)){
            $pars_url=parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
        }
        else{
            $pars_url=$data;
        }
        $divi=explode("/",$pars_url);
    ?>
    <div id="page_content">
        <div id="top_bar">
            <ul id="breadcrumbs">
                <li><a href="<?php echo SITE_URL;?>home" class="no_load">Home</a></li>
                <li><a href="<?php echo SITE_URL;?>products" class="no_load">Products</a></li>
                <?php if(isset($divi[2])){
                    if($divi[2]=="brand"){?>
                <li><a href="<?php echo SITE_URL;?>products/brand/<?php echo urldecode ($divi[3]);?>" class="no_load">Brand</a></li>
                <li><span><?php echo urldecode ($divi[3]);?></span></li>
                <?php } else{?>
                
                <li><span><?php echo urldecode ($divi[2]);?></span></li>
                <?php } } else{?>
                <li><span>Suggested</span></li>
                <?php } ?>
            </ul>
        </div>
        <div id="page_content_inner">
            <div class="">
                <form class="products-filter-form">
                    <?php
                    
                    if(isset($divi['2'])){
                        ?>
                        
                        <?php if($divi['2']=="brand"){?>
                        <input type="hidden" name="brand" value="<?php echo $divi[3];?>">
                    <?php }else{?>
                        <input type="hidden" name="category" value="<?php echo $divi[2];?>">
                    <?php }} ?>
                </form>
                <div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-medium-1-1">
                        <div class="products uk-grid-width-small-1-2 uk-grid-width-medium-1-3 uk-grid-width-large-1-5 " data-uk-grid="{gutter: 20, controls: '#products_sort'}">

                        </div>
                    </div>
                </div>
            </div>



        </div>
    </div>