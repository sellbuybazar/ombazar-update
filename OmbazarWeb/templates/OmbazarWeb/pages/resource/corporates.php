
<div id="page_content">
    <div id="top_bar">
        <ul id="breadcrumbs">
            <li><a href="<?php echo SITE_URL;?>home" class="no_load">Home</a></li>
            <li><a href="<?php echo SITE_URL;?>corporates" class="no_load">Corporates</a></li>
            <li><span>List</span></li>
        </ul>
    </div>
    <div id="page_content_inner">
        <div class="md-card">
            <div class="md-card-content">
            <form class="corporate-search">
                <div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-medium-3-10">
                        <label>Name</label>
                        <input type="text" class="md-input" value="<?php if(isset($_REQUEST['search_name'])){echo $_REQUEST['search_name'];};?>" name="search_name">
                    </div>
                    <div class="uk-width-medium-3-10">
                        <label>Location</label>
                        <input type="text" class="md-input" value="<?php if(isset($_REQUEST['search_location'])){echo $_REQUEST['search_location'];};?>" name="search_location">
                    </div>
                    <div class="uk-width-medium-2-10">
                        <label>Tags</label>
                        <input type="text" class="md-input" value="<?php if(isset($_REQUEST['search_tag'])){echo $_REQUEST['search_tag'];};?>" name="search_tags">
                    </div>
                    <div class="uk-width-medium-2-10 uk-text-center">
                        <button class="uk-margin-small-top md-btn md-btn-primary md-btn-wave-light waves-effect waves-button waves-light">Search</button>
                    </div>
                </div>
            </form>
            </div>
        </div>

        <div class="md-card">
            <div class="md-card-content">
                <div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-1-1">
                        <div class="uk-overflow-container">
                            <table class="uk-table user-uk-table uk-table-align-vertical corporate-search-table" data-type="corporate-search-table" limit="3">
                                <thead>
                                <tr>
                                    <th>Image</th>
                                    <th>Details</th>
                                </tr>
                                </thead>
                                <tbody>


                                </tbody>
                            </table>
                        </div>
                        <ul class="uk-pagination uk-margin-medium-top uk-margin-medium-bottom">
                            <li class="uk-disabled"><a href="#" class="previous-button"><i class="uk-icon-angle-double-left"></i> Previous</a> </li>

                            <li class="uk-text-center"><form class="page-go-form"><label>Page number</label> <input class=" md-input uk-form-width-small uk-text-center selectize-input go-page-number" type="text" value="<?php if(isset($_REQUEST['page_number'])){echo $_REQUEST['page_number'];} else{echo "1";};?>" name="page_number"> </form> </li>

                            <li class=""><a href="#" class="next-button">Next <i class="uk-icon-angle-double-right"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
