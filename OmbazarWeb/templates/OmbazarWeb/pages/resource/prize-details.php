
<div id="page_content">
    <div id="page_content_inner">
        <div class="uk-grid prize-card-container">
            <div class="uk-width-medium-1-10"></div>
            <div class="uk-width-medium-8-10 prize-card relative">
                <button class="prize-print-button md-btn md-btn-flat-success"><i class="material-icons  ">print</i></button>
                <?php
                    $group_id=$_REQUEST['prize'];
                    $coupon_id=$_REQUEST['coupon_id'];
                    $prize_info=$retrive->lucky_prize_info($group_id,$coupon_id);
                    foreach($prize_info as $result){
                ?>
                <div class="md-card">
                    <div class="md-card-content">
                        <div class="uk-grid">
                            <div class="uk-width-medium-3-10">
                                <img src="<?php echo SITE_URL;?>assets/img/default/om-bazar-logo.png" class="prize-company-logo">
                                <img src="<?php echo SITE_URL;?>assets/img/default/prize.png" class="prize-image">
                            </div>
                            <div class="uk-width-medium-7-10">
                                <h1>Coupon prize</h1>
                                <p class="prize-to"><span>To:</span> <span><?php echo $result['mobile']?> </span></p>
                                <p class="prize-from"><span>From:</span> <span>Ombazar.com</span></p>
                                <p class="prize-about">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Asperiores aspernatur at autem doloremque ea
                                </p>
                                <p class="receive-address">
                                    facere illum in incidunt ipsa iusto, labore laborum, libero magnam magni provident quaerat quibusdam vitae voluptatibus!
                                </p>
                                <div class="prize-bar-code">
                                    <?php echo $result['barcode']?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php } ?>

            </div>
            <div class="uk-width-medium-1-10"></div>
        </div>
    </div>
</div>
