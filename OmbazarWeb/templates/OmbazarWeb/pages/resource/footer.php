    <div id="page_content" class="footer">
        <span class="brand-icon" data-uk-tooltip="{pos:'top'}" title="Brands">
            <i class="material-icons">star</i>
        </span>
        <div id="page_content_inner">
            <div class="md-card fixed-brand visibility-off" id="top_bar" >
                <div class="md-card-content">
                    <h3 class="heading_a uk-margin-bottom">Brands</h3>
                    <div class="uk-slidenav-position" data-uk-slider="{autoplay: true,autoplayInterval: '5000',infinite:false}">

                        <div class="uk-slider-container">
                            <ul class="uk-slider brand-slider uk-grid uk-grid-small uk-width-medium-1-10 uk-width-small-2-10">
                                <?php 
                                    $brands=$retrive->brands();
                                    foreach($brands as $brand){
                                        
                                    
                                ?>
                                <li style="max-height: 200px"><a class="no_load" href="<?php echo SITE_URL;?>products/brand/<?php echo $brand['store_name']?>"><img src="<?php echo SITE_URL;?>assets/img/images/<?php echo $brand['image'];?>" alt=""></a></li>
                                <?php } ?>
                            </ul>
                        </div>

                        <a href="#" class="uk-slidenav uk-slidenav-contrast uk-slidenav-previous" data-uk-slider-item="previous"></a>
                        <a href="#" class="uk-slidenav uk-slidenav-contrast uk-slidenav-next" data-uk-slider-item="next"></a>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <form id="form">

    </form>
      <form id="customer-form">

    </form>
      <form id="vendor-form">

    </form>
    <!--    /////initial required script-->
    <script src="<?php echo SITE_URL; ?>assets/js/define.js"></script>
    <!-- common functions -->
    <script src="<?php echo SITE_URL; ?>assets/js/common.min.js"></script>
    <script src="<?php echo SITE_URL; ?>assets/js/plugins/js-cookie-master/src/js.cookie.js"></script>
    <script src="<?php echo SITE_URL; ?>assets/js/plugins/odometer-master/odometer.js"></script>
    <script type='text/javascript' src='<?php echo SITE_URL;?>assets/js/initial.js'></script>
<!--    <div id="style_switcher">-->
<!--        <div id="style_switcher_toggle"><i class="material-icons">&#xE8B8;</i></div>-->
<!--        <div class="uk-margin-medium-bottom">-->
<!--            <h4 class="heading_c uk-margin-bottom">Colors</h4>-->
<!--            <ul class="switcher_app_themes" id="theme_switcher">-->
<!--                <li class="app_style_default active_theme" data-app-theme="">-->
<!--                    <span class="app_color_main"></span>-->
<!--                    <span class="app_color_accent"></span>-->
<!--                </li>-->
<!--                <li class="switcher_theme_a" data-app-theme="app_theme_a">-->
<!--                    <span class="app_color_main"></span>-->
<!--                    <span class="app_color_accent"></span>-->
<!--                </li>-->
<!--                <li class="switcher_theme_b" data-app-theme="app_theme_b">-->
<!--                    <span class="app_color_main"></span>-->
<!--                    <span class="app_color_accent"></span>-->
<!--                </li>-->
<!--                <li class="switcher_theme_c" data-app-theme="app_theme_c">-->
<!--                    <span class="app_color_main"></span>-->
<!--                    <span class="app_color_accent"></span>-->
<!--                </li>-->
<!--                <li class="switcher_theme_d" data-app-theme="app_theme_d">-->
<!--                    <span class="app_color_main"></span>-->
<!--                    <span class="app_color_accent"></span>-->
<!--                </li>-->
<!--                <li class="switcher_theme_e" data-app-theme="app_theme_e">-->
<!--                    <span class="app_color_main"></span>-->
<!--                    <span class="app_color_accent"></span>-->
<!--                </li>-->
<!--                <li class="switcher_theme_f" data-app-theme="app_theme_f">-->
<!--                    <span class="app_color_main"></span>-->
<!--                    <span class="app_color_accent"></span>-->
<!--                </li>-->
<!--                <li class="switcher_theme_g" data-app-theme="app_theme_g">-->
<!--                    <span class="app_color_main"></span>-->
<!--                    <span class="app_color_accent"></span>-->
<!--                </li>-->
<!--                <li class="switcher_theme_h" data-app-theme="app_theme_h">-->
<!--                    <span class="app_color_main"></span>-->
<!--                    <span class="app_color_accent"></span>-->
<!--                </li>-->
<!--                <li class="switcher_theme_i" data-app-theme="app_theme_i">-->
<!--                    <span class="app_color_main"></span>-->
<!--                    <span class="app_color_accent"></span>-->
<!--                </li>-->
<!--                <li class="switcher_theme_dark" data-app-theme="app_theme_dark">-->
<!--                    <span class="app_color_main"></span>-->
<!--                    <span class="app_color_accent"></span>-->
<!--                </li>-->
<!--            </ul>-->
<!--        </div>-->
<!---->
<!--    </div>-->


</body>
</html>
