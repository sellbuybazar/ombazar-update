<div id="page_content">
    <div id="top_bar">
        <ul id="breadcrumbs">
            <li><a href="<?php echo SITE_URL;?>home" class="no_load">Home</a></li>
            <li><a href="<?php echo SITE_URL;?>coupon-unlocker" class="no_load">Coupon</a></li>
            <li><span>Unlocker</span></li>
        </ul>
    </div>
    <div id="page_content_inner">
        <div class="uk-grid">
            <div class="uk-width-medium-2-10"></div>
            <div class="uk-width-medium-6-10 coupon-slider" data-limit="25">
                <div class="uk-slidenav-position uk-margin-large-bottom" data-uk-slider="{center:true,autoplayInterval: '3000',infinite:false}">
                    <div class="uk-slider-container">
                        <ul class="uk-slider uk-slider-center uk-grid " data-uk-grid-margin>

                        </ul>
                    </div>
                    <a href="#" class="uk-slidenav uk-slidenav-previous" data-uk-slider-item="previous"></a>
                    <a href="#" class="uk-slidenav uk-slidenav-next" data-uk-slider-item="next"></a>
                </div>
                <h3 class="not-found"></h3>
            </div>
            <div class="uk-width-medium-2-10"></div>
        </div>
    </div>
</div>
