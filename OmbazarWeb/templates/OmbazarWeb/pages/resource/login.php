<div id="page_content">
    <div id="page_content_inner">
        <div class="login_page_wrapper">
            <div class="md-card" id="login_card">
                <div class="md-card-content large-padding" id="login_form">
                    <div class="login_heading">
                        <div class="user_avatar">
                            <img src="<?php echo SITE_URL; ?>assets/img/avatars/user.png">
                        </div>
                    </div>
                    <form class="login-form">
                        <div class="uk-form-row">
                            <label for="login_username">Mobile</label>
                            <input class="md-input" type="text" id="login_username" name="mobile" required>
                        </div>
                        <div class="uk-form-row">
                            <label for="login_password">Password</label>
                            <input class="md-input" type="text" id="login_password" name="password" required>
                        </div>
                        <div class="recaptcha-area">
                            <?php

                            if (isset($_SESSION['attemp'])) {


                                if ($_SESSION['attemp'] >= 2) {
                                    ?>
                                    <div class="g-recaptcha"
                                         data-sitekey="6Lf9OlQUAAAAAMwvhh8jc7PGeBrm5fElUg3imcFU"></div>
                                <?php }
                            } ?>
                        </div>
                        <div class="uk-margin-medium-top">
                            <button class="md-btn md-btn-primary md-btn-block md-btn-large">Sign In</button>
                        </div>
                        <div class="uk-grid uk-grid-width-1-3 uk-grid-small uk-margin-top">
                            <!--this for place capchA-->
                        </div>
                    </form>

                </div>

            </div>
            <div class="uk-margin-top " style="text-align:right">
<!--                <a href="--><?php //echo SITE_URL; ?><!--views/forgot" id="" class="">Forgot password?</a>-->

            </div>
        </div>
    </div>
</div>