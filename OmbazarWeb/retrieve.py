from OmbazarBackEnd .retrieve import Retrieve
from OmbazarBackEnd .models import *
from .helper import *


class WebRetrieve(Retrieve):

    def slider_coupons(self):
        errors = {}
        messages = {}
        return_object = {
            "status": 0,
            "error": errors,
            "message": messages
        }
        common = self.common
        try:
            # delete before expire blocks
            try:
                del_query = {"expire": {"$lt": time()}}
                delete = common.document_delete("saved_blocks", del_query)
                if not delete:
                    errors['save_delete'] = "expire block not deleted!"
                    return return_object
            except Exception as e:
                errors['expire_del_exception'] = str(e)
                return return_object
            # available round id

            active_query = {"active": 1}
            active_round = common.collection_data_one("rounds", active_query)
            if active_round:
                round_id = active_round['round_id']
            else:
                return_result = {"find_data": []}
                return return_result

            pipeline = [
                {
                    "$lookup": {
                        "from": "saved_blocks",
                        "localField": "block_id",
                        "foreignField": "block_id",
                        "as": "exist_blocks",
                    }
                },
                {
                    "$project": {
                        "_id": 0,
                        "exist_blocks": {"block_id": 1},
                        "block_id": 1,
                        "round_id": 1,
                        "estimated_ids": 1,
                        "total_unused": {
                            "$size": {
                                "$filter": {
                                    "input": "$estimated_ids",
                                    "cond": {
                                        "$eq": ["$$this.status", 0]
                                    }
                                }
                            }
                        },
                    }
                },
                {
                    "$match": {
                        "exist_blocks": [],
                        "round_id": round_id,
                        "total_unused": {
                            "$gte": 1
                        }
                    }
                },
                {
                    "$limit": 20
                }
            ]

            find = common.aggregate_collection_data("round_blocks", pipeline)

            # # Save all block which is show this user
            save_documents = []
            for block in find['find_data']:
                expire = after_minute(5)
                session = common.md5(ip())
                document = {
                    "block_id": block['block_id'],
                    "session": session,
                    "time": time(),
                    "expire": expire,
                }
                save_documents.append(document)
            if save_documents:
                try:
                    save_model = SavedBlock()
                    collection = save_model.collection_name()
                    save = collection.insert_many(save_documents)
                except Exception as e:
                    errors['database'] = "Estimated cause database connection error!"
                    errors['save_block_exception'] = str(e)
                    return return_object
                else:
                    if not save:
                        errors['save_error'] = "Block not saved"
                        return return_object
        except:
            errors['error_info'] = common.error_info()
            return return_object

        return find

    def prize_details(self, coupon_number):
        common = self.common
        query = {"coupon_number": coupon_number}
        info = common.collection_data_one("lucky_coupons", query)
        return info

    def request_collection_data(self, collection_name, query, option, limit=0, skip=0):
        errors = {}
        messages = {}
        return_object = {
            "status": 0,
            "error": errors,
            "message": messages
        }

        try:
            limit = int(limit)
        except ValueError:
            messages['invalid_limit'] = "Enter numeric in per page"
            return return_object

        try:
            skip = int(skip)
        except ValueError:
            errors['invalid_skip'] = "Enter numeric skip number"
            return return_object

        find = self.common.collection_data(collection_name, query, option, limit, skip)
        return find

    def corporate(self, corporate_id):
        common = self.common
        find = common.collection_data_one("corporates", {"corporate_id": corporate_id})
        return find

    def login(self, request):
        common = self.common
        errors = {}
        messages = {}
        return_object = {
            "status": 0,
            "error": errors,
            "message": messages
        }
        try:
            collection = Controller().collection_name()
            mobile = request.POST['mobile']
            try:
                mobile = int(mobile)
            except ValueError:
                messages['invalid_mobile'] = "Enter a valid mobile number"
            else:
                length = len(str(mobile))
                if length < 10:
                    messages['mobile_length'] = "Enter at least 10 digit of your mobile number"
            password = request.POST['password']
            password = common.md5(password)
            id_type = common.id_type("user")
            query = {"mobile": {"$regex": str(mobile)[-10:]}, "password": password, "user_id": {"$regex": "^"+id_type}}
            find = collection.find_one(query, {"_id": 0})
        except Exception as e:
            errors['login_exception'] = str(e)
        else:
            if not return_object['message'] and not return_object['error']:
                if find:
                    request.session['logged_user_id'] = find['user_id']
                    return_object['user_id'] = find['user_id']
                    return_object['status'] = 1
                    return_object['user_info'] = find
                    tree_info = self.tree_info(find['user_id'])
                    tree_info['user_info'] = find
                    return_object['tree_info'] = tree_info

                else:
                    messages['not_match'] = "Username or password not matched"
        return_object['basic_info'] = self.basic_information(request)

        return return_object

    def logged_controller(self, request):
        try:
            user_id = request.session['logged_user_id']
        except Exception:
            return {}
        else:
            collection = Controller().collection_name()
            pipeline = [
                {
                    "$lookup": {
                        "from": "transactions",
                        "localField": "user_id",
                        "foreignField": "user_id",
                        "as": "pending_data"
                    }
                },
                {
                    "$match": {
                        "user_id": user_id,

                    }
                },
                {
                    "$addFields": {
                        "pending": {
                            "$filter": {
                                "input": "$pending_data",
                                "cond": {
                                    "$eq": ["$$this.status", 0]
                                }
                            }
                        }
                    }
                },
                {
                    "$project": {
                        "_id": 0,
                        "pending_data": 0,
                        "pending._id": 0
                    }
                }
            ]
            find = self.common.aggregate_collection_data("controllers", pipeline, 1)
            try:
                find = find['find_data'][0]
            except:
                return_object = {
                    "status": 0,
                    "error": {
                        "error_info": str(self.common.error_info())
                    },
                    "message": {}
                }
                return return_object
            return find

    def basic_information(self, request):
        errors = {}
        messages = {}
        return_object = {
            "status": 0,
            "message": messages,
            "error": errors
        }
        try:
            common = self.common
            # Id types
            id_types = {}
            id_types = common.id_types()
            return_object['id_types'] = id_types

            # Logged user
            logged_user = {}
            logged_user_id = 0
            try:
                logged_user_id = request.session['logged_user_id']
            except Exception as e:
                pass
            else:
                logged_user = common.collection_data_one("controllers", {"user_id": logged_user_id})
                tree_info = self.tree_info(logged_user_id)
                tree_info['user_info'] = logged_user
                return_object['tree_info'] = tree_info
                return_object['logged_user'] = logged_user
            finally:
                return_object['logged_user_id'] = logged_user_id

            # Point Rules
            point_rules = []
            try:
                point_rules_info = common.collection_data("point_rules")
                if point_rules_info['find_data']:
                    point_rules = point_rules_info['find_data']

            except Exception:
                pass
            finally:
                return_object['point_rules'] = point_rules
            # for when load page with selected tree user
            try:
                selected_tree_user = request.POST['selected_tree_user']

            except:
                pass
            else:
                tree_info = self.tree_info(selected_tree_user)
                if tree_info:
                    return_object['selected_tree_info'] = tree_info
        except Exception:
            errors['error_info'] = str(common.error_info())
            messages['basic_info_exception'] = "System need basic information, but not found any information"
        else:
            # First check any error available
            if not return_object['error'] and not return_object['message']:
                return_object['status'] = 1
        return_object['software_info'] = self.software_settings()
        return return_object

    def products(self, request, limit=0, skip=0, count=False):
        common = self.common
        errors = {}
        messages = {}
        return_object = {
            "status": 0,
            "error": errors,
            "message": messages
        }
        try:
            limit = int(limit)
            skip = int(skip)
        except:
            pass
        id_type = common.id_type("vendor")
        pipeline = [
            # count order items by vendor store id
            {
                "$lookup": {
                    "from": "order_items",
                    "localField": "stores",
                    "foreignField": "store_id",
                    "as": "vendor_orders",
                }
            },
            {
              "$match": {
                  "stores": {
                      "$exists": True
                  },
                  "user_id": {"$regex": "^" + id_type},
                  "point_balance": {
                      "$gt": 0
                  }
              }
            },
            {
                "$project": {
                    "_id": 0,
                    "total": {"$sum": "$vendor_orders.item_info.quantity"},
                    "stores": 1,
                }
            },
            {
                "$sort": {"total": -1}
            },
            {
                "$lookup": {
                  "from": "store_items",
                  "localField": "stores",
                  "foreignField": "store_id",
                  "as": "store_items"
                }
            },
            {
                "$project": {
                    "store_items._id": 0
                }
            },
            {
                "$unwind": "$store_items"
            },
            {
                "$addFields": {
                    "stock": "$store_items.quantity"
                }
            },
            {
                "$replaceRoot": {"newRoot": {"$mergeObjects": ["$store_items", "$$ROOT"]}}
            },
            {
                "$lookup": {
                    "from": "items",
                    "let": {"this_item_id": "$item_id", "this_store_id": "$store_id"},
                    "pipeline": [{"$match": {"$expr": {"$and": [{"$eq": ["$item_id", "$$this_item_id"]}]}}}],
                    "as": "exist_item",
                }
            },
            {
                "$replaceRoot": {"newRoot": {"$mergeObjects": [{"$arrayElemAt": ["$exist_item", 0]}, "$$ROOT"]}}
            },
            {
                "$match": {
                    "quantity": {
                        "$gt": 0
                    }
                }
            }
        ]
        project_attr = {
            "_id": 0,
            "store_items": 0,
            "exist_item": 0,
            "stores": 0,
        }
        project = {
            "$project": project_attr
        }
        try:
            category_name = request.POST['category_name']
        except Exception:
            pass
        else:
            category_lookup = {
                "$lookup": {
                    "from": "categories",
                    "localField": "category_id",
                    "foreignField": "category_id",
                    "as": "categories",
                }
            }
            pipeline.append(category_lookup)
            # project_attr["categories"] = {"_id": 0}
        match_attr = {}
        match = {
            "$match": match_attr
        }
        try:
            query = request.POST['query']
        except Exception:
            pass
        else:
            js = json.loads(query)
            if "item_name" in js:
                item_name = js['item_name']
                regex = item_name['$regex']
                expre = re.compile(regex, re.IGNORECASE)
                new_express = {
                    "$regex": expre
                }
                js['item_name'] = new_express
            # common.preview(js)
            match_attr.update(js)
        pipeline.append(match)
        pipeline.append(project)

        try:
            option_query = request.POST['option']
            option = json.loads(option_query)
        except Exception:
            pass
        else:
            if "post_code" in option:
                post_code = option['post_code']
                try:
                    post_code = int(post_code)
                except:
                    errors['post_code'] = "invalid post code"
                more_aggregate = [
                    {
                        "$lookup": {
                            "from": "stores",
                            "localField": "store_id",
                            "foreignField": "store_id",
                            "as": "store_info",
                        }
                    },
                    {
                        "$unwind": "$store_info"
                    },
                    {
                        "$project": {
                            "store_info._id": 0,
                            # "store_item_id": 1
                        }
                    },
                    {
                        "$addFields": {
                            "post_code_rank": {
                                "$cond": {
                                    "if": {"$eq": ["$store_info.post_code", post_code]},
                                    "then": 0,
                                    "else": 1
                                }
                            }
                        }
                    },
                    {
                        "$sort": {
                            "post_code_rank": 1
                        }
                    }
                ]
                pipeline.extend(more_aggregate)
        find = common.aggregate_collection_data("controllers", pipeline, limit, skip, count)
        return find

    def my_coupons(self, request, limit, skip, count=False):
        try:
            user_id = request.session['logged_user_id']
            limit = int(limit)
            skip = int(skip)
        except:
            pass
        else:
            common = self.common
            pipeline = [
                {
                    "$lookup": {
                        "from": "coupons",
                        "localField": "coupon_number",
                        "foreignField": "coupon_number",
                        "as": "exist_item"
                    }
                },
                {
                    "$unwind": "$exist_item"
                },
                {
                    "$addFields": {
                        "status": "$exist_item.status"
                    }
                },
                {
                    "$match": {
                        "user_id": user_id,
                        "status": 0
                    }
                },
                {
                    "$project": {
                        "exist_item": 0,
                        "_id": 0
                    }
                }
            ]
            find = common.aggregate_collection_data("my_coupons", pipeline, limit, skip, count)
            return find

    def orders(self, request, limit, skip, count=False):
        common = self.common
        errors = {}
        messages = {}
        return_object = {
            "status": 0,
            "error": errors,
            "message": messages
        }
        customer_id = ""
        try:
            limit = int(limit)
            skip = int(skip)
        except Exception:
            messages['invalid_number'] = "Try a valid limit or skip"
            return return_object
        try:
            customer_id = request.session['logged_user_id']
        except Exception:
            messages['login_required'] = "Login first"
            return return_object
        pipeline = [
            {
                "$lookup": {
                    "from": "order_items",
                    "localField": "order_id",
                    "foreignField": "order_id",
                    "as": "exist_item",
                }
            },

            {
                "$addFields": {
                    "total_order_item": {
                        "$size": "$exist_item"
                    },
                    "total_complete": {
                        "$size": {
                            "$filter": {
                                "input": "$exist_item",
                                "cond": {
                                    "$eq": ["$$this.status", "Completed"]
                                }
                            }
                        }
                    }
                }
            },
            {
                "$addFields": {
                    "status": {
                        "$cond": {
                            "if": {"$eq": ["$total_complete", "$total_order_item"]},
                            "then": "Completed",
                            "else": "$status"
                        }
                    }
                }
            },
            {
                "$match": {"customer_id": customer_id}
            },
            {
                "$project": {
                    "exist_item": {
                        "store_item_id": 1,
                        "store_id": 1,
                    },

                    "_id": 0,
                    "order_id": 1,
                    "order_code": 1,
                    "time": 1,
                    "status": 1
                }
            },
        ]

        find = common.aggregate_collection_data("orders", pipeline, limit, skip, count)
        return find