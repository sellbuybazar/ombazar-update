$(function() {
    // init prescriptions
    altair_prescriptions.init();
});

// variables
var $prescription_card = $('#prescription'),
    $prescription_preview = $('#prescription_preview'),
    $prescription_form = $('#prescription_form'),
    $prescriptions_list_main = $('#prescriptions_list'),
    prescription_list_class = '.prescriptions_list', // main/sidebar list
    $prescription_add_btn = $('#prescription_add'),
    $prescription_edit_btn = $('#prescription_edit');

altair_prescriptions  = {
    init: function() {
        // copy list to sidebar
        altair_prescriptions.copy_list_sidebar();
        // add new prescription
        altair_prescriptions.add_new();
        // open prescription
        altair_prescriptions.open_prescription();
        altair_prescriptions.edit_prescription();
        // print prescription btn
        altair_prescriptions.print_prescription();
    },
    add_new: function() {
        if($prescription_add_btn) {

            var insert_form = function() {

                var $prescription_form_template = $('#prescription_form_template'),
                    card_height = $prescription_card.height(),
                    content_height = $prescription_card.find('.md-card-content').innerHeight(),
                    prescription_form_template_content = $prescription_form_template.html();
                    template_compiled = Handlebars.compile(prescription_form_template_content);
                    $.ajax({
                        async:false,
                        type: 'post',
                        url: path+'ajax/get_document_sl.php',
                        data: { type: "prescription" },
                        success: function(data,stat) {
                            var context = {
                                "doc_sl": data
                            };

                            $.ajax({
                                async:false,
                                type: 'post',
                                url: path+'ajax/get_default_banner.php',
                                data: { type: "prescription" },
                                dataType: 'json',
                                success: function(banner_data,stat) {
                                    context.default_image=banner_data.image;
                                    context.note=banner_data.note;
                                }

                            })
                            compiled_html=template_compiled(context);
                        }

                    })

                // remove "uk-active" class form prescriptions list
                $(prescription_list_class).find('.md-list-item-active').removeClass('md-list-item-active');
                // set height for card-single
                $prescription_card.height(card_height);

                $prescription_form
                    .hide()
                    // add form to card
                    .html(compiled_html)
                    // set height for card content
                    .find('.md-card-content').innerHeight(content_height);

                // append services to prescription form
                var append_service = function() {

                    var $prescription_form_template_services = $('#prescription_form_template_services'),
                        $prescription_services = $prescription_form.find('#form_prescription_services');

                    var template = $prescription_form_template_services.html(),
                        template_compiled = Handlebars.compile(template);

                    var service_id = (!$prescription_services.children().length) ? 1 : parseInt($prescription_services.children('.uk-grid:last').attr('data-service-number')) + 1,
                        context = {
                            "prescription_service_id": service_id
                        },
                        theCompiledHtml = template_compiled(context);

                    $prescription_services.append(theCompiledHtml);

                    // prescription md inputs
                    altair_md.inputs();
                    // prescription textarea autosize
                    altair_forms.textarea_autosize();
                    altair_forms.select_elements();
                    // reinitialize uikit margin
                    altair_uikit.reinitialize_grid_margin();
                    altair_form_adv.masked_inputs();



                    ///this auto complete for items / drugs
                    var d;
                    function user_data(){
                        $.ajax({
                            async:false,
                            url:path+"ajax/items.php",
                            type:"POST",
                            dataType: 'json',
                            success:function(data,status){
                                d=data;
                            }
                        });
                        return d;
                    }

                    var data=user_data();

                    var options = {
                        sourceData: function(search, success) {
                            dataResponse = [];
                            data.forEach(function(el, i) {
                                dataResponse.push({
                                    label: el.label,
                                    value: el.value
                                });
                            });
                            success(dataResponse);
                        },
                        minChar: 3,
                        onClick: function(item,element) {
                            var this_element=$(element).attr("data-item-number");
                            this_item=$(".service-item[data-service-number='"+this_element+"']");
                            var data_id=item.value;
                            $.ajax({
                                url:path+"ajax/single-items.php",
                                data:{item_id:data_id},
                                type:"POST",
                                dataType: 'json',
                                success:function(item,status){
                                    var data_id=item.value;
                                    var name=item.label;
                                    var image=item.image;
                                    var drug_type=item.drug_type;
                                    var dt=json_get_info("table_id_info",{id:drug_type,table:"drug_more_info"},false);

                                    this_item.find(".item_name").val(name);
                                    if(typeof(dt)!=="undefined"){
                                        this_item.find(".drug-type").val(dt.name);
                                    }

                                    this_item.find(".item_id").val(data_id);
                                    var image_path=path+"assets/img/images/"+image;
                                    this_item.find(".prescription-service-thumb").attr("src",image_path);
                                    this_item.find(".fancy-box-item").attr("href",image_path);

                                    var drug_last_info=json_get_info("drug_last_info",{item_id:data_id},false);
                                    var last_update_length=drug_last_info.length;
                                    if(last_update_length>0){
                                        var l_strength=drug_last_info[0].strength;
                                        var l_drug_type=drug_last_info[0].drug_type;
                                        var l_frequency=drug_last_info[0].frequency;
                                        var l_using_time=drug_last_info[0].using_time;
                                        var l_period=drug_last_info[0].period;
                                        var l_note=drug_last_info[0].note;

                                        //update this
                                        this_item.find(".strength").val(l_strength);
                                        this_item.find(".drug-type").val(l_drug_type);
                                        this_item.find(".frequency").val(l_frequency);
                                        this_item.find(".using_time").val(l_using_time);
                                        this_item.find(".item_note").val(l_note);

                                        var custom_selectize=this_item.find("[data-md-selectize]").selectize({
                                        });
                                        var selectize = custom_selectize[0].selectize;
                                        selectize.setValue(l_period);
                                    }
                                    altair_md.update_input($(this_item.find(".md-input")));
                                }
                            });

                            var data=json_get_info("item-strength",{item_id:data_id},false);
                            simple_auto_complete(".strength",data);

                        },
                        onPressEnter: function(item,element) {
                            var this_element=$(element).attr("data-item-number");
                            this_item=$(".service-item[data-service-number='"+this_element+"']");
                            var data_id=item.value;
                            $.ajax({
                                url:path+"ajax/single-items.php",
                                data:{item_id:data_id},
                                type:"POST",
                                dataType: 'json',
                                success:function(item,status){
                                    var data_id=item.value;
                                    var name=item.label;
                                    var image=item.image;
                                    var drug_type=item.drug_type;
                                    var dt=json_get_info("table_id_info",{id:drug_type,table:"drug_more_info"},false);
                                    this_item.find(".item_name").val(name);
                                    if(typeof(dt)!=="undefined"){
                                        this_item.find(".drug-type").val(dt.name);
                                    }
                                    this_item.find(".item_id").val(data_id);
                                    var image_path=path+"assets/img/images/"+image;
                                    this_item.find(".prescription-service-thumb").attr("src",image_path);
                                    this_item.find(".fancy-box-item").attr("href",image_path);

                                    var drug_last_info=json_get_info("drug_last_info",{item_id:data_id},false);
                                    var last_update_length=drug_last_info.length;
                                    if(last_update_length>0){
                                        var l_strength=drug_last_info[0].strength;
                                        var l_drug_type=drug_last_info[0].drug_type;
                                        var l_frequency=drug_last_info[0].frequency;
                                        var l_using_time=drug_last_info[0].using_time;
                                        var l_period=drug_last_info[0].period;
                                        var l_note=drug_last_info[0].note;

                                        //update this
                                        this_item.find(".strength").val(l_strength);
                                        this_item.find(".drug-type").val(l_drug_type);
                                        this_item.find(".frequency").val(l_frequency);
                                        this_item.find(".using_time").val(l_using_time);
                                        this_item.find(".item_note").val(l_note);

                                        var custom_selectize=this_item.find("[data-md-selectize]").selectize({
                                        });
                                        var selectize = custom_selectize[0].selectize;
                                        selectize.setValue(l_period);
                                    }
                                    altair_md.update_input($(this_item.find(".md-input")));
                                }
                            });

                            var data=json_get_info("item-strength",{item_id:data_id},false);
                            simple_auto_complete(".strength",data);

                        },

                    };
                    $('.add_item_field').lightAutocomplete(options);
                };

                // append first service to prescription form on init
                append_service();

                $('#prescription_form_append_service_btn').on('click', function (e) {
                    e.preventDefault();
                    // append service to prescription form
                    append_service();
//                    $('.modal_product_items').lightAutocomplete(options)
                });


                // append services to prescription form
                var append_any_services = function(service_script,service_place,auto_complete_field="") {

                    var $prescription_form_template_services = $('#'+service_script),
                        $prescription_services = $prescription_form.find('#'+service_place);

                    var template = $prescription_form_template_services.html(),
                        template_compiled = Handlebars.compile(template);

                    var service_id = (!$prescription_services.children().length) ? 1 : parseInt($prescription_services.children('.uk-grid:last').attr('data-service-number')) + 1,
                    context = {
                        "data_service_number": service_id
                    },
                    theCompiledHtml = template_compiled(context);

                    $prescription_services.append(theCompiledHtml);

                    // prescription md inputs
                    altair_md.inputs();
                    // prescription textarea autosize
                    altair_forms.textarea_autosize();

                    // reinitialize uikit margin
                    altair_uikit.reinitialize_grid_margin();

                    if(auto_complete_field!=""){
                        var data=json_get_info("qs-for-prescription",{category:auto_complete_field},false);
                        auto_complete_with_item_id("."+auto_complete_field,data);
                    }

                };
                // append first service to prescription form on init
                append_any_services("complain-service","complain-services","complain");
                append_any_services("investigation-service","investigation-services","investigation");
                append_any_services("advice-service","advice-services","advice");

                $('.service-add').on('click', function (e) {
                    e.preventDefault();
                    var element=$(this).parent().prev();
                    var this_template=$(this).attr("data-template");
                    var this_category=$(this).attr("data-category");
                    var place=element.attr("id");
                    append_any_services(this_template,place,this_category);
                });


                // prescription due-date radio boxes
                altair_md.checkbox_radio();
                var va=$(".form_switcher_area select").val();
                $('.form-type').val(va);

            };

            // show prescription form on animation complete
            var show_form = function() {
                $prescription_card.css({
                    'height': ''
                });
                $prescription_form.show();
                $prescription_preview.html('');
                setTimeout(function() {
                    // reinitialize uikit margin
                    altair_uikit.reinitialize_grid_margin();
                },560); //2 x animation duration
            };

            // show prescription form on fab click event
            $prescription_add_btn.on('click', function (e) {
                e.preventDefault();
                altair_md.card_show_hide($prescription_card,insert_form,show_form,undefined);
                var custom_selectize=$('.company_info2 [data-md-selectize]').selectize({
                });
                var selectize = custom_selectize[0].selectize;

                selectize.on("change",function(value){
                    $(".patient-gender span").text(value);

                });
                $("#date_of_birth").on("change",function(){
                    var date=$(this).val();
                    var age=get_age(date);
                    $(".patient-age span").text(age);
                });
                var d;
                function user_data(){
                    $.ajax({
                        async:false,
                        url:path+"ajax/customer-vendors.php",
                        data:{user:"customer"},
                        type:"POST",
                        dataType: 'json',
                        success:function(data,status){
                            d=data;
                        }
                    });
                    return d;
                }

                var data=user_data();

                var options = {
                    sourceData: function(search, success) {
                        dataResponse = [];
                        data.forEach(function(el, i) {
                            dataResponse.push({
                                label: el.label,
                                value: el.value
                            });
                        });
                        success(dataResponse);
                    },
                    minChar: 3,
                    onClick: function(item) {
                        $('.to_info').val(item.label);
                        var user_id=item.value;

                        $.ajax({
                            async:false,
                            url:path+"ajax/user_info.php",
                            data:{id:user_id},
                            type:"POST",
                            dataType: 'json',
                            success:function(data,status){
                                for(x in data){
                                $(".reciver").val(data[x].user_id);
                                $("#patient_name").val(data[x].name);
                                $("#date_of_birth").val(data[x].birth_day);
                                $("#email").val(data[x].email);
                                $(".patient-gender span").text(data[x].gender);

                                selectize.setValue(data[x].gender);
                                var age=get_age(data[x].birth_day);
                                $(".patient-age span").text(age+" Year");
                                altair_md.update_input($("#patient_name"));
                                altair_md.update_input($("#date_of_birth"));
                                altair_md.update_input($("#email"));
                                }
                            }
                        });
                    },
                    onPressEnter: function(item) {
                        $('.to_info').val(item.label);
                        var user_id=item.value;
                        $.ajax({
                            async:false,
                            url:path+"ajax/user_info.php",
                            data:{id:user_id},
                            type:"POST",
                            dataType: 'json',
                            success:function(data,status){
                                for(x in data){
                                $(".reciver").val(data[x].user_id);
                                $("#patient_name").val(data[x].name);
                                $("#date_of_birth").val(data[x].birth_day);
                                $("#email").val(data[x].email);
                                $(".patient-gender span").text(data[x].gender);
                                selectize.setValue(data[x].gender);
                                var age=get_age(data[x].birth_day);
                                $(".patient-age span").text(age+" Year");
                                altair_md.update_input($("#patient_name"));
                                altair_md.update_input($("#date_of_birth"));
                                altair_md.update_input($("#email"));
                                }
                            }
                        });

                    }


                };

                $('.to_info').lightAutocomplete(options);


                altair_form_file_dropify.init();
                $("a#example2").fancybox();
                altair_forms.select_elements();
                //prescription form discount control by discount type
                    $(".discount_type input").iCheck({
                        checkboxClass: "icheckbox_md",
                        radioClass: "iradio_md",
                        increaseArea: "20%"
                    }).on("ifChecked", function (event) {
                        var val=event.target.value;

                        total=0;
                        var discount_type=val;
                        $("#prescription_form .service-item").each(function(){
                            total += Number($(".service_total",this).val());
                        });


                        if(discount_type=='fixed'){
                           var discount=Number($(".discount").val());
                           }
                        else if(discount_type=='persent'){
                            var discount_val=Number($(".discount").val());
                            var discount=total*discount_val/100;
                                }
//                        var last_total=total-discount;
//                        $(".all-total").val(last_total);
                        $(".discount_view").html(discount);

//                        altair_md.update_input($(".service_total"));


                    });
                altair_form_validation.init();
                //end prescription form discount control by discount type
            })

        }
    },
    edit_prescription: function() {
        if($prescription_edit_btn) {

            var insert_form = function() {

                var $prescription_form_template = $('#prescription_edit_form_template'),
                    card_height = $prescription_card.height(),
                    content_height = $prescription_card.find('.md-card-content').innerHeight(),
                    prescription_form_template_content = $prescription_form_template.html(),
                    template_compiled = Handlebars.compile(prescription_form_template_content);

                    // ajax function to get prescriptions
                    var prescription_id = parseInt($("#prescription_edit").attr('data-prescription-id'));
                    $.ajax({
                        async:false,
                        type: 'post',
                        url: path+'ajax/get_prescription_total_info.php',
                        data: { prescription_id: prescription_id },
                        dataType: 'json',
                        success: function(response,stat) {
//                            console.log(response);
                            services=response.prescription.prescription_services;
                            theCompiledHtml = template_compiled(response);
                        }

                    })

                // remove "uk-active" class form prescriptions list
                $(prescription_list_class).find('.md-list-item-active').removeClass('md-list-item-active');
                // set height for card-single
                $prescription_card.height(card_height);

                $prescription_form
                    .hide()
                    // add form to card
                    .html(theCompiledHtml)
                    // set height for card content
                    .find('.md-card-content').innerHeight(content_height);

                // append services to prescription form
                var append_int_service = function() {

                    var $prescription_form_template_services = $('#prescription_edit_form_template_services'),
                        $prescription_services = $prescription_form.find('#form_prescription_edit_services');

                    var template = $prescription_form_template_services.html(),
                        template_compiled = Handlebars.compile(template);

                    var service_id = (!$prescription_services.children().length) ? 1 : parseInt($prescription_services.children('.uk-grid:last').attr('data-service-number')) + 1,
                        context = {
                            "prescription_service_id": service_id,services
                        },
                        theCompiledHtml = template_compiled(context);
                    $prescription_services.append(theCompiledHtml);

                    // prescription md inputs
                    altair_md.inputs();
                    // prescription textarea autosize
                    altair_forms.textarea_autosize();

                    // reinitialize uikit margin
                    altair_uikit.reinitialize_grid_margin();
                    var d;
                    function user_data(){
                        $.ajax({
                            async:false,
                            url:path+"ajax/items.php",
                            type:"POST",
                            dataType: 'json',
                            success:function(data,status){
                                d=data;
                            }
                        });
                        return d;
                    }

                    var data=user_data();

                    var options = {
                        sourceData: function(search, success) {
                            dataResponse = [];
                            data.forEach(function(el, i) {
                                dataResponse.push({
                                    label: el.label,
                                    value: el.value
                                });
                            });
                            success(dataResponse);
                        },
                        minChar: 3,
                        onClick: function(item,element) {
                            var this_element=$(element).attr("data-item-number");
                            this_item=$(".service-item[data-service-number='"+this_element+"']");
                            var data_id=item.value;

                            $.ajax({
                                url:path+"ajax/single-items.php",
                                data:{item_id:data_id},
                                type:"POST",
                                dataType: 'json',
                                success:function(item,status){

                                    var data_id=item.value;
                                    var name=item.label;
                                    var purchase_price=item.purchase_price;
                                    var selling_price=item.selling_price;
                                    var vat=item.vat;
                                    var image=item.image;

                                    this_item.find(".item_name").val(name);
                                    this_item.find(".service_price").val(selling_price);
                                    this_item.find(".service_vat").val(vat);
                                    this_item.find(".item_id").val(data_id);

                                    var image_path=path+"assets/img/images/"+image;
                                    this_item.find(".prescription-service-thumb").attr("src",image_path);
                                    this_item.find(".fancy-box-item").attr("href",image_path);
                                    altair_md.update_input($(this_item.find("input")));
                                }
                            });


                        },
                        onPressEnter: function(item,element) {
                            var this_element=$(element).attr("data-item-number");
                            this_item=$(".service-item[data-service-number='"+this_element+"']");
                            var data_id=item.value;
                            $.ajax({
                                url:path+"ajax/single-items.php",
                                data:{item_id:data_id},
                                type:"POST",
                                dataType: 'json',
                                success:function(item,status){
                                    var data_id=item.value;
                                    var name=item.label;
                                    var purchase_price=item.purchase_price;
                                    var selling_price=item.selling_price;
                                    var vat=item.vat;
                                    var image=item.image;

                                    this_item.find(".item_name").val(name);
                                    this_item.find(".service_price").val(selling_price);
                                    this_item.find(".service_vat").val(vat);
                                    this_item.find(".item_id").val(data_id);

                                    var image_path=path+"assets/img/images/"+image;
                                    this_item.find(".prescription-service-thumb").attr("src",image_path);
                                    this_item.find(".fancy-box-item").attr("href",image_path);
                                    altair_md.update_input($(this_item.find("input")));
                                }
                            });

                        }
                    };

                    $('.add_item_field').lightAutocomplete(options);
                };

                // append first service to prescription form on init
                append_int_service();

                // append services to prescription form
                var append_service = function() {
                    var $prescription_form_template_services = $('#prescription_blank_form_template_services'),
                        $prescription_services = $prescription_form.find('#form_prescription_edit_services');

                    var template = $prescription_form_template_services.html(),
                        template_compiled = Handlebars.compile(template);

                    var service_id = (!$prescription_services.children().length) ? 1 : parseInt($prescription_services.children('.uk-grid:last').attr('data-service-number')) + 1,
                        context = {
                            "prescription_service_id": service_id,services
                        },
                        theCompiledHtml = template_compiled(context);
                    $prescription_services.append(theCompiledHtml);

                    // prescription md inputs
                    altair_md.inputs();
                    // prescription textarea autosize
                    altair_forms.textarea_autosize();

                    // reinitialize uikit margin
                    altair_uikit.reinitialize_grid_margin();

                    var d;
                    function user_data(){
                        $.ajax({
                            async:false,
                            url:path+"ajax/items.php",
                            type:"POST",
                            dataType: 'json',
                            success:function(data,status){
                                d=data;
                            }
                        });
                        return d;
                    }

                    var data=user_data();

                    var options = {
                        sourceData: function(search, success) {
                            dataResponse = [];
                            data.forEach(function(el, i) {
                                dataResponse.push({
                                    label: el.label,
                                    value: el.value
                                });
                            });
                            success(dataResponse);
                        },
                        minChar: 3,
                        onClick: function(item,element) {
                            var this_element=$(element).attr("data-item-number");
                            this_item=$(".service-item[data-service-number='"+this_element+"']");
                            var data_id=item.value;
                            $.ajax({
                                url:path+"ajax/single-items.php",
                                data:{item_id:data_id},
                                type:"POST",
                                dataType: 'json',
                                success:function(item,status){
                                    var data_id=item.value;
                                    var name=item.label;
                                    var purchase_price=item.purchase_price;
                                    var selling_price=item.selling_price;
                                    var vat=item.vat;
                                    var image=item.image;

                                    this_item.find(".item_name").val(name);
                                    this_item.find(".service_price").val(selling_price);
                                    this_item.find(".service_vat").val(vat);
                                    this_item.find(".item_id").val(data_id);

                                    var image_path=path+"assets/img/images/"+image;
                                    this_item.find(".prescription-service-thumb").attr("src",image_path);
                                    this_item.find(".fancy-box-item").attr("href",image_path);
                                    altair_md.update_input($(this_item.find("input")));
                                }
                            });


                        },
                        onPressEnter: function(item,element) {
                            var this_element=$(element).attr("data-item-number");
                            this_item=$(".service-item[data-service-number='"+this_element+"']");
                            var data_id=item.value;
                            $.ajax({
                                url:path+"ajax/single-items.php",
                                data:{item_id:data_id},
                                type:"POST",
                                dataType: 'json',
                                success:function(item,status){
                                    var data_id=item.value;
                                    var name=item.label;
                                    var purchase_price=item.purchase_price;
                                    var selling_price=item.selling_price;
                                    var vat=item.vat;
                                    var image=item.image;

                                    this_item.find(".item_name").val(name);
                                    this_item.find(".service_price").val(selling_price);
                                    this_item.find(".service_vat").val(vat);
                                    this_item.find(".item_id").val(data_id);

                                    var image_path=path+"assets/img/images/"+image;
                                    this_item.find(".prescription-service-thumb").attr("src",image_path);
                                    this_item.find(".fancy-box-item").attr("href",image_path);
                                    altair_md.update_input($(this_item.find("input")));
                                }
                            });

                        }
                    };

                    $('.add_item_field').lightAutocomplete(options);
                    return service_id;
                };



                $('#prescription_form_append_service_btn').on('click', function (e) {
                    e.preventDefault();
                    // append service to prescription form
                    var service_number=append_service();
                    $(".service-item[data-service-number='"+service_number+"']").find("input").val("");
                });

                // prescription due-date radio boxes
                altair_md.checkbox_radio();
                var va=$(".form_switcher_area select").val();
                $('.form-type').val(va);

            };

            // show prescription form on animation complete
            var show_form = function() {
                $prescription_card.css({
                    'height': ''
                });
                $prescription_form.show();
                $prescription_preview.html('');
                setTimeout(function() {
                    // reinitialize uikit margin
                    altair_uikit.reinitialize_grid_margin();
                },560); //2 x animation duration
            };

            // show prescription form on fab click event
            $("#prescription_preview").on('click','#prescription_edit', function (e) {
                e.preventDefault();
                altair_md.card_show_hide($prescription_card,insert_form,show_form,undefined);


                var d;
                function user_data(){
                    $.ajax({
                        async:false,
                        url:path+"ajax/customer-vendors.php",
                        data:{user:"customer"},
                        type:"POST",
                        dataType: 'json',
                        success:function(data,status){
                            d=data;
                        }
                    });
                    return d;
                }

                var data=user_data();

                var options = {
                    sourceData: function(search, success) {
                        dataResponse = [];
                        data.forEach(function(el, i) {
                            dataResponse.push({
                                label: el.label,
                                value: el.value
                            });
                        });
                        success(dataResponse);
                    },
                    minChar: 3,
                    onClick: function(item) {
                        $('.to_info').val(item.label);
                        var user_id=item.value;

                        $.ajax({
                            async:false,
                            url:path+"ajax/user_info.php",
                            data:{id:user_id},
                            type:"POST",
                            dataType: 'json',
                            success:function(data,status){
                                for(x in data){
                                $(".reciver").val(data[x].user_id);
                                $("#prescription_to_address_1").val(data[x].address_line_1);
                                $("#prescription_to_address_2").val(data[x].address_line_2);
                                $("#to_email").val(data[x].email);
                                $("#to_phone").val(data[x].phone);
                                altair_md.update_input($("#prescription_to_address_1"));
                                altair_md.update_input($("#prescription_to_address_2"));
                                altair_md.update_input($("#to_email"));
                                altair_md.update_input($("#to_phone"));
                                }
                            }
                        });

                      }

                };

                $('.to_info').lightAutocomplete(options);

//                $('.modal_product_items').lightAutocomplete(options);
                altair_form_file_dropify.init();
                $("a#example2").fancybox();
                altair_forms.select_elements();
                //prescription form discount control by discount type
                    $(".discount_type input").iCheck({
                        checkboxClass: "icheckbox_md",
                        radioClass: "iradio_md",
                        increaseArea: "20%"
                    }).on("ifChecked", function (event) {

                        var val=event.target.value;

                        total=0;
                        var discount_type=val;
                        $("#prescription_form .service-item").each(function(){
                            total += Number($(".service_total",this).val());
                        });


                        if(discount_type=='fixed'){
                           var discount=Number($(".discount").val());
                           }
                        else if(discount_type=='persent'){
                            var discount_val=Number($(".discount").val());
                            var discount=total*discount_val/100;
                                }
//                        var last_total=total-discount;
//                        $(".all-total").val(last_total);
                        $(".discount_view").html(discount);

//                        altair_md.update_input($(".service_total"));


                    });
                altair_form_validation.init();
                //end prescription form discount control by discount type
            })

        }
    },

    open_prescription: function() {

        var show_prescription = function(element) {
            var $this = element,
                $prescription_template = $('#prescription_template');

            var template = $prescription_template.html(),
                template_compiled = Handlebars.compile(template);


            // ajax function to get prescriptions
            var prescription_id = parseInt($this.attr('data-prescription-id'));

            $.ajax({
                type: 'post',
                url: path+'ajax/get_prescription.php',
                data: { prescription_id: prescription_id },
                dataType: 'json',
                success: function(response,stat) {
                    console.log(response);
                    var theCompiledHtml = template_compiled(response);
                    $prescription_preview.html(theCompiledHtml);
                    $prescription_form.html('');
                    $window.resize();
                    setTimeout(function() {
                        // reinitialize uikit margin
                        altair_uikit.reinitialize_grid_margin();
                    },290);
                    altair_forms.select_elements();

                    //confirm logistic status

                    $(".logistic_order_status select").selectize({
                        onDropdownOpen: function($dropdown) {
                            $dropdown
                                .hide()
                                .velocity('slideDown', {
                                begin: function() {
                                    if (typeof thisPosBottom !== 'undefined') {
                                        $dropdown.css({'margin-top':'0'})
                                    }
                                },
                                duration: 200,
                                easing: easing_swiftOut
                            })
                        },
                        onDropdownClose: function($dropdown) {
                            $dropdown
                                .show()
                                .velocity('slideUp', {
                                complete: function() {
                                    if (typeof thisPosBottom !== 'undefined') {
                                        $dropdown.css({'margin-top': ''})
                                    }
                                },
                                duration: 200,
                                easing: easing_swiftOut
                            });
                        },
                        onChange: function(value) {
                            var prescription_id=response.prescription.prescription_id;
                            $.ajax({
                                url:path+"ajax/logistic_order_status_change.php",
                                data:{status:value,prescription_id:prescription_id},
                                type:"POST",
                                success:function(data,status){
                                    $(".logistic-status").html(value);

                                    $.ajax({
                                        url:path+"ajax/get_logistic_status.php",
                                        data:{prescription_id:prescription_id},
                                        type:"POST",
                                        dataType: 'json',
                                        success:function(data,status){
                                            $(".logistic-date").html(data.logistic_date);

                                        }
                                    });



                                }
                            });

                        }
                    })
                }
            })




        };

        $(prescription_list_class)
            .on('click','a[data-prescription-id]', function(e) {
                e.preventDefault();
                e.stopPropagation();
                // toggle card and show prescription
                altair_md.card_show_hide($prescription_card,undefined,show_prescription,$(this));
                // set active class
                $(this).closest('li').siblings('li').removeClass('md-list-item-active').end().addClass('md-list-item-active');
            });

        if($(prescription_list_class).find('a[data-prescription-id]').length) {
            // open first prescription
            $(prescription_list_class).find('a[data-prescription-id]').eq(0).click();
        } else {
            // open form
            $prescription_add_btn.trigger('click');
        }

    },
    print_prescription: function() {
        $body.on('click','#prescription_print',function(e) {
            e.preventDefault();
            UIkit.modal.confirm('Do you want to print this?', function () {
                // hide sidebar
                altair_main_sidebar.hide_sidebar();
                // wait for dialog to fully hide
                setTimeout(function () {
                    window.print();
                }, 300)
            }, {
                labels: {
                    'Ok': 'print'
                }
            });
        })
    },
    copy_list_sidebar: function() {
        // hide secondary sidebar toggle btn for large screens
        $sidebar_secondary_toggle.addClass('uk-hidden-large');

        var prescriptions_list_sidebar = $prescriptions_list_main.clone();

        prescriptions_list_sidebar.attr('id','prescriptions_list_sidebar');

        $sidebar_secondary
            .find('.sidebar_secondary_wrapper').html(prescriptions_list_sidebar)
            .end();

    }
};
