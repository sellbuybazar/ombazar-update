$(function() {
    // init invoices
    altair_invoices.init();
});

// variables
var $invoice_card = $('#invoice'),
    $invoice_preview = $('#invoice_preview'),
    $invoice_form = $('#invoice_form'),
    $invoices_list_main = $('#invoices_list'),
    invoice_list_class = '.invoices_list', // main/sidebar list
    $invoice_add_btn = $('#invoice_add'),
    $invoice_edit_btn = $('#invoice_edit');

altair_invoices  = {
    init: function() {
        // copy list to sidebar
        altair_invoices.copy_list_sidebar();
        // add new invoice
        altair_invoices.add_new();
        // open invoice
        altair_invoices.open_invoice();
        altair_invoices.edit_invoice();
        // print invoice btn
        altair_invoices.print_invoice();
    },
    add_new: function() {
        if($invoice_add_btn) {

            var insert_form = function() {

                var $invoice_form_template = $('#invoice_form_template'),
                    card_height = $invoice_card.height(),
                    content_height = $invoice_card.find('.md-card-content').innerHeight(),
                    invoice_form_template_content = $invoice_form_template.html();
                    template_compiled = Handlebars.compile(invoice_form_template_content);
                    $.ajax({
                        async:false,
                        type: 'post',
                        url: path+'ajax/get_document_sl.php',
                        data: { type: "bill" },
                        success: function(data,stat) {
                            var context = {
                                "doc_sl": data
                            };

                            $.ajax({
                                async:false,
                                type: 'post',
                                url: path+'ajax/get_default_banner.php',
                                data: { type: "bill" },
                                dataType: 'json',
                                success: function(banner_data,stat) {
                                    context.default_image=banner_data.image;
                                    context.note=banner_data.note;
                                }

                            })
                            compiled_html=template_compiled(context);
                        }

                    })
                // remove "uk-active" class form invoices list
                $(invoice_list_class).find('.md-list-item-active').removeClass('md-list-item-active');
                // set height for card-single
                $invoice_card.height(card_height);

                $invoice_form
                    .hide()
                    // add form to card
                    .html(compiled_html)
                    // set height for card content
                    .find('.md-card-content').innerHeight(content_height);

                // append services to invoice form
                var append_service = function() {

                    var $invoice_form_template_services = $('#invoice_form_template_services'),
                        $invoice_services = $invoice_form.find('#form_invoice_services');

                    var template = $invoice_form_template_services.html(),
                        template_compiled = Handlebars.compile(template);

                    var service_id = (!$invoice_services.children().length) ? 1 : parseInt($invoice_services.children('.uk-grid:last').attr('data-service-number')) + 1,
                        context = {
                            "invoice_service_id": service_id
                        },
                        theCompiledHtml = template_compiled(context);

                    $invoice_services.append(theCompiledHtml);

                    // invoice md inputs
                    altair_md.inputs();
                    // invoice textarea autosize
                    altair_forms.textarea_autosize();

                    // reinitialize uikit margin
                    altair_uikit.reinitialize_grid_margin();

                    var d;
                    function user_data(){

                        $.ajax({
                            async:false,
                            url:path+"ajax/items.php",
                            type:"POST",
                            dataType: 'json',
                            success:function(data,status){
                                d=data;
                            }
                        });
                        return d;
                    }

                    var data=user_data();

                    var options = {
                        sourceData: function(search, success) {
                            dataResponse = [];
                            data.forEach(function(el, i) {
                                dataResponse.push({
                                    label: el.label,
                                    value: el.value
                                });
                            });
                            success(dataResponse);
                        },
                        minChar: 3,
                        onClick: function(item,element) {
                            var this_element=$(element).attr("data-item-number");
                            this_item=$(".service-item[data-service-number='"+this_element+"']");
                            var data_id=item.value;
                            $.ajax({
                                url:path+"ajax/single-items.php",
                                data:{item_id:data_id},
                                type:"POST",
                                dataType: 'json',
                                success:function(item,status){
                                    var data_id=item.value;
                                    var name=item.label;
                                    var purchase_price=item.purchase_price;
                                    var selling_price=item.selling_price;
                                    var vat=item.vat;
                                    var image=item.image;

                                    this_item.find(".item_name").val(name);
                                    this_item.find(".service_price").val(purchase_price);
                                    this_item.find(".service_vat").val(vat);
                                    this_item.find(".item_id").val(data_id);

                                    var image_path=path+"assets/img/images/"+image;
                                    this_item.find(".invoice-service-thumb").attr("src",image_path);
                                    this_item.find(".fancy-box-item").attr("href",image_path);
                                    altair_md.update_input($(this_item.find("input")));
                                }
                            });


                        },
                        onPressEnter: function(item,element) {
                            var this_element=$(element).attr("data-item-number");
                            this_item=$(".service-item[data-service-number='"+this_element+"']");
                            var data_id=item.value;
                            $.ajax({
                                url:path+"ajax/single-items.php",
                                data:{item_id:data_id},
                                type:"POST",
                                dataType: 'json',
                                success:function(item,status){
                                    var data_id=item.value;
                                    var name=item.label;
                                    var purchase_price=item.purchase_price;
                                    var selling_price=item.selling_price;
                                    var vat=item.vat;
                                    var image=item.image;

                                    this_item.find(".item_name").val(name);
                                    this_item.find(".service_price").val(purchase_price);
                                    this_item.find(".service_vat").val(vat);
                                    this_item.find(".item_id").val(data_id);

                                    var image_path=path+"assets/img/images/"+image;
                                    this_item.find(".invoice-service-thumb").attr("src",image_path);
                                    this_item.find(".fancy-box-item").attr("href",image_path);
                                    altair_md.update_input($(this_item.find("input")));
                                }
                            });

                        }
                    };

                    $('.add_item_field').lightAutocomplete(options);

                };

                // append first service to invoice form on init
                append_service();

                $('#invoice_form_append_service_btn').on('click', function (e) {
                    e.preventDefault();
                    // append service to invoice form
                    append_service();
//                    $('.modal_product_items').lightAutocomplete(options)
                });

                // invoice due-date radio boxes
                altair_md.checkbox_radio();
                var va=$(".form_switcher_area select").val();
                $('.form-type').val(va);

            };

            // show invoice form on animation complete
            var show_form = function() {
                $invoice_card.css({
                    'height': ''
                });
                $invoice_form.show();
                $invoice_preview.html('');
                setTimeout(function() {
                    // reinitialize uikit margin
                    altair_uikit.reinitialize_grid_margin();
                },560); //2 x animation duration
            };

            // show invoice form on fab click event
            $invoice_add_btn.on('click', function (e) {
                e.preventDefault();
                altair_md.card_show_hide($invoice_card,insert_form,show_form,undefined);


                var d;
                function user_data(){
                    $.ajax({
                        async:false,
                        url:path+"ajax/customer-vendors.php",
                        data:{user:"vendor"},
                        type:"POST",
                        dataType: 'json',
                        success:function(data,status){
                            d=data;
                        }
                    });
                    return d;
                }

                var data=user_data();

                var options = {
                    sourceData: function(search, success) {
                        dataResponse = [];
                        data.forEach(function(el, i) {
                            dataResponse.push({
                                label: el.label,
                                value: el.value
                            });
                        });
                        success(dataResponse);
                    },
                    minChar: 3,
                    onClick: function(item) {
                        $('.to_info').val(item.label);
                        var user_id=item.value;

                        $.ajax({
                            async:false,
                            url:path+"ajax/user_info.php",
                            data:{id:user_id},
                            type:"POST",
                            dataType: 'json',
                            success:function(data,status){
                                for(x in data){
                                $(".reciver").val(data[x].user_id);
                                $("#invoice_to_address_1").val(data[x].address_line_1);
                                $("#invoice_to_address_2").val(data[x].address_line_2);
                                $("#to_email").val(data[x].email);
                                $("#to_phone").val(data[x].phone);
                                altair_md.update_input($("#invoice_to_address_1"));
                                altair_md.update_input($("#invoice_to_address_2"));
                                altair_md.update_input($("#to_email"));
                                altair_md.update_input($("#to_phone"));
                                }
                            }
                        });

                    },
                    onPressEnter: function(item) {
                        $('.to_info').val(item.label);
                        var user_id=item.value;

                        $.ajax({
                            async:false,
                            url:path+"ajax/user_info.php",
                            data:{id:user_id},
                            type:"POST",
                            dataType: 'json',
                            success:function(data,status){
                                for(x in data){
                                $(".reciver").val(data[x].user_id);
                                $("#invoice_to_address_1").val(data[x].address_line_1);
                                $("#invoice_to_address_2").val(data[x].address_line_2);
                                $("#to_email").val(data[x].email);
                                $("#to_phone").val(data[x].phone);
                                altair_md.update_input($("#invoice_to_address_1"));
                                altair_md.update_input($("#invoice_to_address_2"));
                                altair_md.update_input($("#to_email"));
                                altair_md.update_input($("#to_phone"));
                                }
                            }
                        });

                    }

                };

                $('.to_info').lightAutocomplete(options);

//                $('.modal_product_items').lightAutocomplete(options);
                altair_form_file_dropify.init();
                $("a#example2").fancybox();
                altair_forms.select_elements();
                //invoice form discount control by discount type
                    $(".discount_type input").iCheck({
                        checkboxClass: "icheckbox_md",
                        radioClass: "iradio_md",
                        increaseArea: "20%"
                    }).on("ifChecked", function (event) {
                        var val=event.target.value;

                        total=0;
                        var discount_type=val;
                        $("#invoice_form .service-item").each(function(){
                            total += Number($(".service_total",this).val());
                        });


                        if(discount_type=='fixed'){
                           var discount=Number($(".discount").val());
                           }
                        else if(discount_type=='persent'){
                            var discount_val=Number($(".discount").val());
                            var discount=total*discount_val/100;
                                }
//                        var last_total=total-discount;
//                        $(".all-total").val(last_total);
                        $(".discount_view").html(discount);

//                        altair_md.update_input($(".service_total"));


                    });
                altair_form_validation.init();
                //end invoice form discount control by discount type
            })

        }
    },
    edit_invoice: function() {
        if($invoice_edit_btn) {

            var insert_form = function() {

                var $invoice_form_template = $('#invoice_edit_form_template'),
                    card_height = $invoice_card.height(),
                    content_height = $invoice_card.find('.md-card-content').innerHeight(),
                    invoice_form_template_content = $invoice_form_template.html(),
                    template_compiled = Handlebars.compile(invoice_form_template_content);

                    // ajax function to get invoices
                    var invoice_id = parseInt($("#invoice_edit").attr('data-invoice-id'));
                    $.ajax({
                        async:false,
                        type: 'post',
                        url: path+'ajax/get_invoice_total_info.php',
                        data: { invoice_id: invoice_id },
                        dataType: 'json',
                        success: function(response,stat) {

                            services=response.invoice.invoice_services;
                            theCompiledHtml = template_compiled(response);
                        }

                    })

                // remove "uk-active" class form invoices list
                $(invoice_list_class).find('.md-list-item-active').removeClass('md-list-item-active');
                // set height for card-single
                $invoice_card.height(card_height);

                $invoice_form
                    .hide()
                    // add form to card
                    .html(theCompiledHtml)
                    // set height for card content
                    .find('.md-card-content').innerHeight(content_height);

                // append services to invoice form
                var append_int_service = function() {

                    var $invoice_form_template_services = $('#invoice_edit_form_template_services'),
                        $invoice_services = $invoice_form.find('#form_invoice_edit_services');

                    var template = $invoice_form_template_services.html(),
                        template_compiled = Handlebars.compile(template);

                    var service_id = (!$invoice_services.children().length) ? 1 : parseInt($invoice_services.children('.uk-grid:last').attr('data-service-number')) + 1,
                        context = {
                            "invoice_service_id": service_id,services
                        },
                        theCompiledHtml = template_compiled(context);
                    $invoice_services.append(theCompiledHtml);

                    // invoice md inputs
                    altair_md.inputs();
                    // invoice textarea autosize
                    altair_forms.textarea_autosize();

                    // reinitialize uikit margin
                    altair_uikit.reinitialize_grid_margin();
                    var d;
                    function user_data(){
                        $.ajax({
                            async:false,
                            url:path+"ajax/items.php",
                            type:"POST",
                            dataType: 'json',
                            success:function(data,status){
                                d=data;
                            }
                        });
                        return d;
                    }

                    var data=user_data();

                    var options = {
                        sourceData: function(search, success) {
                            dataResponse = [];
                            data.forEach(function(el, i) {
                                dataResponse.push({
                                    label: el.label,
                                    value: el.value
                                });
                            });
                            success(dataResponse);
                        },
                        minChar: 3,
                        onClick: function(item,element) {
                            var this_element=$(element).attr("data-item-number");
                            this_item=$(".service-item[data-service-number='"+this_element+"']");
                            var data_id=item.value;
                            $.ajax({
                                url:path+"ajax/single-items.php",
                                data:{item_id:data_id},
                                type:"POST",
                                dataType: 'json',
                                success:function(item,status){
                                    var data_id=item.value;
                                    var name=item.label;
                                    var purchase_price=item.purchase_price;
                                    var selling_price=item.selling_price;
                                    var vat=item.vat;
                                    var image=item.image;

                                    this_item.find(".item_name").val(name);
                                    this_item.find(".service_price").val(purchase_price);
                                    this_item.find(".service_vat").val(vat);
                                    this_item.find(".item_id").val(data_id);

                                    var image_path=path+"assets/img/images/"+image;
                                    this_item.find(".invoice-service-thumb").attr("src",image_path);
                                    this_item.find(".fancy-box-item").attr("href",image_path);
                                    altair_md.update_input($(this_item.find("input")));
                                }
                            });


                        },
                        onPressEnter: function(item,element) {
                            var this_element=$(element).attr("data-item-number");
                            this_item=$(".service-item[data-service-number='"+this_element+"']");
                            var data_id=item.value;
                            $.ajax({
                                url:path+"ajax/single-items.php",
                                data:{item_id:data_id},
                                type:"POST",
                                dataType: 'json',
                                success:function(item,status){
                                    var data_id=item.value;
                                    var name=item.label;
                                    var purchase_price=item.purchase_price;
                                    var selling_price=item.selling_price;
                                    var vat=item.vat;
                                    var image=item.image;

                                    this_item.find(".item_name").val(name);
                                    this_item.find(".service_price").val(purchase_price);
                                    this_item.find(".service_vat").val(vat);
                                    this_item.find(".item_id").val(data_id);

                                    var image_path=path+"assets/img/images/"+image;
                                    this_item.find(".invoice-service-thumb").attr("src",image_path);
                                    this_item.find(".fancy-box-item").attr("href",image_path);
                                    altair_md.update_input($(this_item.find("input")));
                                }
                            });

                        }
                    };

                    $('.add_item_field').lightAutocomplete(options);
                };

                // append first service to invoice form on init
                append_int_service();

                // append services to invoice form
                var append_service = function() {
                    var $invoice_form_template_services = $('#invoice_blank_form_template_services'),
                        $invoice_services = $invoice_form.find('#form_invoice_edit_services');

                    var template = $invoice_form_template_services.html(),
                        template_compiled = Handlebars.compile(template);

                    var service_id = (!$invoice_services.children().length) ? 1 : parseInt($invoice_services.children('.uk-grid:last').attr('data-service-number')) + 1,
                        context = {
                            "invoice_service_id": service_id,services
                        },
                        theCompiledHtml = template_compiled(context);
                    $invoice_services.append(theCompiledHtml);

                    // invoice md inputs
                    altair_md.inputs();
                    // invoice textarea autosize
                    altair_forms.textarea_autosize();

                    // reinitialize uikit margin
                    altair_uikit.reinitialize_grid_margin();
                    var d;
                    function user_data(){
                        $.ajax({
                            async:false,
                            url:path+"ajax/items.php",
                            type:"POST",
                            dataType: 'json',
                            success:function(data,status){
                                d=data;
                            }
                        });
                        return d;
                    }

                    var data=user_data();

                    var options = {
                        sourceData: function(search, success) {
                            dataResponse = [];
                            data.forEach(function(el, i) {
                                dataResponse.push({
                                    label: el.label,
                                    value: el.value
                                });
                            });
                            success(dataResponse);
                        },
                        minChar: 3,
                        onClick: function(item,element) {
                            var this_element=$(element).attr("data-item-number");
                            this_item=$(".service-item[data-service-number='"+this_element+"']");
                            var data_id=item.value;
                            $.ajax({
                                url:path+"ajax/single-items.php",
                                data:{item_id:data_id},
                                type:"POST",
                                dataType: 'json',
                                success:function(item,status){
                                    var data_id=item.value;
                                    var name=item.label;
                                    var purchase_price=item.purchase_price;
                                    var selling_price=item.selling_price;
                                    var vat=item.vat;
                                    var image=item.image;

                                    this_item.find(".item_name").val(name);
                                    this_item.find(".service_price").val(purchase_price);
                                    this_item.find(".service_vat").val(vat);
                                    this_item.find(".item_id").val(data_id);

                                    var image_path=path+"assets/img/images/"+image;
                                    this_item.find(".invoice-service-thumb").attr("src",image_path);
                                    this_item.find(".fancy-box-item").attr("href",image_path);
                                    altair_md.update_input($(this_item.find("input")));
                                }
                            });


                        },
                        onPressEnter: function(item,element) {
                            var this_element=$(element).attr("data-item-number");
                            this_item=$(".service-item[data-service-number='"+this_element+"']");
                            var data_id=item.value;
                            $.ajax({
                                url:path+"ajax/single-items.php",
                                data:{item_id:data_id},
                                type:"POST",
                                dataType: 'json',
                                success:function(item,status){
                                    var data_id=item.value;
                                    var name=item.label;
                                    var purchase_price=item.purchase_price;
                                    var selling_price=item.selling_price;
                                    var vat=item.vat;
                                    var image=item.image;

                                    this_item.find(".item_name").val(name);
                                    this_item.find(".service_price").val(purchase_price);
                                    this_item.find(".service_vat").val(vat);
                                    this_item.find(".item_id").val(data_id);

                                    var image_path=path+"assets/img/images/"+image;
                                    this_item.find(".invoice-service-thumb").attr("src",image_path);
                                    this_item.find(".fancy-box-item").attr("href",image_path);
                                    altair_md.update_input($(this_item.find("input")));
                                }
                            });

                        }
                    };

                    $('.add_item_field').lightAutocomplete(options);
                    return service_id;
                };



                $('#invoice_form_append_service_btn').on('click', function (e) {
                    e.preventDefault();
                    // append service to invoice form
                    var service_number=append_service();
                    $(".service-item[data-service-number='"+service_number+"']").find("input").val("");
                });

                // invoice due-date radio boxes
                altair_md.checkbox_radio();
                var va=$(".form_switcher_area select").val();
                $('.form-type').val(va);

            };

            // show invoice form on animation complete
            var show_form = function() {
                $invoice_card.css({
                    'height': ''
                });
                $invoice_form.show();
                $invoice_preview.html('');
                setTimeout(function() {
                    // reinitialize uikit margin
                    altair_uikit.reinitialize_grid_margin();
                },560); //2 x animation duration
            };

            // show invoice form on fab click event
            $("#invoice_preview").on('click','#invoice_edit', function (e) {
                e.preventDefault();
                altair_md.card_show_hide($invoice_card,insert_form,show_form,undefined);


                var d;
                function user_data(){
                    $.ajax({
                        async:false,
                        url:path+"ajax/customer-vendors.php",
                        data:{user:"vendor"},
                        type:"POST",
                        dataType: 'json',
                        success:function(data,status){
                            d=data;
                        }
                    });
                    return d;
                }

                var data=user_data();

                var options = {
                    sourceData: function(search, success) {
                        dataResponse = [];
                        data.forEach(function(el, i) {
                            dataResponse.push({
                                label: el.label,
                                value: el.value
                            });
                        });
                        success(dataResponse);
                    },
                    minChar: 3,
                    onClick: function(item) {
                        $('.to_info').val(item.label);
                        var user_id=item.value;

                        $.ajax({
                            async:false,
                            url:path+"ajax/user_info.php",
                            data:{id:user_id},
                            type:"POST",
                            dataType: 'json',
                            success:function(data,status){
                                for(x in data){
                                $(".reciver").val(data[x].user_id);
                                $("#invoice_to_address_1").val(data[x].address_line_1);
                                $("#invoice_to_address_2").val(data[x].address_line_2);
                                $("#to_email").val(data[x].email);
                                $("#to_phone").val(data[x].phone);
                                altair_md.update_input($("#invoice_to_address_1"));
                                altair_md.update_input($("#invoice_to_address_2"));
                                altair_md.update_input($("#to_email"));
                                altair_md.update_input($("#to_phone"));
                                }
                            }
                        });

                      }

                };

                $('.to_info').lightAutocomplete(options);

//                $('.modal_product_items').lightAutocomplete(options);
                altair_form_file_dropify.init();
                $("a#example2").fancybox();
                altair_forms.select_elements();
                //invoice form discount control by discount type
                    $(".discount_type input").iCheck({
                        checkboxClass: "icheckbox_md",
                        radioClass: "iradio_md",
                        increaseArea: "20%"
                    }).on("ifChecked", function (event) {
                        var val=event.target.value;

                        total=0;
                        var discount_type=val;
                        $("#invoice_form .service-item").each(function(){
                            total += Number($(".service_total",this).val());
                        });


                        if(discount_type=='fixed'){
                           var discount=Number($(".discount").val());
                           }
                        else if(discount_type=='persent'){
                            var discount_val=Number($(".discount").val());
                            var discount=total*discount_val/100;
                                }
//                        var last_total=total-discount;
//                        $(".all-total").val(last_total);
                        $(".discount_view").html(discount);

//                        altair_md.update_input($(".service_total"));


                    });
                altair_form_validation.init();
                //end invoice form discount control by discount type
            })

        }
    },

    open_invoice: function() {

        var show_invoice = function(element) {
            var $this = element,
                $invoice_template = $('#invoice_template');

            var template = $invoice_template.html(),
                template_compiled = Handlebars.compile(template);



            // ajax function to get invoices
            var invoice_id = parseInt($this.attr('data-invoice-id'));

            $.ajax({
                type: 'post',
                url: path+'ajax/get_invoice.php',
                data: { invoice_id: invoice_id },
                dataType: 'json',
                success: function(response,stat) {

                    var theCompiledHtml = template_compiled(response);
                    $invoice_preview.html(theCompiledHtml);
                    $invoice_form.html('');
                    $window.resize();
                    setTimeout(function() {
                        // reinitialize uikit margin
                        altair_uikit.reinitialize_grid_margin();
                    },290);
                    altair_forms.select_elements();
                }
            })

        };

        $(invoice_list_class)
            .on('click','a[data-invoice-id]', function(e) {
                e.preventDefault();
                e.stopPropagation();
                // toggle card and show invoice
                altair_md.card_show_hide($invoice_card,undefined,show_invoice,$(this));
                // set active class
                $(this).closest('li').siblings('li').removeClass('md-list-item-active').end().addClass('md-list-item-active');
            });

        if($(invoice_list_class).find('a[data-invoice-id]').length) {
            // open first invoice
            $(invoice_list_class).find('a[data-invoice-id]').eq(0).click();
        } else {
            // open form
            $invoice_add_btn.trigger('click');
        }

    },
    print_invoice: function() {
        $body.on('click','#invoice_print',function(e) {
            e.preventDefault();
            UIkit.modal.confirm('Do you want to print this?', function () {
                // hide sidebar
                altair_main_sidebar.hide_sidebar();
                // wait for dialog to fully hide
                setTimeout(function () {
                    window.print();
                }, 300)
            }, {
                labels: {
                    'Ok': 'print'
                }
            });
        })
    },
    copy_list_sidebar: function() {
        // hide secondary sidebar toggle btn for large screens
        $sidebar_secondary_toggle.addClass('uk-hidden-large');

        var invoices_list_sidebar = $invoices_list_main.clone();

        invoices_list_sidebar.attr('id','invoices_list_sidebar');

        $sidebar_secondary
            .find('.sidebar_secondary_wrapper').html(invoices_list_sidebar)
            .end();

    }
};
