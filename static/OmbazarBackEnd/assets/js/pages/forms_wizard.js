$(function() {
    // wizard
    altair_wizar.assign_company();
});

// wizard
altair_wizar = {
    content_height: function(this_wizard,step) {
        var this_height = $(this_wizard).find('.step-'+ step).actual('outerHeight');
        $(this_wizard).children('.content').animate({ height: this_height }, 140, bez_easing_swiftOut);
    },
    assign_company: function() {
        var $wizard_advanced = $('#wizard_advanced_for_company'),
            $wizard_advanced_form = $('#wizard_advanced_form_for_company');

        if ($wizard_advanced.length) {
            $wizard_advanced.steps({
                headerTag: "h3",
                bodyTag: "section",
                transitionEffect: "slideLeft",
                trigger: 'change',
                onInit: function(event, currentIndex) {
                    altair_wizard.content_height($wizard_advanced,currentIndex);
                    // reinitialize textareas autosize
                    altair_forms.textarea_autosize();
                    // reinitialize checkboxes
                    altair_md.checkbox_radio($(".wizard-icheck"));
                    $(".wizard-icheck").on('ifChecked', function(event){
                        console.log(event.currentTarget.value);
                    });
                    // reinitialize uikit margin
                    altair_uikit.reinitialize_grid_margin();
                    // reinitialize selects
                    altair_forms.select_elements($wizard_advanced);
                    // reinitialize switches
                    $wizard_advanced.find('span.switchery').remove();
                    altair_forms.switches();

                    $('.assign-company-logo').dropify();

                    // resize content when accordion is toggled
                    $('.uk-accordion').on('toggle.uk.accordion',function() {
                        $window.resize();
                    });

                    setTimeout(function() {
                        $window.resize();
                    },100);
                    var custom_selectize=$('#wizard_birth_place').selectize({
                        plugins: {
                            'remove_button': {
                                label: ''
                            }
                        },
                        maxItems: 1,
                        valueField: 'value',
                        labelField: 'label',
                        searchField: 'label',
                        create: false,

                    });
                    var barcode_select=custom_selectize[0].selectize;
                    barcode_select.on("change",function(value){
                        $(".business-type-other").addClass("hidden");
                        if(value=="other"){
                            $(".business-type-other").removeClass("hidden");
                        }

                    });
                },
                onStepChanged: function (event, currentIndex) {
                    altair_wizard.content_height($wizard_advanced,currentIndex);
                    setTimeout(function() {
                        $window.resize();
                    },100)
                },
                onStepChanging: function (event, currentIndex, newIndex) {
                    var step = $wizard_advanced.find('.body.current').attr('data-step'),
                        $current_step = $('.body[data-step=\"'+ step +'\"]');

                    // check input fields for errors
                    $current_step.find('.parsley-row').each(function() {
                        $(this).find('input,textarea,select').each(function() {
                            $(this).parsley().validate();
                        });
                    });

                    // adjust content height
                    $window.resize();

                    return $current_step.find('.md-input-danger').length ? false : true;
                },
                onFinished: function() {
                    var loading="<div class='uk-text-center'><img class='uk-margin-top' src='"+path+"assets/img/spinners/spinner.gif' alt=''>";
                    modal=UIkit.modal.blockUI(loading);

                    var company=new FormData($(".assign-company")[0]);
                    $.ajax({
                        url:path+"ajax/assign-company.php",
                        type:"POST",
                        data:company,
                        processData: false,
                        contentType: false,
                        success:function(data,status){
                            modal.hide();
                            if(data==1){
                                window.location=path+"views/success";
                            }
                            else{
                                UIkit.modal.alert(data);
                            }

                        }
                    });

                },
                labels: {
                    cancel: "Cancel",
                    current: "current step:",
                    pagination: "Pagination",
                    finish: "Done",
                    next: "Next",
                    previous: "Previous",
                    loading: "Loading ..."
                }
            })/*.steps("setStep", 2)*/;

            $window.on('debouncedresize',function() {
                var current_step = $wizard_advanced.find('.body.current').attr('data-step');
                altair_wizard.content_height($wizard_advanced,current_step);
            });

            // wizard
            $wizard_advanced_form
                .parsley()
                .on('form:validated',function() {
                    setTimeout(function() {
                        altair_md.update_input($wizard_advanced_form.find('.md-input'));
                        // adjust content height
                        $window.resize();
                    },100)
                })
                .on('field:validated',function(parsleyField) {

                    var $this = $(parsleyField.$element);
                    setTimeout(function() {
                        altair_md.update_input($this);
                        // adjust content height
                        var currentIndex = $wizard_advanced.find('.body.current').attr('data-step');
                        altair_wizard.content_height($wizard_advanced,currentIndex);
                    },100);

                });

        }
    }
};
