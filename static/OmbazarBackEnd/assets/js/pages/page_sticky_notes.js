$(function() {
    // add note
    altair_sticky_notes.add_note();
    // note actions
    altair_sticky_notes.actions();
    // load sticky notes
    altair_sticky_notes.load_notes();

    // slider fix
    $('#top_bar').find('.uk-slidenav-position').on('focusitem.uk.slider', function(event, index, item) {
        $(event.target).addClass('uk-slidenav-hover');
    });
});

var $note_template = $('#note_template').html(),
    $note_form_template = $('#note_form_template').html(),
    $note_form_text = $('.note_form_text'),
    $form = $('#note_form'),
    $form_card = $form.closest('.md-card'),
    $grid = $('#notes_grid'),
    pallete = ['md-bg-white','md-bg-red-500','md-bg-pink-500','md-bg-purple-500','md-bg-indigo-500','md-bg-blue-500','md-bg-cyan-500','md-bg-teal-500','md-bg-green-500','md-bg-lime-500','md-bg-yellow-500','md-bg-amber-500','md-bg-brown-500','md-bg-blue-grey-500'],
    $card = $form.closest('.md-card'),
    // callback after note color select
    changeBg = function(remove) {
        var color = $('#notes_colors_wrapper').find('input').val();
        if(typeof color != 'undefined') {
            $.each(pallete,function (key,value) {
                var color = replaceColor(value);
                if($card.hasClass(color)) {
                    $card.removeClass(color);
                }
            });
            if(!remove) {
                $card.addClass(replaceColor(color));
            }
        }
    },
    // notes color picker
    notesColorPicker = altair_helpers.color_picker($('<div id="notes_colors_wrapper"></div>'),pallete,changeBg).prop('outerHTML');

function hide_note_form() {
    $form.fadeOut('fast',function() {
        changeBg(true);
        $form.html('');
        $form_card.velocity('reverse', {
            complete: function() {
                $note_form_text.show();
                $form_card.removeClass('card-expanded');
            }
        });
        $window.resize();
    });
}

function replaceColor(color) {
    if(color) {
        var replaceColor = color.split('-'),
            lastEl = replaceColor[replaceColor.length -1];
        if(!isNaN(lastEl)) {
            replaceColor.pop();
            var replacedColor = replaceColor.join('-') + '-100'
        } else {
            var replacedColor = replaceColor.join('-')
        }
        return replacedColor;
    } else {
        return false;
    }
}

altair_sticky_notes = {
    add_note: function() {

        $form_card.css({
            minHeight: $form_card.actual('height')
        });

        // show note form
        $form_card.on('click', function(e) {
            if(!$form_card.hasClass('card-expanded')) {
                var template = Handlebars.compile($note_form_template),
                    context = {
                        'labels': [
                            {
                                'text': 'label 1',
                                'text_safe': 'label_1',
                                'type': 'default'
                            },
                            {
                                'text': 'label 2',
                                'text_safe': 'label_2',
                                'type': 'warning'
                            },
                            {
                                'text': 'label 3',
                                'text_safe': 'label_3',
                                'type': 'danger'
                            },
                            {
                                'text': 'label 4',
                                'val': 'label_4',
                                'type': 'success'
                            },
                            {
                                'text': 'label 5',
                                'text_safe': 'label_5',
                                'type': 'primary'
                            }
                        ]
                    };
                    html = template(context);

                $form.hide().html(html);
                $note_form_text.hide();

                var minHeight = $form.actual('height');

                $form_card
                    .velocity({
                        minHeight: minHeight
                    }, {
                        duration: 200,
                        easing: easing_swiftOut,
                        complete: function (elements) {
                            $form.fadeIn('fast');
                            $('#note_f_title').focus();
                            $window.resize();
                        }
                    });

                $form_card.addClass('card-expanded');

                $('#notes_cp').append(notesColorPicker);
                // initialize md inputs
                altair_md.inputs($form);
                // initialize textarea autosize
                altair_forms.textarea_autosize();
                // initialize iCheck checkboxes
                altair_md.checkbox_radio($form.find('[data-md-icheck]'));
            }

        });

        // add note
        $form.on('click', '#note_add', function(e) {
            e.preventDefault();
                $content_input = $('#note_f_content'),
                content = $content_input.val().replace(/\n/g, '<br />');
            $content_input.removeClass('md-input-danger');

            if(content == '') {
                $content_input.addClass('md-input-danger');
                altair_md.update_input($content_input);
                return;
            }

            if(content) {

                    $.ajax({
                        type: 'post',
                        url: path+'ajax/bdm-note-add.php',
                        data: { note: content },
                        success: function(data,stat) {
                            if(data!=1){
                                return false;
                            }
                        }

                    });

                    context = [{
                        'time': new Date().getTime(),
                        'content': content
                    }]



                var template = Handlebars.compile($note_template),
                    html = template(context);

                $grid.prepend(html);
                $window.resize();

                hide_note_form();
            }

        });
    },
    // note actions
    actions: function() {
        // remove note
        $grid.on('click','.note_action_remove', function(e) {
            e.preventDefault();
            var this_id=$(this).attr("data-id");
            $.ajax({
                type: 'post',
                url: path+'ajax/archiver.php',
                data: { table_name: "notes",id:this_id },
                success: function(data,stat) {
                    if(data!=1){
                        return false;
                    }
                }

            });
            var $this_note = $(this).closest('.md-card').parent();
            $this_note.addClass('uk-animation-scale-up uk-animation-reverse');
            setTimeout(function() {
                $this_note.remove();
            },300);
        });
    },
    // load notes
    load_notes: function() {
        var context;
        $.ajax({
            async:false,
            type: 'post',
            url: path+'ajax/bdm-notes.php',
            dataType: 'json',
            success: function(data,stat) {
                context=data;
                if(data!=1){
                    return false;
                }
            }

        });
        var context = context;

        var template = Handlebars.compile($note_template),
            html = template(context);

        $grid.prepend(html);

        // initialize iCheck
        altair_md.checkbox_radio($grid.find('[data-md-icheck]'));

    }
};
