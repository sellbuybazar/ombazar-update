/*
*  Altair Admin
*  Configuration file for ckeditor (wysiwyg editor)
*/

CKEDITOR.editorConfig = function( config ) {
    // Define changes to default configuration here.
    // For complete reference see:
    // http://docs.ckeditor.com/#!/api/CKEDITOR.config

    config.skin = 'material_design,../../assets/skins/ckeditor/material_design/';

    // The toolbar groups arrangement, optimized for two toolbar rows.
    config.toolbarGroups = [

    ];
    config.removeButtons = 'Scayt';

    config.extraPlugins = 'autogrow,colorbutton,colordialog';

    // Set the most common block elements.
    config.format_tags = 'p;h1;h2;h3;pre';

    config.height = 300;

    config.autoGrow_minHeight = 300;
    config.autoGrow_maxHeight = 520;
    config.removePlugins = 'resize';

    config.filebrowserBrowseUrl = 'file_manager/fm_ckeditor.html';

};
