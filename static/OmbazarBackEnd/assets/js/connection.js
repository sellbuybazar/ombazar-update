var path;
onmessage = function (e) {
    var obj=e.data;
    path=obj.path;
}
function loadDoc() {
    if(typeof(path) !=="undefined"){
       var xhttp = new XMLHttpRequest();
      xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
        postMessage(this.responseText);
        }
      };
      xhttp.open("GET",path+"ajax/connection_status.php", true);
      xhttp.send();
    }
    setTimeout("loadDoc()",1000);
}
loadDoc();
