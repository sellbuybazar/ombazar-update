let OmbazarFn = {};
OmbazarFn.countries = [];
OmbazarFn.selected_country = {};
OmbazarFn.logger_id = Cookies.getJSON('usr_id');
OmbazarFn.logger_user_name = Cookies.getJSON('usr_name');
OmbazarFn.logged_user = {};
OmbazarFn.categories = [];
OmbazarFn.custom_filter = function(obj,key_name,value,list=false,negetive=false){
    let find_item=obj.filter(function(item){
            if (negetive) {
                return item[key_name]!==value;
            }
            else{
                return item[key_name]===value;
            }

        });
    if(list){
        return find_item;
    }
    else{
        for(let x in find_item){
            return find_item[x];
        }
    }
};


OmbazarFn.not_found_message = function(message="Not found"){
    var html="<div class='md-card'>";
        html+="<div class='md-card-content'>";
        html+="<h2 class='not-found-message'>"+message+"</h2>";
        html+="</div>";
        html+="</div>";
    return html;

};
OmbazarFn.common_request_object = function(function_name=''){
    let extra_data = false;
    if(function_name === ''){
        console.log('Common request function name empty')
    }
    else{
        extra_data = {
            request_name: function_name,
            request_type:"asset"
        };
    }
    return extra_data;

};
OmbazarFn.retrive_request_object = function(function_name=''){
    let extra_data = false;
    if(function_name === ''){
        console.log('Retrieve request function name empty')
    }
    else{
        extra_data = {
            request_name: function_name,
            request_type:"get"
        };
    }
    return extra_data;

};

OmbazarFn.passed_request_object = function(function_name=''){
    let extra_data = false;
    if(function_name === ''){
        console.log('Passed request function name empty')
    }
    else{
        extra_data = {
            request_name: function_name,
            request_type:"post"
        };
    }
    return extra_data;

};
OmbazarFn.modify_request_object = function(function_name=''){
    let extra_data = false;
    if(function_name === ''){
        console.log('Modify request function name empty')
    }
    else{
        extra_data = {
            request_name: function_name,
            request_type:"update"
        };
    }
    return extra_data;

};


//processing image on
OmbazarFn.loading_on = function(){
    altair_helpers.content_preloader_show('regular');
};
OmbazarFn.loading_off = function (){
    altair_helpers.content_preloader_hide();

};

OmbazarFn.clean = function(form_id) {
    document.getElementById(form_id).reset();
};
OmbazarFn.modal_close = function () {
    $(".uk-modal-close").click();
};

OmbazarFn.redirect = function (page_name) {
    window.location=path+page_name;
};
OmbazarFn.notify = function (message='Changed has been saved',pos="top-center",timeout="5000") {
    UIkit.notify({
        message: message,
        status:  '',
        timeout: timeout,
        group: null,
        pos: pos,
        onClose: function() {

        }
    });

};

OmbazarFn.have = function (element){
    if($(element).length>0){
        return true;
    }
    else{
        return false;
    }
};
OmbazarFn.response = function (data) {
    UIkit.modal.info(data);
};
OmbazarFn.get_form_data = function (form_name) {
    var form_data=new FormData($(form_name)[0]);
    return form_data;

};


// using jQuery
OmbazarFn.getCookie = function (name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
};
OmbazarFn.csrftoken = OmbazarFn.getCookie('csrftoken');


OmbazarFn.request = function (form_name,event,loading=true,extra_data=[],extra_images=[],fancy_tree=[]){
    var form_data=OmbazarFn.get_form_data(form_name);
    for(item in extra_images){
        form_data.append(extra_images[item],document.getElementById(extra_images[item]).files[0]);
    }
    var tree_keys=Object.keys(fancy_tree);
    if(tree_keys.length>0){

        for(item in fancy_tree){    $(fancy_tree[item].element_name).fancytree("getTree").generateFormElements(fancy_tree[item].selected_name,fancy_tree[item].active_name);

        }
        form_data=OmbazarFn.get_form_data(form_name);
    }
    for(item in extra_data){
        var field_name=extra_data[item].name;
        var value=extra_data[item].value;
        form_data.append(field_name,value);

    }
    let token = form_data.get("csrfmiddlewaretoken");
    if (!token){
        if(form_name === ""){
            form_data.append("csrfmiddlewaretoken",OmbazarFn.csrftoken);
        }
        else {
            // form_data.append("csrfmiddlewaretoken",document.getElementsByName('csrfmiddlewaretoken')[0].value);
        }

    }

    if(loading){
       OmbazarFn.loading_on();
       }

    $.ajax({
        url:"/control/request/",
        data:form_data,
        type:"POST",
        processData: false,
        contentType: false,
        // dataType:"json",
        success:function(data,status){
            if (loading) {
                OmbazarFn.loading_off();
            }
            event(data,status);
        },
        complete:function (xhr,status) {
            if(status != "success"){
                let error=xhr.responseText;
                console.log(error);
            }
        }

    });
};

OmbazarFn.form_data_maker = function (form_element_name,extra_data={},extra_images=[],fancy_tree=[]) {
    // form name format like ("element")/ like jquery selector
    var form_data=OmbazarFn.get_form_data(form_element_name);

    // Image an array []
    //format is array = [file_input_id_name]
    //example <input type='file' id='image'> array key is (image)
    for(item in extra_images){
        form_data.append(extra_images[item],document.getElementById(extra_images[item]).files[0]);
    }

    // tree make follow the fancytree rules
    //array format [{},{}]
    // tree = [
    //      {
    //         element_name:".categories.fancytree_radio" ,
    //         selected_name:"category" ,
    //         active_name:"parent"
    //      }
    //  ];
    //
    //
    var tree_keys=Object.keys(fancy_tree);
    if(tree_keys.length>0){

        for(item in fancy_tree){
            $(fancy_tree[item].element_name).fancytree("getTree").generateFormElements(fancy_tree[item].selected_name,fancy_tree[item].active_name);
        }
        form_data=OmbazarFn.get_form_data(form_element_name);
    }
    // extra data followed by object = {name:"xxxx",age:"xxx",other:"xxxx"}
    for(var item in extra_data){
        var field_name= item;
        var value=extra_data[item];
        form_data.append(field_name,value);
    }
    //csrf token for python security
    // form_data.append("csrfmiddlewaretoken",OmbazarFn.csrftoken);
    // for(x of form_data.values()){
    //     console.log(x);
    // }
    return form_data;

};

//// angular js support
OmbazarFn.active_button = function (){
    return $("button[active='true']").attr('data-button-name');
};

OmbazarFn.add_action = function(response,$location,url){
     status = response.status;
    if (status){
        ///First notify a message top bar
        let message = response.message;
            message = OmbazarFn.notify_message(message);
        let active_button = OmbazarFn.active_button();
        // If button name save_and_new form data reset/clean
        if(active_button === 'save_and_new'){
            OmbazarFn.clean("form");
            OmbazarFn.update_inputs();
        }
        // If button name save, page redirect to profile page
        else{
            $location.path(url);
        }
    }
};

OmbazarFn.informer = function(data){
    if(data.status !== undefined) {
        // if (!data.status) {
            let messages = data.message;
            let errors = data.error;
            let m_length = Object.keys(messages).length;
            let e_length = Object.keys(errors).length;
            let info_html = "";
            // If message available
            if(!data.status){
                if (m_length) {
                    let message_html = "<h3>Message</h3>";
                    for (let message in messages) {
                        message_html += "<p>" + messages[message] + "</p>";
                    }
                    info_html += message_html;
                }
            }
            // If error available
            if (e_length) {
                let error_html = "<h3>Error</h3>";
                for (let error in errors) {
                    error_html += "<p>" + errors[error] + "</p>";
                }
                info_html += error_html;
                console.log(data)
            }
            if(info_html){
                OmbazarFn.response(info_html);
            }


        // }

    }
};


OmbazarFn.$http = function($http,data,event,loading = true){
    if(loading){
        OmbazarFn.loading_on();
    }
    // When any form submit first disabled all button in form element
    $("form button").addClass("uk-disabled");
    $http({
        method : "POST",
        url : "/control/request/",
        data : data,
        headers: { 'Content-Type': undefined}
    }).then(
        function (response) {
            let data = response.data;
            let status = response.status;
            if (loading){
                OmbazarFn.loading_off();
            }

            OmbazarFn.informer(data);
            event(data,status);
            $("form button").removeClass("uk-disabled");
        },
        function(response) {
            if (loading){
                OmbazarFn.loading_off();
            }
              console.log(response);
            $("form button").removeClass("uk-disabled");
        })
};

OmbazarFn.notify_message = function(messages){
    let m_length = Object.keys(messages).length;
    let info_html = "";
    // If message available
    if(m_length){
        let message_html = '';
        for (let message in messages) {
            message_html += "<p>"+messages[message]+"</p>";
        }
        info_html += message_html;
        OmbazarFn.notify(info_html,"top-center",5000)
    }
};
OmbazarFn.update_inputs = function(){
    $("input").each(function () {
        altair_md.update_input($(this));
    });

};
OmbazarFn.init_requirement = function(){
    altair_md.inputs();
    altair_forms.switches();
    altair_md.fab_toolbar();
    OmbazarFn.update_inputs();
    OmbazarFn.dropify();
};

OmbazarFn.dropify = function(options={}){
  $('.dropify').dropify(options);
};

OmbazarFn.tags_maker = function(element){
    let $tags_control = $(element).not("input.selectized,.multi.tags").selectize({
      delimiter: ',',
      persist: false,
      create: function(input) {
          return {
              value: input,
              text: input
          }
      }
    });
    var tags_control = $tags_control[0].selectize;
        return tags_control;
};

OmbazarFn.drug_drop_upload = function(params,event) {
    var progressbar = $("#file_upload-progressbar"),
        bar         = progressbar.find('.uk-progress-bar'),
        settings    = {
            action: '/control/request/', // Target url for the upload
            allow : '*.(json)', // File filter  format like jpg|jpeg|gif|png
            params : params,
            loadstart: function() {
                bar.css("width", "0%").text("0%");
                progressbar.removeClass("uk-hidden");

            },
            progress: function(percent) {
                percent = Math.ceil(percent);
                bar.css("width", percent+"%").text(percent+"%");
            },
            allcomplete: function(response,xhr) {

                bar.css("width", "100%").text("100%");
                setTimeout(function(){
                    progressbar.addClass("uk-hidden");
                }, 250);
                setTimeout(function() {
                    UIkit.notify({
                        message: "Upload Completed",
                        pos: 'top-right'
                    });
                },280);

                var data = JSON.parse(response);
                OmbazarFn.informer(data);
                event(data);
            }
        };

    var select = UIkit.uploadSelect($("#file_upload-select"), settings),
        drop   = UIkit.uploadDrop($("#file_upload-drop"), settings);
};

OmbazarFn.find_index = function (docs,key,value) {
  let index_number=docs.findIndex(x => x[key] === value);
  return index_number;
};

OmbazarFn.menu_maker = function (parent_id,main_menus){
    let menu_list = [];
    let menus = OmbazarFn.custom_filter(main_menus,"parent_id",parent_id,true);
    for (let x in menus){
        let item = menus[x];
        let menu_id = item.id;
        item['menus'] = OmbazarFn.menu_maker(menu_id,main_menus);
        menu_list.push(item);
    }
    return menu_list;
};

OmbazarFn.category_maker = function (parent_id,main_categories){
    let category_list = [];
    let categories = OmbazarFn.custom_filter(main_categories,"parent_id",parent_id,true);
    for (let x in categories){
        let item = categories[x];
        let category_id = item.category_id;
        item['categories'] = OmbazarFn.category_maker(category_id,main_categories);
        category_list.push(item);
    }
    return category_list;
};


OmbazarFn.country_init = function (countries) {
    let selectize = OmbazarFn.custom_selectize(".country-code-selector");
    for (let x in countries){
        let item = countries[x];
        let country_code = item.callingCodes[0];
        let label = item.alpha2Code+" ("+country_code+")";
        let value = country_code;
        let opt_ob = {label: label, value: value};
        selectize.addOption(opt_ob);
        countries[x].calling_code = country_code;
    }
    selectize.on("change",function () {

        let country = OmbazarFn.custom_filter(countries,'calling_code',item);
        let country_name_field = $(".own-country-name");
        let nationality = {
            country_name: country.name,
            calling_code: country.calling_code,
            region: country.region,
            flag: country.flag
        };
        OmbazarFn.selected_country = nationality;
        country_name_field.val(country.name);
        altair_md.update_input(country_name_field);
    });
    OmbazarFn.countries = countries;
};
OmbazarFn.custom_selectize1 = function(element,element_type=false /* just name or html object*/){
    if(element_type){
        var available=OmbazarFn.have(element,true);

    }
    else{
        var available=OmbazarFn.have(element);
    }
    if(available){
        if(element_type){
            var this_elem=element;
        }
        else{
            var this_elem=$(element);
        }
        var $this_selectize=this_elem.not("select.selectized,.plugin-remove_button").selectize({
            plugins: {
                'remove_button': {
                    label: ''
                }
            },
            valueField: 'value',
            labelField: 'label',
            searchField: ['label','value','mobile'],
            hideSelected: true,
            onDropdownOpen: function($dropdown) {
                $dropdown
                    .hide()
                    .velocity('slideDown', {
                        begin: function() {
                            $dropdown.css({'margin-top':'0'})
                        },
                        duration: 200,
                        easing: easing_swiftOut
                    });

            },
            onDropdownClose: function($dropdown) {
                $dropdown
                    .show()
                    .velocity('slideUp', {
                        complete: function() {
                            $dropdown.css({'margin-top':''})
                        },
                        duration: 200,
                        easing: easing_swiftOut
                    })
            },

        });
        var selectizeControl = $this_selectize[0].selectize;
        return selectizeControl;
    }
    else {
        return false;
    }
};

OmbazarFn.custom_selectize = function(element,options = {},element_type=false /* just name or html object*/){
    if(element_type){
        var available=OmbazarFn.have(element,true);
    }
    else{
        var available=OmbazarFn.have(element);
    }
    if(available){
        if(element_type){
            var this_elem=element;
        }
        else{
            var this_elem=$(element);
        }
        this_elem.not("select.selectized,.plugin-remove_button").each(function () {
            let default_options = {
                plugins: {
                    'remove_button': {
                        label: ''
                    }
                },
                valueField: 'value',
                labelField: 'label',
                searchField: ['label','value','mobile'],
                hideSelected: true,
                onDropdownOpen: function($dropdown) {
                    $dropdown
                        .hide()
                        .velocity('slideDown', {
                            begin: function() {
                                $dropdown.css({'margin-top':'0'})
                            },
                            duration: 200,
                            easing: easing_swiftOut
                        });

                },
                onDropdownClose: function($dropdown) {
                    $dropdown
                        .show()
                        .velocity('slideUp', {
                            complete: function() {
                                $dropdown.css({'margin-top':''})
                            },
                            duration: 200,
                            easing: easing_swiftOut
                        })
                },

            };
            $.extend(default_options,options);
            this.selectize(default_options);
        });
    }
    else{
        console.log("Not exist select element!");
    }
};
OmbazarFn.sp_selectize_init = function(list=[]){
    let sp_selectize = OmbazarFn.custom_selectize(".specification_selectize");
        for (let x in list){
            sp_selectize.addOption(list[x]);
        }
        list.push({label:"Select specification",value:""})
        sp_selectize.setValue("");
    return sp_selectize;
};

//for add item category an spcifications manage
OmbazarFn.category_selectize_init = function (categories) {
    let selectize = OmbazarFn.custom_selectize(".category-selector");
    for (let x in categories){
        let item = categories[x];
        let label = item.category_name;
        let value = item.category_id;
        let opt_ob = {label: label, value: value};
        selectize.addOption(opt_ob)
    }

    //Initialize  specificatinos select
    let sp_selectize = OmbazarFn.sp_selectize_init();
    let tags = OmbazarFn.tags_maker(".tags");

    let t = $(".specification_selectize").selectize()[0].selectize;

    selectize.on("change",function (item) {
        sp_selectize.clearOptions();
        $(".btnSectionRemove").closest("#d_form_row").remove();
        let specifications = OmbazarFn.custom_filter(categories,"category_id",item).specifications;
        let list = [{
            label:"Select specification",
            value:""
        }];

        for (let sp in specifications){
            let ob = {
                label:sp,
                value:sp
            };
            sp_selectize.addOption(ob);
            list.push(ob);
        }
        OmbazarFn.sp_list = list;
    });

    sp_selectize.on("change",function (item) {
        let elem = $(this)[0].$input;
        let cat_id = $(".category-selector").val();
        let specifications = OmbazarFn.custom_filter(categories,"category_id",cat_id).specifications;
        let values = specifications[item];
        let tag = elem.closest("#d_form_row").find("input.tags");
        let new_instance = tag.selectize()[0].selectize;
        let tag_list = values.split(",");
        console.log(new_instance);
        for(let x in tag_list){
            let ob = {
                text:tag_list[x],
                value:tag_list[x]
            };
            new_instance.addOption(ob);
        }
    });
    OmbazarFn.categories = categories;

};

//this for add category parent category choser
OmbazarFn.access_category_init = function (categories,$scope,$timeout) {
    let parent_categories = OmbazarFn.custom_filter(categories,"parent_id",'0',true);
        let category_list = [];
        for (let x in parent_categories){
            let item = parent_categories[x];
            let category_id = item.category_id;
            let new_categories = OmbazarFn.category_maker(category_id,categories);
            // if(new_categories.length){
                item['categories'] = new_categories;
                category_list.push(item);
            // }
        }
        OmbazarFn.categories = categories;
        $scope.categories = category_list;
        $timeout(function () {
            altair_tree.tree_d();
        });
};