let base_path = '/base/';
let template_dir = base_path+"OmbazarBackEnd/templates/OmbazarBackEnd/resource/";
let static_dir = "/static/OmbazarBackEnd/assets/";
let app = angular.module("ombazar",["ngRoute","ngSanitize"]);


/// url key for find location / keyword like /home and page is html file for which is content of this page
let urls = {
    home:{"url":"/","page":"home"},
    alter_home:{"url":"/home","page":"home"},
    add_controller:{"url":"/add-new/:controller_type","page":"add-controller"},
    profile:{"url":"/profile/:user_name?","page":"profile"},
    logout:{"url":"/logout","page":"logout"},
    not_found:{"url":"/404","page":"404"},
    denied:{"url":"/505","page":"505"},
    login:{"url":"/login","page":"login"},
    collections:{"url":"/collections","page":"collections"},
    install:{"url":"/install","page":"install"},
    add_store:{"url":"/add-store","page":"add-store"},
    store:{"url":"/store/:store_id","page":"store"},
    add_category:{"url":"/add-category","page":"add-category"},
    category:{"url":"/category/:category_id","page":"category"},
    add_item:{"url":"/add-item","page":"add-item"},
    item:{"url":"/item/:item_id","page":"item"},
    controllers:{"url":"/controllers/:type","page":"controllers"},
    categories:{"url":"/categories","page":"categories"},
    stores:{"url":"/stores","page":"stores"},
    items:{"url":"/items","page":"items"},
    edit_controller:{"url":"/edit/:controller_type/:user_id","page":"edit-controller"},
    edit_store:{"url":"/edit-store/:store_id","page":"edit-store"},
    edit_category:{"url":"/edit-category/:category_id","page":"edit-category"},
    edit_item:{"url":"/edit-item/:item_id","page":"edit-item"},
    coupon_generator:{"url":"/coupon-generator","page":"coupon-generator"},
    round_generator:{"url":"/round-generator","page":"round-generator"},
    prize_manager:{"url":"/prize-manager","page":"prize-manager"},
    coupons:{"url":"/coupons/:batch","page":"coupons"},
    lucky_coupons:{"url":"/lucky-coupons","page":"lucky-coupons"},
    add_corporate:{"url":"/add-corporate","page":"add-corporate"},
    edit_corporate:{"url":"/edit-corporate/:corporate_id","page":"edit-corporate"},
    corporate:{"url":"/corporate/:corporate_id","page":"corporate"},
    corporates:{"url":"/corporates","page":"corporates"},
    activate:{"url":"/activate/:activate_id","page":"activate"},
    pos:{"url":"/pos","page":"pos"},
    item_bank:{"url":"/item-bank","page":"item-bank"},
    order:{"url":"/order/:order_id","page":"order"},
    orders:{"url":"/orders","page":"orders"},
    pos_print:{"url":"/pos-print/:order_id","page":"pos-print"},
    my_items:{"url":"/my-items","page":"my-items"},
    my_item:{"url":"/my-item/:store_item_id","page":"my-item"},
    my_item_edit:{"url":"/edit-my-item/:store_item_id","page":"edit-my-item"},
    point_manager:{"url":"/point-manager","page":"point-manager"},
    power_undo:{"url":"/power-undo","page":"power-undo"},
    expire_date_manager:{"url":"/expire-date-manager","page":"expire-date-manager"},
    distributed_point_manager:{"url":"/distributed-point-manager","page":"distributed-point-manager"},
    feature_settings:{"url":"/feature-settings","page":"feature-settings"},
    point_distributor:{"url":"/point-distributor","page":"point-distributor"},
    import_user:{"url":"/import-user","page":"import-user"},
    top_up_requests:{"url":"/requests/:request_type","page":"requests"},
    request:{"url":"/request/:transaction_id","page":"request"},
    forgot_password:{"url":"/forgot-password","page":"forgot-password"},
    cron_reports:{"url":"/cron-reports","page":"cron-reports"},
    brand_manager:{"url":"/brand-manager","page":"brand-manager"},
};


app.config(['$interpolateProvider','$routeProvider','$locationProvider','$httpProvider',function($interpolateProvider,$routeProvider,$locationProvider,$httpProvider) {
    $interpolateProvider.startSymbol('((');
    $interpolateProvider.endSymbol('))');

    $locationProvider.html5Mode(true);

    //provider for csft token security reason
    $httpProvider.defaults.xsrfCookieName = 'csrftoken';
    $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';

    //single page development
    for (let url_info in urls){
        let url = urls[url_info].url;
        let page = urls[url_info].page;

        $routeProvider
        .when(url, {
            templateUrl : template_dir+page+".html",
        })
    }
    $routeProvider
    .otherwise(urls.not_found.url,{
        redirect: urls.not_found.page
    })

}]);
app.run(function ($rootScope,$location) {
    $rootScope.logged = 1;
   $rootScope.currency = "৳";
   $rootScope.site_logo = "/base/media/default/om-bazar-logo.png";
   $rootScope.rows = [];
   $rootScope.require = function(){

   };
   $rootScope.error_page = function(){
        let url = "/404";
        $location.path(url);
    };
   $rootScope.registration_complete = 0;
   $rootScope.rows_load = function(){};
   $rootScope.stores = [];
   $rootScope.basic_info = undefined;
   $rootScope.previous_page = function () {
        window.history.back();
   };
   // basic info scopes
    // ***************************
    //** id_types, logged_user, logged_user_id, logged_user_type, activate_vendor,
    //** vendor_id, activate_store, store_id, activate_employee,
    //** employee_id, stores, point_rules
    // ***************************
   $rootScope.table_pagination_path = '/base/OmbazarBackEnd/templates/OmbazarBackEnd/resource/parts/table-pagination.html';
   $rootScope.denied_page = '/base/OmbazarBackEnd/templates/OmbazarBackEnd/resource/505.html';
});
app.filter('range', function() {
  return function(input, total) {
    total = parseInt(total);

    for (let i=0; i<total; i++) {
        if (i>0){
            input.push(i);
        }

    }

    return input;
  };
});
app.filter('if', function() {
  return function(input, result, comparism='',else_result) {
      if (comparism !== ""){
          if (input===comparism) {
              return result;
          }
          else{
              return else_result;
          }
      }
      else{
          if(input){
              return result;
          }
          else{
              return else_result;
          }
      }
  };
});
app.filter('default', function() {
  return function(input, result) {
      if (input === undefined){
          return result
      }
  };
});
app.filter('object_line_total', function() {
  return function(input) {
      let item = input;
      let price = item.price;
      let sale = item.sale;
      let vat = item.vat;
      let quantity = item.quantity;

      //for gather rice grand total
      let line_total = 0;
      if (item.sale){
          line_total = item.sale*item.quantity;
      }
      else{
          line_total = item.price*item.quantity;
      }
      let line_vat = line_total*(vat/100);
      let line_total_with_vat = line_total+line_vat;
      return line_total_with_vat;

  };
});
app.filter('object_exist', function() {
  return function(input, objects,key_name, result = 0) {
      let item = OmbazarFn.custom_filter(objects,key_name,input);
      if (item !== undefined){
          if (result){
              return result;
          }
          else{
              return item;
          }
      }
      else{
          return "not-found";
      }

  };
});

app.filter('point', function($rootScope) {
  return function(input,quantity=0) {
      return  OmbazarFn.point_maker($rootScope,input,quantity);
  };
});


app.service('service',function ($rootScope,$http) {
    /// logged user information wise  view
    this.logged_user = {};
    this.set_logged_user = function () {
        $rootScope.$emit("set_logged_user")
    };
    this.basic_info = {};
    this.set_basic_info = function () {
        $rootScope.$emit("set_basic_info");
        this.basic_info = $rootScope.basic_info;
    };
    this.get_basic_info = function () {
        return this.basic_info;
    };
    // for main menu handle access have or not
    this.menus = {};
    this.set_menus = function (menus) {
        this.menus = menus;
        $rootScope.$emit("set_menus")
    };
    this.get_menus = function () {
        return this.menus;
    };

    // for category chose tree
    this.category = {};
    this.set_category = function (new_category) {
        this.category = new_category;
        $rootScope.$emit("set_category")
    };
    this.get_category = function () {
        return this.category;
    };

    this.basic_info = {};
    this.set_basic_info = function () {
        $rootScope.$emit("set_basic_info");
        this.basic_info = $rootScope.basic_info;
    };
    this.get_basic_info = function () {
        return this.basic_info;
    };

    this.main_menu_reload = function () {
        $rootScope.$emit("main_menu_reload");
    };
    this.http = $http;


});

// Page header to footer basic controller
app.controller("basic",['$scope','$http','$rootScope','service','$location','$route','$timeout',function ($scope,$http,$rootScope,service,$location,$route,$timeout) {
    let non_requirement_pages = [
        'profile',
        'login',
        'logout',
        "add-new/cs",
        "forgot-password",
        "login-info-helper",
        "branch-error",
        "install",
    ];

    let public_pages = [
        "index",
        "",
        "home",
        "registration",
        "customer",
        "company",
        "switch-site",
        "invoice",
        "bill",
        "receipt",
        "service",
    ];
    public_pages = public_pages.concat(non_requirement_pages);
    let branch_less_pages = [
        "index",
        "",
        "home",
        "add-new/admin",
        "add-new/vendor",
        "add-new/user",
        "add-category",
        "add-item",
        "controllers/admin",
        "controllers/vendor",
        "controllers/employee",
        "controllers/user",
        "categories",
        "items",
        "add-store",
        "stores",
        "item-bank",
        "pos",
        "orders",
        "my-items",
        "report",
        "brand-manager",
        "add-corporate",
        "corporates",
        "add-new/employee",
        "point-manager",
        "requests/top-up",
        "requests/vendor-withdraw",
        "requests/user-withdraw",
        "requests/vendor-top-up",
        "requests/vendor-top-up",
        "requests/vendor-withdraw",
        "distributed-point-manager",
        "expire-date-manager",
        "feature-settings",
        "cron-reports",






        "handlers/admin",
        "reports",
        "initiate",
        "companies",
        "payment-reducer",
        "switch-site",
        "db-settings",
        "payments",
        "company_crons",
        "notification-templates",
        "mail-templates",
        "sms-templates",
        "sms-logs",
        "account-source",
        "document-source",
        "mapping",
    ];
    let initial_pages = [
        "add-new/cs",
        "settings",
        "account-source",
        "document-source",
        "mapping",
        "handlers/system_designer",
        "activities",
        "profile",
    ];
    initial_pages = initial_pages.concat(branch_less_pages);

    $scope.non_requirement_pages = non_requirement_pages;
    $scope.public_pages = public_pages;
    $scope.initial_pages = initial_pages;
    $scope.branch_less_pages = branch_less_pages;

    $rootScope.document_switch = 0;
    $rootScope.shortcut_important_list_switch = 0;
    $scope.nav_switch = 1;
    $scope.main_header_switch = 0;
    $scope.main_menu_switch = 0;
    $scope.rootScope = $rootScope;
    // When nav template data loaded
    $scope.nav_init = function(){
        $scope.main_header_switch = 1;
    };
    // When main header / logo and branch selector template data loaded
    $scope.main_header_init = function(){
        $scope.main_menu_switch = 1;
    };
    // When main menu template data loaded
    $scope.main_menu_init = function(){
        // altair_main_header.init();
        altair_helpers.full_screen();
        $scope.basic_info_init();

        // console.log('menu init all ok')
    };
    $scope.login_init = function(){
        $location.path("/login");
    };

    $scope.basic_info_init = function(){
        $("#sidebar_main,#header_main,.message-box").removeClass("uk-hidden-small");
        $("#page_content").removeClass("left-margin-remove-small");
        /// When default information available asynchronously
        // Models.estimate_storage(function (space) {
        //     Models.smart_informer(1,"Net storage is "+space+" MB");
        // });
        // console.log(result);

        Models.get_basic_info($http,$rootScope,function (data) {
            let basic_info = data;
            OmbazarFn.id_types = basic_info.id_types;
            let logged_user_id = basic_info.logged_user_id;
            if (logged_user_id !== undefined){
                $(".sidebar_actions").removeClass("uk-hidden");
            }
            // store selectize for switch store for admin/system designer/employee
            $scope.deactive_store = function(instance){
                $rootScope.basic_info.logged_store_id = undefined;
                $rootScope.basic_info.logged_store = undefined;
                let software_info = basic_info.software_info;
                $(".sidebar_logo img").attr("src",software_info.logo);
                instance.setValue("","silent");
                $route.reload();
            };


            Models.custom_selectize(".store-selectize",{
                onInitialize:function () {
                    let instance = this;
                    let stores = data.stores;
                    for (let store of stores){
                        store.label = store.store_name;
                        store.value = store.store_id;
                        instance.addOption(store);
                    }
                    $(document).on("click",".return-normal",function () {
                        Models.request_sender($http,"other","deactivate_store",function (response) {
                            if (response.status){
                                $scope.deactive_store(instance);
                            }
                        })
                    });

                    let logged_store_id = basic_info.logged_store_id;
                    if (logged_store_id !== undefined){
                        instance.setValue(logged_store_id,"silent");
                        // $timeout(function () {
                        //     $(".sidebar_logo img").attr("src",$rootScope.basic_info.logged_store.logo);
                        // });

                    }
                    else{
                        let last_activate_store = basic_info.last_activate_store;
                        instance.setValue(last_activate_store);
                    }
                },
                onChange:function (value) {
                    let instance = this;
                    if (value !== ""){
                        Models.request_sender($http,"other","activate",function (response) {
                            if (response.status){
                                if (response.basic_info !== undefined){
                                    $rootScope.basic_info = response.basic_info;

                                }
                                else{
                                    Models.notify("Basic info not found when activate store");
                                }
                                // $rootScope.basic_info.logged_store_id = value;
                                // $rootScope.basic_info.logged_store = response.activate_data;
                                // $(".sidebar_logo img").attr("src",response.activate_data.logo);
                                $(".sidebar_logo img").attr("src",$rootScope.basic_info.logged_store.logo);
                                let page_name = Models.page_name($location);
                                if (page_name === "store-error") {
                                    $rootScope.previous_page();
                                }
                                else{
                                    $route.reload();
                                }

                                /// empty before store wise data
                                $rootScope.document_save_items = [];
                                $rootScope.bank_cashes = undefined;

                            }
                            else{
                                Models.notify("Activate failed");
                            }
                        },["request","'"+value+"'"]);
                    }
                    else{
                        $scope.$applyAsync(function () {
                            $scope.deactive_store(instance);
                        });

                    }


                }
            });



            // let db_config = Models.db_init($rootScope.basic_info.tables_schema);
            $rootScope.logged_user_type = Models.logged_user_type($rootScope);
            // Get main menus data and initiate main menu
            $scope.menu_init = function(){
                if ($rootScope.basic_info!== undefined){
                    Models.user_menu_init($http,$rootScope,function (menu_list) {
                        $scope.menus = menu_list;
                        $timeout(function () {
                            Models.active_menu($location);
                            altair_main_sidebar.init();

                        });

                    });
                }
                else{
                    $scope.menus = [];
                }

            };

            $scope.menu_init();
            if ($rootScope.basic_info !== undefined) {
                if ($rootScope.basic_info.software_info !== undefined){
                    let software_info = $rootScope.basic_info.software_info;
                    $(".sidebar_logo img").attr('src',software_info.logo);
                    $rootScope.currency = software_info.currency;
                }
            }
            $rootScope.$on("main_menu_reload",function () {
                $scope.menu_init();
            });

            $scope.login_check();


        });

    };

    $scope.login_check = function(){
        ///public page
        let page_name = Models.page_name($location,[2]);
        let root_page_name = Models.page_name($location);

        for (let p of public_pages){
            if (page_name === p || p === root_page_name){
                $rootScope.logged = 1;
                return true;
            }
        }

        if(!$rootScope.basic_info.logged_user_id){
            // $location.path("/login");
            $rootScope.logged = 0;
            return false;
        }
        else{
            $rootScope.logged = 1;
            return true;
        }
    };

    $scope.page_controller = function(){
        let page = Models.page_name($location);
        let page_name = Models.page_name($location,[2]);
        Models.get_basic_info($http,$rootScope,function (basic_data) {
            // if(!$rootScope.basic_info.logged_user_id){
            //     $location.path("/login");
            // }


            let controller_type = Models.logged_user_type($rootScope);
            $scope.denied_page = function () {
                $rootScope.page_access = 0;
                $rootScope.denied_page_switch = 1;
                $rootScope.document_switch = 0;
                $rootScope.logged = 0;
                return false;
            };
            $scope.access_open = function () {
                $rootScope.page_access = 1;
                $rootScope.logged = 1;
                $rootScope.denied_page_switch = 0;
            };

            let logged_user_id = $rootScope.basic_info.logged_user_id;

            let public_page = $scope.public_pages.indexOf(page_name);
            let root_public_page = $scope.public_pages.indexOf(page);
            if (public_page !== -1){
                $scope.access_open();
                return false;
            }
            else if (root_public_page !== -1 ){
                $scope.access_open();
                return false;
            }
            else{
                basic_data = $rootScope.basic_info;
                if (controller_type === "cs"){
                    let root_initial_page = $scope.initial_pages.indexOf(page_name);
                    let initial_page = $scope.initial_pages.indexOf(page);
                    if ((initial_page === -1) && (root_initial_page === -1)){

                    }
                }
                else{
                    /// if branch not selected
                    let root_initial_page = $scope.branch_less_pages.indexOf(page_name);
                    let initial_page = $scope.branch_less_pages.indexOf(page);
                    if ((initial_page === -1) && (root_initial_page === -1)){

                    }
                }
            }
            if (controller_type === "cs"){
                return ;
            }
            else{
                let software_info = basic_data.software_info;
                if (software_info !== undefined){
                    let construction_mode = software_info.construction_mode;
                    if (construction_mode !== undefined){
                        let status = construction_mode.active;
                        if (status){
                            $rootScope.construction_time = construction_mode.date +" "+ construction_mode.time;
                            // console.log($rootScope.construction_time);
                            if (Date.parse($rootScope.construction_time) > Date.now()) {
                                $location.path('/construction-mode');
                                let logged_user = $rootScope.basic_info.logged_user;
                                if (logged_user!==undefined){
                                    let access_menus = logged_user.access_menus;
                                    if (access_menus !== undefined){
                                        $rootScope.basic_info.logged_user.access_menus = [];
                                        service.main_menu_reload();
                                    }
                                }
                            }

                        }
                        else{
                            $rootScope.construction_time = null;
                        }
                    }
                }
            }


            if (page === "profile"){
                $rootScope.logged = 1;
                return ;
            }
            let user_access_menus = Models.user_access_menus($rootScope);
            let get_access_keys = [];


            if ($rootScope.basic_info !== undefined && user_access_menus !== undefined){
                for (let x of user_access_menus){
                    get_access_keys.push(Number(x));
                }
                Models.get_main_menus($http,$rootScope,function (main_menus) {
                    let menu_info = Models.custom_filter(main_menus,"link",page_name);
                    if (menu_info !== undefined){
                        let menu_id = Number(menu_info.id);
                        let exist = get_access_keys.indexOf(menu_id);
                        if (exist === -1){
                            $scope.denied_page();
                        }
                        else{
                            $rootScope.page_access = 1;
                            $rootScope.logged = 1;
                            // $rootScope.denied_page_switch = 1;
                        }
                    }
                    // else if (menu_info === undefined) {
                    //     $scope.denied_page();
                    // }
                    else{
                        $scope.access_open();
                    }
                })
            }
            else if(user_access_menus === undefined){
                $scope.denied_page();
            }
        });


    };
    $scope.sidebar_manage = function(){
        let window_width = $(window).width();
        if (window_width <= 1220){
            altair_main_sidebar.hide_sidebar();
        }
    };

    $scope.$on('$routeChangeStart', function($event, next, current) {
        $scope.page_controller();
        $scope.sidebar_manage();
        Models.page_title_generator($location,$scope,$rootScope);
        /// destroy the before initiate data table for error handle of invoice/document page
        // let table = $( '#dt_tableHeeshab' ).dataTable().api();
        // if (table.context.length){
        //     table.destroy();
        // }

    });
    $scope.$on('$routeChangeSuccess', function($event, next, current) {
        $("body").click();
        $timeout(function () {
            Models.active_menu($location,true);
        });
        $("#page_content").removeClass("left-margin-remove-small");

        $rootScope.search_switch = false;
        $rootScope.shortcut_important_list_switch = 0;
        $rootScope.document_switch = 0;



    });

}]);

/// install or import collections
app.controller("install",['$scope','$http','$rootScope','service','$timeout',function ($scope,$http,$rootScope,service,$timeout) {
    let function_name = "installed_collections()";
    let extra_data = OmbazarFn.retrive_request_object(function_name);
    let form_data = OmbazarFn.form_data_maker("",extra_data);
    $scope.collections = [
    ];
    OmbazarFn.$http($http,form_data,function (response,status) {
        let find_data = response.find_data;
        for(let collection in find_data){
            $scope.collections.push(find_data[collection]);

        }
    });

    ///upload new collection
    let  token = OmbazarFn.csrftoken;
    let params = {
        request_type: "post",
        request_name: "upload_collection(request)",
        csrfmiddlewaretoken: token
    };
    OmbazarFn.drug_drop_upload(params,function (response) {
        let new_data = response.new_collection;
        let collection_name = new_data.collection_name;
        let available = OmbazarFn.find_index($scope.collections,"collection_name",collection_name);
        $scope.$apply(function () {
            if (available === -1) {
                $scope.collections.push(new_data);
            }else{
                $scope.collections[available] = new_data;
            }
        })
    });



}]);

// Collections  list page view
app.controller("collections",['$scope','$http','$location','$rootScope','$timeout',function ($scope,$http,$location,$rootScope,$timeout) {
    $scope.page_title = "Collections";
    $scope.collection_name = "collections";
    $scope.data_query = {};
    $scope.query_option = {};
    let function_name = "collections()";
    let extra_data = OmbazarFn.retrive_request_object(function_name);
    let form_data = OmbazarFn.form_data_maker("",extra_data);
    OmbazarFn.$http($http,form_data,function (response,status) {
        let find_data = response.find_data;
        let data = [];
        for(let collection_name of find_data){
            let ob = {
                label: collection_name,
                value: collection_name
            };
            data.push(ob);
        }
        let selectize = OmbazarFn.custom_selectize(".collection-names",{
            options: data,
            onChange: function (value) {
                if (value !==""){
                    OmbazarFn.retrieve_data($http,"collection_keys",{},function (response) {
                        $scope.collection_name = value;
                        let keys = response.find_data;
                        $scope.keys = keys;
                        if (keys.length){
                            if (!$scope.switch){
                                $scope.switch = 1;
                                $scope.data_load_switch = 1;
                            }
                            else{
                                let page_selectize = $(".ts_gotoPage").get(0).selectize;
                                page_selectize.setValue(1,"silent");

                                $rootScope.rows_load(function () {
                                    $scope.data_load_switch = 1;
                                    // $timeout(function () {
                                    //     $('#ts_pager_filter').trigger('update');
                                    // });

                                },1);
                            }
                        }
                        else {
                            $rootScope.rows = [];
                            $scope.keys = [];
                            $scope.data_load_switch = 0;
                        }
                    },{},true,["'"+value+"'"])
                }
            }
        });
    });
    $scope.document_delete = function (document_id) {
        OmbazarFn.confirm(function () {
            let collection_name = $(".collection-names").val();
            let function_name = "document_delete('"+collection_name+"','"+document_id+"')";
            let extra_data = OmbazarFn.modify_request_object(function_name);
            let form_data = OmbazarFn.form_data_maker("",extra_data);
            OmbazarFn.$http($http,form_data,function (response,status) {
                if (response.status){
                    let $index = OmbazarFn.index_number($scope.rows,"_id",document_id);
                    $rootScope.rows.splice($index, 1);
                }
            })
        });
    };
    $scope.drop_collection = function () {
        OmbazarFn.confirm(function () {
            let collection_name = $(".collection-names").val();
            let selectize = $(".collection-names").get(0).selectize;
            let function_name = "drop_collection('"+collection_name+"')";
            let extra_data = OmbazarFn.modify_request_object(function_name);
            let form_data = OmbazarFn.form_data_maker("",extra_data);
            OmbazarFn.$http($http,form_data,function (response,status) {
                if (response.status){
                    selectize.setValue("");
                    selectize.removeOption(collection_name,"silent");
                    $scope.keys = [];
                    $scope.$rootScope = [];
                }
            })
        },"Are you sure drop this collection");
    }

}]);


/// for power of user
app.controller("main_menu",['$scope','$http','service','$timeout','$rootScope',function ($scope,$http,service,$timeout,$rootScope) {
    $rootScope.$on("set_logged_user",function () {
        if ($rootScope.basic_info.logged_user_id !== undefined){
            let access_menus = $rootScope.basic_info.logged_user.access_menus;
            let function_name = "main_menus(request)";
            let extra_data = OmbazarFn.retrive_request_object(function_name);
                extra_data['include'] = JSON.stringify(access_menus);
            let form_data = OmbazarFn.form_data_maker("",extra_data);

            OmbazarFn.$http($http,form_data,function (response,status) {
                let main_menus = response.find_data;
                let parent_menus = OmbazarFn.custom_filter(main_menus,"parent_id",'0',true);
                let menu_list = [];

                for (let x in parent_menus){
                    let item = parent_menus[x];
                    let menu_id = item.id;
                    let new_menus = OmbazarFn.menu_maker(menu_id,main_menus);
                    if(new_menus.length){
                        item['menus'] = new_menus;
                        menu_list.push(item);
                    }
                }
                $scope.menus = menu_list;
                $timeout(function () {
                    altair_main_sidebar.init();
                });
                // any change this menu
                $rootScope.$on('set_menus',function () {
                    let menus = service.get_menus();
                    $scope.menus = menus;
                });
            });
        }
        else{
            $scope.menus = [];
        }

    });
}]);

app.directive("subMenus",function () {
    let template  = ''+
            '<ul ng-if="menu.menus.length>0">\n' +
            '<li title="Dashboard" ng-repeat="menu in menu.menus " >\n' +
            '<a href="((menu.link))">\n' +
            '<span class="menu_title">((menu.menu_name))</span>\n' +
            '</a>\n' +
            '\n' +
            '<!-- directive: sub-menus -->' +
            '</li>\n' +
            '</ul>\n';
    return {
        restrict : "M",
        replace : true,
        template : template
    };
});

app.directive("subCategories",function () {
    let template  = ''+
            '<ul ng-if="category.categories.length>0">\n' +
            '<li id="((category.category_id))" ng-repeat="category in category.categories " >\n' +
            '((category.category_name))\n' +
            '<!-- directive: sub-categories -->' +
            '</li>\n' +
            '</ul>\n';
    return {
        restrict : "M",
        replace : true,
        template : template
    };
});

// Add a new controller
app.controller("add_controller",['$scope','$http','$location','$interpolate','$routeParams','$timeout','service','$rootScope',function ($scope,$http,$location,$interpolate,$routeParams,$timeout,service,$rootScope) {
    //page requirement
    OmbazarFn.get_basic_info($rootScope,service,function () {
        $timeout(function () {
            OmbazarFn.init_requirement();
        });
        $scope.name = '';
        $scope.add_new_label = '';
        let controller_type = $routeParams.controller_type;
        $scope.controller_type = controller_type;
        //check controller type and fix add new form information
        if(controller_type === 'admin' ){
            $scope.add_new_label = "Add new admin";
            $scope.name = "Admin name";
        }
        else if(controller_type === "vendor"){
            $scope.add_new_label = "Add new vendor";
            $scope.name = "Vendor name";
        }
        else if(controller_type === "employee"){
            $scope.add_new_label = "Add new Employee";
            $scope.name = "Employee name"
        }
        else if(controller_type === "user"){
            $scope.add_new_label = "Add new user";
            $scope.name = "User name"
        }
        OmbazarFn.countries_init($http,function () {
            // Any event fire after success
        });
        $scope.default_image = static_dir+"img/default/profile.png";
        OmbazarFn.access_menu_init($http,$routeParams,$scope,$timeout,function () {
            ///////// event after success
        });
        $scope.submit = function () {
            // add an admin
            let function_name = "add_controller(request, '"+controller_type+"')";
            let extra_data = OmbazarFn.passed_request_object(function_name);
                extra_data['nationality'] = JSON.stringify(OmbazarFn.selected_country);
                // console.log(extra_data);
            let  tree = [];
            if($scope.controller_type !== "vendor") {
                tree = [
                    {
                        element_name:".access_menu" ,
                        selected_name:"menus[]" ,
                        active_name:"parent"
                    }
                 ];
            }
            else{
                tree = [];
            }
            if($scope.controller_type === "employee"){
                let store_ob = {
                    element_name:".access_store" ,
                    selected_name:"stores[]" ,
                    active_name:"parent"
                };
                tree.push(store_ob);
            }

            let form_data = OmbazarFn.form_data_maker(".add-controller",extra_data,[],tree);
            OmbazarFn.$http($http,form_data,function (response,status) {
                // console.log(response);
                if(response.status){
                    $rootScope.registration_complete = 1;
                    let user_name = response.user_name;
                    let url = "/profile/"+user_name;
                    OmbazarFn.add_action(response,$location,url);
                }
            });

        }
    });

}]);
// Edit a controller

app.controller("edit_controller",['$scope','$http','$location','$interpolate','$routeParams','$timeout','service','$rootScope',function ($scope,$http,$location,$interpolate,$routeParams,$timeout,service,$rootScope) {
    //page requirement
    OmbazarFn.get_basic_info($rootScope,service,function () {
        // for access stores
        $scope.stores_init = 1;
        $scope.edit_label = '';
        let controller_type = $routeParams.controller_type;
        $scope.controller_type = controller_type;
        //check controller type and fix add new form information
        if(controller_type === 'admin' ){
            $scope.edit_label = "Edit admin";
        }
        else if(controller_type === "vendor"){
            $scope.edit_label = "Edit vendor";
        }
        else if(controller_type === "employee"){
            $scope.edit_label = "Edit Employee";
        }
        else if(controller_type === "user"){
            $scope.edit_label = "Edit user";
        }

        let user_id = $routeParams.user_id;

        let function_name = "controller_info('"+user_id+"')";
        let extra_data = OmbazarFn.retrive_request_object(function_name);
        let form_data = OmbazarFn.form_data_maker("",extra_data);
        OmbazarFn.$http($http,form_data,function (response,status) {
            $scope.controller = response;
            $scope.route_user_id = response.user_id;
            $scope.name = $scope.controller.name;
            $timeout(function () {
                OmbazarFn.init_requirement();
                OmbazarFn.countries_init($http,function (selectize) {
                    // Any event fire after success
                    let instance = selectize;
                    instance.setValue($scope.controller.calling_code);
                });
                OmbazarFn.access_menu_init($http,$routeParams,$scope,$timeout,function () {
                    let access_menus = $scope.controller.access_menus;
                    if (access_menus !== undefined){
                        let tree = $(".access_menu").fancytree("getTree");
                        for (let x in access_menus) {
                            let node = tree.getNodeByKey(access_menus[x]);
                            node.setSelected(true);
                        }
                    }
                    $timeout(function () {
                        altair_tree.tree_checkbox_2();

                        if($scope.controller_type === "employee"){
                            let access_stores = $scope.controller.stores;
                            if (access_stores !== undefined){
                                let tree = $(".access_store").fancytree("getTree");
                                for (let x in access_stores) {
                                    let node = tree.getNodeByKey(access_stores[x]);
                                    node.setSelected(true);
                                }
                            }
                        }
                    });


                });
            });
        });
    });

    $scope.submit = function () {
        //edit controller
        function_name = "edit_controller(request)";
        extra_data = OmbazarFn.modify_request_object(function_name);
        extra_data['nationality'] = JSON.stringify(OmbazarFn.selected_country);
        extra_data['user_id'] = $scope.controller.user_id;
        extra_data['before_password'] = $scope.controller.password;
        // extra_data['password'] = "open1234";
        extra_data['before_image'] = $scope.controller.image;
        extra_data['edit'] = 1;
        let tree = [];
        if($scope.route_user_id !== $rootScope.basic_info.logged_user_id){
            if($scope.controller_type !== "vendor"){
                tree = [
                    {
                        element_name:".access_menu" ,
                        selected_name:"menus[]" ,
                        active_name:"parent"
                    }
                ];
            }

            if($scope.controller_type === "employee"){
                let store_ob = {
                    element_name:".access_store" ,
                    selected_name:"stores[]" ,
                    active_name:"parent"
                };
                tree.push(store_ob);
            }
        }
        else{
            tree = [];
        }

        form_data = OmbazarFn.form_data_maker(".edit-controller",extra_data,[],tree);
        OmbazarFn.$http($http,form_data,function (response,status) {
            if(response.status){
                let url = "/profile/"+$scope.controller.user_name;
                OmbazarFn.add_action(response,$location,url);
            }
        });

    }
}]);

/// add store
app.controller("add_store",['$scope','$http','$timeout','$location',function ($scope,$http,$timeout,$location) {
    OmbazarFn.init_requirement();
    OmbazarFn.countries_init($http,function () {
        // Any event fire after success
    });
    $scope.submit = function(){
        let function_name = "add_store(request)";
        let extra_data = OmbazarFn.passed_request_object(function_name);
        let form_data = OmbazarFn.form_data_maker(".add-store",extra_data);
        OmbazarFn.$http($http,form_data,function (response,status) {
            if(response.status){
                let store_id = response.store_id;
                let url = "/store/"+store_id;
                OmbazarFn.add_action(response,$location,url);
            }
        });
    };
}]);

/// edit store
app.controller("edit_store",['$scope','$http','$timeout','$location','$routeParams','$rootScope',function ($scope,$http,$timeout,$location,$routeParams,$rootScope) {
    // get this store information
    let store_id = $routeParams.store_id;
    let function_name = "store('"+store_id+"')";
    let extra_data = OmbazarFn.retrive_request_object(function_name);
    let form_data = OmbazarFn.form_data_maker("",extra_data);
    OmbazarFn.$http($http,form_data,function (response,status) {
        $scope.store = response;

        $timeout(function () {
            OmbazarFn.init_requirement();
        });
        OmbazarFn.countries_init($http,function (selectize) {
            // Any event fire after success
            let instance = selectize;
            instance.setValue($scope.store.country);
        });
    });
    /// update store
    $scope.submit = function(){
        let function_name = "edit_store(request)";
        let extra_data = OmbazarFn.modify_request_object(function_name);
            extra_data['store_id'] = $scope.store.store_id;
            extra_data['before_image'] = $scope.store.image;
            extra_data['edit'] = 1;
        let form_data = OmbazarFn.form_data_maker(".edit-store",extra_data);
        OmbazarFn.$http($http,form_data,function (response,status) {
            if(response.status){
                let url = "/store/"+$scope.store.store_id;
                OmbazarFn.add_action(response,$location,url);
            }
        });
    };
}]);

/// view store
app.controller("store",['$scope','$http','$routeParams',function ($scope,$http,$routeParams) {
    OmbazarFn.init_requirement();
    let store_id = $routeParams.store_id;
    let function_name = "store('"+store_id+"')";
    let extra_data = OmbazarFn.retrive_request_object(function_name);
    let form_data = OmbazarFn.form_data_maker("",extra_data);
    OmbazarFn.$http($http,form_data,function (response,status) {
        $scope.store = response;
    });


}]);

/// add category
app.controller("add_category",['$scope','$http','$timeout','$location','service',function ($scope,$http,$timeout,$location,service) {
    OmbazarFn.init_requirement();
    altair_forms.sp_dynamic_fields();
    $(".tags").each(function () {
        OmbazarFn.tags_maker(this);
    });
    // Access category fancy tree initiate
    OmbazarFn.access_category_init($http,$scope,$timeout,function () {
        /// any event call when access catgory initiate
    });

    $scope.submit = function(){
        let function_name = "add_category(request)";
        let extra_data = OmbazarFn.passed_request_object(function_name);
        let tree = [
             {
                element_name:".access_category" ,
                selected_name:"parent_id" ,
                active_name:"parent"
             }
         ];
        let form_data = OmbazarFn.form_data_maker(".add-category",extra_data,[],tree);
        OmbazarFn.$http($http,form_data,function (response,status) {
            if(response.status){
                let category_id = response.category_id;
                let url = "/category/"+category_id;
                OmbazarFn.add_action(response,$location,url);
            }
        });
    };
}]);

/// edit category
app.controller("edit_category",['$scope','$http','$timeout','$location','$routeParams','$rootScope',function ($scope,$http,$timeout,$location,$routeParams,$rootScope) {
    // get this store information
    let category_id = $routeParams.category_id;
    let function_name = "category('"+category_id+"')";
    let extra_data = OmbazarFn.retrive_request_object(function_name);

    let form_data = OmbazarFn.form_data_maker("",extra_data);
    OmbazarFn.$http($http,form_data,function (response,status) {
        $scope.category = response;
        let sp = $scope.category.specifications;
        let sp_length = Object.keys(sp).length;
        $scope.sp_length = sp_length;
        $timeout(function () {
            OmbazarFn.init_requirement();
            altair_forms.sp_dynamic_fields();
            $(".tags").each(function () {
                OmbazarFn.tags_maker(this);
            });
            // Access category fancy tree initiate
            OmbazarFn.access_category_init($http,$scope,$timeout,function () {
                /// any event call when access catgory initiate
                let parent_id = $scope.category.parent_id;
                if (parent_id !== undefined){
                    let tree = $(".access_category").fancytree("getTree");
                    let node = tree.getNodeByKey(parent_id);
                        node.setSelected(true);
                        node.setExpanded(true);
                        node.setActive();
                        node.setFocus();
                }
            });
        });
    });
    /// update store
    $scope.submit = function(){
        let function_name = "edit_category(request)";
        let extra_data = OmbazarFn.modify_request_object(function_name);
            extra_data['category_id'] = $scope.category.category_id;
            extra_data['before_image'] = $scope.category.image;
            extra_data['edit'] = 1;
            let fancy_tree = [
                 {
                    element_name:".access_category" ,
                    selected_name:"parent_id" ,
                    active_name:"parent"
                 }
             ];
        let form_data = OmbazarFn.form_data_maker(".edit-category",extra_data,[],fancy_tree);
        OmbazarFn.$http($http,form_data,function (response,status) {
            if(response.status){
                let url = "/category/"+$scope.category.category_id;
                OmbazarFn.add_action(response,$location,url);
            }
        });
    };
}]);



/// view category
app.controller("category",['$scope','$http','$routeParams',function ($scope,$http,$routeParams) {
    OmbazarFn.init_requirement();
    let category_id = $routeParams.category_id;
    let function_name = "category('"+category_id+"')";
    let extra_data = OmbazarFn.retrive_request_object(function_name);
    let form_data = OmbazarFn.form_data_maker("",extra_data);
    OmbazarFn.$http($http,form_data,function (response,status) {
        let parent_id = response.parent_id;
        $scope.category = response;
        $scope.category.parent_name = "Uncategorized";
        if(!parent_id || parent_id!=='0'){
            if(OmbazarFn.categories.length){
                let categories = OmbazarFn.categories;
                let parent = OmbazarFn.custom_filter(categories,"category_id",parent_id);
                $scope.category.parent_name = parent.category_name;
            }
            else{
                let function_name = "category('"+parent_id+"')";
                let extra_data = OmbazarFn.retrive_request_object(function_name);
                let form_data = OmbazarFn.form_data_maker("",extra_data);
                OmbazarFn.$http($http,form_data,function (response) {
                    let category_name = response.category_name;
                    $scope.category.parent_name = category_name;
                })
            }
        }

    });
}]);

/// add item
app.controller("add_item",['$scope','$http','$timeout','$location','service','$rootScope',function ($scope,$http,$timeout,$location,service,$rootScope) {
    OmbazarFn.get_basic_info($rootScope,service,function () {
        $scope.quantity_field = 0;
        let logged_user_id = $rootScope.basic_info.logged_user_id;
        let id_type_name = OmbazarFn.id_type_name(logged_user_id);
        if (id_type_name === "vendor"){
            $scope.quantity_field = 1;
        }
        else if($rootScope.basic_info.vendor_id !== undefined && $rootScope.basic_info.vendor_id !== "" ){
            $scope.quantity_field = 1;
        }

        OmbazarFn.init_requirement();
        $('.image_uploader').imageuploadify();
        OmbazarFn.item_category_init($http);
        $scope.submit = function(){
            let function_name = "add_item(request)";
            let extra_data = OmbazarFn.passed_request_object(function_name);

            let form_data = OmbazarFn.form_data_maker(".add-item",extra_data);
            OmbazarFn.$http($http,form_data,function (response,status) {
                if(response.status){
                    let item_id = response.item_id;
                    let url = "/item/"+item_id;
                    OmbazarFn.add_action(response,$location,url);
                }
            });
        };
    });

}]);
/// edit item
app.controller("edit_item",['$scope','$http','$timeout','$location','service','$routeParams','$rootScope',function ($scope,$http,$timeout,$location,service,$routeParams,$rootScope) {

    let item_id = $routeParams.item_id;
    let function_name = "item('"+item_id+"')";
    let extra_data = OmbazarFn.retrive_request_object(function_name);
    let form_data = OmbazarFn.form_data_maker("",extra_data);
    let delete_images = [];
    OmbazarFn.$http($http,form_data,function (response,status) {
        $scope.item = response;
        $scope.sp_length = 0;
        if ($scope.item.specifications !== undefined){
            $scope.sp_length = Object.keys($scope.item.specifications).length;
        }
        let images = response.images;
        $timeout(function () {
            OmbazarFn.init_requirement();
            OmbazarFn.item_category_init($http,function (category) {
                let category_id = $scope.item.category_id;
                category.setValue(category_id,'silent');
                let sp_list =  OmbazarFn.get_specifications(category_id);
                $(".form_section select.specification_selectize").each(function () {
                    let instance = $(this).get(0).selectize;
                    for (let x in sp_list){
                        instance.addOption(sp_list[x]);
                    }
                })
            });
        });
        $scope.finish_repeat = function(){
            $('.image_uploader').imageuploadify();
        };
        $scope.delete_image = function ($index) {
            delete_images.push($scope.item.images[$index]);
            $scope.item.images.splice($index, 1);

        }
    });

    $scope.submit = function(){
        let function_name = "edit_item(request)";
        let extra_data = OmbazarFn.modify_request_object(function_name);
            extra_data['item_id'] = $scope.item.item_id;
            extra_data['edit'] = 1;
            let json_str = JSON.stringify($scope.item.images);
            extra_data['net_images'] = json_str;
            let json_d_str = JSON.stringify(delete_images);
            extra_data['delete_images'] = json_d_str;
            if ($scope.item.bar_code !== ""){
                extra_data['before_bar_code'] = JSON.stringify($scope.item.bar_code);
            }

        let form_data = OmbazarFn.form_data_maker(".edit-item",extra_data);
        OmbazarFn.$http($http,form_data,function (response,status) {
            if(response.status){
                let item_id = $scope.item.item_id;
                let url = "/item/"+item_id;
                OmbazarFn.add_action(response,$location,url);
            }
        });
    };
}]);
/// edit my item
app.controller("edit_my_item",['$scope','$http','$timeout','$location','service','$routeParams','$rootScope',function ($scope,$http,$timeout,$location,service,$routeParams,$rootScope) {

    let item_id = $routeParams.store_item_id;
    let function_name = "store_item(request,'"+item_id+"')";
    let extra_data = OmbazarFn.retrive_request_object(function_name);
    let form_data = OmbazarFn.form_data_maker("",extra_data);
    let delete_images = [];
    OmbazarFn.$http($http,form_data,function (response,status) {
        $scope.item = response;
        $scope.sp_length = 0;
        if ($scope.item.specifications !== undefined){
            $scope.sp_length = Object.keys($scope.item.specifications).length;
        }
        let images = response.images;
        $timeout(function () {
            OmbazarFn.init_requirement();
            OmbazarFn.item_category_init($http,function (category) {
                let category_id = $scope.item.category_id;
                category.setValue(category_id,'silent');
                let sp_list =  OmbazarFn.get_specifications(category_id);
                $(".form_section select.specification_selectize").each(function () {
                    let instance = $(this).get(0).selectize;
                    for (let x in sp_list){
                        instance.addOption(sp_list[x]);
                    }
                })
            });
        });

    });

    $scope.submit = function(){
        let function_name = "edit_my_item(request)";
        let extra_data = OmbazarFn.modify_request_object(function_name);
            extra_data['store_item_id'] = $scope.item.store_item_id;
            extra_data['edit'] = 1;

        let form_data = OmbazarFn.form_data_maker(".edit-my-item",extra_data);
        OmbazarFn.$http($http,form_data,function (response,status) {
            if(response.status){
                let item_id = $scope.item.store_item_id;
                let url = "/my-item/"+item_id;
                OmbazarFn.add_action(response,$location,url);
            }
        });
    };
}]);

/// category chose
app.controller("category_selector",['$scope','$http','$timeout','service','$rootScope',function ($scope,$http,$timeout,service,$rootScope) {
    altair_forms.sp_dynamic_fields();
    let function_name = "categories()";
    let extra_data = OmbazarFn.retrive_request_object(function_name);
    let form_data = OmbazarFn.form_data_maker("",extra_data);
    if(OmbazarFn.categories.length){
        OmbazarFn.category_selectize_init(OmbazarFn.categories,service,$timeout);
    }
    else{
        OmbazarFn.$http($http,form_data,function (response,status) {
            let categories = response.find_data;
            OmbazarFn.category_selectize_init(categories,service,$timeout);
        });
    }
}]);
app.controller("bar_code_generator",['$scope','$http',function ($scope,$http) {
    OmbazarFn.init_requirement();
    $scope.details = "";
    let function_name = "bar_codes()";
    let extra_data = OmbazarFn.retrive_request_object(function_name);
    let form_data = OmbazarFn.form_data_maker("",extra_data);
    if(OmbazarFn.bar_codes.length){

        OmbazarFn.bar_code_selectize_init(OmbazarFn.bar_codes,$scope);
    }
    else{
        OmbazarFn.$http($http,form_data,function (response,status) {
            let bar_codes = response.find_data;
            OmbazarFn.bar_code_selectize_init(bar_codes,$scope);
        });
    }

}]);


/// view item
app.controller("item",['$scope','$http','$routeParams',function ($scope,$http,$routeParams) {
    OmbazarFn.init_requirement();
    let item_id = $routeParams.item_id;
    let function_name = "item('"+item_id+"')";
    let extra_data = OmbazarFn.retrive_request_object(function_name);
    let form_data = OmbazarFn.form_data_maker("",extra_data);
    OmbazarFn.$http($http,form_data,function (response,status) {
        $scope.item = response;

        let parent_id = response.category_id;
        $scope.item = response;
        $scope.item.parent_name = "Uncategorized";
        if(!parent_id || parent_id!=='0'){
            if(OmbazarFn.categories.length){
                let categories = OmbazarFn.categories;
                let parent = OmbazarFn.custom_filter(categories,"category_id",parent_id);
                $scope.item.parent_name = parent.category_name;

            }
            else{
                let function_name = "category('"+parent_id+"')";
                let extra_data = OmbazarFn.retrive_request_object(function_name);
                let form_data = OmbazarFn.form_data_maker("",extra_data);
                OmbazarFn.$http($http,form_data,function (response) {
                    let category_name = response.category_name;
                    $scope.item.parent_name = category_name;
                })
            }
        }
    });
}]);
/// view my item
app.controller("my_item",['$scope','$http','$routeParams',function ($scope,$http,$routeParams) {
    OmbazarFn.init_requirement();
    let item_id = $routeParams.store_item_id;
    let function_name = "store_item(request,'"+item_id+"')";
    let extra_data = OmbazarFn.retrive_request_object(function_name);
    let form_data = OmbazarFn.form_data_maker("",extra_data);
    OmbazarFn.$http($http,form_data,function (response,status) {
        $scope.item = response;
        console.log(response);
        let parent_id = response.category_id;
        $scope.item = response;
        $scope.item.parent_name = "Uncategorized";
        if(!parent_id || parent_id!=='0'){
            if(OmbazarFn.categories.length){
                let categories = OmbazarFn.categories;
                let parent = OmbazarFn.custom_filter(categories,"category_id",parent_id);
                $scope.item.parent_name = parent.category_name;

            }
            else{
                let function_name = "category('"+parent_id+"')";
                let extra_data = OmbazarFn.retrive_request_object(function_name);
                let form_data = OmbazarFn.form_data_maker("",extra_data);
                OmbazarFn.$http($http,form_data,function (response) {
                    let category_name = response.category_name;
                    $scope.item.parent_name = category_name;
                })
            }
        }
    });
}]);

// Profile page view using this controller
app.controller("profile",['$scope','$http','$location','$routeParams','$rootScope','service','$timeout',function ($scope,$http,$location,$routeParams,$rootScope,service,$timeout) {
    Models.get_basic_info($http,$rootScope,function () {
        //page requirement

        $scope.controller_type = "";
        $scope.top_up_switch = 0;
        $scope.withdraw_switch = 0;
        $scope.balance_action = 0;
        // Pending switch
        $scope.top_up_pending_switch = 0;
        $scope.withdraw_pending_switch = 0;

        OmbazarFn.init_requirement();
        let user_name = $routeParams.user_name;
        if (user_name === undefined){
            $scope.balance_action = 1;
        }
        else{
            $scope.balance_action = 0;
        }
        let user_type = OmbazarFn.logged_user_type($rootScope);
        // console.log($rootScope.basic_info);
        if(user_type === "admin"){
            $scope.balance_action = 0;
        }
        $scope.employee = 1;
        let function_name = '';
        if(user_name !== undefined){
            function_name = "controller_info('"+user_name+"',True)";
        }
        else{
            function_name = "logged_controller(request)";
        }

        let extra_data = OmbazarFn.retrive_request_object(function_name);
        let form_data = OmbazarFn.form_data_maker("",extra_data);
        OmbazarFn.$http($http,form_data,function (response,status) {
            // console.log(response);
            $timeout(function(){
                if(response.user_id === undefined){
                $rootScope.error_page();
                return false
            }
            if (response.pending !== undefined){
                // Top up pending possibility
                let top_up_pending = OmbazarFn.custom_filter(response.pending,"type","vendor_top_up",true);
                if (top_up_pending !== undefined){
                    if (top_up_pending.length){
                        $scope.top_up_pending_switch = 1;
                    }
                }
                // Withdraw pending possibility
                let withdraw_pending = OmbazarFn.custom_filter(response.pending,"type","vendor_withdraw",true);
                if (withdraw_pending !== undefined){
                    if (withdraw_pending.length){
                        $scope.withdraw_pending_switch = 1;
                    }
                }

            }
            $scope.user = response;
            let user_id = response.user_id;
            let controller_type = OmbazarFn.id_type_name(user_id);
            $scope.controller_type = controller_type;
            $timeout(function () {
                altair_md.fab_speed_dial($scope);
            });
            });


        });

        // Top up
        $scope.top_up = function () {
            function_name = "top_up(request,'vendor_top_up')";
            let extra_data = OmbazarFn.passed_request_object(function_name);
            let form_data = OmbazarFn.form_data_maker(".top-up",extra_data);
            OmbazarFn.$http($http,form_data,function (response,status) {
                if (response.status){
                    OmbazarFn.notify("Request send.");
                    OmbazarFn.clean("form");
                    $scope.top_up_pending_switch = 1;
                }
            });
        };
        // with draw
        $scope.withdraw = function () {
            function_name = "withdraw(request,'vendor_withdraw')";
            let extra_data = OmbazarFn.passed_request_object(function_name);
            let form_data = OmbazarFn.form_data_maker(".withdraw",extra_data);
            OmbazarFn.$http($http,form_data,function (response,status) {
                if (response.status){
                    OmbazarFn.notify("Request send.");
                    OmbazarFn.clean("form");
                    $scope.withdraw_pending_switch = 1;
                }
            });
        };

        $scope.open_top_up = function () {
            $scope.fab_close();
            $scope.top_up_switch = 1;
        };
        $scope.open_withdraw = function () {
            $scope.fab_close();
            $scope.withdraw_switch = 1;
        };
        $scope.fab_close = function () {
            $scope.top_up_switch = 0;
            $scope.withdraw_switch = 0;
        };
    });
}]);
// Logout using this controller
app.controller("logout",['$scope','$http','$location','$rootScope','service',function ($scope,$http,$location,$rootScope,service) {
    //page requirement
    let function_name = "logout(request)";

    let extra_data = OmbazarFn.common_request_object(function_name);
    let form_data = OmbazarFn.form_data_maker("",extra_data);
    OmbazarFn.$http($http,form_data,function (response,status) {
        if(response.logout !== undefined){
            $rootScope.basic_info.logged_user = {};
            $rootScope.basic_info.logged_user_id = undefined;
            $rootScope.basic_info.stores = {};
            $rootScope.basic_info.vendor_id = undefined;
            $rootScope.basic_info.activate_vendor = {};
            $rootScope.basic_info.activate_employee = {};
            $rootScope.basic_info.employee_id = undefined;
            $rootScope.basic_info.store_id = undefined;
            $rootScope.basic_info.activate_store = {};
            service.set_logged_user();
            if ($(".store-selectize").length){
                let store_selectize = $(".store-selectize").get(0);
                if (store_selectize !== undefined){
                    let store_switch = store_selectize.selectize;
                    store_switch.clearOptions();
                    let url = "/login";
                    $location.path(url);
                }
            }

        }
        else{
            console.log(response);
        }

    });
}]);

// login
app.controller("login",['$scope','$http','$rootScope','service','$location','$route','$timeout',function ($scope,$http,$rootScope,service,$location,$route,$timeout) {
    let elements,body,page_content;
    $timeout(function () {
        elements = $("#header_main,#sidebar_main");
        body = $("body");
        page_content = $("#page_content");
        elements.addClass("uk-hidden");
        page_content.removeAttr("id");
        body.addClass("login_page login_page_v2");

        Models.init_requirement();
    });
    Models.get_basic_info($http,$rootScope,function () {
        let container_total_height = $(".login_page_forms").outerHeight();
        let single_part = container_total_height / 5;
        let video_part = single_part * 4;

        let banner_video = Models.software_setting_by_key("login_banner_video",$rootScope);
        let login_banner = Models.software_setting_by_key("login_banner",$rootScope);
        $timeout(function () {
            $(".login_page_info .iframe").height(video_part);
            $(".login_page_info .iframe iframe").attr("src",banner_video);
            $(".login_page_info .login-banner-img").attr("src",login_banner).height(single_part);
        });

    });
    $scope.login = function () {
        let others = {
            form_name: ".login-form"
        };
        Models.request_sender($http,"get","login",function (data) {
            // console.log(data);
            if (data.status){
                $rootScope.basic_info = data.basic_info;
                $rootScope.main_menus = undefined;

                let logged_user = $rootScope.basic_info.logged_user;
                let member_id = logged_user.member_id;
                $scope.main_menu_switch = 0;
                service.main_menu_reload();
                $rootScope.logged_user_type = Models.logged_user_type($rootScope);


                let store_selectize = $(".store-selectize").get(0).selectize;
                let available_stores = $rootScope.basic_info.stores;
                store_selectize.clear("silent");
                store_selectize.clearOptions();

                for (let store of available_stores){
                    store.label = store.name;
                    store.value = store.store_id;
                    store_selectize.addOption(store);
                }
                let last_active_store = $rootScope.basic_info.last_activate_store;
                store_selectize.setValue(last_active_store);
                $(".sidebar_actions").removeClass("uk-hidden");

                elements.removeClass("uk-hidden");
                page_content.attr("id","page_content");
                body.removeClass("login_page login_page_v2");
                $location.path("/profile/");
                altair_main_sidebar.init();



            }

        },['request'],others);
    }

}]);

// Controllers  list page view
app.controller("controllers",['$scope','$http','$location','$routeParams','$timeout','$rootScope','service',function ($scope,$http,$location,$routeParams,$timeout,$rootScope,service) {
    let type = $routeParams.type;
    let add_url = "add-new/"+$routeParams.type;
    $scope.type = type;
    $scope.add_url = add_url;

    let page_title = "";
    if(type === 'admin'){
        page_title = "Admin list";
    }
    else if(type === 'vendor'){
        page_title ="Vendor list";
    }
    else if(type === 'employee'){
        page_title ="Employee list";
    }
    else if(type === 'user'){
        page_title ="User list";
    }
    $scope.page_title = page_title;
    $scope.collection_name = "controllers";
    OmbazarFn.get_basic_info($rootScope,service,function () {
        let id_type = OmbazarFn.id_types[type];
        $scope.data_query = {"user_id": {"$regex": "^"+id_type}};
        $scope.query_option = {};

        let logged_user_type = $rootScope.basic_info.logged_user_type;
        if (type === "employee"){
            let vendor_id = 0;
            if (logged_user_type === "vendor"){
                vendor_id = $rootScope.basic_info.logged_user_id;
            }
            else if(logged_user_type === "admin"){
                let active_vendor_id = $rootScope.basic_info.vendor_id;
                if (active_vendor_id !== undefined){
                    vendor_id = active_vendor_id;
                }

            }
            if (vendor_id){
                let parent = OmbazarFn.id_info(vendor_id,"type")+OmbazarFn.id_info(vendor_id,"sl");
                $scope.data_query['user_id'] = {"$regex":"\\d{18}"+parent}
            }
        }
        if(logged_user_type === "admin") {
            let access_menus = $rootScope.basic_info.logged_user.access_menus;
            let ind = -1;
            if (type === "admin"){
                ind = access_menus.indexOf("4");
            }
            else if(type === "vendor"){
                ind = access_menus.indexOf("5");
            }
            else if(type === "user"){
                ind = access_menus.indexOf("6");
            }
            else if(type === "employee"){
                ind = access_menus.indexOf("29");
            }

            if (ind !== -1){
                $scope.add_access = 1;
            }
        }
        $scope.switch = 1;
        $scope.table_data_init = 1;
    });


}]);
// Categories  list page view
app.controller("categories",['$scope','$http','$location','$routeParams','$timeout','$rootScope','service',function ($scope,$http,$location,$routeParams,$timeout,$rootScope,service) {
    OmbazarFn.get_basic_info($rootScope,service,function () {
        $scope.page_title = "Category list";
        $scope.collection_name = "categories";
        $scope.data_query = {};
        $scope.query_option = {};
        $scope.switch = 1;
        let access_menus = OmbazarFn.user_access_menus($rootScope);
        let ind = access_menus.indexOf("7");
        if (ind !== -1){
            $scope.add_category_access = 1;
        }
    });


}]);
// Categories  list page view
app.controller("stores",['$scope','$http','$location','$routeParams','$timeout','$rootScope','service',function ($scope,$http,$location,$routeParams,$timeout,$rootScope,service) {
    OmbazarFn.get_basic_info($rootScope,service,function () {

        $scope.page_title = "Store list";
        $scope.collection_name = "stores";
        $scope.data_query = {};
        let logged_user_type = $rootScope.basic_info.logged_user_type;
        if (logged_user_type === "vendor"){
            $scope.data_query['vendor_id'] = $rootScope.basic_info.logged_user_id;
        }
        else if(logged_user_type === "admin"){
            let vendor_id = $rootScope.basic_info.vendor_id;
            if (vendor_id !== undefined){
                $scope.data_query['vendor_id'] = vendor_id;
            }
        }

        $scope.query_option = {};
        $scope.switch = 1;
        $scope.init = 1;
        let access_menus = OmbazarFn.user_access_menus($rootScope);
        let ind = access_menus.indexOf("15");
        if (ind !== -1){
            $scope.add_store_access = 1;
        }
    });

}]);
// Items  list page view
app.controller("items",['$scope','$http','$location','$routeParams','$timeout','$rootScope','service',function ($scope,$http,$location,$routeParams,$timeout,$rootScope,service) {
    $scope.page_title = "Item list";
    OmbazarFn.get_basic_info($rootScope,service,function () {
        $scope.collection_name = "items";
        $scope.data_query = {};
        $scope.query_option = {};
        $scope.switch = 1;
        let access_menus = OmbazarFn.user_access_menus($rootScope);
        let ind = access_menus.indexOf("8");
        if (ind !== -1){
            $scope.add_item_access = 1;
        }
    });

}]);

// MY Items  list page view
app.controller("my_items",['$scope','$http','$location','$routeParams','$timeout','$rootScope','service',function ($scope,$http,$location,$routeParams,$timeout,$rootScope,service) {

    OmbazarFn.get_basic_info($rootScope,service,function () {
        $scope.page_title = "My store item list";
        $scope.collection_name = "stock_items";
        $scope.data_query = {
            zero_quantity: 1
        };
        $scope.query_option = {};
        $scope.switch = 1;
        $scope.custom_function = {
            function_name: "stock_items",
            parameteres: ['request'],
        };
        let access_menus = OmbazarFn.user_access_menus($rootScope);
        let ind = access_menus.indexOf("8");
        if (ind !== -1){
            $scope.add_item_access = 1;
        }
    });

}]);

// Orders list page view
app.controller("orders",['$scope','$http','$location','$routeParams','$timeout',function ($scope,$http,$location,$routeParams,$timeout) {
    $scope.page_title = "Order list";
    $scope.collection_name = "orders";
    $scope.data_query = {};
    $scope.query_option = {};
    $scope.switch = 1;
    $scope.custom_function = {
        function_name: "orders",
        parameteres: ['request']
    };

}]);
/// Coupon generator
app.controller("coupon_generator",['$scope','$http','$location','$timeout','$rootScope',function ($scope,$http,$location,$timeout,$rootScope) {
    OmbazarFn.init_requirement();
    $scope.collection_name = "coupon_batches";
    $scope.data_query = {};
    $scope.query_option = {};
    $scope.page_title = "Coupon batches";
    $scope.switch = 1;


    OmbazarFn.custom_selectize(".coupon-type");
    $scope.submit = function(){
        let function_name = "coupon_generator(request)";
        let extra_data = OmbazarFn.passed_request_object(function_name);
        let form_data = OmbazarFn.form_data_maker(".coupon-generator",extra_data);

        OmbazarFn.$http($http,form_data,function (response,status) {
            if(response.status){
                let url = "/coupon-generator";
                OmbazarFn.add_action(response,$location,url);
                $rootScope.rows_load();
            }
        });

    };

}]);
/// Coupons by batch id
app.controller("coupons",['$scope','$http','$location','$timeout','$rootScope','$routeParams',function ($scope,$http,$location,$timeout,$rootScope,$routeParams) {
    OmbazarFn.init_requirement();
    let batch = $routeParams.batch;
    $scope.collection_name = "coupons";
    $scope.data_query = {
        "batch": batch
    };
    $scope.query_option = {};
    $scope.page_title = "Coupons of ("+ batch+ ") batch";
    $scope.switch = 1;

}]);

/// Round generator
app.controller("round_generator",['$scope','$http','$location','$timeout','$rootScope',function ($scope,$http,$location,$timeout,$rootScope) {
    OmbazarFn.init_requirement();
    $scope.collection_name = "rounds";
    $scope.data_query = {};
    $scope.query_option = {};
    $scope.page_title = "Round list";
    $scope.switch = 1;
    $rootScope.require = function(){
        altair_forms.switches();
    };
    $scope.submit = function(){
        let function_name = "round_generator(request)";
        let extra_data = OmbazarFn.passed_request_object(function_name);
        let form_data = OmbazarFn.form_data_maker(".round-generator",extra_data);

        OmbazarFn.$http($http,form_data,function (response,status) {
            if(response.status){
                let url = "/round-generator";
                OmbazarFn.add_action(response,$location,url);
                $rootScope.rows_load();
            }
        });

    };
    $scope.status_change = function(round_id,status){
        let function_name = "round_status_change('"+round_id+"',"+status+")";
        let extra_data = OmbazarFn.modify_request_object(function_name);
        let form_data = OmbazarFn.form_data_maker("",extra_data);
        OmbazarFn.$http($http,form_data,function (response,status) {
            if(response.status){
                $rootScope.rows_load();
            }
        });
    };
    $("body").on("change",".round-manage-area input",function () {
        let status = $(this).prop("checked");
        let round_id = $(this).val();
        if(status){
            status = 1;
        }
        else{
            status = 0;
        }
        $scope.status_change(round_id,status);
    });
    

}]);

/// Brand manager
app.controller("brand_manager",['$scope','$http','$location','$timeout','$rootScope',function ($scope,$http,$location,$timeout,$rootScope) {
    OmbazarFn.init_requirement();
    $scope.collection_name = "brands";
    $scope.data_query = {};
    $scope.query_option = {};
    $scope.page_title = "Brand list";
    $scope.switch = 1;
    $scope.custom_function = {
        function_name: "brands",
        parameteres: [],
    };
    $rootScope.require = function(){
        altair_forms.switches();
    };
    OmbazarFn.custom_selectize(".brand-store-selectize",{
        onType: function (str) {
                let that = this;
                let query = {"store_name": {"$regex": str}};
                let option = {};
                OmbazarFn.retrieve_data($http,"estimated_brands",query,function (response,status) {
                    console.log(response);
                    let finds = response.find_data;
                    for(let x in finds){
                        let item = finds[x];
                        let ob = {
                            "label": item.store_name,
                            "value": item.store_id
                        };
                        // console.log(ob);
                        that.addOption(ob);
                    }
                    that.refreshOptions() ;
                },{},true)
            },
    });
    $scope.submit = function(){
        let function_name = "add_brand(request)";
        let extra_data = OmbazarFn.passed_request_object(function_name);
        let form_data = OmbazarFn.form_data_maker(".brand-manager",extra_data);

        OmbazarFn.$http($http,form_data,function (response,status) {
            if(response.status){
                let url = "/round-generator";
                OmbazarFn.add_action(response,$location);
                $rootScope.rows_load();
                let selectize = $(".brand-store-selectize").get(0).selectize;
                selectize.clearOptions();
            }
        });

    };
    $scope.status_change = function(round_id,status){
        let function_name = "brand_status_change('"+round_id+"',"+status+")";
        let extra_data = OmbazarFn.modify_request_object(function_name);
        let form_data = OmbazarFn.form_data_maker("",extra_data);
        OmbazarFn.$http($http,form_data,function (response,status) {
            if(response.status){
                $rootScope.rows_load();
            }
        });
    };
    $("body").on("change",".round-manage-area input",function () {
        let status = $(this).prop("checked");
        let round_id = $(this).val();
        if(status){
            status = 1;
        }
        else{
            status = 0;
        }
        $scope.status_change(round_id,status);
    });


}]);


/// Prize manager
app.controller("prize_manager",['$scope','$http','$location','$timeout','$rootScope','$compile','$interpolate',function ($scope,$http,$location,$timeout,$rootScope,$compile,$interpolate) {
    $scope.modal_path = template_dir+"parts/prize-edit-content.html";
    OmbazarFn.init_requirement();
    $scope.collection_name = "prizes";
    $scope.data_query = {};
    $scope.query_option = {};
    $scope.page_title = "Prize list";
    $scope.switch = 1;
    $scope.submit = function(){
        let function_name = "add_prize(request)";
        let extra_data = OmbazarFn.passed_request_object(function_name);
        let form_data = OmbazarFn.form_data_maker(".add-prize",extra_data);

        OmbazarFn.$http($http,form_data,function (response,status) {
            if(response.status){
                let url = "/prize-manager";
                OmbazarFn.add_action(response,$location,url);
                $rootScope.rows_load();
            }
        });
    };
    $scope.prize_name = "";
    $scope.unlock_amount = "0";
    $scope.edit = function (event) {
        $scope.init = 1;
        let prize_id = $(event.target).attr("data-prize-id");
        let filter = OmbazarFn.custom_filter($rootScope.rows,"prize_id",prize_id);
        $scope.edit_switch = 1;
        $scope.modal_context = {
            edit_prize_name:filter.prize_name,
            edit_unlock_amount:filter.unlock_amount,
            edit_prize_id:filter.prize_id,
        };
        // Initiate modal with merge context document object
        OmbazarFn.modal($interpolate,$scope,"#prize-edit",function () {
            // event making when form submit
            OmbazarFn.form_submit(".prize-edit",function (form, e) {
                let function_name = "edit_prize(request)";
                let extra_data = OmbazarFn.modify_request_object(function_name);
                    extra_data['prize_id'] = $scope.modal_context.edit_prize_id;
                    extra_data['edit'] = 1;
                let form_data = OmbazarFn.form_data_maker(form,extra_data);
                OmbazarFn.$http($http,form_data,function (response,status) {
                    console.log(response);
                    if(response.status){
                        let url = "/prize-manager";
                        OmbazarFn.add_action(response,$location,url);
                        OmbazarFn.modal_close();
                        $rootScope.rows_load();
                    }
                });
            })
        });
    };

}]);


/// Lucky coupons
app.controller("lucky_coupons",['$scope','$http','$location','$timeout','$rootScope',function ($scope,$http,$location,$timeout,$rootScope) {
    OmbazarFn.init_requirement();
    $scope.collection_name = "lucky_coupons";
    $scope.data_query = {};
    $scope.query_option = {};
    $scope.page_title = "Lucky coupons";
    $scope.switch = 1;

    $scope.status_change = function (lucky_id) {
        let function_name = "document_update(request)";
        let extra_data = OmbazarFn.modify_request_object(function_name);
        let update_object = {
            collection_name: "lucky_coupons",
            which:{active:1},
            where:{lucky_id:lucky_id}
        };
        extra_data['update_document'] = JSON.stringify(update_object);
        let form_data = OmbazarFn.form_data_maker("",extra_data);
        OmbazarFn.$http($http,form_data,function (response,status) {
            console.log(response);
            if(response.status){
                let lucky_index = OmbazarFn.index_number($rootScope.rows,"lucky_id",lucky_id);
                $rootScope.rows[lucky_index].active = 1;
            }
        });
    };

}]);


// Add a new controller
app.controller("add_corporate",['$scope','$http','$location','$interpolate','$routeParams','$timeout','service',function ($scope,$http,$location,$interpolate,$routeParams,$timeout,service) {
    //page requirement
    $timeout(function () {
        OmbazarFn.init_requirement();
        OmbazarFn.tags_maker(".tags");
        $('.image_uploader').imageuploadify();
        altair_forms.sp_dynamic_fields();

    });
    $scope.submit = function () {
        //add an admin
        let function_name = "add_corporate(request)";
        let extra_data = OmbazarFn.passed_request_object(function_name);
        let form_data = OmbazarFn.form_data_maker(".add-corporate",extra_data);
        OmbazarFn.$http($http,form_data,function (response,status) {
            if(response.status){
                let corporate_id = response.corporate_id;
                let url = "/corporate/"+corporate_id;
                OmbazarFn.add_action(response,$location,url);
            }
        });
    }
}]);
// Edit a Corporate

app.controller("edit_corporate",['$scope','$http','$location','$interpolate','$routeParams','$timeout','service','$rootScope',function ($scope,$http,$location,$interpolate,$routeParams,$timeout,service,$rootScope) {
    //page requirement

    let corporate_id = $routeParams.corporate_id;
    let function_name = "corporate('"+corporate_id+"')";
    let extra_data = OmbazarFn.retrive_request_object(function_name);
    let form_data = OmbazarFn.form_data_maker("",extra_data);
    let delete_images = [];
    OmbazarFn.$http($http,form_data,function (response,status) {
        $scope.corporate = response;
        let images = response.images;
        $timeout(function () {
            OmbazarFn.init_requirement();
            OmbazarFn.tags_maker(".tags");
            altair_forms.sp_dynamic_fields();
        });

        $scope.finish_repeat = function(){
            $('.image_uploader').imageuploadify();
        };
        $scope.delete_image = function ($index) {
            delete_images.push($scope.corporate.images[$index]);
            $scope.corporate.images.splice($index, 1);

        }
    });
    $scope.submit = function () {
        //edit controller
        function_name = "edit_corporate(request)";
        let extra_data = OmbazarFn.modify_request_object(function_name);
            extra_data['corporate_id'] = $scope.corporate.corporate_id;
            extra_data['edit'] = 1;
            let json_str = JSON.stringify($scope.corporate.images);
            extra_data['net_images'] = json_str;
            let json_d_str = JSON.stringify(delete_images);
            extra_data['delete_images'] = json_d_str;
        let form_data = OmbazarFn.form_data_maker(".edit-corporate",extra_data);
        OmbazarFn.$http($http,form_data,function (response,status) {
            if(response.status){
                let url = "/corporate/"+$scope.corporate.corporate_id;
                OmbazarFn.add_action(response,$location,url);
            }
        });
    }
}]);

/// view Corporate
app.controller("corporate",['$scope','$http','$routeParams','$timeout',function ($scope,$http,$routeParams,$timeout) {
    OmbazarFn.init_requirement();
    let corporate_id = $routeParams.corporate_id;
    let function_name = "corporate('"+corporate_id+"')";
    let extra_data = OmbazarFn.retrive_request_object(function_name);
    let form_data = OmbazarFn.form_data_maker("",extra_data);
    OmbazarFn.$http($http,form_data,function (response,status) {
        $scope.corporate = response;
        $timeout(function () {
            altair_md.init();
        })

    });
}]);

/// Activate user
app.controller("activate",['$scope','$http','$routeParams','$timeout','$location','$rootScope',function ($scope,$http,$routeParams,$timeout,$location,$rootScope) {
    OmbazarFn.init_requirement();
    let activate_id = $routeParams.activate_id;
    OmbazarFn.activate($http,$rootScope,activate_id,function (response,status) {
        if (response.status){
            let url = '';
            let id_type = response.id_type;
            let id_info = response.id_info;
            if(response.stores !== undefined){
                if (response.stores.length){
                    let stores = response.stores;
                    let store_instance = $(".store-selectize").get(0).selectize;
                        store_instance.clearOptions();
                    for (x in stores){
                        let store = stores[x];
                        let ob = {
                            label: store.store_name,
                            value: store.store_id,
                        };
                        store_instance.addOption(ob);
                        if(response.store_id){
                            store_instance.setValue(response.store_id, "silent");
                        }
                    }
                }
            }

            if (id_type === 'vendor'){
                url = "/stores";
                $rootScope.basic_info.activate_vendor = id_info;
                $rootScope.basic_info.vendor_id = id_info.user_id;
                if (response.store_id !== undefined){
                    $rootScope.basic_info.store_id = response.store_id;
                    $rootScope.basic_info.activate_store = response.store_info;

                }
            }
            else if (id_type === 'employee'){
                url = "/orders";
                $rootScope.basic_info.activate_employee = id_info;
                $rootScope.basic_info.employee_id = id_info.user_id;
            }
            else if (id_type === 'store'){
                url = "/orders";
                $rootScope.basic_info.activate_store = id_info;
                $rootScope.basic_info.store_id = id_info.store_id;
                let store_switch = $(".store-selectize").get(0).selectize;
                store_switch.setValue(activate_id);
            }


            $location.path(url);
        }
        else {
            window.history.back();
        }
    });
}]);

/// Undo power
app.controller("power_undo",['$scope','$http','$routeParams','$timeout','$location','$rootScope',function ($scope,$http,$routeParams,$timeout,$location,$rootScope) {
    let function_name = "power_undo(request)";
    let extra_data = OmbazarFn.common_request_object(function_name);
    let form_data = OmbazarFn.form_data_maker("",extra_data);

    OmbazarFn.$http($http,form_data,function (response,status) {
        if (response.status){
            $location.path("/home");
            $rootScope.basic_info.activate_store = undefined;
            $rootScope.basic_info.store_id = undefined;
            $rootScope.basic_info.activate_employee = undefined;
            $rootScope.basic_info.employee_id = undefined;
            let store_switch = $(".store_switcher").get(0).selectize;
            if ($rootScope.basic_info.logged_user_type === "admin"){
                store_switch.clearOptions();
            }
            if ($rootScope.basic_info.logged_user_type !== "employee"){
                $rootScope.basic_info.vendor_id = undefined;
                $rootScope.basic_info.activate_vendor = undefined;

            }
            store_switch.setValue("");
        }

    });
}]);


/// Corporates
app.controller("corporates",['$scope','$http','$location','$timeout','$rootScope',function ($scope,$http,$location,$timeout,$rootScope) {
    OmbazarFn.init_requirement();
    $scope.collection_name = "corporates";
    $scope.data_query = {};
    $scope.query_option = {};
    $scope.page_title = "Corporates";
    $scope.switch = 1;

}]);

/// Item bank
app.controller("item_bank",['$scope','$http','$location','$timeout','$rootScope',function ($scope,$http,$location,$timeout,$rootScope) {

    $scope.collection_name = "item_bank";
    $scope.data_query = {};
    $scope.query_option = {};
    $scope.page_title = "Item bank";
    $scope.switch = 1;
    $scope.custom_function = {
        function_name: "item_bank",
        parameteres: ['request']
    };
    $scope.search_name = "";
    $scope.search_min_price = 0;
    $scope.search_max_price = 0;
    OmbazarFn.item_category_init($http,function () {},false);
    $timeout(function () {
        OmbazarFn.init_requirement();
    });

    $scope.add_to_store = function(event){

        let target = event.target;
        let item = $(target).closest("tr");
        let this_button = item.find(".add-to-store-button");
            OmbazarFn.small_loading(this_button);
        let purchase = item.find(".purchase").val();
        let price = item.find(".price").val();
        let sale = item.find(".sale").val();
        let quantity = item.find(".quantity").val();
        let item_id = item.find(".item_id").val();
        // console.log(event);
        let function_name = "add_to_store(request)";
        let extra_data = OmbazarFn.passed_request_object(function_name);
            extra_data['purchase'] = purchase;
            extra_data['price'] = price;
            extra_data['sale'] = sale;
            extra_data['quantity'] = quantity;
            extra_data['item_id'] = item_id;
        let form_data = OmbazarFn.form_data_maker("",extra_data);
        OmbazarFn.$http($http,form_data,function (response,status) {
            console.log(response);
            if(response.status){
                OmbazarFn.small_loading(this_button,0,"Save");
                let index_of_item = OmbazarFn.index_number($rootScope.rows,"item_name",item_id);
                let find_selected_ob = OmbazarFn.custom_filter($rootScope.rows,"item_id",item_id);
                let new_data = response.item_info;
                let merge = OmbazarFn.merge(find_selected_ob,new_data);
                $rootScope.rows[index_of_item] = merge;
            }
            else{
                OmbazarFn.small_loading(this_button,0,"Failed");
            }
        });
    };
    $scope.update_stock = function(event){
        let target = event.target;
        let item = $(target).closest("tr");
        let this_button = item.find(".update-stock-button");
            OmbazarFn.small_loading(this_button);
        let purchase = item.find(".purchase").val();
        let price = item.find(".price").val();
        let sale = item.find(".sale").val();
        let quantity = item.find(".quantity").val();
        let store_item_id = item.find(".store_item_id").val();
        let item_id = item.find(".item_id").val();
        // console.log(event);
        let function_name = "update_stock(request)";
        let extra_data = OmbazarFn.modify_request_object(function_name);
            extra_data['purchase'] = purchase;
            extra_data['price'] = price;
            extra_data['sale'] = sale;
            extra_data['quantity'] = quantity;
            extra_data['item_id'] = item_id;
            extra_data['store_item_id'] = store_item_id;
        let form_data = OmbazarFn.form_data_maker("",extra_data);
        OmbazarFn.$http($http,form_data,function (response,status) {
            OmbazarFn.small_loading(this_button,0,"Added");
            if(response.status){
                OmbazarFn.small_loading(this_button,0,"Added");
            }
        });
    };
    $("body").on("keypress",".bank-item input",function () {
        let update_button = $(this).closest("tr").find(".update-stock-button");
        if(update_button.length){
            update_button.html("add more");
        }
    });
    $scope.search = function () {
        $scope.data_query = {};
        if ($scope.search_name !== ""){
            $scope.data_query['item_name'] = {"$regex": $scope.search_name};
        }
        if ($scope.search_min_price){
            $scope.data_query['price'] = {"$gte": Number($scope.search_min_price)};
        }
        if($scope.search_max_price){
            $scope.data_query['$and'] = [{"price": {"$lte": Number($scope.search_max_price)}}];
        }
        let category = $(".category-selector").val();
        if (category !== ""){
            $scope.data_query['category_id'] = category;
        }
        $rootScope.rows_load();
    };

}]);

/// Pos for sell
app.controller("pos",['$scope','$http','$location','$timeout','$rootScope','service',function ($scope,$http,$location,$timeout,$rootScope,service) {
    OmbazarFn.get_basic_info($rootScope,service,function () {
        $scope.limit = '10';
        $scope.total = 0;
        $scope.skip = 0;
        $scope.collection_name = "store_items";
        $scope.data_query = {};
        $scope.query_option = {};
        $scope.page_title = "Pos";
        $scope.switch = 1;
        $scope.customer_id = "";
        $scope.customer_image = "";
        $scope.customer_name = "Not selected";
        $scope.search_name = "";
        $scope.search_bar_code = "";
        $scope.custom_function = {
            function_name: "stock_items",
            parameteres: ['request'],
        };
        $scope.rows = [];
        $scope.pos_load = function(event=function () {}){
            OmbazarFn.request_data_load($http,$scope,$rootScope,$timeout,function (response,status) {
                    // console.log(response);
                    event(response,status);
                    altair_helpers.hierarchical_show();
            });
        };
        OmbazarFn.get_basic_info($rootScope,service,function () {
            OmbazarFn.total_counter($http,$scope,function (response, status) {
                // console.log(response);
                $scope.total = response.total;
                $scope.pos_load()
            });
        });
        var element=window;
            $(element).on("scroll",function(e){
                var scrollTop=$(this).scrollTop();
                var element_offset_top=$(document).height()-$(window).height();
                if(scrollTop==element_offset_top){
                    $scope.$apply(function () {
                        $scope.pos_load()
                    });

                }
            });
        $scope.search = function(){
            let active_index=$("#pos_input li[class='uk-active']").index();
            if(active_index==0){
                if ($scope.search_bar_code !== ""){
                    $scope.rows = [];
                    $scope.skip = 0;
                    $scope.limit = 0;
                    $scope.data_query = {};
                    $scope.data_query['bar_code.code'] = {"$regex": $scope.search_bar_code};
                    OmbazarFn.request_data_load($http,$scope,$rootScope,$timeout,function (response,status) {
                        if (response.find_data.length){
                            $scope.add_to_cart(response.find_data[0].store_item_id);
                            $scope.search_bar_code = "";
                            let bar = $(".product_bar_code_name");

                            $("#product_bar_code_name").val("").focus();

                        }
                        altair_helpers.hierarchical_show();
                    });
                }
            }else{
                $scope.rows = [];
                $scope.skip = 0;
                $scope.limit = 0;
                $scope.data_query = {};
                $scope.data_query['item_name'] = {"$regex": $scope.search_name};
                OmbazarFn.request_data_load($http,$scope,$rootScope,$timeout,function (response,status) {
                    console.log(response);
                    altair_helpers.hierarchical_show();

                });
            }

        };

        let search_customers = [];
        OmbazarFn.custom_selectize(".pos-user-search",{
            searchField: ["label","mobile"],
            onType: function (str) {
                let that = this;
                let id_type = OmbazarFn.id_types['user'];
                let query = {"mobile": {"$regex": str},"user_id": {"$regex": "^"+id_type}};
                let option = {};
                OmbazarFn.retrieve_data($http,"controllers",query,function (response,status) {
                    console.log(response);
                    let finds = response.find_data;

                    search_customers = finds;
                    for(let x in finds){
                        let item = finds[x];
                        let ob = {
                            "label": item.name,
                            "value": item.user_id,
                            "mobile": item.mobile
                        };
                        that.addOption(ob);
                    }
                    that.refreshOptions() ;
                })
            },
            onChange: function (value) {
                let customer = OmbazarFn.custom_filter(search_customers,"user_id",value);
                if (customer !== undefined){
                    $scope.$apply(function () {
                        $scope.customer_id = value;
                        $scope.customer_name = customer.name;
                        $scope.customer_image = customer.image;
                    });
                }
                else{
                    $scope.$apply(function () {
                        $scope.customer_id = "";
                        $scope.customer_name = "Not selected";
                        $scope.customer_image = "";
                    });
                }


            }

        });


        $scope.cart = [];

        let before_item = [];
        let cookies = Cookies.getJSON();
        for (let x in cookies){
            if(x !== 'csrftoken'){
                before_item.push(x);
            }
        }
        let c_query = {
            "store_item_id": {"$in": before_item}
        };
        let parameters = ["request"];
        OmbazarFn.retrieve_data($http,"cart_items",c_query,function (response,status) {
           let finds = response.find_data;
           for (let x in finds){
               let target = finds[x];
               let store_item_id = target.store_item_id;
               let cart_ob = cookies[store_item_id];
               target.quantity = cart_ob.quantity;
               let point = OmbazarFn.point_maker($rootScope,target);
               target.point = point;
               $scope.cart.push(target);

           }
           $scope.calculate();
        },{},true,parameters);
        $scope.add_to_cart = function(store_item_id){
            OmbazarFn.add_to_cart($rootScope,$scope, store_item_id);
            // console.log($scope.cart);
        };
        $scope.delete_cart_item = function($index,store_item_id){
            $scope.cart.splice($index, 1);
            Cookies.remove(store_item_id);
            $scope.calculate();
        };
        $scope.sub_total = 0;
        $scope.vat = 0;
        $scope.discount = 0;
        $scope.grand_total = 0;
        $scope.total_discount = 0;
        $scope.calculate = function(){
            let cart_items = $scope.cart;
            let total_info = OmbazarFn.order_total_info(cart_items,$scope.discount);
            $scope.sub_total = total_info.sub_total;
            $scope.vat = total_info.vat;
            $scope.point = total_info.point;
            $scope.grand_total = total_info.grand_total;
            $scope.total_discount = total_info.total_discount;
        };
        $scope.clear_cart = function(){
            let cookies = Cookies.getJSON();
            for (let x in cookies){
                if(x !== 'csrftoken'){
                    Cookies.remove(x);
                }
            }
            $scope.cart = [];
            $scope.calculate();
        };
        $("body").on("change keyup",".quantity",function () {
            let quantity = Number($(this).val());
            let store_item_id = $(this).closest("tr").attr("data-id");
            $scope.$apply(function () {
                OmbazarFn.add_to_cart($rootScope,$scope, store_item_id,quantity,true);
            });
        });
        $("body").on("keyup",".cart-discount-field",function () {

            let discount = Number($(this).val());
            $scope.$apply(function () {
                $scope.discount = discount;
                $scope.calculate();
            });
        });
        $scope.order = function(){
            console.log($scope.cart);
            let data = JSON.stringify($scope.cart);
            let method = "order(request)";
            let customer_id = $(".pos-user-search").val();
            let extra_data = OmbazarFn.passed_request_object(method);
                extra_data['order_type'] = "pos";
                extra_data['discount'] = $scope.discount;
                extra_data['data'] = data;
                extra_data['customer_id'] = customer_id;
            let form_data = OmbazarFn.form_data_maker("", extra_data);
            OmbazarFn.$http($http,form_data,function (response,status) {
                if(response.status){
                    let order_id = response.order_id;
                    // let url = "/order/"+order_id;
                    $scope.discount = 0;
                    $scope.clear_cart();
                    // $location.path(url);
                    let open_url = "/control/pos-print/"+order_id;
                    let new_window = window.open(open_url,"pos print","width=400,height=600");
                    new_window.print();
                    new_window.close();
                }
            })
        };
        $timeout(function () {
            OmbazarFn.init_requirement();
        },100);
    });


}]);

/// view order
app.controller("order",['$scope','$http','$routeParams','$timeout','$rootScope',function ($scope,$http,$routeParams,$timeout,$rootScope) {
    OmbazarFn.init_requirement();
    let order_id = $routeParams.order_id;
    let function_name = "order(request,'"+order_id+"')";
    let extra_data = OmbazarFn.retrive_request_object(function_name);
    let form_data = OmbazarFn.form_data_maker("",extra_data);
    OmbazarFn.$http($http,form_data,function (response,status) {
        if (response.find_data.length){
            let order = response.find_data[0];
            $scope.order = order;
            // console.log(order);
            order = OmbazarFn.order_info_filter(order,$rootScope);

            $timeout(function () {
                altair_md.init();
            });
            let disabled = 0;
            if (order.status === "Completed"){
                disabled = 1;
            }
            OmbazarFn.order_status_selectize(order.status,disabled,function (that) {});
            let status_selectize = $(".order-status")[0].selectize;
            status_selectize.on("change",function(value){
                    let items = [];
                    for (let item in order.order_items){
                        let this_item = order.order_items[item];
                        let item_info = {
                            store_item_id: this_item.store_item_id,
                            point: this_item.point,
                            quantity: this_item.quantity,
                            line_total: this_item.line_total,
                        };
                        items.push(item_info);
                    }
                    // console.log(items);
                let function_name = "order_status_change(request,'"+order_id+"','"+value+"')";
                let extra_data = OmbazarFn.modify_request_object(function_name);
                    extra_data['items'] = JSON.stringify(items);
                    extra_data['customer_id'] = order.customer_id;
                let form_data = OmbazarFn.form_data_maker("",extra_data);
                OmbazarFn.$http($http,form_data,function (response,status) {
                    if (value == "Completed"){
                        $scope.route_reload();
                    }
                })
            });
        }
        else{
            console.log(response);
        }

    });
}]);

/// point manager
app.controller("point_manager",['$scope','$http','$timeout','$location','$routeParams','$rootScope',function ($scope,$http,$timeout,$location,$routeParams,$rootScope) {
    // get this store information
    let function_name = "point_rules()";
    let extra_data = OmbazarFn.retrive_request_object(function_name);
    let form_data = OmbazarFn.form_data_maker("",extra_data);
    OmbazarFn.$http($http,form_data,function (response,status) {
        $scope.rules = response.find_data;
        $timeout(function () {
            OmbazarFn.init_requirement();
        });
    });
    /// update rule
    $scope.submit = function(event){
        let target_elem = $(event.target);
        OmbazarFn.small_loading(target_elem);
        let item = target_elem.closest(".md-card");
        let this_form = item.find(".rule-edit");
        let function_name = "edit_point_rule(request)";
        let extra_data = OmbazarFn.modify_request_object(function_name);
        let form_data = OmbazarFn.form_data_maker(this_form,extra_data);
        OmbazarFn.$http($http,form_data,function (response,status) {
            if(response.status){
                OmbazarFn.small_loading(target_elem,0,"Updated");
            }
            else{
                OmbazarFn.small_loading(target_elem, 0, "Failed");
            }
        });
    };
}]);

/// All feature settings
app.controller("feature_settings",['$scope','$http','$timeout','$location','$routeParams','$rootScope',function ($scope,$http,$timeout,$location,$routeParams,$rootScope) {
    // get all feature information
    let function_name = "feature_settings()";
    let extra_data = OmbazarFn.retrive_request_object(function_name);
    let form_data = OmbazarFn.form_data_maker("",extra_data);
    OmbazarFn.$http($http,form_data,function (response,status) {
        let position_cost = response.position_cost;
        let position_expire_time = response.position_expire_time;
        let cost_per_coupon = response.cost_per_coupon;
        let forgot_email = response.forgot_email;
        let login_banner = response.login_banner;
        let logo = response.logo;
        let login_banner_video = response.login_banner_video;
        let login_banner_back_end = response.login_banner_back_end;
        $scope.position_cost = position_cost;
        $scope.position_expire_time = position_expire_time;
        $scope.cost_per_coupon = cost_per_coupon;
        $scope.forgot_email = forgot_email;
        $scope.login_banner = login_banner;
        $scope.logo = logo;
        $scope.login_banner_video = login_banner_video;
        $scope.login_banner_back_end = login_banner_back_end;
        $timeout(function () {
            OmbazarFn.init_requirement();
            altair_md.card_overlay();
            Models.dropify({},".dropify-logo");
            Models.dropify({},".dropify-favicon");

        });
    });
    /// update settings
    $scope.submit = function(event){
        let target_elem = $(event.target);
        let this_form = target_elem.closest("form");
        let form_type = this_form.attr("data-type");

        OmbazarFn.small_loading(target_elem);
        let function_name = "update_feature_setting(request)";
        let update_object = {};
        let images = [];
        if (form_type === "position_cost"){
            let amount = this_form.find("[name='amount']").val();
            update_object = {
                "type": "position_cost",
                "option": "",
                "more_data": {
                    "amount": amount
                }
            }
        }
        else if (form_type === "cost_per_coupon"){
            let amount = this_form.find("[name='amount']").val();
            update_object = {
                "type": "cost_per_coupon",
                "option": "",
                "more_data": {
                    "amount": Number(amount)
                }
            }
        }
        else if(form_type === "position_expire_time"){
            let day = this_form.find("[name='day']").val();
            let hour = this_form.find("[name='hour']").val();
            let minute = this_form.find("[name='minute']").val();
            update_object = {
                "type": "position_expire_time",
                "option": "",
                "more_data": {
                    "value": {
                        "day": day,
                        "hour": hour,
                        "minute": minute,
                    }
                }
            }
        }
        else if(form_type === "forgot_email"){
            let forgot_email = this_form.find("[name='forgot_email']").val();
            update_object = {
                "type": "forgot_email",
                "option": "",
                "more_data": {
                    "value": forgot_email
                }
            }
        }
        else if(form_type === "logo"){
            update_object = {
                "type": "logo",
                "option": "",
                "more_data": {
                    "value": ""
                }
            }
        }
        else if(form_type === "login_banner"){
            update_object = {
                "type": "login_banner",
                "option": "",
                "more_data": {
                    "value": ""
                }
            }
        }
        else if(form_type === "login_banner_video"){
            let video = this_form.find("[name='video']").val();
            update_object = {
                "type": "login_banner_video",
                "option": "",
                "more_data": {
                    "value": video
                }
            }
        }
        else if(form_type === "login_banner_back_end"){
            update_object = {
                "type": "login_banner_back_end",
                "option": "",
                "more_data": {
                    "value": ""
                }
            }
        }





        let extra_data = OmbazarFn.modify_request_object(function_name);
            extra_data.update_object = JSON.stringify(update_object);
        let form_data = OmbazarFn.form_data_maker(this_form,extra_data);
        OmbazarFn.$http($http,form_data,function (response,status) {
            if(response.status){
                OmbazarFn.small_loading(target_elem,0,"Updated");
            }
            else{
                OmbazarFn.small_loading(target_elem, 0, "Failed");
            }
        });


    };
}]);

/// Expire date rules
app.controller("expire_date_manager",['$scope','$http','$timeout','$location','$routeParams','$rootScope',function ($scope,$http,$timeout,$location,$routeParams,$rootScope) {
    // get this store information
    let function_name = "expire_date_rules()";
    let extra_data = OmbazarFn.retrive_request_object(function_name);
    let form_data = OmbazarFn.form_data_maker("",extra_data);
    OmbazarFn.$http($http,form_data,function (response,status) {
        if (response.length === undefined){
            $scope.rules = response.value;
        }
        $timeout(function () {
            OmbazarFn.init_requirement();
            altair_forms.sp_dynamic_fields();
        });
    });
    /// update rule
    $scope.submit = function(){
        let function_name = "edit_expire_date_rule(request)";
        let extra_data = OmbazarFn.modify_request_object(function_name);
        let form_data = OmbazarFn.form_data_maker(".expire-date-manager",extra_data);
        OmbazarFn.$http($http,form_data,function (response,status) {
            if (response.status){
                OmbazarFn.notify();
            }
        });
    };
}]);

/// Distributed point settings
app.controller("distributed_point_manager",['$scope','$http','$timeout','$location','$routeParams','$rootScope',function ($scope,$http,$timeout,$location,$routeParams,$rootScope) {
    // get this store information
    let function_name = "distributed_point_slabs()";
    let extra_data = OmbazarFn.retrive_request_object(function_name);
    let form_data = OmbazarFn.form_data_maker("",extra_data);
    OmbazarFn.$http($http,form_data,function (response,status) {
        if (response.length === undefined){
            $scope.slabs = response.value;
        }
        $timeout(function () {
            OmbazarFn.init_requirement();
            altair_forms.sp_dynamic_fields();
        });
    });
    /// update slab
    $scope.submit = function(){
        let function_name = "edit_distributed_point_slab(request)";
        let extra_data = OmbazarFn.modify_request_object(function_name);
        let form_data = OmbazarFn.form_data_maker(".distributed-point-manager",extra_data);
        OmbazarFn.$http($http,form_data,function (response,status) {
            if (response.status){
                OmbazarFn.notify();
            }
        });
    };
}]);


/// Point distributor
app.controller("point_distributor",['$scope','$http','$timeout','$location','$routeParams','$rootScope',function ($scope,$http,$timeout,$location,$routeParams,$rootScope) {

    /// update rule
    $scope.load = function(){
        // get this store information
        let function_name = "point_distributor()";
        let extra_data = OmbazarFn.passed_request_object(function_name);
        let form_data = OmbazarFn.form_data_maker("",extra_data);
        OmbazarFn.$http($http,form_data,function (response,status) {
            console.log(response);
        });
    };
}]);

/// import user
app.controller("import",['$scope','$http','$rootScope','service','$timeout',function ($scope,$http,$rootScope,service,$timeout) {
    let function_name = "imported_user_info()";
    let extra_data = OmbazarFn.retrive_request_object(function_name);
    let form_data = OmbazarFn.form_data_maker("",extra_data);
    $scope.total_user  = 0;
    OmbazarFn.$http($http,form_data,function (response,status) {
        $scope.total_user = response.total;
    });

    ///upload new collection
    let  token = OmbazarFn.csrftoken;
    let params = {
        request_type: "post",
        request_name: "import_user(request)",
        csrfmiddlewaretoken: token
    };
    let options = {
        allow : '*.(xlsx)'
    };
    OmbazarFn.drug_drop_upload(params,function (response) {

        let import_errors = response.import_errors;
        if (import_errors !== undefined){
            let message_html = "<h3>Message</h3>";
            for (let sheet_info of import_errors){
                let sheet_number = sheet_info.sheet;
                let sheet_errors = sheet_info.errors;
                    message_html += "<label class='uk-display-block uk-text-bold'>"+sheet_number+" no. sheet errors</label>";
                for (let error of sheet_errors){
                    for (let err in error){
                        message_html += "<p class='md-card md-card-primary'> " + error[err] + "</p>";
                    }
                }
            }
            OmbazarFn.response(message_html);
        }
        else{
            console.log(response);
        }
        if (response.status){
            $scope.$apply(function () {
                $scope.total_user = response.total_user;
            });
        }

    },options);
    $scope.clear_users = function (event) {
        let this_button = $(event.target);
        OmbazarFn.small_loading(this_button);

        let function_name = "all_user_clear()";
        let extra_data = OmbazarFn.modify_request_object(function_name);
        let form_data = OmbazarFn.form_data_maker("",extra_data);
        $scope.total_user  = 0;
        OmbazarFn.$http($http,form_data,function (response,status) {
            if(response.status){
                OmbazarFn.small_loading(this_button,0,"<i class=\"material-icons\">done</i> Done");
            }
            else{
                OmbazarFn.small_loading(this_button,0,"<i class=\"material-icons\">delete_sweep</i> Failed");
            }
            $timeout(function () {
                $scope.total_user = response.total_user;
            },2000);

        });
    }
}]);

// Requests
app.controller("requests",['$scope','$http','$location','$routeParams','$timeout','$rootScope','service',function ($scope,$http,$location,$routeParams,$timeout,$rootScope,service) {
    OmbazarFn.get_basic_info($rootScope,service,function () {
        let vendor_id = OmbazarFn.vendor_id($rootScope);
        let reqeust_type = $routeParams.request_type;
        $scope.page_title = "";
        $scope.type = "";
        if (reqeust_type === "top-up"){
            $scope.page_title = "Top up requests";
            $scope.type = "top_up";
        }
        else if(reqeust_type === "vendor-withdraw"){
            if (vendor_id){
                $scope.page_title = "Withdraw requests";
            }
            else{
                $scope.page_title = "Vendor withdraw requests";
            }

            $scope.type = "vendor_withdraw"
        }
        else if(reqeust_type === "user-withdraw"){
            $scope.page_title = "User withdraw requests";
            $scope.type = "user_withdraw";
        }
        else if(reqeust_type === "vendor-top-up"){
            if(vendor_id){
                $scope.page_title = "Top up requests";
            }
            else{
                $scope.page_title = "Vendor top up requests";
            }

            $scope.type = "vendor_top_up";
        }

        $scope.collection_name = "transactions";
        $scope.data_query = {
            "type": $scope.type
        };
        let logged_user_type = OmbazarFn.logged_user_type($rootScope);
        $scope.user_type = logged_user_type;
        if(vendor_id && logged_user_type !== "admin"){
            $scope.data_query['user_id'] = vendor_id;
        }
        $scope.query_option = {};
        $scope.switch = 1;
        $scope.custom_function = {
            function_name: "transactions",
            parameteres: ['request']
        };
    });


}]);

// Request
app.controller("request",['$scope','$http','$location','$routeParams','$timeout',function ($scope,$http,$location,$routeParams,$timeout) {
    let transaction_id = $routeParams.transaction_id;
    $scope.page_title = "";
    let function_name = "transactions(request,'1')";
    let extra_data = OmbazarFn.retrive_request_object(function_name);
        extra_data['query'] = JSON.stringify({
            "transaction_id": transaction_id
        });
    let form_data = OmbazarFn.form_data_maker("",extra_data);
    OmbazarFn.$http($http,form_data,function (response,status) {
        let info = response.find_data[0];
        $scope.info = info;
        if ($scope.info !== undefined){
            let type = info.type;
            if (type === "top_up"){
                $scope.page_title = "Top up request";
            }
            else if(type === "vendor_withdraw"){
                $scope.page_title = "Vendor withdraw request";
            }
            else if(type === "user_withdraw"){
                $scope.page_title = "User withdraw request";
                $scope.type = "user_withdraw";
            }
        }

        $timeout(function () {
            let action_elem = ".transaction-action";
            OmbazarFn.custom_selectize(action_elem, {
                onInitialize: function () {
                    let original_element = $(this.$input);
                    let default_value = original_element.attr("value");
                    this.setValue(default_value,"silent");
                    // console.log(default_value);
                },
                onChange: function (value) {
                    let original_element = $(this.$input);
                    let transaction_id = original_element.attr("data-id");
                    $scope.change_status(value, transaction_id);
                }
            })
        });
        console.log(response);
    });
    $scope.note_switch = 0;
    $scope.change_status = function (status,transaction_id) {
        let function_name = "transaction_status_change(request,'"+status+"','"+transaction_id+"')";
        let extra_data = OmbazarFn.modify_request_object(function_name);
        let form_data = OmbazarFn.form_data_maker(".transaction-status",extra_data);
        OmbazarFn.$http($http,form_data,function (response,status) {
            console.log(response);
            if (response.status){
                OmbazarFn.notify();
                if ($("#form").length){
                    OmbazarFn.clean("form");
                }
                OmbazarFn.init_requirement();
            }
        });
    };
    $scope.note_open = function () {

        if($scope.note_switch){
            $scope.note_switch = 0;
        }
        else{
            $scope.note_switch = 1;
        }
        $timeout(function () {
            OmbazarFn.init_requirement();
        });
    }
}]);


/// forgot password
app.controller("forgot_password",['$scope','$http','$routeParams','$rootScope','$location','$timeout',function ($scope,$http,$routeParams,$rootScope,$location,$timeout) {
    $scope.all_off = function(){
        $scope.find_form_switch = 0;
        $scope.confirmation_switch = 0;
        $scope.find_list_switch = 0;
        $scope.new_password_switch = 0;
        $scope.not_found_switch = 0;
    };
    $scope.all_off();
    $scope.find_form_switch = 1;

    $timeout(function () {
        OmbazarFn.init_requirement();
    });


    $scope.find_forgots = function () {
        let function_name = "find_users(request)";
        let extra_data = OmbazarFn.retrive_request_object(function_name);
        let form_data = OmbazarFn.form_data_maker(".find-form",extra_data);
        OmbazarFn.$http($http,form_data,function (response,status) {
            if (response.find_data !== undefined){
                if (response.find_data.length){
                    $scope.find_list = response.find_data;
                    $scope.all_off();
                    $scope.find_list_switch = 1;
                }
                else {
                    $scope.all_off();
                    $scope.not_found_switch = 1;
                }
            }
        })
    };
    $scope.user_select = function (user_id, user_email, mobile) {
        let function_name = "send_code_forgot_user(request)";
        let extra_data = OmbazarFn.passed_request_object(function_name);
            extra_data['email'] = user_email;
            extra_data['user_id'] = user_id;
            extra_data['mobile'] = mobile;
        let form_data = OmbazarFn.form_data_maker("",extra_data);
        OmbazarFn.$http($http,form_data,function (response,status) {
            if(response.status){
                $scope.code_sender_email = user_email;
                $scope.all_off();
                $scope.confirmation_switch = 1;
                $timeout(function () {
                    OmbazarFn.init_requirement();
                })
            }
        })
    };
    $scope.confirm_code = function () {
        let function_name = "confirm_code(request)";
        let extra_data = OmbazarFn.passed_request_object(function_name);
        let form_data = OmbazarFn.form_data_maker(".confirm-form",extra_data);
        OmbazarFn.$http($http,form_data,function (response,status) {
            if (response.status){
                $scope.all_off();
                $scope.new_password_switch = 1;
            }
            $timeout(function () {
                OmbazarFn.init_requirement();
            })
        })
    };
    $scope.new_password = function () {
        let function_name = "new_password(request)";
        let extra_data = OmbazarFn.modify_request_object(function_name);
        let form_data = OmbazarFn.form_data_maker(".new-password-form",extra_data);
        OmbazarFn.$http($http,form_data,function (response,status) {
            if (response.status){
                $scope.all_off();
                $scope.done_switch = 1;
            }
            $timeout(function () {
                OmbazarFn.init_requirement();
            })
        })
    };
    $scope.back_to_find = function () {
        $scope.all_off();
        $scope.find_form_switch = 1;
        $timeout(function () {
            OmbazarFn.init_requirement();
        })
    }

}]);

/// Corporates
app.controller("cron_reports",['$scope','$http','$location','$timeout','$rootScope',function ($scope,$http,$location,$timeout,$rootScope) {
    OmbazarFn.init_requirement();
    $scope.collection_name = "crones";
    $scope.data_query = {};
    $scope.query_option = {};
    $scope.page_title = "Cron reports";
    $scope.switch = 1;

}]);

/// Home
app.controller("home",['$scope','$http','$location','$timeout','$rootScope','$routeParams','service',function ($scope,$http,$location,$timeout,$rootScope,$routeParams,service) {

    Models.get_basic_info($http,$rootScope,function (data) {
        let logged_user_type = Models.logged_user_type($rootScope);
        let url = "";
        let active_store = $rootScope.basic_info.store_id;
        if (Models.logged_user($rootScope)){
            if (logged_user_type === "admin"){
                url = "/controllers/vendor"
            }
            else if (logged_user_type === "vendor"){
                if (active_store !== undefined){
                    url = "/pos"
                }
                else{
                    url = "/stores"
                }
            }
            else if (logged_user_type === "employee"){
                if (active_store !== undefined){
                    url = "/pos"
                }
                else{
                    url = "/profile"
                }
            }
            $location.path(url);
        }
        else{
            window.location = "login";
        }


    })

}]);


app.controller("data_table_handler",['$scope','$http','$timeout','$rootScope',function ($scope,$http,$timeout,$rootScope) {

    $scope.limit = '10';
    $scope.filtered = 0;
    $scope.start_row = 0;
    $scope.total = 0;
    $scope.page = '1';
    $scope.prev_switch = 1;
    $scope.next_switch = 1;

    OmbazarFn.total_counter($http,$scope,function (response, status) {
        $scope.total = response.total;
        $scope.pages = Math.ceil($scope.total/$scope.limit);


        OmbazarFn.data_load($http,$scope,$rootScope,$timeout,function (response,status) {
            console.log(response);
        });
        $scope.next = function () {
            $scope.page = Number($scope.page)+1;
            OmbazarFn.data_load($http,$scope,$rootScope,$timeout,function (response,status) {
            });
        };
        $scope.prev = function () {
            $scope.page = $scope.page-1;
            OmbazarFn.data_load($http,$scope,$rootScope,$timeout,function (response,status) {
            });
        };
        $scope.reload = function (action=function () {}) {
            OmbazarFn.data_load($http,$scope,$rootScope,$timeout,function (response,status) {
                action();
            });
        };
        $scope.first = function () {
            $scope.page = 1;
            OmbazarFn.data_load($http,$scope,$rootScope,$timeout,function (response,status) {

            });
        };
        $scope.last = function () {
            let last_page = $scope.pages;
            $scope.page = last_page;
            OmbazarFn.data_load($http,$scope,$rootScope,$timeout,function (response,status) {

            });
        };
        $rootScope.rows_load = function (action=function () {},page=false) {
            if (page){
                $scope.page = page;
            }
            OmbazarFn.total_counter($http,$scope,function (response,status) {
                $scope.total = response.total;
                $scope.pages = Math.ceil($scope.total/$scope.limit);
                $scope.reload(function () {
                    action();
                });
            });
        };
        $rootScope.export = function () {
            OmbazarFn.export_data($http,$scope,$rootScope,$timeout,function (response,status) {

            });
        };

    });
}]);



