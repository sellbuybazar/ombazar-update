function loadDoc() {
    onmessage = function (e) {
        var obj=e.data;
        var path=obj.path;
        var file_name=obj.file_name;
        var xhttp = new XMLHttpRequest();
          xhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
            postMessage(this.responseText);

            }
          };
          xhttp.open("GET",path+"ajax/"+file_name+".php", true);
          xhttp.send();
    }
}
loadDoc();
