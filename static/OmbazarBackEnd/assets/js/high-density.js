$(function() {
    if(isHighDensity()) {
        $.getScript( "bower_components/dense/src/dense.js", function() {
            // enable hires images
            altair_helpers.retina_images();
        });
    }
    if(Modernizr.touch) {
        // fastClick (touch devices)
        FastClick.attach(document.body);
    }
});
$window.load(function() {
    // ie fixes
    altair_helpers.ie_fix();
});