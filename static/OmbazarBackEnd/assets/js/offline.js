var var_reminders,var_assign_online,var_notification,var_data_sync,r=0,o=0,n=0,b=0,connection_stat=0;
var items_obj,users_obj,team_members_obj,note_obj,income_expense_obj,company_obj,user_power_obj;
var connection_interval=0,conn_interval;
//element exist test


function document_name(){
    if(default_doc_name()=="invoice"){
       return "Invoice";
    }
    else if(default_doc_name()=="bill"){
        return "Bill";
    }
    else if(default_doc_name()=="reciept"){
        return "Receipt";
    }
}
function next_date(mydate,howday){
    var d=mydate.split(".");
    var day=d[0];
    var month=d[1];
    var year=d[2];

    var new_date=year+"-"+month+"-"+day;
    var day = new Date(new_date);
    var day_as_sec=day.getSeconds();
    var how_day_sec=60*60*24*Number(howday);
    var next_day_sec=day_as_sec+how_day_sec;
        day.setSeconds(next_day_sec);
    var nextDay = new Date(day);
    var m=Number(nextDay.getMonth())+1;
    return nextDay.getDate()+"."+m+"."+nextDay.getFullYear();
}
function get_age(mydate){
    var d=mydate.split(".");
    var day=d[0];
    var month=d[1];
    var year=d[2];
    var dateString=year+"-"+month+"-"+day;
    var today = new Date();
    var birthDate = new Date(dateString);
    var age = today.getFullYear() - birthDate.getFullYear();
    var m = today.getMonth() - birthDate.getMonth();
    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
        age--;
    }
    return age;
}
function custom_date(date){
    var d=date.split(".");
    var day=d[0];
    var month=d[1];
    var year=d[2];

    var new_date=year+"-"+month+"-"+day;
    var day = new Date(new_date);
    var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    m=months[day.getMonth()];
    return day.getDate()+" "+m+" "+day.getFullYear();
}
function custom_filter(obj,key_name,value){
    var find_item=obj.filter(function(item){
            return item[key_name]==value;
        });
    for(x in find_item){
        return find_item[x];
    }
};
function custom_object_filter(obj,key_name,value){
    var find_items=obj.filter(function(item){
            return item[key_name]==value;
        });
    return find_items;
};

function document_object_filter(obj,key_name,value){
    var find_items=obj.filter(function(item){
            return item[key_name]==value;
        });
    return find_items;
};

function custom_worker(file_name,path,obj={}){
    var w;
    if(typeof(Worker) !== "undefined") {
        if(typeof(w) == "undefined") {
            w = new Worker(path+"assets/js/worker.js");
        }
        var r;
        obj.path=path;
        obj.file_name=file_name;
        w.postMessage(obj);
        w.onmessage = function(event) {
            r=event;
            console.log(r);
        };
        console.log(r);
    } else {
        return "Sorry! No Web Worker support.";
    }
}

function default_doc_name(){
    var doc_name=$(".offline-doc").attr("data-default-document");
    return doc_name;
}
function default_doc_type(){
    var doc_type=".offline-"+defautl_doc_name()+" ";
    return doc_type;
}

function list_update(){
    $(".offline-item").remove();
    var docs=localStorage.getItem(default_doc_name());
    if(docs!==null){
        var docs_object=JSON.parse(docs);
//        console.log(docs_object);
//        docs_object.reverse();
//        var sl=docs_object.length;
        if(default_doc_name()=="invoice"){
           var doc_name="Invoice";
        }
        else if(default_doc_name()=="bill"){
            var doc_name="Bill";
        }
        else if(default_doc_name()=="reciept"){
            var doc_name="Receipt";
        }


        if(have("li .form_switcher_area")){
            for(x in docs_object){
                var int_number=docs_object[x].int_number;
                if(default_doc_name()!="reciept"){
                    var date=docs_object[x].issue_date;
                    var user_name=docs_object[x].name;
                }
                else{
                    var date=docs_object[x].date;

                    var this_user=custom_filter(team_members_obj,"user_id",docs_object[x].team_member);
                    var user_name=this_user.name;
                }
                var this_date=custom_date(date);


                var d_html=' <span class="uk-text-small uk-text-muted"> ('+this_date+')</span>';
                var user_html='<span class="uk-text-small uk-text-muted">'+user_name+'</span>';
                var data="<li class='offline-item'><a href='#' class='md-list-content ' offline-data-invoice-id='"+int_number+"'><span class='md-list-heading uk-text-truncate'>"+doc_name+" O  "+int_number+d_html+'</span>'+user_html+"</a></li>";
                $(".invoices_list li:nth-of-type(1)").after(data);
            }
        }
        else{
            for(x in docs_object){
                var int_number=docs_object[x].int_number;
                if(default_doc_name()!="reciept"){
                    var date=docs_object[x].issue_date;
                    var user_name=docs_object[x].name;
                }
                else{
                    var date=docs_object[x].date;

                    var this_user=custom_filter(team_members_obj,"user_id",docs_object[x].team_member);
                    var user_name=this_user.name;
                }
                var this_date=date;


                var d_html=' <span class="uk-text-small uk-text-muted"> ('+this_date+')</span>';
                var user_html='<span class="uk-text-small uk-text-muted">'+user_name+'</span>';
                var data="<li class='offline-item'><a href='#' class='md-list-content ' offline-data-invoice-id='"+int_number+"'><span class='md-list-heading uk-text-truncate'>"+doc_name+" O  "+int_number+d_html+'</span>'+user_html+"</a></li>";
                $(".invoices_list li:nth-of-type(1)").after(data);
            }
        }
    }
}

function open_first_offline_document(){
    if($(offline_invoice_list_class).find('a[offline-data-invoice-id]').length) {
        // open first invoice
        $(offline_invoice_list_class).find('a[offline-data-invoice-id]').eq(0).click();
    } else {
        // open form
        $offline_invoice_add_btn.trigger('click');
    }
}
function open_selected_offline_document(id){
    $(offline_invoice_list_class).find('a[offline-data-invoice-id="'+id+'"]').eq(0).click();
}

function form_data_object_maker(form_data){
    if(default_doc_name()!="reciept"){
        var int_number=form_data.get("int_number");
        var issue_date=form_data.get("issue_date");
        var due_date=form_data.get("due_date");
        var reciver=form_data.get("reciver");
        var sender=form_data.get("sender");
        var company_name=form_data.get("name");
        var address_line_1=form_data.get("address_line_1");
        var address_line_2=form_data.get("address_line_2");
        var email=form_data.get("email");
        var phone=form_data.get("phone");
        var services=[];
        var item_names=form_data.getAll("item_name[]");
        var item_rates=form_data.getAll("rate[]");
        var item_quantities=form_data.getAll("quantity[]");
        var item_vats=form_data.getAll("vat[]");
        var item_descriptions=form_data.getAll("description[]");
        var item_ids=form_data.getAll("item_id[]");
        var i=0;
        for(x in item_names){
            var name=item_names[x];
            var rate=item_rates[i];
            var quantity=item_quantities[i];
            var vat=item_vats[i];
            var description=item_descriptions[i];
            var item_id=item_ids[i];
            var service_object={
                name:name,
                rate:rate,
                quantity:quantity,
                vat:vat,
                description:description,
                item_id:item_id
            };
            services.push(service_object);
            i++;
        }
        var note=form_data.get("note");
        var discount=form_data.get("discount");
        var discount_type=form_data.get("discount_type");
        var total_data={
            int_number:int_number,
            issue_date:issue_date,
            due_date:due_date,
            reciver:reciver,
            sender:sender,
            name:company_name,
            address_line_1:address_line_1,
            address_line_2:address_line_2,
            email:email,
            phone:phone,
            services:services,
            note:note,
            discount:discount,
            discount_type:discount_type
        };
        return total_data;
    }
    else{
        var int_number=form_data.get("int_number");
        var team_member=form_data.get("team_member");
        var services=[];
        var descriptions=form_data.getAll("description[]");
        var amounts=form_data.getAll("amount[]");
        var i=0;
        for(x in descriptions){
            var description=descriptions[x];
            var amount=amounts[i];
            var service_object={
                description:description,
                amount:amount
            };
            services.push(service_object);
            i++;
        }
        var date=new Date();
        var m=Number(date.getMonth())+1;
        var today=date.getDate()+"."+m+"."+date.getFullYear();
        var type=form_data.get("type");
        var category=form_data.get("category");
        var note=form_data.get("note");
        var total_data={
            int_number:int_number,
            team_member:team_member,
            services:services,
            type:type,
            category:category,
            note:note,
            date:today
        };
        return total_data;
    }
}

function data_saver(name,object){
    if (typeof(Storage) !== "undefined") {
        var da=localStorage.getItem(name);
        if(da==null ){
            var new_object=[object];
            localStorage.setItem(name,JSON.stringify(new_object));
           }
        else{
            var before_data=JSON.parse(localStorage.getItem(name));
            var new_data=before_data;
            new_data.push(object);
            localStorage.setItem(name,JSON.stringify(new_data));
        }
    } else {
        return "Sorry! No Web Storage support..";
    }


}

function data_modifier(name,object){
    if (typeof(Storage) !== "undefined") {
        var da=localStorage.getItem(name);
        localStorage.setItem(name,JSON.stringify(object));

    } else {
        return "Sorry! No Web Storage support..";
    }


}

function object_delete(name,int_number){
    var docs=localStorage.getItem(name);
    var docs_object=JSON.parse(docs);
    var index_number=docs_object.findIndex(x => x.int_number==int_number);
    docs_object.splice(index_number, 1);
    data_modifier(name,docs_object);
}

function simple_auto_complete(field_name,data){
    var options = {
        sourceData: function(search, success) {
            dataResponse = [];
            data.forEach(function(el, i) {
                dataResponse.push({
                    label: el.label,
                    value: el.value
                });
            });
            success(dataResponse);
        },
        minChar: 3
    };
    $(field_name).lightAutocomplete(options);
}
function auto_complete_with_item_id(field_name,data){
    var options = {
        sourceData: function(search, success) {
            dataResponse = [];
            data.forEach(function(el, i) {
                dataResponse.push({
                    label: el.label,
                    value: el.value
                });
            });
            success(dataResponse);
        },
        minChar: 3,
        onPressEnter: function(item,element) {
            var this_element=$("#"+element.context.id);
            var attr_value=this_element.attr('data-field-name');
            this_element.val(item.label);
            $("#"+attr_value).val(item.value);
        },
        onClick: function(item,element) {
            var this_element=$("#"+element.context.id);
            var attr_value=this_element.attr('data-field-name');
            this_element.val(item.label);
            $("#"+attr_value).val(item.value);
        }
    };
    $(field_name).lightAutocomplete(options);
}

    function get_info(page_name,form_data,loading=true){
        var info;
        if(loading){
           loading_on();
           }
        $.ajax({
            async:false,
            url:path+"ajax/"+page_name+".php",
            data:form_data,
            type:"POST",
//            dataType:"json",
            success:function(data,status){
                info=data;
            }
        });
        return info;
    }

    function json_get_info(page_name,form_data,loading=true){
        var info;
        if(loading){
           loading_on();
           }
        $.ajax({
            async:false,
            url:path+"ajax/"+page_name+".php",
            data:form_data,
            type:"POST",
            dataType:"json",
            success:function(data,status){
                info=data;
            }
        });
        return info;
    }



$(document).ready(function(){
    function loading_on(){
        var loading="<div class='uk-text-center'><img class='uk-margin-top' src='"+path+"assets/img/spinners/spinner.gif' alt=''>";
        modal=UIkit.modal.blockUI(loading);
    }




    function connection_status(){
        return json_get_info("connection_status",{},false);
    }
    //test the connection
    var w;
//    if(typeof(Worker) !== "undefined") {
//        if(typeof(w) == "undefined") {
//            w = new Worker(path+"assets/js/connection.js");
//        }
//        var obj={
//            path:path
//        };
//        w.postMessage(obj);
//        w.onmessage = function(event) {
//            var data=event.data;
//            if(data!="1"){
//                if(connection_stat!=0){
//                    var s=offline();
//                    if(s){
//                        offline_mode(1);
//                    }
////
//                    connection_stat=0;
//                }
//
//            }
//            else{
//                if(connection_stat!=1){
//                    var s=online();
//                    if(s){
//                        offline_mode(0);
//                    }
////
//                    connection_stat=1;
//                }
//            }
//
//        };
//    } else {
//        alert("Sorry! No Web Worker support.");
//    }
    //end connection test

    function offline(){
        if($(".offline").length==0){
            var html='<div class="offline">Offline</div>';
            $("body").append(html);
        }

        $("a,button,.sidebar_actions").attr("not_allow","true");
        $("#printTable,[title='Colum select'],#sidebar_main_toggle,#invoice_print,.js-modal-confirm,.js-modal-confirm-cancel,#invoice_add,#offline_doc_add,#sidebar_secondary_toggle").removeAttr("not_allow");
        $(".offline-doc a,.offline-doc button").removeAttr("not_allow");

        if($("#offline_doc_add.hidden").length==1){
            $("#offline_doc_add").removeClass("hidden");
        }
        if($("#invoice_add.hidden").length==0){
            $("#invoice_add").addClass("hidden");
        }
        return true;
    }
    function online(){
        $(".offline").remove();
        $("a,button,.sidebar_actions").removeAttr("not_allow");
        if($("#invoice_add.hidden").length==1){
            $("#invoice_add").removeClass("hidden");
        }
        if($("#offline_doc_add.hidden").length==0){
            $("#offline_doc_add").addClass("hidden");
        }
        $(".offline-item").remove();
        return true;
    }


        function offline_mode(status=1){
        if(status==1){
            if(typeof(var_notification)!=="undefined"){
                if(n!=0){
                    clearInterval(var_notification);
                    n=0;
                }

            }

            if(typeof(var_assign_online)!=="undefined"){
                if(o!=0){
                    clearInterval(var_assign_online);
                    o=0;
                }

            }
//
            if(typeof(var_reminders)!=="undefined"){
                if(r!=0){
                    clearInterval(var_reminders);
                    r=0;
                }

            }
//
            if(typeof(var_data_sync)!=="undefined"){
                if(b!=0){
                    clearInterval(var_data_sync);
                    b=0;
                }

            }




        }
        else{


            var data=user_power_obj;
            if(typeof(data) !=="undefined"){
                if(data.add_team_member_power==1){
                    if(typeof(var_notification)==="undefined"){
                        n=1;
                        notification();
                        var_notification=setInterval(notification,11000);


                    }
                    else{
                        if(n!=1){
                            n=1;
                            notification();
                            var_notification=setInterval(notification,11000);
                        }
                    }
                }
                if(data.team_member_privilege==1){
                    if(typeof(var_reminders)==="undefined"){
                        r=1;
                        reminders();
                        var_reminders=setInterval(reminders,11000);


                    }
                    else{
                        if(r!=1){
                            r=1;
                            reminders();
                            var_reminders=setInterval(reminders,11000);
                        }
                    }
                }
                if(data.privilege_status==1){
                    if(typeof(var_assign_online)==="undefined"){
                        o=1;
                        assign_online();
                        var_assign_online=setInterval(assign_online,5000);


                    }
                    else{
                        if(o!=1){
                            o=1;
                            assign_online();
                            var_assign_online=setInterval(assign_online,5000);
                        }
                    }
                }
                if(data.branch_id==1){
                    if(typeof(var_data_sync)==="undefined"){
                        b=1;
                        data_sync();
//                        var_data_sync=setInterval(data_sync,3000);


                    }
                    else{
                        if(b!=1){
                            b=1;
                            data_sync();
//                            var_data_sync=setInterval(data_sync,3000);
                        }
                    }
                }

            }
        }

    }

    function notification(){
        $.ajax({
         type: "POST",
         url: path+"ajax/push-notification.php",
         dataType:"json",
         success: function(data)
          {
            if(data.length!=0){
                Push.create(data.title, {
                    body: data.details,
                    icon: data.icon,
                    timeout: 10000,
                    onClick: function () {
                        window.open(path+"views/notification/?id="+data.id,"notification","").focus();
                    }
                });
            }

          }
        });
    }

    function reminders(){
        $.ajax({
         type: "POST",
         url: path+"ajax/push-reminder.php",
         dataType:"json",
         success: function(data)
          {
            if(data.length!=0){
                for(x in data){
                    Push.create(data[x].title, {
                        body: data[x].details,
                        icon: data[x].icon,
                        timeout: 10000,
                        onClick: function () {
                            window.open(path+"views/reminder/?id="+data[x].id,"notification","").focus();
                        }
                    });
                }

            }

          }
        });

    }

    function assign_online(){
        $.ajax({
         type: "POST",
         url: path+"ajax/assign-online.php",
         success: function(data){
         }

        });
    }
    var i=1;
    function data_sync(){
        //this for invoice
        var data_object={};
        var docs=localStorage.invoice;
        if(docs!=null){
            var docs_object=JSON.parse(docs);
            var invoice=docs_object;
            data_object.invoice=invoice;
        }

        var docs=localStorage.bill;
        if(docs!=null){
            var docs_object=JSON.parse(docs);
            var bill=docs_object;
            data_object.bill=bill;
        }

        var docs=localStorage.reciept;
        if(docs!=null){
            var docs_object=JSON.parse(docs);
            var reciept=docs_object;
            data_object.reciept=reciept;
        }

        var key=Object.keys(data_object).length;
        var keys_object=0;
        if(typeof(data_object.invoice)!=="undefined"){
            keys_object+=data_object.invoice.length;
        }
        if(typeof(data_object.bill)!=="undefined"){
            keys_object+=data_object.bill.length;
        }
        if(typeof(data_object.reciept)!=="undefined"){
            keys_object+=data_object.reciept.length;
        }
        if(keys_object>0){
            var data=json_get_info("data-sync",data_object,false);
//            console.log(data);
            if(typeof(data)!=="undefined"){
                if(typeof(data_object.invoice)!=="undefined" && data_object.invoice.length!==0){

                    var invoice_status_success=data.invoice.success;
                    for(x in invoice_status_success){
                        var int_number=invoice_status_success[x];
                        object_delete("invoice",int_number);
                    }
                }
    //                        var invoice_status=data.invoice;
    //                        var bill_status=data.bill;
    //                        var reciept_status=data.reciept;
                if(typeof(data_object.bill)!=="undefined" && data_object.bill.length!==0){
                var bill_status_success=data.bill.success;
                    for(x in bill_status_success){
                        var int_number=bill_status_success[x];
                        object_delete("bill",int_number);

                    }
                }
                if(typeof(data_object.reciept)!=="undefined" && data_object.reciept.length!==0){
                var reciept_status_success=data.reciept.success;
                    for(x in reciept_status_success){
                        var int_number=reciept_status_success[x];
                        object_delete("reciept",int_number);

                    }
                }
            }
        }

    }


});

