let base_path = '/base/';
let template_dir = base_path+"OmbazarWeb/templates/OmbazarWeb/resource/";
let static_dir = "/static/OmbazarWeb/assets/";
let default_dir = base_path+"media/default/";
let app = angular.module("ombazar",["ngRoute","ngSanitize"]);

/// url key for find location / keyword like /home and page is html file for which is content of this page
let urls = {
    home:{"url":"/","page":"home"},
    alter_home:{"url":"/home","page":"home"},
    profile:{"url":"/profile/:user_name?","page":"profile"},
    logout:{"url":"/logout","page":"logout"},
    not_found:{"url":"/404","page":"404"},
    login:{"url":"/login","page":"login"},
    coupon_unloker:{"url":"/coupon-unlocker","page":"coupon-unlocker"},
    prize_details:{"url":"/prize-details/:coupon_number","page":"prize-details"},
    no_prize_info:{"url":"/no-prize-info","page":"no-prize-info"},
    corporates:{"url":"/corporates","page":"corporates"},
    corporate:{"url":"/corporate/:corporate_id","page":"corporate"},
    registration:{"url":"/registration","page":"registration"},
    products:{"url":"/products/:category_name?","page":"products"},
    order:{"url":"/order/:order_id","page":"order"},
    orders:{"url":"/orders","page":"orders"},
    item:{"url":"/item/:store_item_id","page":"item"},
    tree:{"url":"/tree/:tree_user_id?","page":"tree"},
    forgot_password:{"url":"/forgot-password","page":"forgot-password"},
    top_up_requests:{"url":"/requests/:request_type","page":"requests"},
    edit_profile:{"url":"/edit-profile","page":"edit-profile"},
    buy_coupon:{"url":"/buy-coupon","page":"buy-coupon"},
    brand:{"url":"/brand/:brand_id","page":"brand"},
};


app.config(['$interpolateProvider','$routeProvider','$locationProvider','$httpProvider',function($interpolateProvider,$routeProvider,$locationProvider,$httpProvider) {
    $interpolateProvider.startSymbol('((');
    $interpolateProvider.endSymbol('))');

    $locationProvider.html5Mode(true);

    //provider for csft token security reason
    $httpProvider.defaults.xsrfCookieName = 'csrftoken';
    $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';

    // single page development
    for (let url_info in urls){
        let url = urls[url_info].url;
        let page = urls[url_info].page;

        $routeProvider
        .when(url, {
            templateUrl : template_dir+page+".html",
        })
    }
    $routeProvider
    .otherwise(urls.not_found.url,{
        redirect: urls.not_found.page
    })

}]);

app.run(function ($rootScope, $location) {
    $rootScope.logged = 1;
    $rootScope.global_cart_switch = 0;
    $rootScope.currency = "৳";
    $rootScope.error_page = function(){
        let url = "/404";
        $location.path(url);
    };
    $rootScope.site_logo = "/static/OmbazarBackEnd/assets/img/default/global-face.png";
    $rootScope.rows = [];
    $rootScope.products = [];
    /// Breadcrumbs path
    $rootScope.path = $location.path();
    $rootScope.breadcrumbs = [];
    $rootScope.require = function(){};
    // $rootScope.basic_info = {};
    // basic info scopes
    // ***************************
    //** id_types, logged_user, tree_info, logged_user_id, point_rules,
    // ***************************

    $rootScope.registration_complete = 0;
    $rootScope.table_pagination_path = '/base/OmbazarBackEnd/templates/OmbazarBackEnd/resource/parts/table-pagination.html';
    $rootScope.cart_path = '/base/OmbazarWeb/templates/OmbazarWeb/resource/parts/cart.html';
    $rootScope.brand_url = '/base/OmbazarWeb/templates/OmbazarWeb/resource/parts/brands.html';
    $rootScope.bag_switch = function(){
        $(".cart").toggleClass("cart-hidden");
    };
    $rootScope.selected_tree_info = {};
    $rootScope.product_search = function () {
        $location.path("/products");
        // $scope.search();
    };
    $rootScope.cart = [];
});
app.filter('point', function($rootScope) {
  return function(input,quantity=0) {
      return  OmbazarFn.point_maker($rootScope,input,quantity);
  };
});

app.filter('range', function() {
  return function(input, total) {
    total = parseInt(total);

    for (let i=0; i<total; i++) {
        if (i>0){
            input.push(i);
        }

    }

    return input;
  };
});
app.filter('user_tooltip', function() {
  return function(input) {
      return OmbazarFn.user_tooltip(input);
  };
});

app.filter('if', function() {
  return function(input, result, comparism='') {
      if (comparism !== ""){
          if (input===comparism) {
              return result;
          }
      }
      else{
          if(input){
              return result;
          }
      }
  };
});

app.filter('default', function() {
  return function(input, result) {
      if (input === undefined){
          return result
      }
  };
});
app.filter('object_exist', function() {
  return function(input, objects,key_name, result = 0,else_result='not found') {
      let item = OmbazarFn.custom_filter(objects,key_name,input);
      if (item !== undefined){
          if (result){
              return result;
          }
          else{
              return item;
          }
      }
      else{
          return else_result;
      }

  };
});

app.filter('line_total', function() {
  return function(input, objects,key_name) {
      let item = OmbazarFn.custom_filter(objects,key_name,input);
      if(item !== undefined){
          let price = 0;
          if(item.sale){
              price = item.sale*item.quantity;
          }
          else{
              price = item.price*item.quantity;
          }
          return price;
      }
      else{
          return 0;
      }

  };
});

app.filter('line_quantity', function() {
  return function(input, objects,key_name) {
      let item = OmbazarFn.custom_filter(objects,key_name,input);
      if(item !== undefined){
          return item.quantity;
      }
      else{
          return 0;
      }

  };
});
app.filter('object_line_total', function() {
  return function(input) {
      let item = input;
      let price = item.price;
      let sale = item.sale;
      let vat = item.vat;
      let quantity = item.quantity;

      //for gather rice grand total
      let line_total = 0;
      if (item.sale){
          line_total = item.sale*item.quantity;
      }
      else{
          line_total = item.price*item.quantity;
      }
      let line_vat = line_total*(vat/100);
      let line_total_with_vat = line_total+line_vat;
      return line_total_with_vat;

  };
});


app.service('service',function ($rootScope,$http) {
    /// logged user information wise  view
    this.logged_user = {};
    this.set_logged_user = function (user_data) {
        this.logged_user = user_data;
        $rootScope.$emit("set_logged_user")
    };
    this.get_logged_user = function () {
        return this.logged_user;
    };
    this.basic_info = {};
    this.set_basic_info = function () {
        $rootScope.$emit("set_basic_info");
        this.basic_info = $rootScope.basic_info;
    };
    this.get_basic_info = function () {
        return this.basic_info;
    };
    // for main menu handle access have or not
    this.menus = {};
    this.set_menus = function (menus) {
        this.menus = menus;
        $rootScope.$emit("set_menus")
    };
    this.get_menus = function () {
        return this.menus;
    };

    // for products access have or not
    this.set_products = function () {
        $rootScope.$emit("set_products")
    };
    this.get_products = function () {
        return $rootScope.products;
    };
    // for cart access have or not
    this.set_cart = function () {
        $rootScope.$emit("set_cart");
    };
    this.get_cart = function () {
        return true;
    };
    this.main_menu_reload = function () {
        $rootScope.$emit("main_menu_reload");
    };


    // for category chose tree
    this.category = {};
    this.set_category = function (new_category) {
        this.category = new_category;
        $rootScope.$emit("set_category")
    };
    this.get_category = function () {
        return this.category;
    };
    this.http = $http ;


});

// Page header to footer basic controller
app.controller("basic",['$scope','$http','$rootScope','service','$location','$route','$timeout',function ($scope,$http,$rootScope,service,$location,$route,$timeout) {
    let non_requirement_pages = [
        // 'profile',
        'login',
        'logout',
        // "add-new/cs",
        "forgot-password",
        "login-info-helper",
        "branch-error",
        "install",
    ];

    let public_pages = [
        // "index",
        // "",
        // "home",
        "registration",
        "customer",
        "company",
        "switch-site",
        "invoice",
        "bill",
        "receipt",
        "service",
    ];
    public_pages = public_pages.concat(non_requirement_pages);
    let branch_less_pages = [
        "index",
        "",
        "home",
        "add-new/admin",
        "add-new/vendor",
        "add-new/user",
        "add-category",
        "add-item",
        "controllers/admin",
        "controllers/vendor",
        "controllers/employee",
        "controllers/user",
        "categories",
        "items",
        "add-store",
        "stores",
        "item-bank",
        "pos",
        "orders",
        "my-items",
        "report",
        "brand-manager",
        "add-corporate",
        "corporates",
        "add-new/employee",
        "point-manager",
        "requests/top-up",
        "requests/vendor-withdraw",
        "requests/user-withdraw",
        "requests/vendor-top-up",
        "requests/vendor-top-up",
        "requests/vendor-withdraw",
        "distributed-point-manager",
        "expire-date-manager",
        "feature-settings",
        "cron-reports",
        "tree",
        "corporates",
        "requests/top-up",
        "requests/exchange-point",
        "requests/user-withdraw",


    ];
    let initial_pages = [
        "add-new/cs",
        "settings",
        "account-source",
        "document-source",
        "mapping",
        "handlers/system_designer",
        "activities",
        "profile",
    ];
    initial_pages = initial_pages.concat(branch_less_pages);

    $scope.non_requirement_pages = non_requirement_pages;
    $scope.public_pages = public_pages;
    $scope.initial_pages = initial_pages;
    $scope.branch_less_pages = branch_less_pages;

    $rootScope.document_switch = 0;
    $rootScope.shortcut_important_list_switch = 0;
    $scope.nav_switch = 1;
    $scope.main_header_switch = 0;
    $scope.main_menu_switch = 0;
    $scope.rootScope = $rootScope;
    // When nav template data loaded
    $scope.nav_init = function(){
        $scope.main_header_switch = 1;
    };
    // When main header / logo and branch selector template data loaded
    $scope.main_header_init = function(){
        $scope.main_menu_switch = 1;
    };
    // When main menu template data loaded
    $scope.main_menu_init = function(){
        // altair_main_header.init();
        altair_helpers.full_screen();
        $scope.basic_info_init();

        // console.log('menu init all ok')
    };
    $scope.login_init = function(){
        $location.path("/login");
    };

    $scope.basic_info_init = function(){
        $("#sidebar_main,#header_main,.message-box").removeClass("uk-hidden-small");
        $("#page_content").removeClass("left-margin-remove-small");
        /// When default information available asynchronously
        // Models.estimate_storage(function (space) {
        //     Models.smart_informer(1,"Net storage is "+space+" MB");
        // });
        // console.log(result);

        Models.get_basic_info($http,$rootScope,function (data) {
            let basic_info = data;
            OmbazarFn.id_types = basic_info.id_types;
            let logged_user_id = basic_info.logged_user_id;

            // let db_config = Models.db_init($rootScope.basic_info.tables_schema);
            $rootScope.logged_user_type = Models.logged_user_type($rootScope);
            // Get main menus data and initiate main menu
            $scope.menu_init = function(){
                $timeout(function () {
                    Models.active_menu($location);
                    if (Models.logged_user_id($rootScope)){
                        $rootScope.web_main_menu_switch = 1;
                    }
                    else{
                        $rootScope.web_main_menu_switch = 0;
                    }

                    altair_main_sidebar.init();

                });

            };
            $scope.menu_init();

            if ($rootScope.basic_info !== undefined) {
                if ($rootScope.basic_info.software_info !== undefined){
                    let software_info = $rootScope.basic_info.software_info;
                    $(".sidebar_logo img").attr('src',software_info.logo);
                    $rootScope.currency = software_info.currency;
                }
            }
            $rootScope.$on("main_menu_reload",function () {
                $scope.menu_init();
            });

            $scope.login_check();


        });

    };

    $scope.login_check = function(){
        ///public page
        let page_name = Models.page_name($location,[2]);
        let root_page_name = Models.page_name($location);

        for (let p of public_pages){
            if (page_name === p || p === root_page_name){
                $rootScope.logged = 1;
                return true;
            }
        }

        if(!$rootScope.basic_info.logged_user_id){
            // $location.path("/login");
            $rootScope.logged = 0;
            return false;
        }
        else{
            $rootScope.logged = 1;
            return true;
        }
    };

    $scope.page_controller = function(){
        let page = Models.page_name($location);
        let page_name = Models.page_name($location,[2]);
        Models.get_basic_info($http,$rootScope,function (basic_data) {
            // if(!$rootScope.basic_info.logged_user_id){
            //     $location.path("/login");
            // }


            let controller_type = Models.logged_user_type($rootScope);
            $scope.denied_page = function () {
                $rootScope.page_access = 0;
                $rootScope.denied_page_switch = 1;
                $rootScope.document_switch = 0;
                $rootScope.logged = 0;
                return false;
            };
            $scope.access_open = function () {
                $rootScope.page_access = 1;
                $rootScope.logged = 1;
                $rootScope.denied_page_switch = 0;
            };

            let logged_user_id = $rootScope.basic_info.logged_user_id;

            let public_page = $scope.public_pages.indexOf(page_name);
            let root_public_page = $scope.public_pages.indexOf(page);
            if (public_page !== -1){
                $scope.access_open();
                return false;
            }
            else if (root_public_page !== -1 ){
                $scope.access_open();
                return false;
            }

            if (controller_type === "cs"){
                return ;
            }
            else{
                let software_info = basic_data.software_info;
                if (software_info !== undefined){
                    let construction_mode = software_info.construction_mode;
                    if (construction_mode !== undefined){
                        let status = construction_mode.active;
                        if (status){
                            $rootScope.construction_time = construction_mode.date +" "+ construction_mode.time;
                            // console.log($rootScope.construction_time);
                            if (Date.parse($rootScope.construction_time) > Date.now()) {
                                $location.path('/construction-mode');
                                let logged_user = $rootScope.basic_info.logged_user;
                                if (logged_user!==undefined){
                                    let access_menus = logged_user.access_menus;
                                    if (access_menus !== undefined){
                                        $rootScope.basic_info.logged_user.access_menus = [];
                                        service.main_menu_reload();
                                    }
                                }
                            }

                        }
                        else{
                            $rootScope.construction_time = null;
                        }
                    }
                }
            }

            if (page === "profile"){
                $rootScope.logged = 1;
                return ;
            }

        });


    };
    $scope.sidebar_manage = function(){
        let window_width = $(window).width();
        if (window_width <= 1220){
            altair_main_sidebar.hide_sidebar();
        }
    };

    $scope.$on('$routeChangeStart', function($event, next, current) {
        $scope.page_controller();
        $scope.sidebar_manage();
        Models.page_title_generator($location,$scope,$rootScope);
        /// destroy the before initiate data table for error handle of invoice/document page
        // let table = $( '#dt_tableHeeshab' ).dataTable().api();
        // if (table.context.length){
        //     table.destroy();
        // }


    });
    $scope.$on('$routeChangeSuccess', function($event, next, current) {
        $("body").click();
        $timeout(function () {
            Models.active_menu($location,true);
        });
        $("#page_content").removeClass("left-margin-remove-small");

        $rootScope.search_switch = false;
        $rootScope.shortcut_important_list_switch = 0;
        $rootScope.document_switch = 0;



    });

}]);


// for prize details
app.controller("home",['$scope','$http','$timeout','$routeParams','$rootScope','$location',function ($scope,$http,$timeout,$routeParams,$rootScope,$location) {
    $rootScope.breadcrumbs = OmbazarFn.route_info($rootScope,$location);

}]);

// Page header to
app.controller("coupon_unlocker",['$scope','$http','$rootScope','service','$timeout','$location',function ($scope,$http,$rootScope,service,$timeout,$location) {
    $rootScope.breadcrumbs = OmbazarFn.route_info($rootScope,$location);
    let function_name = "slider_coupons()";
    let extra_data = OmbazarFn.retrive_request_object(function_name);
    let form_data = OmbazarFn.form_data_maker("",extra_data);
    OmbazarFn.$http($http,form_data,function (response,status) {
        $scope.sliders = response.find_data;
        $timeout(function () {
            OmbazarFn.init_requirement();
            $scope.not_found = "Currently not available!"
        });
    });
    $scope.my_coupon_list = [];
    $("body").on("click",".crach-bar",function () {
        let item = $(this).closest(".single-coupon");
        item.find(".coupon-bar").addClass("hidden");
        item.find(".mobile-number-bar").removeClass("hidden");
    });
    $("body").on("click",".coupon-next",function () {
        let item = $(this).closest(".single-coupon");
        item.find(".coupon-bar").addClass("hidden");
        item.find(".input-bar").removeClass("hidden");
    });
    $("body").on("click",".coupon-close",function () {
        let item = $(this).closest(".single-coupon");
        item.find(".coupon-bar").addClass("hidden");
        item.find(".crach-bar").removeClass("hidden");
    });
    $scope.unlock = function (es_id,event) {
        let target_elem = $(event.target);
        let item = target_elem.closest(".single-coupon");
        let coupon_number = item.find(".input-bar .coupon-input-field input").val();

        let this_form = item.find(".coupon-unlock");
        let function_name = "coupon_unlock(request)";
        let extra_data = OmbazarFn.passed_request_object(function_name);
            extra_data['es_id'] = es_id;
        let form_data = OmbazarFn.form_data_maker(this_form,extra_data);
        OmbazarFn.$http($http,form_data,function (response,status) {
            // console.log(response);
            if(response.status){
                let index_number = OmbazarFn.index_number($scope.my_coupon_list,"coupon_number",coupon_number);
                if (index_number !== undefined){
                    $scope.my_coupon_list.splice(index_number, 1);
                }
                if (typeof (response.expect_message) !== "undefined"){
                    item.find(".coupon-bar").addClass("hidden");
                    item.find(".coupon-message").removeClass("hidden").find(".used-text").html(response.expect_message);

                }
            }
        });
    };
    $scope.my_coupons = function (event) {
        OmbazarFn.get_my_coupons($http,$scope,function () {
        })
    };
    $scope.past_coupon = function(coupon_number,event){
        // console.log(coupon_number);
        let target = $(event.target);
        let parent = target.closest(".coupon-bar");
        let field = parent.find(".coupon-input-field").find("input");
        field.val(coupon_number);
        OmbazarFn.update_inputs();
    };
    $scope.paste_self_number = function(event){
        event.stopPropagation();
        // console.log(coupon_number);
        let target = $(event.target);
        let parent = target.closest(".coupon-bar");
        let field = parent.find(".coupon-input-field").find("input");
        let user_info = $rootScope.basic_info.logged_user;
        let mobile = user_info.calling_code + user_info.mobile;
        field.val(mobile);
        OmbazarFn.update_inputs();
    };

    /// SHow the prize information
    $("body").on("click",".prize-details",function () {
        let coupon_number = $(this).attr("data-coupon-number");
        let url ="/prize-details/"+coupon_number;
        $scope.$apply(function () {
            $location.path(url)
        })
    });

}]);

// for prize details
app.controller("prize_details",['$scope','$http','$timeout','$routeParams','$rootScope','$location',function ($scope,$http,$timeout,$routeParams,$rootScope,$location) {
    $rootScope.breadcrumbs = OmbazarFn.route_info($rootScope,$location);
    $scope.ombazar_logo = "/media/default/om-bazar-logo.png/";
    $scope.prize_image = "/media/default/prize.png";
    let coupon_number = $routeParams.coupon_number;
    let function_name = "prize_details('"+coupon_number+"')";
    let extra_data = OmbazarFn.retrive_request_object(function_name);
    let form_data = OmbazarFn.form_data_maker("",extra_data);
    OmbazarFn.$http($http,form_data,function (response,status) {
        $scope.prize_details = response;
        $timeout(function () {
            OmbazarFn.init_requirement();
        });
    });
}]);

// login
app.controller("login",['$scope','$http','$rootScope','service','$location','$route','$timeout',function ($scope,$http,$rootScope,service,$location,$route,$timeout) {
    let elements,body,page_content;
    $timeout(function () {
        elements = $("#header_main,#sidebar_main,#top_bar,.cart-page-content");
        body = $("body");
        page_content = $("#page_content");
        elements.addClass("uk-hidden");
        page_content.removeAttr("id");
        body.addClass("login_page login_page_v2");

        Models.init_requirement();
    });
    $rootScope.registration_complete = 0;
    Models.get_basic_info($http,$rootScope,function () {

        $timeout(function () {
            let container_total_height = $(".login_page_forms").outerHeight();
        let single_part = container_total_height / 5;
        let video_part = single_part * 4;

        let banner_video = Models.software_setting_by_key("login_banner_video",$rootScope);
        let login_banner = Models.software_setting_by_key("login_banner",$rootScope);


            $(".login_page_info .iframe").height(video_part);
            $(".login_page_info .iframe iframe").attr("src",banner_video);
            $(".login_page_info .login-banner-img").attr("src",login_banner).height(single_part);
        });

    });
    $scope.login = function () {
        let others = {
            form_name: ".login-form"
        };
        Models.request_sender($http,"get","login",function (data) {
            // console.log(data);
            if (data.status){

                $rootScope.basic_info = data.basic_info;
                $rootScope.main_menus = undefined;

                let logged_user = $rootScope.basic_info.logged_user;
                let member_id = logged_user.member_id;
                $scope.main_menu_switch = 0;
                service.main_menu_reload();
                $rootScope.logged_user_type = Models.logged_user_type($rootScope);


                elements.removeClass("uk-hidden");
                page_content.attr("id","page_content");
                body.removeClass("login_page login_page_v2");
                service.set_logged_user(logged_user);
                let url = "/profile";
                $location.path(url);
                altair_main_sidebar.init();



            }

        },['request'],others);
    }

}]);

/// Corporates
app.controller("corporates",['$scope','$http','$location','$timeout','$rootScope',function ($scope,$http,$location,$timeout,$rootScope) {
    $rootScope.breadcrumbs = OmbazarFn.route_info($rootScope,$location);
    $scope.collection_name = "corporates";
    $scope.data_query = {};
    $scope.search_name = "";
    $scope.search_location = "";
    $scope.search_tags = "";
    $scope.query_option = {};
    $scope.page_title = "Corporates";
    $scope.switch = 1;
    $scope.search = function () {
        $scope.data_query['company_name'] = {"$regex": ""+$scope.search_name};
        $scope.data_query['location'] = {"$regex": ""+$scope.search_location};
        $scope.data_query['tags'] = {"$regex": ""+$scope.search_tags};
        $rootScope.rows_load();
    };
    $timeout(function () {
        OmbazarFn.init_requirement();
    });

}]);

/// view Corporate
app.controller("corporate",['$scope','$http','$routeParams','$timeout','$rootScope','$location',function ($scope,$http,$routeParams,$timeout,$rootScope,$location) {
    $rootScope.breadcrumbs = OmbazarFn.route_info($rootScope,$location);
    OmbazarFn.init_requirement();
    let corporate_id = $routeParams.corporate_id;
    let function_name = "corporate('"+corporate_id+"')";
    let extra_data = OmbazarFn.retrive_request_object(function_name);
    let form_data = OmbazarFn.form_data_maker("",extra_data);
    OmbazarFn.$http($http,form_data,function (response,status) {
        $scope.corporate = response;
        $timeout(function () {
            // altair_md.init();
        })

    });
}]);

// Add a new controller
app.controller("add_controller",['$scope','$http','$location','$interpolate','$routeParams','$timeout','service','$rootScope',function ($scope,$http,$location,$interpolate,$routeParams,$timeout,service,$rootScope) {
    $rootScope.breadcrumbs = OmbazarFn.route_info($rootScope,$location);
    //page requirement
    $timeout(function () {
        OmbazarFn.init_requirement();
        Models.remove_page_content();
    });
    $scope.name = '';
    $scope.add_new_label = '';
    let controller_type = 'user';
    $scope.controller_type = controller_type;

    //check controller type and fix add new form information
    // if(controller_type === 'admin' ){
    //     $scope.add_new_label = "Add new admin";
    //     $scope.name = "Admin name";
    // }

    if(controller_type === 'user' ){
        $scope.add_new_label = "New registration";
        $scope.name = "User name";
    }

    OmbazarFn.countries_init($http,function () {
        // Any event fire after success
    });
    $scope.default_image = default_dir+"profile.png";

    $scope.submit = function () {
        //add an admin
        let function_name = "add_controller(request,'user')";
        let extra_data = OmbazarFn.passed_request_object(function_name);
            extra_data['nationality'] = JSON.stringify(OmbazarFn.selected_country);
            // console.log(extra_data);
        let form_data = OmbazarFn.form_data_maker(".add-controller",extra_data);
        OmbazarFn.$http($http,form_data,function (response,status) {
            if(response.status){
                $rootScope.registration_complete = 1;
                let user_name = response.user_name;
                let url = "/profile/"+user_name;
                OmbazarFn.add_action(response,$location,url);
            }
        });
    };

}]);

// Profile page view using this controller
app.controller("profile",['$scope','$http','$location','$routeParams','$rootScope','service','$timeout',function ($scope,$http,$location,$routeParams,$rootScope,service,$timeout) {
    $rootScope.breadcrumbs = OmbazarFn.route_info($rootScope,$location);
    //page requirement
    $scope.top_up_switch = 0;
    $scope.exchange_point_switch = 0;
    $scope.withdraw_switch = 0;
    $scope.balance_action = 0;
    // Pending switch
    $scope.top_up_pending_switch = 0;
    $scope.withdraw_pending_switch = 0;

    OmbazarFn.get_basic_info($rootScope,service,function () {
        let user_name = $routeParams.user_name;
        if (user_name === undefined){
            $scope.balance_action = 1;
        }
        else{
            $scope.balance_action = 0;
        }
        let function_name = '';
        if(user_name !== undefined){
            function_name = "controller_info('"+user_name+"',True)";
        }
        else{
            function_name = "logged_controller(request)";
        }

        let extra_data = OmbazarFn.retrive_request_object(function_name);
        let form_data = OmbazarFn.form_data_maker("",extra_data);
        OmbazarFn.$http($http,form_data,function (response,status) {
            // console.log(response);
            if (response.pending !== undefined){
                // Top up pending possibility
                let top_up_pending = OmbazarFn.custom_filter(response.pending,"type","top_up",true);
                if (top_up_pending !== undefined){
                    if (top_up_pending.length){
                        $scope.top_up_pending_switch = 1;
                    }
                }
                // Withdraw pending possibility
                let withdraw_pending = OmbazarFn.custom_filter(response.pending,"type","user_withdraw",true);
                if (withdraw_pending !== undefined){
                    if (withdraw_pending.length){
                        $scope.withdraw_pending_switch = 1;
                    }
                }

            }
            altair_md.fab_speed_dial($scope);
            $scope.user = response;
            if(response.user_id === undefined){
                $rootScope.error_page();
            }
            $scope.tree_info = $rootScope.basic_info.tree_info;
            $timeout(function () {
                OmbazarFn.init_requirement();
            });

        });
        // exchange point
        $scope.exchange_point = function () {
            function_name = "point_exchange(request)";
            let extra_data = OmbazarFn.passed_request_object(function_name);
            let form_data = OmbazarFn.form_data_maker(".point-exchange",extra_data);
            OmbazarFn.$http($http,form_data,function (response,status) {
                if (response.status){
                    OmbazarFn.notify("Balance added");
                    $scope.tree_info.account_balance = response.account_balance;
                    $scope.tree_info.point_balance = response.point_balance;
                    OmbazarFn.clean("form");
                }
            });
        };

        // Top up
        $scope.top_up = function () {
            function_name = "top_up(request)";
            let extra_data = OmbazarFn.passed_request_object(function_name);
            let form_data = OmbazarFn.form_data_maker(".top-up",extra_data);
            OmbazarFn.$http($http,form_data,function (response,status) {
                if (response.status){
                    OmbazarFn.notify("Request send.");
                    OmbazarFn.clean("form");
                    $scope.top_up_pending_switch = 1;
                }
            });
        };
        // with draw
        $scope.withdraw = function () {
            function_name = "withdraw(request,'user_withdraw')";
            let extra_data = OmbazarFn.passed_request_object(function_name);
            let form_data = OmbazarFn.form_data_maker(".withdraw",extra_data);
            OmbazarFn.$http($http,form_data,function (response,status) {
                if (response.status){
                    OmbazarFn.notify("Request send.");
                    OmbazarFn.clean("form");
                    $scope.withdraw_pending_switch = 1;
                }
            });
        };

        $scope.open_top_up = function () {
            $scope.fab_close();
            $scope.top_up_switch = 1;
        };
        $scope.open_exchange = function () {
            $scope.fab_close();
            $scope.exchange_point_switch = 1;
        };
        $scope.open_withdraw = function () {
            $scope.fab_close();
            $scope.withdraw_switch = 1;
        };

        $scope.fab_close = function () {
            $scope.top_up_switch = 0;
            $scope.exchange_point_switch = 0;
            $scope.withdraw_switch = 0;
        };
        $("body").on("click",".md-fab-action-close",function () {
            $scope.$apply(function () {
               $scope.fab_close();
            });
        });
    });


}]);
// Logout using this controller
app.controller("logout",['$scope','$http','$location','$rootScope','service',function ($scope,$http,$location,$rootScope,service) {
    //page requirement
    let function_name = "logout(request)";

    let extra_data = OmbazarFn.common_request_object(function_name);
    let form_data = OmbazarFn.form_data_maker("",extra_data);
    OmbazarFn.$http($http,form_data,function (response,status) {
        if(response.logout !== undefined){
            $rootScope.basic_info.logged_user = {};
            $rootScope.basic_info.logged_user_id = 0;
            service.set_logged_user();
            let url = "/login";
            $location.path(url);
        }
        else{
            console.log(response);
        }

    });
}]);

// cart controller
app.controller("cart",['$scope','$http','$rootScope','service','$timeout','$location',function ($scope,$http,$rootScope,service,$timeout,$location) {
    OmbazarFn.get_basic_info($rootScope,service,function () {
        $timeout(function () {
            $("#sidebar_main").next("#page_content").css({"margin-top":"49px"});
        });

        $scope.products = $rootScope.products;

        service.set_cart();
        let before_item = [];
        let cookies = Cookies.getJSON();
        for (let x in cookies){
            if(x !== 'csrftoken'){
                before_item.push(x);
            }
        }
        let c_query = {
            "store_item_id": {"$in": before_item}
        };
        let parameters = ["request"];
        OmbazarFn.retrieve_data($http,"cart_items",c_query,function (response,status) {
           let finds = response.find_data;
           for (let x in finds){
               let target = finds[x];
               let store_item_id = target.store_item_id;
               let cart_ob = cookies[store_item_id];
               target.quantity = cart_ob.quantity;
               let point = OmbazarFn.point_maker($rootScope,target);
               target.point = point;
               $rootScope.cart.push(target);

           }
           $timeout(function () {
               $rootScope.global_cart_switch = 1;
           });


           $scope.calculate();
        },{},true,parameters);

        $rootScope.add_to_cart = function(store_item_id,$event=0){
            if ($event){
                $event.stopImmediatePropagation();
            }
            $scope.products = $rootScope.products;

            OmbazarFn.add_to_cart($rootScope,$scope, store_item_id);
            // console.log($scope.products);
        };
        $rootScope.delete_cart_item = function($index,store_item_id){
            $rootScope.cart.splice($index, 1);
            Cookies.remove(store_item_id);
            $scope.calculate();
        };
        $rootScope.left_to_cart = function(store_item_id,$event){
            $event.stopImmediatePropagation();
            let index = OmbazarFn.index_number($rootScope.cart,"store_item_id",store_item_id);
            let item = $rootScope.cart[index];
            let before_quantity = item.quantity;
            let net_quantity = Number(before_quantity)-1;
                if(net_quantity){
                    OmbazarFn.add_to_cart($rootScope,$scope, store_item_id,net_quantity,true);
                }
                else{
                    $rootScope.delete_cart_item(index,store_item_id);
                }

        };

        $scope.sub_total = 0;
        $scope.vat = 0;
        $scope.discount = 0;
        $scope.grand_total = 0;
        $scope.total_discount = 0;
        $scope.total_item = 0;
        $scope.calculate = function(){
            let cart_items = $rootScope.cart;
            let total_info = OmbazarFn.order_total_info(cart_items,$scope.discount);
            $scope.sub_total = total_info.sub_total;
            $scope.vat = total_info.vat;
            $scope.point = total_info.point;
            $scope.grand_total = total_info.grand_total;
            $scope.total_discount = total_info.total_discount;
            $scope.total_item = total_info.total_item;
            $('.odometer').html($scope.grand_total);
        };
        $scope.clear_cart = function(){
            let cookies = Cookies.getJSON();
            for (let x in cookies){
                if(x !== 'csrftoken'){
                    Cookies.remove(x);
                }
            }
            $rootScope.cart = [];
            $scope.calculate();
        };
        $("body").on("change keyup",".quantity",function () {
            let quantity = Number($(this).val());
            let store_item_id = $(this).closest("tr").attr("data-id");

            $scope.$apply(function () {
                $scope.products = $rootScope.products;
                OmbazarFn.add_to_cart($rootScope,$scope, store_item_id,quantity,true);
            });
        });
        $("body").on("keyup",".cart-discount-field",function () {

            let discount = Number($(this).val());
            $scope.$apply(function () {
                $scope.discount = discount;
                $scope.calculate();
            });
        });
        $scope.order = function(){
            let data = JSON.stringify($rootScope.cart);
            let method = "order(request)";
            let extra_data = OmbazarFn.passed_request_object(method);
                extra_data['order_type'] = "web";
                extra_data['discount'] = $scope.discount;
                extra_data['data'] = data;
                extra_data['customer_id'] = $rootScope.basic_info.logged_user_id;
            let form_data = OmbazarFn.form_data_maker("", extra_data);
            OmbazarFn.$http($http,form_data,function (response,status) {

                if(response.status){
                    let order_id = response.order_id;
                    let url = "/order/"+order_id;
                    $scope.discount = 0;
                    $scope.clear_cart();
                    $location.path(url);
                    $rootScope.bag_switch();
                }
            })
        };
        $timeout(function () {
            OmbazarFn.init_requirement();
        },100);
    });


}]);


/// Pos for sell
app.controller("products",['$scope','$http','$location','$timeout','$rootScope','service','$routeParams',function ($scope,$http,$location,$timeout,$rootScope,service,$routeParams) {
    $rootScope.breadcrumbs = OmbazarFn.route_info($rootScope,$location);
    $timeout(function () {
        Models.remove_page_content();
    });
    Models.get_basic_info($http,$rootScope,function () {
        $scope.limit = '10';
        $scope.total = 0;
        $scope.skip = 0;
        $scope.collection_name = "products";
        $scope.data_query = {};
        $scope.query_option = {};
        $scope.page_title = "Pos";
        $scope.switch = 1;
        $scope.rows = [];
        // $scope.products = $scope.rows;
        $scope.custom_function = {
            function_name: "products",
            parameteres: ['request'],
        };
        OmbazarFn.get_categories($http,function (categories) {
            let category_name = $routeParams.category_name;
            // For brand
            let brand_id = $routeParams.brand_id;
            if (category_name!==undefined){
                let category_info = OmbazarFn.custom_filter(categories,"category_name",category_name);
                let category_id = category_info.category_id;
                if(category_id !== undefined){
                    $timeout(function () {
                        // $scope.rows = [];
                    });

                    $scope.data_query['category_id'] = category_id;
                }
                else{
                    // console.log('category not found');
                }

            }
            if (brand_id !== undefined){
                $scope.data_query.store_id = brand_id;
            }
            let logged_user = $rootScope.basic_info.logged_user;
            if (logged_user !== undefined){
                $scope.query_option['post_code'] = logged_user.post_code;
            }

            $scope.pos_load = function(event=function () {}){
                OmbazarFn.request_data_load($http,$scope,$rootScope,$timeout,function (response,status) {


                    for (let item in $scope.rows){
                        let item_info = new Object();
                        Object.assign(item_info,$scope.rows[item]);
                        let point = OmbazarFn.point_maker($rootScope,item_info,1);
                        $scope.rows[item].point = point;
                    }
                        $timeout(function () {
                            $rootScope.products = $scope.rows;
                            service.set_products();
                        });

                        // console.log($scope.products);
                        event(response,status);
                        altair_helpers.hierarchical_show();
                });
            };
            OmbazarFn.get_basic_info($rootScope,service,function () {
                OmbazarFn.total_counter($http,$scope,function (response, status) {
                    $scope.total = response.total;
                    $scope.pos_load()
                });
            });

            var element=window;
                $(element).on("scroll",function(e){
                    var scrollTop=$(this).scrollTop();
                    var element_offset_top=$(document).height()-$(window).height();
                    if(scrollTop==element_offset_top){
                        $scope.$apply(function () {
                            $scope.pos_load()
                        });
                    }
                });
            $scope.search = function(){
                // console.log($scope.search_name);
                let search_name = $scope.search_name;
                $scope.rows = [];
                $scope.skip = 0;
                // $scope.data_query = {};
                $scope.data_query['item_name'] = {"$regex": $scope.search_name};
                OmbazarFn.request_data_load($http,$scope,$rootScope,$timeout,function (response,status) {
                    console.log(response);
                    altair_helpers.hierarchical_show();
                });


            };
            $rootScope.product_search = function () {
                // console.log("df");
                // $location.path("/products");
                $scope.search();
            }
        });
    });


}]);

/// view order
app.controller("order",['$scope','$http','$routeParams','$timeout','$rootScope','$location',function ($scope,$http,$routeParams,$timeout,$rootScope,$location) {
    $rootScope.breadcrumbs = OmbazarFn.route_info($rootScope,$location);
    OmbazarFn.init_requirement();
    let order_id = $routeParams.order_id;
    let function_name = "order(request,'"+order_id+"')";
    let extra_data = OmbazarFn.retrive_request_object(function_name);
    let form_data = OmbazarFn.form_data_maker("",extra_data);
    OmbazarFn.$http($http,form_data,function (response,status) {
        // console.log(order_id);
        if (response.find_data.length){
            let order = response.find_data[0];
            $scope.order = order;

            order = OmbazarFn.order_info_filter(order,$rootScope);
            // console.log(order);
            $timeout(function () {
                // altair_md.init();
            });
            OmbazarFn.order_status_selectize(order.status,1);
        }
        else{
            console.log(response);
        }

    });
}]);

/// Corporates
app.controller("orders",['$scope','$http','$location','$timeout','$rootScope','service',function ($scope,$http,$location,$timeout,$rootScope,service) {
    $rootScope.breadcrumbs = OmbazarFn.route_info($rootScope,$location);
    OmbazarFn.get_basic_info($rootScope,service,function () {
        $scope.page_title = "Order list";
        $scope.collection_name = "orders";
        $scope.data_query = {};
        $scope.query_option = {};
        $scope.switch = 1;
        $scope.custom_function = {
            function_name: "orders",
            parameteres: ['request']
        };
    });
}]);

/// view item using store item id
app.controller("item",['$scope','$http','$routeParams','$rootScope','$location','service',function ($scope,$http,$routeParams,$rootScope,$location,service) {
    $rootScope.breadcrumbs = OmbazarFn.route_info($rootScope,$location);
    $rootScope.cart_button_switch = 0;
    OmbazarFn.get_cart($scope,$rootScope,service,function () {
        OmbazarFn.init_requirement();
        let item_id = $routeParams.store_item_id;
        let function_name = "store_item(request,'"+item_id+"')";
        let extra_data = OmbazarFn.retrive_request_object(function_name);
        let form_data = OmbazarFn.form_data_maker("",extra_data);
        OmbazarFn.$http($http,form_data,function (response,status) {
            $scope.item = response;
            let store_item_id = $scope.item.store_item_id;
            let cookie = Cookies.get(store_item_id);
            if (cookie !== undefined){
                $rootScope.cart_button_switch = 0;
            }
            else{
                $rootScope.cart_button_switch = 1;
            }

            console.log(response);
            let parent_id = response.category_id;
            $scope.item = response;
            $scope.item.parent_name = "Uncategorized";
            if(!parent_id || parent_id!=='0'){
                if(OmbazarFn.categories.length){
                    let categories = OmbazarFn.categories;
                    let parent = OmbazarFn.custom_filter(categories,"category_id",parent_id);
                    $scope.item.parent_name = parent.category_name;

                }
                else{
                    let function_name = "category('"+parent_id+"')";
                    let extra_data = OmbazarFn.retrive_request_object(function_name);
                    let form_data = OmbazarFn.form_data_maker("",extra_data);
                    OmbazarFn.$http($http,form_data,function (response) {
                        let category_name = response.category_name;
                        $scope.item.parent_name = category_name;
                    })
                }
            }
        });
        $scope.add_to_cart = function(){
            let item_info = OmbazarFn.item_info_for_cart($scope.item,1);
            OmbazarFn.item_in_cart(item_info,$scope,$rootScope);
            $rootScope.cart_button_switch = 0;
        };

        // console.log($rootScope.cart);



    });
    

}]);



/// add category
app.controller("categories",['$scope','$http','$timeout','$location','service',function ($scope,$http,$timeout,$location,service) {
    // Access category fancy tree initiate
    OmbazarFn.access_category_init($http,$scope,$timeout,function () {
        /// any event call when access catgory initiate

    });

}]);
app.directive("subCategories",function () {
    let template  = ''+
            '<ul ng-if="category.categories.length>0">\n' +
            '<li id="((category.category_id))" ng-repeat="category in category.categories " >\n' +
            '<a href="products/((category.category_name))">\n'+
            '<span class="menu_title">((category.category_name))</span>\n'+
            '</a>\n'+
            '<!-- directive: sub-categories -->' +
            '</li>\n' +
            '</ul>\n';
    return {
        restrict : "M",
        replace : true,
        template : template
    };
});

/// Tree
app.controller("tree",['$scope','$http','$routeParams','$route','$rootScope','service','$timeout','$location',function ($scope,$http,$routeParams,$route,$rootScope,service,$timeout,$location) {
    $rootScope.breadcrumbs = OmbazarFn.route_info($rootScope,$location);
    $scope.exchange = true;
    OmbazarFn.get_basic_info($rootScope,service,function () {
        OmbazarFn.init_requirement();
        $scope.self_tree_info = $rootScope.basic_info.tree_info;
        $scope.top_tree_info = new Object();
        Object.assign($scope.top_tree_info,$scope.self_tree_info);
        $scope.more_data = {
            "limit": 0,
            "status": "Paid"
        };
        let user_id = $routeParams.tree_user_id;
        // For when select any other user tree view

        if (user_id !== undefined){
            $scope.selected_tree_info = $rootScope.selected_tree_info[user_id];
            if ($scope.selected_tree_info === undefined){
                $location.path("/tree");
                return false;
            }
            $scope.more_data.parent_id = user_id;
            $scope.top_tree_info = $scope.selected_tree_info;
        }
        else{
            user_id = $scope.top_tree_info.user_id;
            $rootScope.selected_tree_info = {};
        }

        // console.log(user_id);
        let free_tree,main_tree = undefined;
        $scope.load_tree = function(){
            OmbazarFn.get_tree($http,$scope.top_tree_info,function (tree,response) {
                $scope.member_tree_view = function(member_id){
                    let tree_data = response.tree_data.find_data;
                    let selected_user = OmbazarFn.custom_filter(tree_data,"user_id",member_id);
                    $rootScope.selected_tree_info[member_id] = selected_user;
                };
                let data = response.exchange_child_count.total;
                if (data>2){
                    $scope.exchange = false;
                }
                if ($rootScope.basic_info.logged_user_id !== $scope.top_tree_info.user_id){
                    tree.child = 1;
                }
                $scope.tree = tree;
                $timeout(function(){
                    main_tree = OmbazarFn.tree(".network",{
                        'class_name': 'network',
                        'exchange_option': $scope.exchange,
                        'drag_disabled': true,
                        'drag_start': function (e) {
                            // console.log(e);
                        },
                        'before_drop': function (this_element,tree,ui,event=function () {}) {
                            let dropped_parent = $(this_element).closest(".tree");
                            let draggable_item_amount = ui.draggable.closest("li").children("ul").children("li").length;
                            let draggable_item = ui.draggable.closest("li");
                            let draggable_item_id = draggable_item.attr("id");
                            let available_child=$(this_element).closest("li").children("ul").children("li").length;

                            if ($(this_element).hasClass('check_div')) {
                                OmbazarFn.notify('Cant Move on Child Element.',"top-center",2000);
                                event(false);
                            }
                            else if(available_child>=2 && !dropped_parent.hasClass("free-tree")){
                                OmbazarFn.notify('Tow hand close.',"top-center",2000);
                                event(false);
                            }
                            else if(draggable_item_amount>0){
                                OmbazarFn.notify("Group not allow! chose a single person");
                                event(false);
                            }
                            else if(draggable_item_id===$rootScope.basic_info.logged_user_id){
                                OmbazarFn.notify("Not adjustable!");
                                event(false);
                            }

                            else{

                                let dropped_element = $(this_element).closest("li");
                                let dropped_element_user_id = dropped_element.attr("id");
                                let drooped_user_info = OmbazarFn.custom_filter(response.tree_data.find_data,'user_id',dropped_element_user_id);
                                if (drooped_user_info !== undefined){
                                    let total_child = drooped_user_info.total_child;
                                    if (total_child >= 2){
                                        OmbazarFn.notify('Members already exist!',"top-center",2000);
                                        event(false);
                                    }
                                    else{
                                        let new_child = $(ui.draggable).closest("li").attr('id');
                                        let up_line_referer = dropped_element.attr('id');
                                        // console.log(up_line_referer);
                                        let referer = $rootScope.basic_info.logged_user_id;
                                        let available_child=$(this_element).closest("li").children("ul").children("li").length;
                                        let position = "Left";
                                        if (available_child ===1){
                                            position = "Right";
                                        }
                                        let new_position_object = {
                                            new_child:new_child,
                                            up_line_referer: up_line_referer,
                                            referer: referer,
                                            position: position,
                                        };
                                        OmbazarFn.set_tree_position($http,new_position_object,function (response,status) {
                                            // console.log(response);
                                            if (response.status){
                                                event(true);
                                            }
                                            else{
                                                event(false);
                                            }
                                        });
                                    }
                                }
                                else{
                                    console.log("Up line referer info not found");
                                }

                            }
                        },
                        'drop': function (element,tree,ui) {
                            if (free_tree !== undefined){
                                free_tree[0].call_structure();
                            }
                            let available_child=$(element).closest("li").children("ul").children("li").length;
                            // console.log(available_child);
                            let new_item_id = $(ui.draggable).closest("li").attr('id');
                            let new_item = $(".network #"+new_item_id);
                            if ($scope.free_tree !== undefined){
                                let members = $scope.free_tree[0].tree;
                                if (members !== undefined){
                                    let new_member = OmbazarFn.custom_filter(members,'user_id',new_item_id);
                                        new_member.status = "Paid";
                                        let position = "Left";
                                        if (available_child === 2){
                                            position = "Right";
                                        }
                                        new_member.position = position;
                                    let user_tooltip = OmbazarFn.user_tooltip(new_member);
                                    new_item.children(".tree-user").children("a").attr("title",user_tooltip).attr("data-position",position)
                                    .attr("href","tree/"+new_item_id);
                                }
                            }
                        },
                        on_exchange:function (this_parent) {
                            let childes = this_parent.children("ul").children("li");
                            let first_user_id = $(childes[0]).attr("id");
                            let second_user_id = $(childes[1]).attr("id");
                            if(childes.length === 2){
                                let update_list = [
                                    {
                                        "user_id": first_user_id,
                                        "position": "Left"
                                    },
                                    {
                                        "user_id": second_user_id,
                                        "position": "Right"
                                    }
                                ];
                                let data_object = {
                                    "data": JSON.stringify(update_list)
                                };
                                OmbazarFn.exchange_position($http,data_object,function (response) {
                                    $scope.route_reload();
                                });
                            }
                        }
                    });

                });
            },$scope.more_data);
        };

        $scope.load_tree();

        $scope.load_free_tree = function () {
            $scope.more_data.status = "Free";
            $scope.more_data.limit = 0;
            OmbazarFn.get_tree($http,$scope.self_tree_info,function (tree,response) {
                $scope.free_tree = tree;
                $timeout(function(){
                    free_tree = OmbazarFn.tree(".free-tree",{
                        'class_name': 'free-tree',
                        'exchange_option': false,
                        'edit_option':true,
                        before_edit: function (that,e) {
                            let item = $(that).closest("li");
                            let item_id = item.attr("id");
                            let item_info = OmbazarFn.custom_filter(response.find_data,'user_id',item_id);
                            let form = item.children(".tree-user").find("form");
                            if (item_info !== undefined){
                                let position = item_info.position;
                                form.children("select").val(position);
                            }
                            form.children("select").change(function () {
                                let value = $(this).val();
                                let where = {
                                    "user_id": item_id
                                };
                                let which = {
                                    "position": value
                                };
                                OmbazarFn.document_update($http,"tree",where,which,function (response) {
                                    if (response.status){
                                        item_info.position = value;
                                    }
                                });

                            });
                        }
                    });
                    $scope.control_free_tree = function () {
                        $(".tree-area").toggleClass("tree-overflow");
                        free_tree[0].call_structure();
                        main_tree[0].call_structure();
                    };
                });
            },$scope.more_data,false);
        };
        $scope.load_free_tree();


    });
}]);


app.directive("stalks",function () {
    let not_condition = 'stalk.status !== \"Paid\" ';
    let condition = 'stalk.status == \"Paid\" ';
    let template  = ""+
            "<ul>\n" +
            "<li class='((stalk.child | if : \"child\"))' id='((stalk.user_id))' ng-repeat='stalk in stalk.tree' > \n" +
            "<div class='tree-user'> "+
            "<a ng-click='member_tree_view( ((stalk.user_id)) )' ng-if='"+condition+"' class='' data-position='((stalk.position))' href='tree/((stalk.user_info.user_id))' data-uk-tooltip='{pos:\'right\'}' title='((stalk | user_tooltip))'>\n"+
            "<img src='((stalk.user_info.image))'>"+
            "<span>((stalk.user_info.name))</span>\n"+
            "</a>\n"+
            ""+
            "<a ng-if='"+not_condition+"' class='' data-position='((stalk.position))'  data-uk-tooltip='{pos:'right'}' title='((stalk | user_tooltip))'>\n"+
            "<img src='((stalk.user_info.image))'>"+
            "<span>((stalk.user_info.name))</span>\n"+
            "</a>\n"+
            "</div>"+
            "<!-- directive: stalks -->" +
            "</li>\n" +
            "</ul>\n";
    return {
        restrict : "M",
        replace : true,
        template : template
    };
});

/// forgot password
app.controller("forgot_password",['$scope','$http','$routeParams','$rootScope','$location','$timeout',function ($scope,$http,$routeParams,$rootScope,$location,$timeout) {
    $rootScope.breadcrumbs = OmbazarFn.route_info($rootScope,$location);
    $scope.all_off = function(){
        $scope.find_form_switch = 0;
        $scope.confirmation_switch = 0;
        $scope.find_list_switch = 0;
        $scope.new_password_switch = 0;
        $scope.not_found_switch = 0;
    };
    $scope.all_off();
    $scope.find_form_switch = 1;

    $timeout(function () {
        OmbazarFn.init_requirement();
        Models.remove_page_content();
    });


    $scope.find_forgots = function () {
        let function_name = "find_users(request)";
        let extra_data = OmbazarFn.retrive_request_object(function_name);
        let form_data = OmbazarFn.form_data_maker(".find-form",extra_data);
        OmbazarFn.$http($http,form_data,function (response,status) {
            if (response.find_data !== undefined){
                if (response.find_data.length){
                    $scope.find_list = response.find_data;
                    $scope.all_off();
                    $scope.find_list_switch = 1;
                }
                else {
                    $scope.all_off();
                    $scope.not_found_switch = 1;
                }
            }
        })
    };
    $scope.user_select = function (user_id, user_email, mobile) {
        let function_name = "send_code_forgot_user(request)";
        let extra_data = OmbazarFn.passed_request_object(function_name);
            extra_data['email'] = user_email;
            extra_data['user_id'] = user_id;
            extra_data['mobile'] = mobile;
        let form_data = OmbazarFn.form_data_maker("",extra_data);
        OmbazarFn.$http($http,form_data,function (response,status) {
            if(response.status){
                $scope.code_sender_email = user_email;
                $scope.all_off();
                $scope.confirmation_switch = 1;
                $timeout(function () {
                    OmbazarFn.init_requirement();
                })
            }
        })
    };
    $scope.confirm_code = function () {
        let function_name = "confirm_code(request)";
        let extra_data = OmbazarFn.passed_request_object(function_name);
        let form_data = OmbazarFn.form_data_maker(".confirm-form",extra_data);
        OmbazarFn.$http($http,form_data,function (response,status) {
            if (response.status){
                $scope.all_off();
                $scope.new_password_switch = 1;
            }
            $timeout(function () {
                OmbazarFn.init_requirement();
            })
        })
    };
    $scope.new_password = function () {
        let function_name = "new_password(request)";
        let extra_data = OmbazarFn.modify_request_object(function_name);
        let form_data = OmbazarFn.form_data_maker(".new-password-form",extra_data);
        OmbazarFn.$http($http,form_data,function (response,status) {
            if (response.status){
                $scope.all_off();
                $scope.done_switch = 1;
            }
            $timeout(function () {
                OmbazarFn.init_requirement();
            })
        })
    };
    $scope.back_to_find = function () {
        $scope.all_off();
        $scope.find_form_switch = 1;
        $timeout(function () {
            OmbazarFn.init_requirement();
        })
    }

}]);
// Requests
app.controller("requests",['$scope','$http','$location','$routeParams','$timeout','$rootScope','service',function ($scope,$http,$location,$routeParams,$timeout,$rootScope,service) {
    $rootScope.breadcrumbs = OmbazarFn.route_info($rootScope,$location);
    OmbazarFn.get_basic_info($rootScope,service,function () {
        $scope.user_type = OmbazarFn.logged_user_type($rootScope);
        let request_type = $routeParams.request_type;
        $scope.page_title = "";
        $scope.type = "";
        if (request_type === "top-up"){
            $scope.page_title = "Top up requests";
            $scope.type = "top_up";
        }
        else if(request_type === "user-withdraw"){
            $scope.page_title = "Withdraws";
            $scope.type = "user_withdraw";
        }
        else if(request_type === "exchange-point"){
            $scope.page_title = "Exchange point";
            $scope.type = "point_exchange";
        }

        $scope.collection_name = "transactions";
        $scope.data_query = {
            "type": $scope.type,
            "user_id": $rootScope.basic_info.logged_user_id
        };
        $scope.query_option = {};
        $scope.switch = 1;
        $scope.custom_function = {
            function_name: "transactions",
            parameteres: ['request']
        };
    });


}]);

app.controller("edit_controller",['$scope','$http','$location','$interpolate','$routeParams','$timeout','service','$rootScope',function ($scope,$http,$location,$interpolate,$routeParams,$timeout,service,$rootScope) {
    $rootScope.breadcrumbs = OmbazarFn.route_info($rootScope,$location);
    //page requirement
    OmbazarFn.get_basic_info($rootScope,service,function () {
        // for access stores
        $scope.stores_init = 1;
        $scope.edit_label = "Edit profile";
        $scope.controller_type = "user";
        //check controller type and fix add new form information

        let user_id = $rootScope.basic_info.logged_user_id;
        let function_name = "controller_info('"+user_id+"')";
        let extra_data = OmbazarFn.retrive_request_object(function_name);
        let form_data = OmbazarFn.form_data_maker("",extra_data);
        OmbazarFn.$http($http,form_data,function (response,status) {
            $scope.controller = response;
            $scope.name = $scope.controller.name;
            $timeout(function () {
                OmbazarFn.init_requirement();
                OmbazarFn.countries_init($http,function (selectize) {
                    // Any event fire after success
                    let instance = selectize;
                    instance.setValue($scope.controller.calling_code);
                });

            });
        });
    });

    $scope.submit = function () {
        //edit controller
        function_name = "edit_controller(request)";
        extra_data = OmbazarFn.modify_request_object(function_name);
        extra_data['nationality'] = JSON.stringify(OmbazarFn.selected_country);
        extra_data['user_id'] = $scope.controller.user_id;
        extra_data['before_password'] = $scope.controller.password;
        // extra_data['password'] = "open1234";
        extra_data['before_image'] = $scope.controller.image;
        extra_data['edit'] = 1;

        form_data = OmbazarFn.form_data_maker(".edit-controller",extra_data);
        OmbazarFn.$http($http,form_data,function (response,status) {
            if(response.status){
                let url = "/profile/"+$scope.controller.user_name;
                OmbazarFn.add_action(response,$location,url);
            }
        });

    }
}]);


/// Prize manager
app.controller("buy_coupon",['$scope','$http','$location','$timeout','$rootScope','$compile','$interpolate','service',function ($scope,$http,$location,$timeout,$rootScope,$compile,$interpolate,service) {
    OmbazarFn.get_basic_info($rootScope,service,function () {
        OmbazarFn.init_requirement();
        $scope.collection_name = "my_coupons";
        $scope.data_query = {};
        $scope.query_option = {
            "user_id": $rootScope.basic_info.user_id
        };
        $scope.custom_function = {
            function_name: "my_coupons",
            parameteres: ['request']
        };
        $scope.page_title = "Buy coupon";
        $scope.switch = 1;
        $scope.submit = function(){
            let function_name = "buy_coupon(request)";
            let extra_data = OmbazarFn.passed_request_object(function_name);
            let form_data = OmbazarFn.form_data_maker(".buy-coupon",extra_data);

            OmbazarFn.$http($http,form_data,function (response,status) {
                // console.log(response);
                if(response.status){
                    OmbazarFn.add_action(response,$location);
                    $rootScope.rows_load();
                }
            });
        };
    });
}]);


/// Brand manager
app.controller("brand_slides",['$scope','$http','$location','$timeout','$rootScope',function ($scope,$http,$location,$timeout,$rootScope) {
    $scope.brands_init = 0;
    // $scope.brand_slides = [];
    let loading_elem = ".brand-icon";
    $scope.slide = function () {

        if ($scope.brands_init){
            $scope.brands_init = 0;
        }
        else{
            OmbazarFn.small_loading(loading_elem);
            OmbazarFn.get_brand_slides($http,$scope,function (data) {
                $scope.brands_init = 1;
            });

        }

    };
    $scope.brandOnLoad = function(){
        OmbazarFn.small_loading(loading_elem,0,"<i class=\"material-icons\">star</i>");
    };

}]);

/// Brand manager
app.controller("brand",['$scope','$http','$location','$timeout','$rootScope','$routeParams',function ($scope,$http,$location,$timeout,$rootScope,$routeParams) {
    let brand_id = $routeParams.brand_id;
    let function_name = "store('"+brand_id+"')";
    let extra_data = OmbazarFn.retrive_request_object(function_name);
    let form_data = OmbazarFn.form_data_maker("",extra_data);
    OmbazarFn.$http($http,form_data,function (response,status) {
        $scope.brand = response;
    });
}]);



app.controller("data_table_handler",['$scope','$http','$timeout','$rootScope',function ($scope,$http,$timeout,$rootScope) {

    $scope.limit = '10';
    $scope.filtered = 0;
    $scope.start_row = 0;
    $scope.total = 0;
    $scope.page = '1';
    $scope.prev_switch = 1;
    $scope.next_switch = 1;

    OmbazarFn.total_counter($http,$scope,function (response, status) {
        // console.log(response);
        $scope.total = response.total;
        $scope.pages = Math.ceil($scope.total/$scope.limit);


        OmbazarFn.data_load($http,$scope,$rootScope,$timeout,function (response,status) {

        });
        $scope.next = function () {
            $scope.page = Number($scope.page)+1;
            OmbazarFn.data_load($http,$scope,$rootScope,$timeout,function (response,status) {
            });
        };
        $scope.prev = function () {
            $scope.page = $scope.page-1;
            OmbazarFn.data_load($http,$scope,$rootScope,$timeout,function (response,status) {
            });
        };
        $scope.reload = function () {
            OmbazarFn.data_load($http,$scope,$rootScope,$timeout,function (response,status) {

            });
        };
        $scope.first = function () {
            $scope.page = 1;
            OmbazarFn.data_load($http,$scope,$rootScope,$timeout,function (response,status) {

            });
        };
        $scope.last = function () {
            let last_page = $scope.pages;
            $scope.page = last_page;
            OmbazarFn.data_load($http,$scope,$rootScope,$timeout,function (response,status) {

            });
        };
        $rootScope.rows_load = function () {
            OmbazarFn.total_counter($http,$scope,function (response,status) {
                $scope.total = response.total;
                $scope.pages = Math.ceil($scope.total/$scope.limit);
                $scope.reload();
            });
        }
    });
}]);



