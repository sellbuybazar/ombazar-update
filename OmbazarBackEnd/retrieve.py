from .common import Common
from .models import *
import json
from django.shortcuts import HttpResponse
from django.utils.encoding import smart_str
from . helper import *


class Retrieve:
    def __init__(self):
        self.common = Common()
        self.db = self.common.db

    def total_user(self, type):
        count = self.common.total_user(type)
        return count

    def access_menus(self, user_type):
        common = self.common
        query = {"archive": '0', "parent_id": {"$not": {"$in": ['0']}}}
        collection_name = "menus"
        query[user_type] = '1'
        menus = common.collection_data(collection_name, query)
        return menus

    def categories(self):
        common = self.common
        query = {"archive": 0}
        collection_name = "categories"
        menus = common.collection_data(collection_name, query)
        return menus

    def bar_codes(self):
        common = self.common
        query = {"active": '1'}
        collection_name = "bar_codes"
        bar_codes = common.collection_data(collection_name, query)
        return bar_codes

    def main_menus(self, request=''):
        include = []
        try:
            include = request.POST['include']
            include = json.loads(include)
        except:
            pass
        common = self.common
        query = {"archive": '0'}
        collection_name = "menus"
        if request:
            try:
                logged_user_id = request.session['logged_user_id']
            except:
                pass
            else:
                _or = []


                id_type_name = common.id_type_name(logged_user_id)
                if id_type_name != "cs":
                    query["$or"] = _or
                    _or.append({
                        "parent_id": "0"
                    })

                    _or.append({
                        id_type_name: "1"
                    })

                # elif id_type_name == "admin" or id_type_name == "employee":
                #     # query['$and'] = []
                #     _or.append({"$and": [{"id": {"$in": include}}]})

                # else:
                #     _or.append({
                #         "admin": "1"
                #     })
        # common.preview(query)
        menus = common.collection_data(collection_name, query)
        return menus

    def controller_info(self, user_id, user_name=False):
        collection = Controller().collection_name()
        query = {"user_id": user_id}
        if user_name:
            query['user_name'] = user_id
            del query['user_id']
        find = collection.find_one(query, {"_id": 0})
        return find

    def logged_controller(self, request):
        errors = {}
        return_object = {
            "status": 0,
            "error": errors,
            "message": {}
        }
        try:
            user_id = request.session['logged_user_id']
        except Exception:
            return {}
        else:
            collection = Controller().collection_name()
            pipeline = [
                {
                    "$lookup": {
                        "from": "transactions",
                        "localField": "user_id",
                        "foreignField": "user_id",
                        "as": "pending_data"
                    }
                },
                {
                    "$match": {
                        "user_id": user_id
                    }
                },
                {
                    "$addFields": {
                        "pending": {
                            "$filter": {
                                "input": "$pending_data",
                                "cond": {
                                    "$eq": ["$$this.status", 0]
                                }
                            }
                        }
                    }
                },
                {
                    "$project": {
                        "_id": 0,
                        "pending_data": 0,
                        "pending._id": 0
                    }
                }
            ]
            id_type_name = self.common.id_type_name(user_id)
            try:
                vendor_id = request.session['vendor_id']
            except:
                errors['vendor_undefined'] = "Undefined vendor "
            else:
                vendor_id = self.common.id_info(user_id, "option") + "000000000000000000"
            if id_type_name == "employee":
                new_pipe = [
                    {
                        "$lookup": {
                            "from": "controllers",
                            "let": {"vendor_id": vendor_id},
                            "pipeline": [{"$match": {"$expr": {
                                "$and": [{"$eq": ["$user_id", "$$vendor_id"]}]
                            }}}],
                            "as": "vendor_info",
                        }
                    },
                    {
                        "$unwind": "$vendor_info"
                    },
                    {
                        "$addFields": {
                            "point_balance": "$vendor_info.point_balance",
                            # "account_balance": "",
                        }
                    },
                    {
                        "$lookup": {
                            "from": "transactions",
                            "let": {"vendor_id": vendor_id},
                            "pipeline": [{"$match": {"$expr": {
                                "$and": [{"$eq": ["$user_id", "$$vendor_id"]}]
                            }}}],
                            "as": "pending_data",
                        }
                    },
                    {
                        "$addFields": {
                            "pending": {
                                "$filter": {
                                    "input": "$pending_data",
                                    "cond": {
                                        "$eq": ["$$this.status", 0]
                                    }
                                }
                            }
                        }
                    },
                    {
                        "$project": {
                            "vendor_info": 0,
                            "pending_data": 0,
                            "pending._id": 0
                        }
                    }
                ]
                pipeline.extend(new_pipe)
            find = self.common.aggregate_collection_data("controllers", pipeline, 1)
            try:
                find = find['find_data'][0]
            except:
                errors['error_info'] = str(self.common.error_info())
                return return_object
            return find

    def installed_collections(self):
        common = self.common
        collection = InstalledCollection().collection_name()
        find = collection.find({}, {"_id": 0})
        clean_data = common.json_find_data(find)
        return clean_data

    def store(self, store_id):
        common = self.common
        find = common.collection_data_one("stores", {"store_id": store_id})
        return find

    def category(self, category_id):
        common = self.common
        find = common.collection_data_one("categories", {"category_id": category_id})
        return find

    def item(self, item_id):
        common = self.common
        find = common.collection_data_one("items", {"item_id": item_id})
        return find

    def corporate(self, corporate_id):
        common = self.common
        find = common.collection_data_one("corporates", {"corporate_id": corporate_id})
        return find

    def controllers(self, user_type, limit=0, skip=0):
        common = self.common
        id_type = common.id_type(user_type)
        find = common.collection_data("controllers", {"user_id": {"$regex": "^"+id_type}}, {}, limit, skip)
        return find

    def categories(self, limit=0, skip=0):
        common = self.common
        find = common.collection_data("categories", {}, {}, limit, skip)
        return find

    def stores(self, limit=0, skip=0):
        common = self.common
        find = common.collection_data("stores", {}, {}, limit, skip)
        return find

    def countries(self, limit=0, skip=0):
        common = self.common
        find = common.collection_data("countries", {}, {}, limit, skip)
        return find

    def items(self, limit=0, skip=0):
        common = self.common
        find = common.collection_data("items", {}, {}, limit, skip)
        return find

    def coupons(self, limit=0, skip=0):
        common = self.common
        find = common.collection_data("coupons", {}, {}, limit, skip)
        return find

    def vendor_stores(self, vendor_id):
        common = self.common
        find = common.collection_data("stores", {"vendor_id": vendor_id})
        return find

    def point_rules(self):
        common = self.common
        find = common.collection_data("point_rules")
        return find

    def request_collection_data(self, collection_name, query, option, limit=0, skip=0):
        errors = {}
        messages = {}
        return_object = {
            "status": 0,
            "error": errors,
            "message": messages
        }

        try:
            limit = int(limit)
        except ValueError:
            messages['invalid_limit'] = "Enter numeric in per page"
            return return_object

        try:
            skip = int(skip)
        except ValueError:
            errors['invalid_skip'] = "Enter numeric skip number"
            return return_object

        find = self.common.collection_data(collection_name, query, option, limit, skip)
        return find

    def item_bank(self, request, limit, skip, count=False):
        common = self.common
        errors = {}
        messages = {}
        return_object = {
            "status": 0,
            "error": errors,
            "message": messages
        }
        try:
            limit = int(limit)
            skip = int(skip)
        except Exception:
            messages['invalid_number'] = "Try a valid limit or skip"
            return return_object
        try:
            store_id = request.session['store_id']
        except Exception:
            messages['undefined_store'] = "Select a store first"
            return return_object
        pipeline = [
            {
                "$lookup": {
                    "from": "store_items",
                    "let": {"this_item_id": "$item_id"},
                    "pipeline": [{"$match": {"$expr": {"$and": [{"$eq": ["$item_id", "$$this_item_id"]}, {"$eq": ["$store_id", store_id]}]}}}],
                    "as": "exist_item",
                }
            },
            {
                "$unwind": {
                    "path": "$exist_item",
                    "preserveNullAndEmptyArrays": True
                }
            },
            {
                "$addFields": {
                    "price": {
                        "$ifNull": ["$exist_item.price", "$price"]
                    },
                    "sale": {
                        "$ifNull": ["$exist_item.sale", "$sale"]
                    },
                    "purchase": {
                        "$ifNull": ["$exist_item.purchase", "$purchase"]
                    },
                    "before_quantity": "$exist_item.before_quantity",
                    "store_id": "$exist_item.store_id",
                    "store_item_id": "$exist_item.store_item_id",
                }
            },
            # {
            #     "$replaceRoot": {"newRoot": {"$mergeObjects": [{"$arrayElemAt": ["$exist_item", 0]}, "$$ROOT"]}}
            # },

            {
                "$project": {
                    "_id": 0,
                    "exist_item": 0,
                }
            },

        ]

        match_attr = {}
        match = {
            "$match": match_attr
        }

        try:
            query = request.POST['query']
        except Exception:
            pass
        else:
            js = json.loads(query)
            if "item_name" in js:
                item_name = js['item_name']
                regex = item_name['$regex']
                expre = re.compile(regex, re.IGNORECASE)
                new_express = {
                    "$regex": expre
                }
                js['item_name'] = new_express
            match_attr.update(js)
        pipeline.append(match)

        find = common.aggregate_collection_data("items", pipeline, limit, skip, count)
        return find

    def stock_items(self, request, limit=0, skip=0, count=False, quantity=True):
        common = self.common
        errors = {}
        messages = {}
        return_object = {
            "status": 0,
            "error": errors,
            "message": messages
        }
        store_id = ""
        try:
            limit = int(limit)
            skip = int(skip)
        except Exception:
            messages['invalid_number'] = "Try a valid limit or skip"
            return return_object

        # If store_item_id exist store id not checked
        try:
            query = request.POST['query']
        except Exception:
            pass
        else:
            js = json.loads(query)
            try:
                store_item_id = js['store_item_id']
            except:
                try:
                    store_id = request.session['store_id']
                except Exception:
                    messages['undefined_store'] = "Select a store first"
                    return return_object

        pipeline = [
            {
                "$lookup": {
                    "from": "items",
                    "let": {"this_item_id": "$item_id", "this_store_id": "$store_id"},
                    "pipeline": [{"$match": {"$expr": {"$and": [{"$eq": ["$item_id", "$$this_item_id"]}]}}}],
                    "as": "exist_item",
                }
            },
            {
                "$addFields": {
                    "stock": "$quantity"
                }
            },
            {
                "$replaceRoot": {"newRoot": {"$mergeObjects": [{"$arrayElemAt": ["$exist_item", 0]}, "$$ROOT"]}}
            },
            {
                "$project": {
                    "_id": 0,
                    "exist_item": 0,
                }
            }
        ]

        try:
            query = request.POST['query']
        except Exception:
            pass
        else:
            js = json.loads(query)
            if "zero_quantity" in js:
                quantity = False
        if quantity:
            pipeline.append({
                "$match": {
                    "quantity": {
                        "$gt": 0
                    }
                }
            })
        match_attr = {"store_id": store_id}
        match = {
            "$match": match_attr
        }
        try:
            query = request.POST['query']
        except Exception:
            pass
        else:
            js = json.loads(query)
            if "item_name" in js:
                item_name = js['item_name']
                regex = item_name['$regex']
                expre = re.compile(regex, re.IGNORECASE)
                new_express = {
                    "$regex": expre
                }
                js['item_name'] = new_express
            # common.preview(js)
            if "zero_quantity" in js:
                del js['zero_quantity']
            match_attr.update(js)

            try:
                store_item_id = js['store_item_id']
            except:
                pass
            else:
                del match_attr['store_id']
        pipeline.append(match)

        find = common.aggregate_collection_data("store_items", pipeline, limit, skip, count)
        return find

    def store_item(self, request, store_item_id):
        query = {
            "store_item_id": store_item_id
        }
        str_query = json.dumps(query)
        request.POST = request.POST.copy()
        request.POST['query'] = str_query
        item_info = self.stock_items(request, 1, 0, False, False)
        find_data = item_info['find_data']
        if find_data:
            item_info = find_data[0]
            return item_info

    def cart_items(self, request):
        common = self.common
        errors = {}
        messages = {}
        return_object = {
            "status": 0,
            "error": errors,
            "message": messages
        }
        pipeline = [
            {
                "$lookup": {
                    "from": "items",
                    "let": {"this_item_id": "$item_id", "this_store_id": "$store_id"},
                    "pipeline": [{"$match": {"$expr": {"$and": [{"$eq": ["$item_id", "$$this_item_id"]}]}}}],
                    "as": "exist_item",
                }
            },
            {
                "$addFields": {
                    "stock": "$quantity"
                }
            },
            {
                "$replaceRoot": {"newRoot": {"$mergeObjects": [{"$arrayElemAt": ["$exist_item", 0]}, "$$ROOT"]}}
            },
            {
                "$project": {
                    "_id": 0,
                    "exist_item": 0,
                }
            },
        ]
        match_attr = {}
        match = {
            "$match": match_attr
        }
        try:
            query = request.POST['query']
        except Exception:
            pass
        else:
            js = json.loads(query)
            match_attr.update(js)
        pipeline.append(match)

        find = common.aggregate_collection_data("store_items", pipeline)
        return find

    def orders(self, request, limit, skip, count=False):
        common = self.common
        errors = {}
        messages = {}
        return_object = {
            "status": 0,
            "error": errors,
            "message": messages
        }
        try:
            limit = int(limit)
            skip = int(skip)
        except Exception:
            messages['invalid_number'] = "Try a valid limit or skip"
            return return_object
        try:
            store_id = request.session['store_id']
        except Exception:
            messages['undefined_store'] = "Select a store first"
            return return_object
        pipeline = [
            {
                "$lookup": {
                    "from": "order_items",
                    "localField": "order_id",
                    "foreignField": "order_id",
                    "as": "exist_item",
                }
            },
            {
                "$lookup": {
                    "from": "controllers",
                    "localField": "customer_id",
                    "foreignField": "user_id",
                    "as": "customer_info",
                }
            },
            {
                "$unwind": {
                    "path": "$customer_info",
                    "preserveNullAndEmptyArrays": True
                }
            },
            {
                "$addFields": {
                    "total_order_item": {
                        "$size": "$exist_item"
                    },
                    "total_complete": {
                        "$size": {
                            "$filter": {
                                "input": "$exist_item",
                                "cond": {
                                    "$eq": ["$$this.status", "Completed"]
                                }
                            }
                        }
                    }
                }
            },
            {
                "$addFields": {
                    "status": {
                        "$cond": {
                            "if": {"$eq": ["$total_complete", "$total_order_item"]},
                            "then": "Completed",
                            "else": "$status"
                        }
                    }
                }
            },
            {
                "$project": {
                    "exist_item": {
                        "store_item_id": 1,
                        "store_id": 1,
                    },
                    "customer_info": {
                        "name": 1,
                        "email": 1,
                        "mobile": 1,
                        "calling_code": 1,
                    },

                    "_id": 0,
                    "order_id": 1,
                    "order_code": 1,
                    "time": 1,
                    "status": 1

                }
            },

            {
                "$match": {"exist_item.store_id": store_id}
            },
        ]

        find = common.aggregate_collection_data("orders", pipeline, limit, skip, count)
        return find

    def order(self, request, order_id):
        common = self.common
        errors = {}
        messages = {}
        return_object = {
            "status": 0,
            "error": errors,
            "message": messages
        }
        store_id = ""
        try:
            logged_user_id = request.session['logged_user_id']
        except Exception:
            errors['login_exception'] = "Login first"
        else:
            id_type_name = common.id_type_name(logged_user_id)
            if id_type_name != "user":
                try:
                    store_id = request.session['store_id']
                except:
                    messages['store_required'] = "Select a store first"

        pipeline = [
            {
                "$lookup": {
                    "from": "order_items",
                    "localField": "order_id",
                    "foreignField": "order_id",
                    "as": "order_items",
                }
            },
            {
                "$lookup": {
                    "from": "controllers",
                    "localField": "customer_id",
                    "foreignField": "user_id",
                    "as": "customer_info",
                }
            },
            {
                "$lookup": {
                    "from": "stores",
                    "localField": "order_items.store_id",
                    "foreignField": "store_id",
                    "as": "stores",
                }
            },
            {
                "$lookup": {
                    "from": "items",
                    "localField": "order_items.item_info.item_id",
                    "foreignField": "item_id",
                    "as": "items",
                }
            },
            {
                "$unwind": {
                    "path": "$customer_info",
                    "preserveNullAndEmptyArrays": True
                }
            },
            {
                "$project": {
                    "order_items": {
                        "_id": 0,
                    },
                    "customer_info": {
                        "_id": 0,
                    },
                    "stores": {
                        "_id": 0,
                    },
                    "items": {
                        "_id": 0,
                    },

                    "_id": 0,

                }
            },
            {
                "$addFields": {
                    "total_order_item": {
                        "$size": "$order_items"
                    },
                    "total_complete": {
                        "$size": {
                            "$filter": {
                                "input": "$order_items",
                                "cond": {
                                    "$eq": ["$$this.status", "Completed"]
                                }
                            }
                        }
                    }
                }
            },
            {
                "$addFields": {
                    "status": {
                        "$cond": {
                            "if": {"$eq": ["$total_complete", "$total_order_item"]},
                            "then": "Completed",
                            "else": "$status"
                        }
                    }
                }
            },

        ]
        if store_id:
            pipeline.append({
                "$match": {
                    "order_items.store_id": store_id,

                }
            })
        pipeline.append({
            "$match": {
                "order_id": order_id
            }
        })
        find = common.aggregate_collection_data("orders", pipeline, 1, 0, False)
        return find

    def export_excel(self, request):
        common = self.common
        errors = {}
        messages = {}
        return_object = {
            "status": 0,
            "error": errors,
            "message": messages
        }
        try:
            this_request = request.POST
            try:
                method = this_request['export_method']
                method = 'self.' + method

            except Exception:
                errors['export_method'] = "Export method not found"
            else:
                try:
                    result = eval(method)
                except:
                    errors['export_eval_error'] = str(common.error_info())
                    errors['result_error'] = "Eval result not generated"
                else:
                    if result:
                        find_data = result['find_data']
                        if find_data:
                            # errors['fne_data'] = find_data
                            file_info = common.data_export_to_excel(find_data, "coupons_export.xlsx")
                            if file_info:
                                return_object['status'] = 1
                                return_object['download'] = file_info
                            else:
                                messages['export'] = "Export have something wrong"
        except:
            errors['error_info'] = str(common.error_info())
        else:
            pass

        return return_object

    def documents_counter(self, collection_name, query={}):
        for key, value in query.items():
            key_object = query[key]
            try:
                if "$regex" in key_object:
                    regex = key_object['$regex']
                    express = re.compile(regex, re.IGNORECASE)
                    new_express = {
                        "$regex": express
                    }
                    query[key] = new_express
            except:
                pass

        return_data = {}
        collection = self.db[collection_name]
        count = collection.count_documents(query)
        return_data['total'] = count
        return return_data

    def login(self, request):
        common = self.common
        errors = {}
        messages = {}
        return_object = {
            "status": 0,
            "error": errors,
            "message": messages
        }
        try:
            collection = Controller().collection_name()
            mobile = request.POST['mobile']
            try:
                mobile = int(mobile)
            except ValueError:
                messages['invalid_mobile'] = "Enter a valid mobile number"
            else:
                length = len(str(mobile))
                if length < 10:
                    messages['mobile_length'] = "Enter at least 10 digit of your mobile number"
            password = request.POST['password']
            password = common.md5(password)
            id_type = common.id_type("user")
            query = {
                "mobile": {
                    "$regex": str(mobile)[-10:]
                },
                "password": password,
                "user_id": {
                    "$regex": "^(?!" + id_type + ")"
                }
            }
            find = collection.find_one(query, {"_id": 0})
        except Exception as e:
            errors['login_exception'] = str(e)
        else:
            if not return_object['message'] and not return_object['error']:
                if find:
                    request.session['logged_user_id'] = find['user_id']
                    return_object['user_id'] = find['user_id']
                    return_object['logged_user_type'] = common.id_type_name(find['user_id'])
                    return_object['status'] = 1
                    id_type_name = common.id_type_name(find['user_id'])
                    if id_type_name == "vendor" or id_type_name == "employee":
                        stores = common.collection_data("stores", {"store_id": {"$in": find['stores']}}, {"store_name": 1, "store_id": 1})
                        find['stores'] = stores['find_data']
                    if id_type_name == "employee":
                        user_id = find['user_id']
                        vendor_id = common.id_info(user_id, "option")+"000000000000000000"
                        request.session['vendor_id'] = vendor_id

                    return_object['user_info'] = find
                    basic_information = self.basic_information(request)
                    return_object['basic_info'] = basic_information

                    # last activities
                    last_activities = common.last_activities(request)
                    return_object.update(last_activities)
                else:
                    messages['not_match'] = "Username or password not matched"


        return return_object

    def basic_information(self, request):
        errors = {}
        messages = {}
        return_object = {
            "status": 0,
            "message": messages,
            "error": errors
        }
        common = self.common
        try:

            return_object['is_main_domain'] = 1
            return_object['software_info'] = self.software_settings()

            # Id types
            id_types = common.id_types()

            return_object['id_types'] = id_types

            # Stores
            stores = []

            # Logged user
            logged_user = {}
            logged_user_id = 0
            try:
                logged_user_id = request.session['logged_user_id']
            except Exception as e:
                pass
            else:
                logged_user = common.collection_data_one("controllers", {"user_id": logged_user_id})
                return_object['logged_user'] = logged_user
            finally:
                return_object['logged_user_id'] = logged_user_id
                return_object['logged_user_type'] = common.id_type_name(logged_user_id)

            # vendor id
            vendor_id = 0
            activate_vendor = {}
            try:
                vendor_id = request.session['vendor_id']
            except Exception:
                pass
            else:
                activate_vendor = common.collection_data_one("controllers", {"user_id": vendor_id})
                return_object['activate_vendor'] = activate_vendor
            finally:
                return_object['vendor_id'] = vendor_id

            # store id
            store_id = 0
            try:
                store_id = request.session['store_id']
            except Exception:
                pass
            else:
                activate_store = common.collection_data_one("stores", {"store_id": store_id})
                return_object['activate_store'] = activate_store
                return_object['logged_store'] = activate_store
            finally:
                return_object['store_id'] = store_id
                return_object['logged_store_id'] = store_id

            # employee id
            employee_id = 0
            try:
                employee_id = request.session['employee_id']
            except Exception:
                pass
            else:
                activate_employee = common.collection_data_one("controllers", {"user_id": employee_id})
                return_object['activate_employee'] = activate_employee
            finally:
                return_object['employee_id'] = employee_id

            try:
                if logged_user_id:
                    logged_user_id_type_name = common.id_type_name(logged_user_id)

                    if logged_user_id_type_name == "vendor":
                        stores = logged_user['stores']
                    elif logged_user_id_type_name == "employee":
                        stores = logged_user['stores']
                    elif vendor_id:
                        stores = activate_vendor['stores']
                    # get last activities
                    if logged_user_id_type_name == "vendor" or logged_user_id_type_name == "employee" or vendor_id:
                        last_activities = common.last_activities(request)
                        return_object.update(last_activities)

            except Exception:
                pass
            else:
                store_details = common.collection_data("stores", {"store_id": {"$in": stores}}, {"store_name": 1, "store_id": 1})
                if store_details['find_data']:
                    stores = store_details['find_data']

            finally:
                return_object['stores'] = stores

            # Point Rules
            point_rules = []
            try:
                point_rules_info = common.collection_data("point_rules")
                if point_rules_info['find_data']:
                    point_rules = point_rules_info['find_data']

            except Exception:
                pass
            finally:
                return_object['point_rules'] = point_rules

            # For unexpected land url protected
            page_access = 1
            try:
                page_path = request.POST['land_page_path']
                pl = page_path.split('/')
                page_name = pl[1]
                if page_name == "add-new" or page_name == "controllers":
                    page_name = page_name + "/" + pl[2]

                if page_name:
                    page_info = common.collection_data_one("menus", {"link": page_name}, {"id": 1})
                    if page_info:
                        page_id = page_info['id']
                        user_available_pages = logged_user['access_menus']
                        if page_id in user_available_pages:
                            page_access = 1
                        else:
                            page_access = 0

            except:
                page_access = 1
            else:
                if page_name == "profile":
                    page_access = 1


            return_object['page_access'] = page_access
        except Exception:
            messages['basic_info_exception'] = "System need basic information, but not found any information"
        else:
            # First check any error available
            if not return_object['error'] and not return_object['message']:
                return_object['status'] = 1

        return return_object

    def software_settings(self):
        return_object = {
            "title": "Global face",
            "currency": "$",
            "time_zone": "Asia/Dhaka",
            "logo": "/static/OmbazarBackEnd/assets/img/default/no-image.png",
            "login_banner": "/static/OmbazarBackEnd/assets/img/default/no-image.png",
            "login_banner_back_end": "/static/OmbazarBackEnd/assets/img/default/no-image.png",
            "favicon": "/static/app/assets/img/default/no-image.png",
            "company_name": "Company name",
            "is_main_domain": 1,
            "https": 0,
            "email": "company@domain.com",
            "login_banner_video": "",
        }

        settings = self.common.collection_data("last_updates",{
            "type":{
                "$in":["logo","login_banner","login_banner_video","login_banner_back_end"]
            }
        })

        for item in settings['find_data']:
            data_type = item['type']
            return_object[data_type] = item['value']

        return return_object

    def expire_date_rules(self):
        result = self.common.last_update("expire_date_rules")
        return result

    def distributed_point_slabs(self):
        result = self.common.last_update("distributed_point_slabs")
        return result

    def tree(self, request):
        errors = {}
        messages = {}
        return_object = {
            "status": 0,
            "message": messages,
            "error": errors
        }
        common = self.common
        status = ""
        try:
            this_request = request.POST
            try:
                logged_user_id = request.session['logged_user_id']
            except:
                messages['login_required'] = "Login first"
                return return_object
            else:
                user_id = logged_user_id
            try:
                user_id = this_request['parent_id']
            except:
                pass

            field_name = "referer"
            try:
                status = this_request['status']
            except:
                pass
            else:
                if status == "Paid":
                    field_name = "up_line_referer"
            look = {}
            if status == "Paid":
                look = {
                    "$graphLookup": {
                        "from": "tree",
                        "startWith": "$user_id",
                        "connectFromField": "childes",
                        "connectToField": "user_id",
                        "as": "exist_item",
                        "maxDepth": 5
                    }
                }
            else:
                look = {
                    "$lookup": {
                        "from": "tree",
                        "localField": "user_id",
                        "foreignField": "referer",
                        "as": "exist_item",
                    }
                }

            pipeline = [
                look,
                {
                    "$project": {
                        "_id": 0,
                        "exist_item._id": 0,
                    }
                },
                {
                    "$match": {
                        "user_id": user_id
                    }
                },
                {
                    "$unwind": "$exist_item"
                },
                {
                    "$replaceRoot": {"newRoot": "$exist_item"}
                },
                {
                    "$lookup": {
                        "from": "controllers",
                        "localField": "user_id",
                        "foreignField": "user_id",
                        "as": "user_info"
                    }
                },

                {
                    "$project": {
                        "user_info._id": 0
                    }
                },
                {
                    "$unwind": "$user_info"
                },

                {
                    "$addFields": {
                        "total_child": {"$size": "$childes"}
                    }
                },
            ]

            try:
                limit = int(this_request['limit'])
            except:
                limit = 10
            try:
                status = this_request['status']
            except:
                pass
            else:
                match_object = {
                    "$match": {
                        "status": status
                    }
                }
                pipeline.append(match_object)
            find = common.aggregate_collection_data("tree", pipeline, limit)

            child_count = common.aggregate_collection_data_count("tree", pipeline, 3)
            return_object['tree_data'] = find
            return_object['pipline'] = pipeline
            return_object['exchange_child_count'] = child_count
            return return_object

        except:
            errors['error_info'] = common.error_info()
        return return_object

    def tree_info(self, user_id):
        pipeline = [
            {
                "$lookup": {
                    "from": "tree",
                    "localField": 'referer',
                    "foreignField": 'user_id',
                    "as": "referer"
                }
            },

            {
                "$unwind": {
                    "path": "$referer",
                    "preserveNullAndEmptyArrays": True
                }
            },
            {
                "$lookup": {
                    "from": "tree",
                    "localField": 'up_line_referer',
                    "foreignField": 'user_id',
                    "as": "up_line_referer"
                }
            },
            {
                "$unwind": {
                    "path": "$up_line_referer",
                    "preserveNullAndEmptyArrays": True
                }
            },
            {
                "$lookup": {
                    "from": "controllers",
                    "localField": 'referer.user_id',
                    "foreignField": 'user_id',
                    "as": "referer_info"
                }
            },
            {
                "$lookup": {
                    "from": "controllers",
                    "localField": 'up_line_referer.user_id',
                    "foreignField": 'user_id',
                    "as": "up_line_referer_info"
                }
            },
            {
                "$unwind": {
                    "path": "$referer_info",
                    "preserveNullAndEmptyArrays": True
                }
            },
            {
                "$unwind": {
                    "path": "$up_line_referer_info",
                    "preserveNullAndEmptyArrays": True
                }
            },
            {
                "$addFields": {
                    "referer": {"$mergeObjects": ["$referer_info", "$referer"]},
                    "up_line_referer": {"$mergeObjects": ["$up_line_referer_info", "$up_line_referer"]},
                }
            },

            {
                "$project": {
                    "_id": 0,
                    "referer._id": 0,
                    "up_line_referer._id": 0,
                    "referer_info": 0,
                    "up_line_referer_info": 0,
                }
            },
            {
                "$addFields": {
                    "total_child": {"$size": "$childes"}
                }
            },
            {
                "$match": {
                    "user_id": user_id
                }
            }
        ]
        find = self.common.aggregate_collection_data("tree", pipeline, 1)
        if find['find_data']:
            find = find['find_data'][0]
        else:
            find = {}
        return find

    def feature_settings(self):
        errors = {}
        messages = {}
        return_object = {
            "status": 0,
            "message": messages,
            "error": errors
        }
        feature_settings = {}
        common = self.common
        try:

            # Position cost setting info
            position_cost = common.last_update('position_cost')
            cost_amount = 0
            if position_cost:
                cost_amount = position_cost['amount']
            feature_settings['position_cost'] = cost_amount

            # Position cost setting info
            cost_per_coupon_info = common.last_update('cost_per_coupon')
            cost_per_coupon = 0
            if cost_per_coupon_info:
                cost_per_coupon = cost_per_coupon_info['amount']
            feature_settings['cost_per_coupon'] = cost_per_coupon

            # Tree user position expire time
            position_expire = common.last_update('position_expire_time')
            position_expire_time = {
                "day": 0,
                "hour": 0,
                "minute": 0,
            }
            if position_expire:
                position_expire_time = position_expire['value']
            feature_settings['position_expire_time'] = position_expire_time

            # system emails
            forgot_email = common.last_update('forgot_email')
            email = ""
            if forgot_email:
                email = forgot_email['value']
            feature_settings['forgot_email'] = email

            software_settings = self.software_settings()
            feature_settings.update(software_settings)
        except:
            errors['error_info'] = str(common.error_info())
            return return_object
        else:
            return feature_settings

    def imported_user_info(self):
        common = self.common
        id_type = common.id_type("user")
        query = {"user_id": {"$regex": "^" + id_type}}
        count = common.collection_data("controllers", query, {}, 0, 0, True)
        return count

    def find_users(self, request):
        errors = {}
        messages = {}
        return_object = {
            "status": 0,
            "message": messages,
            "error": errors
        }
        common = self.common
        try:
            mobile = request.POST['mobile']
            try:
                mobile = int(mobile)
            except ValueError:
                messages['invalid_mobile'] = "Enter a valid mobile number"
            else:
                length = len(str(mobile))
                if length < 10:
                    messages['mobile_length'] = "Enter at least 10 digit of your mobile number"
        except:
            errors['error_info'] = str(common.error_info())
        else:
            if not return_object['error'] and not return_object['message']:
                id_type = common.id_type("user")
                # "user_id": {"$regex": "^" + id_type}
                query = {"mobile": {"$regex": str(mobile)[-10:]}}
                users = common.collection_data("controllers", query)
                return users
        return return_object

    def transactions(self, request, limit, skip=0, count=False):
        errors = {}
        messages = {}
        return_object = {
            "status": 0,
            "message": messages,
            "error": errors
        }
        common = self.common
        try:
            logged_user_id = request.session['logged_user_id']
        except:
            messages['login_required'] = "Login first"
        try:
            limit = int(limit)
            skip = int(skip)
        except:
            errors['limit_skipt'] = "Invalid limit skip"
        if not return_object['error'] and not return_object['message']:
            pipeline = [
                {
                    "$lookup": {
                        "from": "controllers",
                        "localField": "user_id",
                        "foreignField": "user_id",
                        "as": "user"
                    }
                },
                {
                    "$unwind": "$user"
                },
                {
                    "$project": {
                        "_id": 0,
                        "user": {
                            "_id": 0
                        }
                    }
                }

            ]
            match_attr = {}
            match = {
                "$match": match_attr
            }
            try:
                query = request.POST['query']
            except Exception:
                pass
            else:
                js = json.loads(query)
                match_attr.update(js)
            pipeline.append(match)

            find = common.aggregate_collection_data("transactions", pipeline, limit, skip, count)
        else:
            find = return_object
        return find

    def brands(self, limit, skip, count=False, status=False):
        try:
            limit = int(limit)
            skip = int(skip)
        except:
            pass
        pipeline = [
            {
                "$lookup": {
                    "from": "stores",
                    "localField": "store_id",
                    "foreignField": "store_id",
                    "as": "store_info"
                }

            },
            {
                "$unwind": "$store_info"
            },
            {"$replaceRoot": {"newRoot": {"$mergeObjects": ["$store_info", "$$ROOT"]}}},
            {
                "$project": {
                    "_id": 0,
                    "store_info": 0
                }
            }
        ]
        if status:
            stage = {
                "$match": {
                    "active": 1
                }
            }
            pipeline.append(stage)
        find = self.common.aggregate_collection_data("brands", pipeline, limit, skip, count)
        return find

    def active_brands(self):
        return self.brands(0, 0, False, True)

    def estimated_brands(self):
        pipeline = [
            {
                "$lookup": {
                    "from": "brands",
                    "localField": "store_id",
                    "foreignField": "store_id",
                    "as": "store_info"
                }

            },
            {
                "$match": {
                    "store_info": []
                }
            },
            {
                "$project": {
                    "_id": 0,
                    "store_info": 0
                }
            }
        ]
        find = self.common.aggregate_collection_data("stores", pipeline)
        return find

    def collections(self):
        collections = self.db.list_collection_names()
        try:
            collections = self.common.json_find_data(collections)
        except:
            pass
        return collections

    def collection_keys(self, collection):
        common = self.common
        find = common.collection_data_one(collection)
        try:
            find_data = []
            for key, value in find.items():
                find_data.append(key)
        except:
            find = {
                "find_data": []
            }
        else:
            find = {
                "find_data": find_data
            }
        return find



