from django.conf import settings
from deepdiff import DeepDiff

Database = settings.DB
# Follow this pattern. all collection name carry on a ('s)


def object_compare(model, new_model):
    dif = DeepDiff(model, new_model)
    if 'type_changes' in dif:
        return True
    elif 'dictionary_item_removed' in dif:
        return True

    return False


class LastUpdate:
    def collection_name(self):
        collection = Database['last_updates']
        collection.create_index("type")
        collection.create_index("option")
        return collection

    def model(self):
        model_design = {
            "type": "",
            "amount": "",
            "option": "",
            "archive": "",
            "time": ""
        }
        return model_design

    def compare(self, new_model):
        model = self.model()
        compare = object_compare(model, new_model)
        return compare


class InstalledCollection:
    def collection_name(self):
        collection = Database['installed_collections']
        collection.create_index("collection_name")
        return collection

    def model(self):
        model_design = {
            "collection_name": "",
            "collection_keys": "",
            "total_document": "",
            "time": ""
        }
        return model_design

    def compare(self, new_model):
        model = self.model()
        compare = object_compare(model, new_model)
        return compare


class Controller:
    def collection_name(self):
        collection = Database['controllers']
        collection.create_index("user_id")
        collection.create_index("mobile")
        return collection

    def model(self):
        model_design = {
            "user_id": "",
            "calling_code": "",
            "mobile": "",
            "password": "",
            "active": "",
            "time": "",
            "name": "",
            "email": "",
            "address_line_1": "",
            "address_line_2": "",
            "image": "",
            "user_name": "",
            "nationality": "",
            "access_menus": "",
            "stores": "",
            "archive": "",
            "post_code": "",
            "referer_mobile": "",
            "point_balance": "",
            "account_balance": "",
        }
        return model_design

    def compare(self, new_model):
        model = self.model()
        compare = object_compare(model, new_model)
        return compare


class Store:
    def collection_name(self):
        collection = Database['stores']
        collection.create_index("post_code")
        collection.create_index("store_id")
        collection.create_index("vendor_id")
        return collection

    def model(self):
        model_design = {
            "store_id": "",
            "vendor_id": "",
            "store_name": "",
            "address_line_1": "",
            "address_line_2": "",
            "post_code": "",
            "country": "",
            "image": "",
            "active": "",
            "time": "",
        }
        return model_design

    def compare(self, new_model):
        model = self.model()
        compare = object_compare(model, new_model)
        return compare


class Category:
    def collection_name(self):
        collection = Database['categories']
        collection.create_index("category_name")
        collection.create_index("category_id")
        return collection

    def model(self):
        model_design = {
            "category_id": "",
            "parent_id": "",
            "category_name": "",
            "image": "",
            "archive": "",
            "active": "",
            "time": "",
        }
        return model_design

    def compare(self, new_model):
        model = self.model()
        compare = object_compare(model, new_model)
        return compare


class Item:
    def collection_name(self):
        collection = Database['items']
        collection.create_index("item_id")
        collection.create_index("bar_code")
        collection.create_index("category_id")
        return collection

    def model(self):
        model_design = {
            "item_id": "",
            "category_id": "",
            "item_name": "",
            "short_description": "",
            "description": "",
            "archive": "",
            "active": "",
            "time": "",
            "purchase": "",
            "price": "",
            "sale": "",
            "vat": "",
            "bar_code": "",
            "bar_code_label": "",
            "specifications": "",
            "quantity": "",  # only for vendor item add
        }
        return model_design

    def compare(self, new_model):
        model = self.model()
        compare = object_compare(model, new_model)
        return compare


class CouponBatch:
    def collection_name(self):
        collection = Database['coupon_batches']
        collection.create_index("batch")
        return collection

    def model(self):
        model_design = {
            "batch": "",
            "amount": "",
            "time": "",
            "type": ""
        }
        return model_design

    def compare(self, new_model):
        model = self.model()
        compare = object_compare(model, new_model)
        return compare


class Coupon:
    def collection_name(self):
        collection = Database['coupons']
        collection.create_index("coupon_number")
        collection.create_index("type")
        collection.create_index("batch")
        return collection

    def model(self):
        model_design = {
            "coupon_number": "",
            "status": "",
            "batch": "",
            "time": "",
            "type": ""
        }
        return model_design

    def compare(self, new_model):
        model = self.model()
        compare = object_compare(model, new_model)
        return compare


class Round:
    def collection_name(self):
        collection = Database['rounds']
        collection.create_index("round_id")
        return collection

    def model(self):
        model_design = {
            "round_id": "",
            "active": "",
            "round_name": "",
            "amount": "",
            "time": "",
            "archive": "",
        }
        return model_design

    def compare(self, new_model):
        model = self.model()
        compare = object_compare(model, new_model)
        return compare


class Round:
    def collection_name(self):
        collection = Database['rounds']
        collection.create_index("round_id")
        return collection

    def model(self):
        model_design = {
            "round_id": "",
            "active": "",
            "round_name": "",
            "amount": "",
            "time": "",
            "archive": "",
        }
        return model_design

    def compare(self, new_model):
        model = self.model()
        compare = object_compare(model, new_model)
        return compare


class RoundBlock:
    def collection_name(self):
        collection = Database['round_blocks']
        collection.create_index("round_id")
        collection.create_index("block_id")
        return collection

    def model(self):
        model_design = {
            "round_id": "",
            "block_id": "",
            "estimated_ids": "",
            "active": "",
            "time": "",
        }
        return model_design

    def compare(self, new_model):
        model = self.model()
        compare = object_compare(model, new_model)
        return compare


class EstimatedId:  # its not collection but helper demo
    def collection_name(self):
        collection = Database['estimated_ids']
        return collection

    def model(self):
        model_design = {
            "id": "",
            "prize_id": "",
            "status": "",
        }
        return model_design

    def compare(self, new_model):
        model = self.model()
        compare = object_compare(model, new_model)
        return compare


class Prize:
    def collection_name(self):
        collection = Database['prizes']
        collection.create_index("prize_id")
        return collection

    def model(self):
        model_design = {
            "prize_id": "",
            "prize_name": "",
            "unlock_amount": "",
            "archive": "",
            "active": "",
            "time": "",
        }
        return model_design

    def compare(self, new_model):
        model = self.model()
        compare = object_compare(model, new_model)
        return compare


class UnlockCoupon:
    def collection_name(self):
        collection = Database['unlock_coupons']
        collection.create_index("coupon_id")
        return collection

    def model(self):
        model_design = {
            "coupon_number": "",
            "block_id": "",
            "es_id": "",  # estimated id
            "mobile_number": "",
            "time": "",
        }
        return model_design

    def compare(self, new_model):
        model = self.model()
        compare = object_compare(model, new_model)
        return compare


class Lucky_Coupon:
    def collection_name(self):
        collection = Database['lucky_coupons']
        collection.create_index("lucky_id")
        collection.create_index("coupon_number")
        return collection

    def model(self):
        model_design = {
            "coupon_number": "",
            "block_id": "",
            "es_id": "",  # estimated id
            "mobile_number": "",
            "lucky_id": "",
            "prize_id": "",
            "prize_name": "",
            "time": "",
            "active": "",
        }
        return model_design

    def compare(self, new_model):
        model = self.model()
        compare = object_compare(model, new_model)
        return compare


class SavedBlock:
    def collection_name(self):
        collection = Database['saved_blocks']
        collection.create_index("block_id")
        collection.create_index("time")
        return collection

    def model(self):
        model_design = {
            "block_id": "",
            "session": "",
            "time": "",
            "expire": "",
        }
        return model_design

    def compare(self, new_model):
        model = self.model()
        compare = object_compare(model, new_model)
        return compare


class Corporate:
    def collection_name(self):
        collection = Database['corporates']
        collection.create_index("corporate_id")
        collection.create_index("location")
        collection.create_index("company_name")
        collection.create_index("tags")
        return collection

    def model(self):
        model_design = {
            "corporate_id": "",
            "company_name": "",
            "tags": "",
            "phone": "",
            "email": "",
            "location": "",
            "address": "",
            "notes": "",
            "images": "",
            "time": "",
            "archive": "",
            "active": "",
        }
        return model_design

    def compare(self, new_model):
        model = self.model()
        compare = object_compare(model, new_model)
        return compare


class StoreItem:
    def collection_name(self):
        collection = Database['store_items']
        collection.create_index("store_item_id")
        collection.create_index("vendor_id")
        collection.create_index("store_id")
        return collection

    def model(self):
        model_design = {
            "store_item_id": "",
            "vendor_id": "",
            "store_id": "",
            "item_id": "",
            "purchase": "",
            "price": "",
            "sale": "",
            "point": "",
            "time": "",
            "item_code": "",
            "archive": "",
            "active": "",
            "bar_code": "",
            "specifications": "",
            "before_quantity": "",
        }
        return model_design

    def compare(self, new_model):
        model = self.model()
        compare = object_compare(model, new_model)
        return compare


class Order:
    def collection_name(self):
        collection = Database['orders']
        collection.create_index("order_id")
        return collection

    def model(self):
        model_design = {
            "order_id": "",
            "active": "",
            "archive": "",
            "time": "",
            "status": "",
            "discount": "",
            "customer_id": "",
            "order_type": "",
            "total_info": "",
        }
        return model_design

    def compare(self, new_model):
        model = self.model()
        compare = object_compare(model, new_model)
        return compare


class OrderItems:
    def collection_name(self):
        collection = Database['order_items']
        collection.create_index("order_id")
        collection.create_index("store_id")
        collection.create_index("store_item_id")
        return collection

    def model(self):
        model_design = {
            "order_id": "",
            "store_id": "",
            "item_info": "",
            "store_item_id": "",
            "status": "",
            "time": "",
        }
        return model_design

    def compare(self, new_model):
        model = self.model()
        compare = object_compare(model, new_model)
        return compare


class PointRule:
    def collection_name(self):
        collection = Database['point_rules']
        collection.create_index("id")
        return collection

    def model(self):
        model_design = {
            "id": "",
            "rule_name": "",
            "unit": "",
            "point": "",
            "active": "",
            "description": "",
        }
        return model_design

    def compare(self, new_model):
        model = self.model()
        compare = object_compare(model, new_model)
        return compare


class Tree:
    def collection_name(self):
        collection = Database['tree']
        collection.create_index("user_id")
        collection.create_index("level")
        collection.create_index("up_line_referer")
        return collection

    def model(self):
        model_design = {
            "user_id": "",
            "level": "",
            "referer": "",
            "up_line_referer": "",
            "childes": "",
            "position": "",
            "account_balance": "",
            "point_balance": "",
            "status": "",
            "time": "",
            "expire": "",  # renewal expire date
            "active_expire": "",  # point,bonus available expire
            "distributed_point": "",
            "distribute_status": "",
            "last_distribute_time": "",
        }
        return model_design

    def compare(self, new_model):
        model = self.model()
        compare = object_compare(model, new_model)
        return compare


class Transaction:
    def collection_name(self):
        collection = Database['transactions']
        collection.create_index("transaction_id")
        collection.create_index("type")
        return collection

    def model(self):
        model_design = {
            "transaction_id": "",
            "user_id": "",
            "type": "",
            "amount": "",
            "description": "",
            "status": "",
            "time": ""
        }
        return model_design

    def compare(self, new_model):
        model = self.model()
        compare = object_compare(model, new_model)
        return compare


class Cron:
    def collection_name(self):
        collection = Database['crones']
        collection.create_index("status")
        collection.create_index("type")
        return collection

    def model(self):
        model_design = {
            "cron_id": "",
            "type": "",
            "description": "",
            "status": "",
            "time": "",
            "more_info": ""
        }
        return model_design

    def compare(self, new_model):
        model = self.model()
        compare = object_compare(model, new_model)
        return compare


class MyCoupon:
    def collection_name(self):
        collection = Database['my_coupons']
        collection.create_index("status")
        collection.create_index("user_id")
        return collection

    def model(self):
        model_design = {
            "coupon_number": "",
            "status": "",
            "time": "",
            "user_id": "",
        }
        return model_design

    def compare(self, new_model):
        model = self.model()
        compare = object_compare(model, new_model)
        return compare


class Brand:
    def collection_name(self):
        collection = Database['brands']
        collection.create_index("store_id")
        return collection

    def model(self):
        model_design = {
            "store_id": "",
            "active": "",
            "time": "",
        }
        return model_design

    def compare(self, new_model):
        model = self.model()
        compare = object_compare(model, new_model)
        return compare


