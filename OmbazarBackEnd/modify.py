import re
import json
import os, sys
from pymongo import UpdateMany, UpdateOne
from .common import Common
from .retrieve import Retrieve
from .models import *
from .document_actions import *
from .helper import *


class Modify:
    def __init__(self):
        self.common = Common()
        self.retrieve = Retrieve()
        self.db = self.common.db

    def edit_controller(self, request):
        common = self.common
        retrieve = self.retrieve
        model = Controller()
        controller_action = ControllerAction(request)
        errors = {}
        messages = {}
        return_object = {
            "status": 0,
            "message": messages,
            "error": errors
        }
        try:
            collection = model.collection_name()
            # Data validation
            request_data = ""
            this_request = request.POST
            user_id = this_request['user_id']
            validation = controller_action.data_validation()
            errors.update(validation['errors'])
            messages.update(validation['messages'])
            if "request_data" in validation:
                request_data = validation['request_data']
                try:
                    if user_id == request.session['logged_user_id']:
                        if "access_menus" in request_data:
                            del request_data['access_menus']
                        if "stores" in request_data:
                            del request_data['stores']
                except:
                    pass
                compare = controller_action.compare(request_data)
                errors.update(compare['errors'])
            else:
                errors['request_data'] = "Request data not generated!"

        except Exception as e:
            errors['controller_edit_exception'] = str(e)
        else:
            # First check any error available
            if not return_object['error'] and not return_object['message']:

                # Upload image. If image upload fail a default image will be upload
                upload = common.file_upload(request, 'image')
                if upload:
                    request_data['image'] = upload
                    # delete before image
                    before_image = this_request['before_image']
                    delete = common.file_delete([before_image])
                    errors.update(delete)

                # If any error not exit then data has been sent
                try:
                    where = {"user_id": user_id}
                    which = {"$set": request_data}
                    send_data = collection.update_one(where, which)
                except Exception as e:
                    errors['data_update'] = "Data not sent. Estimated cause database connection error"
                    errors['date_update_exception'] = str(e)
                else:
                    return_object['status'] = 1
                    messages['registration_message'] = 'Changed has been saved'

        return return_object

    def edit_store(self, request):
        errors = {}
        messages = {}
        return_object = {
            "status": 0,
            "error": errors,
            "message": messages
        }
        id_maker = ""
        try:
            request_data = ''
            common = self.common
            model = Store()
            collection = model.collection_name()
            store_action = StoreAction(request)
            store_id = request.POST['store_id']
            # validation data
            validation = store_action.data_validation()
            errors.update(validation['errors'])
            messages.update(validation['messages'])
            if "request_data" in validation:
                request_data = validation['request_data']
                compare = store_action.compare(request_data)
                errors.update(compare['errors'])
            else:
                errors['request_data'] = "Request data generate problem"

        except Exception as e:
            errors['edit_store_exception'] = str(e)
        else:
            # First check any error available# First check any error available
            if not return_object['error'] and not return_object['message']:
                # Upload image. If image upload fail a default image will be upload
                upload = common.file_upload(request, 'image')
                if upload:
                    request_data['image'] = upload
                    # delete before image
                    before_image = request.POST['before_image']
                    delete = common.file_delete([before_image])
                    errors.update(delete)

                # If any error not exit then data has been sent
                try:
                    where = {"store_id": store_id}
                    which = {"$set": request_data}
                    send_data = collection.update_one(where, which)
                except Exception as e:
                    errors['data_update'] = "Data not sent. Estimated cause database connection error"
                    errors['date_update_exception'] = str(e)
                else:
                    return_object['status'] = 1
                    messages['store_add_message'] = 'Changed has been saved'
        return return_object

    def edit_category(self, request):
        errors = {}
        messages = {}
        return_object = {
            "status": 0,
            "error": errors,
            "message": messages
        }
        request_data = ""
        try:
            common = self.common
            model = Category()
            collection = model.collection_name()
            category_action = CategoryAction(request)
            category_id = request.POST['category_id']
            # validation
            validation = category_action.data_validation()
            errors.update(validation['errors'])
            messages.update(validation['messages'])
            if "request_data" in validation:
                request_data = validation['request_data']
                compare = category_action.compare(request_data)
                errors.update(compare['errors'])
                messages.update(compare['messages'])
            else:
                errors['request_data'] = "Request data generate problem"

        except Exception as e:
            errors['edit_category_exception'] = str(e)
        else:
            # First check any error available# First check any error available
            if not return_object['error'] and not return_object['message']:
                # Upload image. If image upload fail a default image will be upload
                upload = common.file_upload(request, 'image')
                if upload:
                    request_data['image'] = upload
                    # delete before image
                    before_image = request.POST['before_image']
                    delete = common.file_delete([before_image])
                    errors.update(delete)
                # If any error not exit then data has been sent
                try:
                    where = {"category_id": category_id}
                    which = {"$set": request_data}
                    send_data = collection.update_one(where, which)
                except Exception as e:
                    errors['data_edit'] = "Data not sent. Estimated cause database connection error"
                    errors['date_edit_exception'] = str(e)
                else:
                    return_object['status'] = 1
                    messages['category_edit_message'] = 'Changed has been changed'
        return return_object

    def edit_item(self, request):
        errors = {}
        messages = {}
        return_object = {
            "status": 0,
            "error": errors,
            "message": messages
        }
        request_data = ""
        common = self.common
        try:

            model = Item()
            collection = model.collection_name()
            item_action = ItemAction(request)
            item_id = request.POST['item_id']

            # validation
            validation = item_action.data_validation()
            errors.update(validation['errors'])
            messages.update(validation['messages'])
            if "request_data" in validation:
                request_data = validation['request_data']
                compare = item_action.compare(request_data)
                errors.update(compare['errors'])
                messages.update(compare['messages'])

                # bar code update or not
                before_bar_code = request_data['before_bar_code']
                now_bar_code = request.POST['bar_code']
                # common.preview(before_bar_code)
                # return return_object
                if before_bar_code:
                    if 'code' in before_bar_code:
                        before_code = before_bar_code['code']
                        if before_code != now_bar_code:
                            if now_bar_code:
                                existence = common.collection_data_one("items", {"bar_code.code": now_bar_code})
                                if not existence:
                                    bar_code_generate = common.barcode_generator(now_bar_code)
                                    bar_code_ob = bar_code_generate['bar_code']
                                    request_data['bar_code'] = bar_code_ob
                                    try:
                                        delete = common.file_delete([before_bar_code['image']])
                                    except Exception:
                                        errors['before_bar_delete'] = "Before barcode image not deleted"
                                else:
                                    messages['bar_code_exist'] = "This barcode already exist"
                            else:
                                if before_bar_code:
                                    try:
                                        delete = common.file_delete([before_bar_code['image']])
                                    except Exception:
                                        errors['before_bar_delete'] = "Before barcode image not deleted"
                        else:
                            request_data['bar_code'] = before_bar_code
                else:
                    if now_bar_code:
                        existence = common.collection_data_one("items", {"bar_code.code": now_bar_code})
                        if not existence:
                            bar_code_generate = common.barcode_generator(now_bar_code)
                            bar_code_ob = bar_code_generate['bar_code']
                            request_data['bar_code'] = bar_code_ob

                        else:
                            messages['bar_code_exist'] = "This barcode already exist"
                if "before_bar_code" in request_data:
                    del request_data['before_bar_code']

        except Exception as e:
            errors['edit_item_exception'] = str(common.error_info())
        else:
            # First check any error available# First check any error available
            if not return_object['error'] and not return_object['message']:
                try:
                    # Upload image. If image upload fail a default image will be upload
                    upload = common.file_upload(request, 'images[]', True)
                    net_images = request.POST['net_images']
                    net_images = json.loads(net_images)

                    delete_images = request.POST['delete_images']
                    delete_images = json.loads(delete_images)
                    net_images.extend(upload)
                    # remove duplicate
                    total_images = list(set(net_images))
                    if total_images:
                        request_data['images'] = total_images

                    delete = common.file_delete(delete_images)
                    # any error give into info object
                    errors.update(delete)

                except Exception as e:
                    errors['upload'] = str(e)
                    pass

                # If any error not exit then data has been sent
                try:
                    where = {"item_id": item_id}
                    which = {"$set": request_data}
                    send_data = collection.update_one(where, which)
                except Exception as e:
                    errors['data_edit'] = "Data not sent. Estimated cause database connection error"
                    errors['date_edit_exception'] = str(e)
                else:
                    return_object['status'] = 1
                    messages['item_edit_message'] = 'Changed has been saved'
        return return_object

    def round_status_change(self, round_id, status):
        errors = {}
        messages = {}
        return_object = {
            "status": 0,
            "error": errors,
            "message": messages
        }
        try:
            model = Round()
            collection = model.collection_name()
            requests = []
            if status:
                upm = UpdateMany({}, {"$set": {"active": 0}})
                requests.append(upm)
            up = UpdateOne({"round_id": round_id}, {"$set": {"active": status}})
            requests.append(up)

        except Exception as e:
            errors['status_change_exception'] = str(e)
        else:
            if not return_object['error'] and not return_object['message']:
                # If any error not exit then data has been sent
                try:
                    update_data = collection.bulk_write(requests)
                except Exception as e:
                    errors['data_edit'] = "Data not sent. Estimated cause database connection error"
                    errors['date_edit_exception'] = str(e)
                else:
                    return_object['status'] = 1
                    messages['round_edit_message'] = 'Changed has been saved'
        return return_object

    def brand_status_change(self, store_id, status):
        errors = {}
        messages = {}
        return_object = {
            "status": 0,
            "error": errors,
            "message": messages
        }
        try:
            model = Brand()
            collection = model.collection_name()

        except Exception as e:
            errors['status_change_exception'] = str(e)
        else:
            if not return_object['error'] and not return_object['message']:
                # If any error not exit then data has been sent
                try:
                    where = {
                        "store_id": store_id,
                    }
                    which = {
                        "$set": {
                            "active": status
                        }
                    }
                    update_data = collection.update_one(where, which)
                except Exception as e:
                    errors['data_edit'] = "Data not sent. Estimated cause database connection error"
                    errors['date_edit_exception'] = str(e)
                else:
                    return_object['status'] = 1
                    messages['brand_edit_message'] = 'Changed has been saved'
        return return_object

    def edit_corporate(self, request):
        errors = {}
        messages = {}
        return_object = {
            "status": 0,
            "error": errors,
            "message": messages
        }
        request_data = ""
        try:
            common = self.common
            model = Corporate()
            collection = model.collection_name()
            item_action = CorporateAction(request)
            corporate_id = request.POST['corporate_id']

            # validation
            validation = item_action.data_validation()
            errors.update(validation['errors'])
            messages.update(validation['messages'])
            if "request_data" in validation:
                request_data = validation['request_data']
                compare = item_action.compare(request_data)
                errors.update(compare['errors'])
                messages.update(compare['messages'])

        except Exception as e:
            errors['edit_corporate_exception'] = str(e)
        else:
            # First check any error available# First check any error available
            if not return_object['error'] and not return_object['message']:
                try:
                    # Upload image. If image upload fail a default image will be upload
                    upload = common.file_upload(request, 'images[]', True)
                    net_images = request.POST['net_images']
                    net_images = json.loads(net_images)

                    delete_images = request.POST['delete_images']
                    delete_images = json.loads(delete_images)
                    net_images.extend(upload)
                    # remove duplicate
                    total_images = list(set(net_images))
                    if total_images:
                        request_data['images'] = total_images

                    delete = common.file_delete(delete_images)
                    # any error give into info object
                    errors.update(delete)

                except Exception as e:
                    errors['upload'] = str(e)
                    pass

                # If any error not exit then data has been sent
                try:
                    where = {"corporate_id": corporate_id}
                    which = {"$set": request_data}
                    send_data = collection.update_one(where, which)
                except Exception as e:
                    errors['data_edit'] = "Data not sent. Estimated cause database connection error"
                    errors['date_edit_exception'] = str(e)
                else:
                    return_object['status'] = 1
                    messages['corporate_edit_message'] = 'Changed has been saved'
        return return_object

    def edit_my_item(self, request):
        errors = {}
        messages = {}
        return_object = {
            "status": 0,
            "error": errors,
            "message": messages
        }
        request_data = ""
        common = self.common
        try:
            model = StoreItem()
            collection = model.collection_name()
            try:
                store_id = request.session['store_id']
            except Exception:
                errors['login_required'] = "Select a store first"
                return return_object

            this_request = request.POST
            store_item_id = this_request['store_item_id']
            now_bar_code = this_request['bar_code']
            purchase = this_request['purchase']
            price = this_request['price']
            sale = this_request['sale']
            try:
                purchase = float(purchase)
            except ValueError:
                messages['invalid_purchase'] = "Enter a valid numeric purchase"
            try:
                price = float(price)
                if price < 1:
                    messages['zero_amount'] = "Zero price not allow"
            except ValueError:
                messages['invalid_price'] = "Enter a valid numeric price"
            try:
                sale = float(sale)
            except ValueError:
                messages['invalid_sale'] = "Enter a valid nemeric sale"

            request_data = {
                "purchase": purchase,
                "price": price,
                "sale": sale
            }
            # specifications
            specifications = this_request.getlist("specification_name[]")
            tags = this_request.getlist("tags[]")
            i = 0
            if specifications:
                specifications_list = {}
                for sp in specifications:
                    if sp:
                        specifications_list[sp] = tags[i]
                        i += 1
                if specifications_list:
                    request_data['specifications'] = specifications_list

            # bar code update or not
            if now_bar_code:
                this_info = common.collection_data_one("store_items", {"store_item_id": store_item_id})
                try:
                    before_bar_code = this_info['bar_code']
                except Exception:
                    existence = common.collection_data_one("store_items", {"bar_code.code": now_bar_code, "store_id": store_id})
                    if not existence:
                        bar_code_generate = common.barcode_generator(now_bar_code)
                        bar_code_ob = bar_code_generate['bar_code']
                        request_data['bar_code'] = bar_code_ob

                    else:
                        messages['bar_code_exist'] = "This barcode already exist"
                else:
                    before_code = before_bar_code['code']
                    if before_code != now_bar_code:
                        existence = common.collection_data_one("store_items", {"bar_code.code": now_bar_code, "store_id": store_id})
                        if not existence:
                            bar_code_generate = common.barcode_generator(now_bar_code)
                            bar_code_ob = bar_code_generate['bar_code']
                            request_data['bar_code'] = bar_code_ob
                            try:
                                delete = common.file_delete([before_bar_code['image']])
                            except Exception:
                                errors['before_bar_delete'] = "Before barcode image not deleted"
                        else:
                            messages['bar_code_exist'] = "This barcode already exist"
            # compare  this object
            compare = model.compare(request_data)
            if not compare:
                errors['compare_edit_my_item'] = "My items data compare faild!"

        except Exception as e:
            errors['edit_item_exception'] = str(common.error_info())
        else:
            # First check any error available# First check any error available
            if not return_object['error'] and not return_object['message']:
                # If any error not exit then data has been sent
                try:
                    where = {"store_item_id": store_item_id}
                    which = {"$set": request_data}

                    send_data = collection.update_one(where, which)
                except Exception as e:
                    errors['data_edit'] = "Data not sent. Estimated cause database connection error"
                    errors['date_edit_exception'] = str(e)
                else:
                    return_object['status'] = 1
                    messages['my_item_edit_message'] = 'Changed has been saved'
        return return_object

    def update_stock(self, request):
        errors = {}
        messages = {}
        return_object = {
            "status": 0,
            "error": errors,
            "message": messages
        }
        common = ""
        try:
            common = self.common
            model = StoreItem()
            store_item_action = StoreItemAction(request)
            collection = model.collection_name()
            store_id = store_item_id = id_maker = request_data = vendor_id = ""
            item_point = 0
            store_item_id = request.POST['store_item_id']
            # Store id available
            try:
                store_id = request.session['store_id']
            except Exception:
                messages['undefined_store'] = "Select a store first"

            try:
                logged_user_id = request.session['logged_user_id']
            except:
                messages['login_undefined'] = "Login first"
            else:
                id_type_name = common.id_type_name(logged_user_id)
                if id_type_name == "vendor":
                    vendor_id = logged_user_id
                elif id_type_name == "admin" or id_type_name == "employee":
                    try:
                        vendor_id = request.session['vendor_id']
                    except:
                        errors['undefined_vendor'] = "Undefined vendor"

            # validation
            validation = store_item_action.data_validation()
            errors.update(validation['errors'])
            messages.update(validation['messages'])
            if "request_data" in validation:
                request_data = validation['request_data']
                # item point for charge point balance
                item_point = request_data['point']
                # instant before quantity
                bef_info = common.collection_data_one("store_items", {"store_item_id": store_item_id})
                if bef_info:
                    bef_quantity = bef_info['quantity']
                    if request_data['quantity']:
                        request_data['quantity'] = bef_quantity + request_data['quantity']
                else:
                    errors['before_info'] = "Before information not found this store item"
                compare = store_item_action.compare(request_data)
                errors.update(compare['errors'])
                messages.update(compare['messages'])

        except Exception as e:
            errors['update_stock_exception'] = str(common.error_info())

        else:
            # First check any error available# First check any error available
            if not return_object['error'] and not return_object['message']:
                try:
                    # update stock item information
                    where = {"store_item_id": store_item_id}
                    which = {"$set": request_data}
                    send = collection.update_one(where, which)
                    if not send:
                        errors['store_item_data_send'] = "Data not send"
                    else:
                        return_object['status'] = 1
                        return_object['store_item_id'] = store_item_id
                except Exception as e:
                    errors['store_item_exception'] = str(e)
        return return_object

    def edit_point_rule(self, request):
        errors = {}
        messages = {}
        return_object = {
            "status": 0,
            "error": errors,
            "message": messages
        }
        common = ""
        try:
            common = self.common
            model = PointRule()
            collection = model.collection_name()
            this_request = request.POST
            rule_id = this_request['id']
            amount = this_request['amount']
            # Check point if number
            try:
                logged_user = request.session['logged_user_id']
            except:
                messages['undefined_user'] = "Login first"
            try:
                amount = float(amount)
            except Exception:
                messages['invalid_point'] = "Enter a valid numeric amount"

            # validation
            request_data = {
                "amount": amount
            }
            # Compare this request data with rule model
            compare = model.compare(request_data)
            if not compare:
                errors['compare_error'] = "Rule data not compared"

        except Exception as e:
            errors['update_rule_exception'] = str(common.error_info())
        else:
            # First check any error available
            if not return_object['error'] and not return_object['message']:
                try:
                    where = {"id": rule_id}
                    which = {"$set": request_data}
                    send = collection.update_one(where, which)
                    if not send:
                        errors['rule_data_send'] = "Data not send"
                    else:
                        return_object['status'] = 1
                        return_object['rule_id'] = rule_id
                except Exception as e:
                    errors['update_rule_exception'] = str(e)
        return return_object

    def edit_prize(self, request):
        errors = {}
        messages = {}
        return_object = {
            "status": 0,
            "error": errors,
            "message": messages
        }
        request_data = ""
        try:
            common = self.common
            model = Prize()
            collection = model.collection_name()
            prize_action = PrizeAction(request)
            prize_id = request.POST['prize_id']
            # validation
            validation = prize_action.data_validation()
            errors.update(validation['errors'])
            messages.update(validation['messages'])
            if "request_data" in validation:
                request_data = validation['request_data']
                compare = prize_action.compare(request_data)
                errors.update(compare['errors'])
                messages.update(compare['messages'])
            else:
                errors['request_data'] = "Request data generate problem"

        except Exception as e:
            errors['edit_category_exception'] = str(e)
        else:
            # First check any error available# First check any error available
            if not return_object['error'] and not return_object['message']:

                # If any error not exit then data has been sent
                try:
                    where = {"prize_id": prize_id}
                    which = {"$set": request_data}
                    send_data = collection.update_one(where, which)
                except Exception as e:
                    errors['data_edit'] = "Data not sent. Estimated cause database connection error"
                    errors['date_edit_exception'] = str(e)
                else:
                    return_object['status'] = 1
                    messages['prize_edit_message'] = 'Changed has been changed'
        return return_object

    def document_update(self, request):
        errors = {}
        messages = {}
        return_object = {
            "status": 0,
            "error": errors,
            "message": messages
        }
        request_data = collection_name = which = where = ""
        try:
            common = self.common
            update_document = request.POST['update_document']
            document = json.loads(update_document)
            login = common.login_validation(request)
            messages.update(login)
            if document:
                try:
                    collection_name = document['collection_name']
                    which = document['which']
                    where = document['where']
                except:
                    errors['document_object'] = str(common.error_info())

        except Exception as e:
            errors['status_exchange_exception'] = str(e)
        else:
            # First check any error available# First check any error available
            if not return_object['error'] and not return_object['message']:
                # If any error not exit then data has been sent
                try:
                    where = where
                    which = {"$set": which}
                    send_data = common.db[collection_name].update_one(where, which)
                except Exception as e:
                    errors['data_edit'] = "Data not sent. Estimated cause database connection error"
                    errors['date_edit_exception'] = str(e)
                else:
                    return_object['status'] = 1
                    messages['prize_edit_message'] = 'Changed has been changed'
        return return_object

    def edit_expire_date_rule(self, request):
        errors = {}
        messages = {}
        return_object = {
            "status": 0,
            "error": errors,
            "message": messages
        }
        rules = []
        try:
            common = self.common
            this_request = request.POST
            slab_amounts = this_request.getlist("slab_amount[]")
            days = this_request.getlist("days[]")
            hours = this_request.getlist("hours[]")
            minutes = this_request.getlist("minutes[]")
            i = 0
            if slab_amounts:
                rules = []
                for sa in slab_amounts:
                    try:
                        sa = float(sa)
                    except ValueError:
                        messages['invalid_amount'] = "Enter a valid numeric slab amount"
                    else:
                        day = days[i]
                        hour = hours[i]
                        minute = minutes[i]
                        # validation for real value of day
                        if day:
                            try:
                                day = int(day)
                            except ValueError:
                                messages['invalid_hour'] = "Enter a valid numeric day"
                        else:
                            day = 0

                        # validation for real value of hour
                        if hour:
                            try:
                                hour = int(hour)
                            except ValueError:
                                messages['invalid_hour'] = "Enter a valid numeric hour"
                        else:
                            hour = 0

                        # validation for real value of minute
                        if minute:
                            try:
                                minute = int(minute)
                            except ValueError:
                                messages['invalid_hour'] = "Enter a valid numeric minute"
                        else:
                            minute = 0

                        sa_object = {
                            "amount": sa,
                            "expire": {
                                "day": day,
                                "hour": hour,
                                "minute": minute,
                            }
                        }
                        rules.append(sa_object)
                        i += 1

        except Exception as e:
            errors['status_exchange_exception'] = str(e)
        else:
            # First check any error available# First check any error available
            if not return_object['error'] and not return_object['message']:
                # If any error not exit then data has been sent
                try:
                    if rules:
                        update = common.assign_last_update("expire_date_rules", "", {"value": rules})
                    else:
                        messages['rules empty'] = "Enter a complete rule info"
                except Exception as e:
                    errors['data_edit'] = "Data not sent. Estimated cause database connection error"
                    errors['date_edit_exception'] = str(e)
                else:
                    return_object['status'] = 1
                    messages['expire_date_rules_edit_message'] = 'Changed has been changed'
        return return_object

    def edit_distributed_point_slab(self, request):
        errors = {}
        messages = {}
        return_object = {
            "status": 0,
            "error": errors,
            "message": messages
        }
        rules = []
        try:
            common = self.common
            this_request = request.POST
            slab_amounts = this_request.getlist("slab_amount[]")
            i = 0
            if slab_amounts:
                rules = []
                for sa in slab_amounts:
                    try:
                        sa = float(sa)
                    except ValueError:
                        messages['invalid_amount'] = "Enter a valid numeric slab amount"
                    else:
                        rules.append({"amount": sa})
                        i += 1

        except Exception as e:
            errors['distribution_point_exception'] = str(e)
        else:
            # First check any error available# First check any error available
            if not return_object['error'] and not return_object['message']:
                # If any error not exit then data has been sent
                try:
                    if rules:
                        update = common.assign_last_update("distributed_point_slabs", "", {"value": rules})
                    else:
                        messages['rules empty'] = "Enter a complete rule info"
                except Exception as e:
                    errors['data_edit'] = "Data not sent. Estimated cause database connection error"
                    errors['date_edit_exception'] = str(e)
                else:
                    return_object['status'] = 1
                    messages['distribution_edit_message'] = 'Changed has been changed'
        return return_object




    def exchange_tree_position(self, request):
        errors = {}
        messages = {}
        return_object = {
            "status": 0,
            "error": errors,
            "message": messages
        }
        request_data = collection_name = which = where = ""
        requests = []
        try:
            model = Tree()
            collection = model.collection_name()
            common = self.common
            update_document = request.POST['data']
            document = json.loads(update_document)
            login = common.login_validation(request)
            messages.update(login)
            if document:
                try:
                    for x in document:

                        collection_name = document
                        which = {
                            "$set": {
                                "position": x['position']
                            }
                        }
                        where = {
                            "user_id": x['user_id']
                        }
                        requests.append(UpdateOne(where, which))
                    common.preview(requests)
                except:
                    errors['document_object'] = str(common.error_info())
            else:
                errors['document_object'] = "document data not found"
        except Exception as e:
            errors['status_exchange_exception'] = str(e)
        else:
            # First check any error available# First check any error available
            if not return_object['error'] and not return_object['message']:
                # If any error not exit then data has been sent
                try:
                    collection.bulk_write(requests)
                except Exception as e:
                    errors['data_edit'] = "Data not sent. Estimated cause database connection error"
                    errors['date_edit_exception'] = str(e)
                else:
                    return_object['status'] = 1
                    messages['exchange_edit_message'] = 'Changed has been changed'
        return return_object

    def update_feature_setting(self, request):
        errors = {}
        messages = {}
        return_object = {
            "status": 0,
            "error": errors,
            "message": messages
        }
        common = self.common
        update_object = {}
        try:
            try:
                logged_user_id = request.session['logged_user_id']
            except:
                messages['login_required'] = "Login first"
            this_request = request.POST
            # validation for position cost
            try:
                amount = this_request['amount']
                try:
                    amount = float(amount)
                except:
                    messages['invalid_amount'] = "Enter a valid numeric amount"
            except:
                pass
            # validation for position expire time
            try:
                day = this_request['day']
                try:
                    day = int(day)
                except:
                    messages['invalid_day'] = "Enter a valid numeric day"
            except:
                pass
            try:
                hour = this_request['hour']
                try:
                    hour = int(hour)
                except:
                    messages['invalid_hour'] = "Enter a valid numeric hour"
            except:
                pass
            try:
                minute = this_request['minute']
                try:
                    minute = int(minute)
                except:
                    messages['invalid_minute'] = "Enter a valid numeric minute"
            except:
                pass

            try:
                update_object = json.loads(this_request['update_object'], cls=Decoder)
            except:
                errors['update_object'] = "Update object not found"
                errors['error_info'] = str(common.error_info())

            data_type = update_object['type']
            if data_type == 'logo' or data_type == 'login_banner' or data_type == 'login_banner_back_end':
                try:
                    my_file = request.FILES['image']
                    if my_file:
                        # Upload image. If image upload fail a default image will be upload
                        upload = common.file_upload(request, 'image')
                        if upload:
                            update_object['more_data']['value'] = upload
                            software_info = self.retrieve.software_settings()

                            # delete before image
                            before_image = software_info[data_type]
                            delete = common.file_delete([before_image])
                            errors.update(delete)

                except:
                    messages['empty_image'] = "Please select a image."



        except Exception as e:
            errors['feature_setting_exception'] = str(common.error_info())
        else:
            # First check any error available# First check any error available
            if not return_object['error'] and not return_object['message']:
                # If any error not exit then data has been sent
                try:
                    assign = ""
                    if update_object:
                        info_type = update_object['type']
                        option = update_object['option']
                        more_data = update_object['more_data']
                        assign = common.assign_last_update(info_type, option, more_data)
                        # assign = common.assign_last_update("position_cost", "", {"amount": amount})
                except Exception as e:
                    errors['data_edit'] = "Data not sent. Estimated cause database connection error"
                    errors['date_edit_exception'] = str(e)
                else:
                    return_object['status'] = 1
                    messages['feature_setting_message'] = 'Changed has been changed'
                    if not assign:
                        messages['unknown_error'] = "Update fail"
                        return_object['status'] = 0
        return return_object

    def order_status_change(self, request, order_id, status):
        errors = {}
        messages = {}
        return_object = {
            "status": 0,
            "error": errors,
            "message": messages
        }
        vendor_model = Controller()
        vendor_collection = vendor_model.collection_name()
        common = self.common
        logged_user_id = vendor_id = customer_id = ""
        total_point = vendor_new_point = total_amount = 0
        items = []
        try:
            # if order id and status data have
            if status and order_id:
                try:
                    customer_id = request.POST['customer_id']
                except:
                    pass

                try:
                    items = json.loads(request.POST['items'])
                except:
                    errors['data_error'] = "Data error"
                    return return_object
                else:
                    for x in items:
                        total_point += float(x['point'])
                        total_amount += float(x['line_total'])
            else:
                messages['data_undefined'] = "Data not found"
                return return_object
            # try to fine vendor information and check the balance for order status change possibility
            try:
                logged_user_id = request.session['logged_user_id']
            except:
                messages['login_required'] = "Login first"

            try:
                id_type_name = common.id_type_name(logged_user_id)
                if id_type_name == "admin":
                    try:
                        vendor_id = request.session['vendor_id']
                    except:
                        messages['vendor_undefined'] = "Select vendor first"
                elif id_type_name == "vendor":
                    vendor_id = logged_user_id
                elif id_type_name == "employee":
                    try:
                        vendor_id = request.session['vendor_id']
                    except:
                        messages['vendor_undefined'] = "Login first"
                        return return_object

                if status == "Completed":
                    if total_amount:
                        if vendor_id:
                            # update vendor account balance

                            where = {
                                "user_id": vendor_id
                            }
                            which = {
                                "$inc": {
                                    "account_balance": total_amount
                                }
                            }
                            try:
                                vendor_collection.update_one(where, which)
                            except:
                                errors['vendor_balance'] = "Vendor balance not updated"

                        else:
                            messages['login_required'] = "Login first"
            except:
                pass

        except Exception as e:
            errors['change_order_status_exception'] = str(common.error_info())
        else:
            # First check any error available
            if not return_object['error'] and not return_object['message']:
                try:
                    # update status of order items
                    order_item_model = OrderItems()
                    order_item_collection = order_item_model.collection_name()
                    requests = []
                    for x in items:
                        store_item_id = x['store_item_id']
                        where = {
                            "store_item_id": store_item_id,
                            "order_id": order_id
                        }
                        which = {
                            "$set": {
                                "status": status
                            }
                        }
                        requests.append(UpdateOne(where, which))
                    order_item_collection.bulk_write(requests)
                except Exception as e:
                    errors['data_edit'] = "Data not sent. Estimated cause database connection error"
                    errors['date_edit_exception'] = str(e)
                else:
                    return_object['status'] = 1
                    messages['order_status_message'] = 'Changed has been changed'

        return return_object

    def all_user_clear(self):
        errors = {}
        messages = {}
        return_object = {
            "status": 0,
            "error": errors,
            "message": messages
        }
        common = self.common
        try:
            id_type = common.id_type("user")
            # delete all users from controllers
            query = {"user_id": {"$regex": "^" + id_type}}
            delete_users = common.document_delete("controllers", query)

            # delete all tree documents
            delete_trees = common.document_delete("tree", {})

            # update last total user amount
            assign_last = common.assign_last_update("user", "", {"amount": 0})

            # delete all orders documents
            delete_orders = common.document_delete("orders", {})
            delete_order_items = common.document_delete("order_items", {})
            # update last total order amount
            assign_last = common.assign_last_update("order", "", {"amount": 0})
            # delete all user transactions
            delete_transactions = common.document_delete("transactions", {})
            # update last total transaction amount
            assign_last = common.assign_last_update("transaction", "", {"amount": 0})
            # check total amount
            total_amount = self.retrieve.imported_user_info()
        except:
            errors['error_info'] = str(common.error_info())
        else:
            return_object['status'] = 1
            return_object['total_user'] = total_amount['total']
        return return_object

    def new_password(self, request):
        errors = {}
        messages = {}
        return_object = {
            "status": 0,
            "error": errors,
            "message": messages
        }
        common = self.common
        user_id = ""
        try:
            try:
                conf_object = request.session['confirmation']
                user_id = conf_object['user_id']
            except:
                errors['invalid_user'] = "Invalid user"
            new_password = request.POST['new_password']
            if len(new_password) < 8:
                messages['password_length'] = "Password length at least 8"
            repeat_password = request.POST['repeat_password']
            if new_password != repeat_password:
                messages['password_matched'] = "Password not matched"

        except:
            errors['error_info'] = str(common.error_info())
        else:
            # First check any error available# First check any error available
            if not return_object['error'] and not return_object['message']:
                try:
                    controller_model = Controller()
                    collection = controller_model.collection_name()
                    where = {
                        "user_id": user_id
                    }
                    which = {
                        "$set": {
                            "password": common.md5(new_password)
                        }
                    }
                    update_password = collection.update_one(where, which)
                except:
                    errors['error_info'] = str(common.error_info())
                else:
                    return_object['status'] = 1
        return return_object

    def transaction_status_change(self, request, status, transaction_id):
        errors = {}
        messages = {}
        return_object = {
            "status": 0,
            "error": errors,
            "message": messages
        }
        common = self.common
        user_id = ""
        rule_amount = 0
        try:
            if status == "" and transaction_id == "":
                errors['data_empty'] = "Data empty"
                return return_object
            try:
                status = int(status)
            except:
                errors['unknown_status'] = "Unknown status"
            try:
                logged_user_id = request.session['logged_user_id']
            except:
                messages['login_required'] = "Login first"

            # First check any error available
            if return_object['error'] and return_object['message']:
                return return_object

            transaction_info = common.collection_data_one("transactions", {"transaction_id": transaction_id})
            if not transaction_info:
                messages['invalid'] = "Invalid Transaction id"
            try:
                transaction_type = transaction_info['type']
                if transaction_type == "vendor_top_up":
                    point_rule = common.collection_data_one("point_rules",{"id": "3"})
                    if not point_rule:
                        messages['point_rule'] = "Point rule not set"
                        return return_object
                    else:
                        rule_amount = point_rule['amount']
            except:
                errors['error_info'] = str(common.error_info())
            # First check any error available
            # if not return_object['error'] and not return_object['message']:

        except:
            errors['error_info'] = str(common.error_info())
        else:
            # First check any error available# First check any error available
            if not return_object['error'] and not return_object['message']:
                try:
                    transaction_type = transaction_info['type']
                    user_id = transaction_info['user_id']
                    amount = transaction_info['amount']
                    if status == 1:
                        tree_model = Tree()
                        tree_collection = tree_model.collection_name()
                        controller_model = Controller()
                        controller_collection = controller_model.collection_name()

                        # if transaction top up
                        if transaction_type == "top_up":
                            where = {
                                "user_id": user_id
                            }
                            which = {
                                "$inc": {
                                    "account_balance": amount
                                }
                            }
                            tree_collection.update_one(where, which)
                        # if transaction user withdraw
                        elif transaction_type == "user_withdraw":
                            amount = amount - (amount + amount)
                            where = {
                                "user_id": user_id
                            }
                            which = {
                                "$inc": {
                                    "account_balance": amount
                                }
                            }
                            tree_collection.update_one(where, which)

                        elif transaction_type == "vendor_withdraw":
                            amount = amount - (amount + amount)
                            where = {
                                "user_id": user_id
                            }
                            which = {
                                "$inc": {
                                    "account_balance": amount
                                }
                            }
                            controller_collection.update_one(where, which)

                        elif transaction_type == "vendor_top_up":
                            if not rule_amount:
                                messages['rule_amount'] = "Rule amount not set"
                                return return_object
                            amount = amount * rule_amount
                            where = {
                                "user_id": user_id
                            }
                            which = {
                                "$inc": {
                                    "point_balance": amount
                                }
                            }
                            controller_collection.update_one(where, which)

                except:
                    errors['error_info'] = str(common.error_info())
                else:
                    try:
                        transaction_model = Transaction()
                        transaction_collection = transaction_model.collection_name()
                        where = {
                            "transaction_id": transaction_id
                        }
                        set_option = {
                            "status": status
                        }
                        which = {
                            "$set": set_option
                        }
                        more_info = {}
                        try:
                            transaction_number = request.POST['transaction_number']
                        except:
                            pass
                        else:
                            more_info['transaction_number'] = transaction_number
                        try:
                            note = request.POST['note']
                        except:
                            pass
                        else:
                            more_info['note'] = note
                        if more_info:
                            set_option['more_info'] = more_info
                        transaction_collection.update_one(where, which)
                    except:
                        errors['error_info'] = str(common.error_info())
                    else:
                        return_object['status'] = 1
        return return_object

    def document_delete(self,collection_name,object_id):
        errors = {}
        messages = {}
        return_object = {
            "status": 0,
            "error": errors,
            "message": messages
        }

        query = {
            "_id": ObjectId(object_id)
        }
        try:
            self.common.document_delete_one(collection_name,query)
        except:
            errors['error_info'] = self.common.error_info()
        else:
            return_object['status'] = 1
        return return_object

    def drop_collection(self, collection_name):
        errors = {}
        messages = {}
        return_object = {
            "status": 0,
            "error": errors,
            "message": messages
        }
        try:
            self.db[collection_name].drop()
        except:
            errors['error_info'] = self.common.error_info()
        else:
            return_object['status'] = 1
        return return_object
