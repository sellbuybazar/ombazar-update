<?php require_once "support.php"?>
{{#each rows}}
<li  class="uk-width-medium-8-10 " data-id="{{id}}">
    {{#each coupons}}
    <div  class="single-coupon-area">
        <div class="md-card single-coupon relative">
            <div class="processing-bar"></div>
            <form class="class-unlock-form" id="form">
                <input type="hidden" name="group_id" value="{{id}}">
                <div class="coupon-bar crach-bar {{#if status}} hidden {{/if}}">
                    <div class="md-btn md-btn-flat-primary coupon-button">
                        <label>Crach your coupon</label>
                    </div>
                    <div class="coupon-toolbar">
                        <i class="md-icon material-icons coupon-open">&#xE5D4;</i>
                    </div>
                </div>
                <div class="coupon-bar mobile-number-bar hidden">
                    <div class="coupon-input-field parsley-row">
                        <label>Enter Mobile number</label>
                        <input type="text" class="md-input" name="mobile_number" required>
                    </div>
                    <div class="coupon-toolbar">
                        <i class="md-icon material-icons coupon-next">navigate_next</i>
                        <i class="md-icon material-icons coupon-close">close</i>
                    </div>
                </div>
                <div class="coupon-bar input-bar hidden">
                    <div class="coupon-input-field parsley-row">
                        <label>Enter coupon number</label>
                        <input type="text" class="md-input" name="coupon_number" required>
                    </div>
                    <div class="coupon-toolbar">
                        <button class="md-icon material-icons coupon-submit">lock_open</button>
                        <i class="md-icon material-icons coupon-close">close</i>
                    </div>
                </div>
                <div class="coupon-bar coupon-message {{#if status}} {{else}}hidden{{/if}}">
                    <div class="coupon-input-field ">
                        {{#if status}} <span class="used-text">Already used ({{mobile}})</span> {{/if}}
                    </div>
                    <div class="coupon-toolbar">
                    </div>
                </div>
            </form>
        </div>
    </div>
    {{/each}}
</li>
{{/each}}
