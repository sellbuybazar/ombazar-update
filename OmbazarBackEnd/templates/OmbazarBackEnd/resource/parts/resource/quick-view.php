<?php
    require_once "helper.php";
    $path=$_REQUEST['path'];
?>
<div class="jas-row mb__50 uk-padding uk-padding-remove-left" >
    <div class="jas-col-md-6 jas-col-sm-6 jas-col-xs-12 pr">
        <div class="badge tu tc fs__12 ls__2">

        </div>
        <div class="single-product-thumbnail pr left">
            <div class="p-thumb images woocommerce-product-gallery jas-carousel"
                 data-slick='{"slidesToShow": 1, "slidesToScroll": 1, "asNavFor": ".p-nav", "fade":true}'>

                <div class="p-item woocommerce-product-gallery__image jas-image-zoom"><a
                            href="<?php echo $path; ?>assets/img/images/image.jpg"><img
                                width="600" height="600"
                                src="<?php echo $path; ?>assets/img/images/image.jpg"
                                class="attachment-shop_single size-shop_single" alt="" title=""
                                data-caption=""

                                data-large_image="<?php echo $path; ?>assets/img/images/image.jpg"
                                data-large_image_width="800" data-large_image_height="800"
                        /></a></div>
                <div class="p-item woocommerce-product-gallery__image jas-image-zoom"><a
                            href="<?php echo $path; ?>assets/img/images/image2.jpg"><img
                                width="600" height="600"
                                src="<?php echo $path; ?>assets/img/images/image2.jpg"
                                class="attachment-shop_single size-shop_single" alt="" title=""
                                data-caption=""
                                data-src="<?php echo $path; ?>assets/img/images/image2.jpg"
                                data-large_image="<?php echo $path; ?>assets/img/images/image2.jpg"
                                data-large_image_width="800" data-large_image_height="800"
                        /></a></div>
            </div>

            <div class="p-nav oh jas-carousel"
                 data-slick='{"slidesToShow": 4,"slidesToScroll": 1,"asNavFor": ".p-thumb","arrows": false, "focusOnSelect": true, "vertical": true,  "responsive":[{"breakpoint": 736,"settings":{"slidesToShow": 4, "vertical":false}}]}'>
                <div><img width="100" height="100"
                          src="<?php echo $path; ?>assets/img/images/image.jpg"
                          class="attachment-shop_thumbnail size-shop_thumbnail wp-post-image" alt=""
                          title="s2"
                          data-src="http://localhost/e-theme/wp-content/uploads/2017/01/s2.jpg"
                          data-large_image="http://localhost/e-theme/wp-content/uploads/2017/01/s2.jpg"
                          data-large_image_width="900" data-large_image_height="1200"/></div>

                <div><img width="100" height="100"
                          src="<?php echo $path; ?>assets/img/images/image2.jpg"
                          class="attachment-shop_thumbnail size-shop_thumbnail" alt="" title=""
                          data-caption=""
                          data-src="http://localhost/e-theme/wp-content/uploads/2017/01/222.jpg"
                          data-large_image="http://localhost/e-theme/wp-content/uploads/2017/01/222.jpg"
                          data-large_image_width="800" data-large_image_height="1100"/></div>
            </div>
        </div>
    </div>

    <div class="jas-col-md-6 jas-col-sm-6 jas-col-xs-12">
        <div class="summary entry-summary">
            <h1 class="product_title entry-title">Cuffed Beanie</h1>
            <div class="flex between-xs middle-xs price-review">
                <p class="price"><span class="woocommerce-Price-amount amount"><span
                                class="woocommerce-Price-currencySymbol">&#36;</span>15.00</span>
                </p>
            </div>
            <div class="woocommerce-product-details__short-description">
                <p>Go sporty this summer with this vintage navy and white striped v-neck t-shirt
                    from the Nike. Perfect for pairing with denim and white kicks for a stylish
                    sporty vibe.</p>
            </div>
            <div class="btn-atc atc-popup">
                <form class="variations_form cart" method="post" enctype='multipart/form-data'
                      data-product_id="5269"
                      data-galleries="{&quot;default_gallery&quot;:[{&quot;single&quot;:&quot;http:\/\/localhost\/e-theme\/wp-content\/uploads\/2018\/06\/111-150x150.jpg&quot;,&quot;single_w&quot;:370,&quot;single_h&quot;:493,&quot;thumbnail&quot;:&quot;http:\/\/localhost\/e-theme\/wp-content\/uploads\/2018\/06\/111-100x100.jpg&quot;,&quot;catalog&quot;:&quot;http:\/\/localhost\/e-theme\/wp-content\/uploads\/2018\/06\/111-150x150.jpg&quot;,&quot;data-src&quot;:&quot;http:\/\/localhost\/e-theme\/wp-content\/uploads\/2018\/06\/111-150x150.jpg&quot;,&quot;data-large_image&quot;:&quot;http:\/\/localhost\/e-theme\/wp-content\/uploads\/2018\/06\/111-150x150.jpg&quot;,&quot;data-large_image_width&quot;:370,&quot;data-large_image_height&quot;:493},{&quot;single&quot;:&quot;http:\/\/localhost\/e-theme\/wp-content\/uploads\/2018\/06\/img-140.png&quot;,&quot;single_w&quot;:370,&quot;single_h&quot;:493,&quot;thumbnail&quot;:&quot;http:\/\/localhost\/e-theme\/wp-content\/uploads\/2018\/06\/img-140-100x100.png&quot;,&quot;catalog&quot;:&quot;http:\/\/localhost\/e-theme\/wp-content\/uploads\/2018\/06\/img-140.png&quot;,&quot;data-src&quot;:&quot;http:\/\/localhost\/e-theme\/wp-content\/uploads\/2018\/06\/img-140.png&quot;,&quot;data-large_image&quot;:&quot;http:\/\/localhost\/e-theme\/wp-content\/uploads\/2018\/06\/img-140.png&quot;,&quot;data-large_image_width&quot;:370,&quot;data-large_image_height&quot;:493},{&quot;single&quot;:&quot;http:\/\/localhost\/e-theme\/wp-content\/uploads\/2018\/06\/img-141.png&quot;,&quot;single_w&quot;:370,&quot;single_h&quot;:493,&quot;thumbnail&quot;:&quot;http:\/\/localhost\/e-theme\/wp-content\/uploads\/2018\/06\/img-141-100x100.png&quot;,&quot;catalog&quot;:&quot;http:\/\/localhost\/e-theme\/wp-content\/uploads\/2018\/06\/img-141.png&quot;,&quot;data-src&quot;:&quot;http:\/\/localhost\/e-theme\/wp-content\/uploads\/2018\/06\/img-141.png&quot;,&quot;data-large_image&quot;:&quot;http:\/\/localhost\/e-theme\/wp-content\/uploads\/2018\/06\/img-141.png&quot;,&quot;data-large_image_width&quot;:370,&quot;data-large_image_height&quot;:493},{&quot;single&quot;:&quot;http:\/\/localhost\/e-theme\/wp-content\/uploads\/2018\/06\/img-142.png&quot;,&quot;single_w&quot;:370,&quot;single_h&quot;:493,&quot;thumbnail&quot;:&quot;http:\/\/localhost\/e-theme\/wp-content\/uploads\/2018\/06\/img-142-100x100.png&quot;,&quot;catalog&quot;:&quot;http:\/\/localhost\/e-theme\/wp-content\/uploads\/2018\/06\/img-142.png&quot;,&quot;data-src&quot;:&quot;http:\/\/localhost\/e-theme\/wp-content\/uploads\/2018\/06\/img-142.png&quot;,&quot;data-large_image&quot;:&quot;http:\/\/localhost\/e-theme\/wp-content\/uploads\/2018\/06\/img-142.png&quot;,&quot;data-large_image_width&quot;:370,&quot;data-large_image_height&quot;:493}]}"
                      data-product_variations="[{&quot;attributes&quot;:{&quot;attribute_pa_color&quot;:&quot;grey&quot;,&quot;attribute_pa_size&quot;:&quot;&quot;},&quot;availability_html&quot;:&quot;&quot;,&quot;backorders_allowed&quot;:false,&quot;dimensions&quot;:{&quot;length&quot;:&quot;&quot;,&quot;width&quot;:&quot;&quot;,&quot;height&quot;:&quot;&quot;},&quot;dimensions_html&quot;:&quot;N\/A&quot;,&quot;display_price&quot;:15,&quot;display_regular_price&quot;:15,&quot;image&quot;:{&quot;title&quot;:&quot;bobbi_chunky_pom_beanie4&quot;,&quot;caption&quot;:&quot;&quot;,&quot;url&quot;:&quot;http:\/\/localhost\/e-theme\/wp-content\/uploads\/2018\/06\/img-141.png&quot;,&quot;alt&quot;:&quot;&quot;,&quot;src&quot;:&quot;http:\/\/localhost\/e-theme\/wp-content\/uploads\/2018\/06\/img-141.png&quot;,&quot;srcset&quot;:&quot;http:\/\/localhost\/e-theme\/wp-content\/uploads\/2018\/06\/img-141.png 370w, http:\/\/localhost\/e-theme\/wp-content\/uploads\/2018\/06\/img-141-111-300x300.jpg 225w&quot;,&quot;sizes&quot;:&quot;(max-width: 370px) 100vw, 370px&quot;,&quot;full_src&quot;:&quot;http:\/\/localhost\/e-theme\/wp-content\/uploads\/2018\/06\/img-141.png&quot;,&quot;full_src_w&quot;:370,&quot;full_src_h&quot;:493,&quot;gallery_thumbnail_src&quot;:&quot;http:\/\/localhost\/e-theme\/wp-content\/uploads\/2018\/06\/img-141-100x100.png&quot;,&quot;gallery_thumbnail_src_w&quot;:100,&quot;gallery_thumbnail_src_h&quot;:100,&quot;thumb_src&quot;:&quot;http:\/\/localhost\/e-theme\/wp-content\/uploads\/2018\/06\/img-141.png&quot;,&quot;thumb_src_w&quot;:300,&quot;thumb_src_h&quot;:400,&quot;src_w&quot;:370,&quot;src_h&quot;:493},&quot;image_id&quot;:&quot;5275&quot;,&quot;is_downloadable&quot;:false,&quot;is_in_stock&quot;:true,&quot;is_purchasable&quot;:true,&quot;is_sold_individually&quot;:&quot;no&quot;,&quot;is_virtual&quot;:false,&quot;max_qty&quot;:&quot;&quot;,&quot;min_qty&quot;:1,&quot;price_html&quot;:&quot;&quot;,&quot;sku&quot;:&quot;P15&quot;,&quot;variation_description&quot;:&quot;&quot;,&quot;variation_id&quot;:5270,&quot;variation_is_active&quot;:true,&quot;variation_is_visible&quot;:true,&quot;weight&quot;:&quot;&quot;,&quot;weight_html&quot;:&quot;N\/A&quot;},{&quot;attributes&quot;:{&quot;attribute_pa_color&quot;:&quot;pink&quot;,&quot;attribute_pa_size&quot;:&quot;&quot;},&quot;availability_html&quot;:&quot;&quot;,&quot;backorders_allowed&quot;:false,&quot;dimensions&quot;:{&quot;length&quot;:&quot;&quot;,&quot;width&quot;:&quot;&quot;,&quot;height&quot;:&quot;&quot;},&quot;dimensions_html&quot;:&quot;N\/A&quot;,&quot;display_price&quot;:15,&quot;display_regular_price&quot;:15,&quot;image&quot;:{&quot;title&quot;:&quot;bobbi_chunky_pom_beanie2&quot;,&quot;caption&quot;:&quot;&quot;,&quot;url&quot;:&quot;http:\/\/localhost\/e-theme\/wp-content\/uploads\/2018\/06\/111-150x150.jpg&quot;,&quot;alt&quot;:&quot;&quot;,&quot;src&quot;:&quot;http:\/\/localhost\/e-theme\/wp-content\/uploads\/2018\/06\/111-150x150.jpg&quot;,&quot;srcset&quot;:&quot;http:\/\/localhost\/e-theme\/wp-content\/uploads\/2018\/06\/111-150x150.jpg 370w, http:\/\/localhost\/e-theme\/wp-content\/uploads\/2018\/06\/img-139-111-300x300.jpg 225w&quot;,&quot;sizes&quot;:&quot;(max-width: 370px) 100vw, 370px&quot;,&quot;full_src&quot;:&quot;http:\/\/localhost\/e-theme\/wp-content\/uploads\/2018\/06\/111-150x150.jpg&quot;,&quot;full_src_w&quot;:370,&quot;full_src_h&quot;:493,&quot;gallery_thumbnail_src&quot;:&quot;http:\/\/localhost\/e-theme\/wp-content\/uploads\/2018\/06\/111-100x100.jpg&quot;,&quot;gallery_thumbnail_src_w&quot;:100,&quot;gallery_thumbnail_src_h&quot;:100,&quot;thumb_src&quot;:&quot;http:\/\/localhost\/e-theme\/wp-content\/uploads\/2018\/06\/111-150x150.jpg&quot;,&quot;thumb_src_w&quot;:300,&quot;thumb_src_h&quot;:400,&quot;src_w&quot;:370,&quot;src_h&quot;:493},&quot;image_id&quot;:&quot;5273&quot;,&quot;is_downloadable&quot;:false,&quot;is_in_stock&quot;:true,&quot;is_purchasable&quot;:true,&quot;is_sold_individually&quot;:&quot;no&quot;,&quot;is_virtual&quot;:false,&quot;max_qty&quot;:&quot;&quot;,&quot;min_qty&quot;:1,&quot;price_html&quot;:&quot;&quot;,&quot;sku&quot;:&quot;P15&quot;,&quot;variation_description&quot;:&quot;&quot;,&quot;variation_id&quot;:5271,&quot;variation_is_active&quot;:true,&quot;variation_is_visible&quot;:true,&quot;weight&quot;:&quot;&quot;,&quot;weight_html&quot;:&quot;N\/A&quot;},{&quot;attributes&quot;:{&quot;attribute_pa_color&quot;:&quot;black&quot;,&quot;attribute_pa_size&quot;:&quot;&quot;},&quot;availability_html&quot;:&quot;&quot;,&quot;backorders_allowed&quot;:false,&quot;dimensions&quot;:{&quot;length&quot;:&quot;&quot;,&quot;width&quot;:&quot;&quot;,&quot;height&quot;:&quot;&quot;},&quot;dimensions_html&quot;:&quot;N\/A&quot;,&quot;display_price&quot;:15,&quot;display_regular_price&quot;:15,&quot;image&quot;:{&quot;title&quot;:&quot;bobbi_chunky_pom_beanie5&quot;,&quot;caption&quot;:&quot;&quot;,&quot;url&quot;:&quot;http:\/\/localhost\/e-theme\/wp-content\/uploads\/2018\/06\/img-142.png&quot;,&quot;alt&quot;:&quot;&quot;,&quot;src&quot;:&quot;http:\/\/localhost\/e-theme\/wp-content\/uploads\/2018\/06\/img-142.png&quot;,&quot;srcset&quot;:&quot;http:\/\/localhost\/e-theme\/wp-content\/uploads\/2018\/06\/img-142.png 370w, http:\/\/localhost\/e-theme\/wp-content\/uploads\/2018\/06\/img-142-111-300x300.jpg 225w&quot;,&quot;sizes&quot;:&quot;(max-width: 370px) 100vw, 370px&quot;,&quot;full_src&quot;:&quot;http:\/\/localhost\/e-theme\/wp-content\/uploads\/2018\/06\/img-142.png&quot;,&quot;full_src_w&quot;:370,&quot;full_src_h&quot;:493,&quot;gallery_thumbnail_src&quot;:&quot;http:\/\/localhost\/e-theme\/wp-content\/uploads\/2018\/06\/img-142-100x100.png&quot;,&quot;gallery_thumbnail_src_w&quot;:100,&quot;gallery_thumbnail_src_h&quot;:100,&quot;thumb_src&quot;:&quot;http:\/\/localhost\/e-theme\/wp-content\/uploads\/2018\/06\/img-142.png&quot;,&quot;thumb_src_w&quot;:300,&quot;thumb_src_h&quot;:400,&quot;src_w&quot;:370,&quot;src_h&quot;:493},&quot;image_id&quot;:&quot;5276&quot;,&quot;is_downloadable&quot;:false,&quot;is_in_stock&quot;:true,&quot;is_purchasable&quot;:true,&quot;is_sold_individually&quot;:&quot;no&quot;,&quot;is_virtual&quot;:false,&quot;max_qty&quot;:&quot;&quot;,&quot;min_qty&quot;:1,&quot;price_html&quot;:&quot;&quot;,&quot;sku&quot;:&quot;P15&quot;,&quot;variation_description&quot;:&quot;&quot;,&quot;variation_id&quot;:5272,&quot;variation_is_active&quot;:true,&quot;variation_is_visible&quot;:true,&quot;weight&quot;:&quot;&quot;,&quot;weight_html&quot;:&quot;N\/A&quot;}]">

                    <div class="variations">
                    </div>

                    <div class="single_variation_wrap">
                        <div class="woocommerce-variation single_variation"></div>
                        <div class="woocommerce-variation-add-to-cart variations_button">

                            <div class="quantity pr fl mr__10">
                                <input type="number" id="quantity_5b2b889f1c8b1"
                                       class="input-text qty text tc" step="1" min="1" max=""
                                       name="quantity" value="1" title="Qty" size="4"
                                       pattern="[0-9]*" inputmode="numeric" aria-labelledby=""/>

                                <div class="qty tc">
                                    <a class="plus db cb pa" href="javascript:void(0);">
                                        <i class="fa fa-plus"></i>
                                    </a>
                                    <a class="minus db cb pa" href="javascript:void(0);">
                                        <i class="fa fa-minus"></i>
                                    </a>
                                </div>
                            </div>

                            <button type="submit" class="single_add_to_cart_button button alt">Add
                                to cart
                            </button>


                            <input type="hidden" name="add-to-cart" value="5269"/>
                            <input type="hidden" name="product_id" value="5269"/>
                            <input type="hidden" name="variation_id" class="variation_id"
                                   value="0"/>
                        </div>
                        <div class="yith-wcwl-add-to-wishlist ts__03 mg__0 ml__10 pr add-to-wishlist-5269">
                            <div class="yith-wcwl-add-button show"><a
                                        href="http://localhost/e-theme/wishlist/"
                                        data-product-id="5269" data-product-type="variable"
                                        class="add_to_wishlist cw"><i class="fa fa-heart-o"></i></a><i
                                        class="fa fa-spinner fa-pulse ajax-loading pa"
                                        style="visibility:hidden"></i></div>
                            <div class="yith-wcwl-wishlistaddedbrowse hide" style="display:none;"><a
                                        class="chp" href="http://localhost/e-theme/wishlist/"><i
                                            class="fa fa-heart"></i></a></div>
                            <div class="yith-wcwl-wishlistexistsbrowse hide" style="display:none"><a
                                        href="http://localhost/e-theme/wishlist/" class="chp"><i
                                            class="fa fa-heart"></i></a></div>
                        </div>
                    </div>


                </form>

                <div class="extra-link mt__25 fwsb"><a class="cd chp jas-magnific-image  mr__20"
                                                       href="http://janstudio.net/claue/demo/wp-content/uploads/2017/01/sizecharts.png">Size
                        Guide</a><a data-type="shipping-return" class="jas-wc-help cd chp" href="#">Delivery
                        &amp; Return</a></div>
            </div>
            <div class="product_meta">


                <span class="sku_wrapper">SKU: <span class="sku">P15</span></span>


                <span class="posted_in">Categories: <a
                            href="http://localhost/e-theme/product-category/accessories/" rel="tag">Accessories</a>, <a
                            href="http://localhost/e-theme/product-category/denim/"
                            rel="tag">Denim</a>, <a
                            href="http://localhost/e-theme/product-category/dress/"
                            rel="tag">Dress</a>, <a
                            href="http://localhost/e-theme/product-category/jewellery/" rel="tag">Jewellery</a>, <a
                            href="http://localhost/e-theme/product-category/tops/"
                            rel="tag">Tops</a>, <a
                            href="http://localhost/e-theme/product-category/women/"
                            rel="tag">Women</a></span>
                <span class="tagged_as">Tags: <a
                            href="http://localhost/e-theme/product-tag/jewellery/" rel="tag">jewellery</a>, <a
                            href="http://localhost/e-theme/product-tag/watch/" rel="tag">watch</a>, <a
                            href="http://localhost/e-theme/product-tag/women/"
                            rel="tag">women</a></span>

            </div>

            <div class="wpa-wcpb-list">
                <h4 class="wpa-title">Buy this bundle and get off to 25%</h4>
                <p class="wpa-bundle-promo-text">Buy more save more. Save 15% when you purchase 4
                    products, save 10% when you purchase 3 products</p>
                <div class="list-image flxwr">
                    <div class="item flx alc">
                        <div class="image"><img width="70" height="70"
                                                src="http://localhost/e-theme/wp-content/uploads/2018/06/111-100x100.jpg"
                                                class="attachment-70x70 size-70x70 wp-post-image"
                                                alt=""
                                                srcset="http://localhost/e-theme/wp-content/uploads/2018/06/111-100x100.jpg 100w, http://localhost/e-theme/wp-content/uploads/2018/06/img-139-150x150.png 150w, http://localhost/e-theme/wp-content/uploads/2018/06/img-139-300x300.png 300w"
                                                sizes="(max-width: 70px) 100vw, 70px"/></div>
                        <span class="plus">+</span>
                    </div>
                    <div class="item flx alc">
                        <div class="image"><a
                                    href="http://localhost/e-theme/product/analogue-resin-strap-watch/"><img
                                        width="70" height="70"
                                        src="http://localhost/e-theme/wp-content/uploads/2018/06/img-146-100x100.png"
                                        class="attachment-70x70 size-70x70 wp-post-image" alt=""
                                        srcset="http://localhost/e-theme/wp-content/uploads/2018/06/img-146-100x100.png 100w, http://localhost/e-theme/wp-content/uploads/2018/06/img-146-150x150.png 150w, http://localhost/e-theme/wp-content/uploads/2018/06/img-146-300x300.png 300w"
                                        sizes="(max-width: 70px) 100vw, 70px"/></a></div>
                        <span class="plus">+</span>
                    </div>
                </div>
                <div class="list-select px-product-bundles" data-total-discount="0,10">
                    <div class="item item-main" data-product-id="5269" data-item-price="15"
                         data-item-percent="0">
                        <div class="info-item">
                            <input type="checkbox" checked="checked" disabled="disabled"/>
                            <span class="name">This product: Cuffed Beanie</span> -
                            <span class="price"><span class="woocommerce-Price-amount amount"><span
                                            class="woocommerce-Price-currencySymbol">&#36;</span>13.50</span> / <del><span
                                            class="woocommerce-Price-amount amount"><span
                                                class="woocommerce-Price-currencySymbol">&#36;</span>15.00</span></del></span>
                        </div>
                    </div>
                    <div class="item" data-product-id="5346" data-item-price="85"
                         data-item-percent="10">
                        <div class="info-item in-of-stock">
                            <input type="checkbox"
                                   onchange="wpa_wcpb_onchange_input_check_total_discount()"
                                   checked="checked"/>
                            <span class="name">Analogue Resin Strap Watch</span> -
                            <span class="price"><span class="woocommerce-Price-amount amount"><span
                                            class="woocommerce-Price-currencySymbol">&#36;</span>76.50</span> / <del><span
                                            class="woocommerce-Price-amount amount"><span
                                                class="woocommerce-Price-currencySymbol">&#36;</span>85.00</span></del></span>
                        </div>
                    </div>
                </div>

                <div class="total price">
                    <strong>Price for all</strong>: <span class="current-price"><span
                                class="woocommerce-Price-amount amount"><span
                                    class="woocommerce-Price-currencySymbol">&#36;</span>90.00</span>
                                                </span> /
                    <del class="old-price"><span class="woocommerce-Price-amount amount"><span
                                    class="woocommerce-Price-currencySymbol">&#36;</span>100.00</span>
                    </del>
                    (save <span class="save-percent">10</span>% )
                </div>
                <button class="btn-wpa wpa_wcpb_add_to_cart single_add_to_cart_button button wc-variation-selection-needed disabled"
                        type="submit" onclick="wpa_wcpb_add_to_cart( jQuery(this) )">Add bundle to
                    cart
                </button>
                <div class="showbox">
                    <div class="loader">
                        <svg viewBox="25 25 50 50">
                            <circle class="loader_background" cx="50" cy="50" r="20"
                                    stroke-width="3"/>
                            <circle class="loader_rotation" cx="50" cy="50" r="20" fill="none"
                                    stroke-width="4"/>
                            <path class="loader_path" d="m48,58l11,-16" stroke-dasharray="23"
                                  stroke-dashoffset="23"/>
                            <path class="loader_path" d="m48,58l-8,-6" stroke-dasharray="10"
                                  stroke-dashoffset="10"/>
                        </svg>
                    </div>
                </div>
                <div class="wpa-error">Please select some product options before adding this product
                    to your cart.
                </div>
                <div class="wpa-message">Product bundle already add to cart, <a
                            href="http://localhost/e-theme/cart/" class="wc-forward">View cart</a>
                </div>
                <style>
                    .wpa_wcpb_add_to_cart {
                        background-color: #56cfe1;
                    }

                    .wpa_wcpb_add_to_cart {
                        color: #ffffff;
                    }

                    .wpa_wcpb_add_to_cart:hover {
                        background-color: #000000;
                    }

                    .wpa_wcpb_add_to_cart:hover {
                        color: #ffffff;
                    }
                </style>
            </div>
            <div class="social-share">
                <div class="jas-social">
                    <a title="Share this post on Facebook" class="cb facebook"
                       href="http://www.facebook.com/sharer.php?u=http://localhost/e-theme/product/cuffed-beanie/"
                       onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=380,width=660');return false;">
                        <i class="fa fa-facebook"></i>
                    </a>
                    <a title="Share this post on Twitter" class="cb twitter"
                       href="https://twitter.com/share?url=http://localhost/e-theme/product/cuffed-beanie/"
                       onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=380,width=660');return false;">
                        <i class="fa fa-twitter"></i>
                    </a>
                    <a title="Share this post on Google Plus" class="cb google-plus"
                       href="https://plus.google.com/share?url=http://localhost/e-theme/product/cuffed-beanie/"
                       onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=380,width=660');return false;">
                        <i class="fa fa-google-plus"></i>
                    </a>
                    <a title="Share this post on Pinterest" class="cb pinterest"
                       href="//pinterest.com/pin/create/button/?url=http://localhost/e-theme/product/cuffed-beanie/&media=http://localhost/e-theme/wp-content/uploads/2018/06/111-150x150.jpg&description=Cuffed Beanie"
                       onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;">
                        <i class="fa fa-pinterest"></i>
                    </a>
                    <a title="Share this post on Tumbr" class="cb tumblr"
                       data-content="http://localhost/e-theme/wp-content/uploads/2018/06/111-150x150.jpg"
                       href="//tumblr.com/widgets/share/tool?canonicalUrl=http://localhost/e-theme/product/cuffed-beanie/"
                       onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=540');return false;">
                        <i class="fa fa-tumblr"></i>
                    </a>
                </div>
            </div>
        </div>
        <!-- .summary -->
    </div>
</div>