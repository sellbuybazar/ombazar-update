<?php require_once "support.php"?>
{{#each rows}}
<tr id="row_id_{{id}}" data-id="{{id}}">
    <td><img class="img_thumb" src="{{image_path}}" alt=""/></td>
    <td class="uk-text-large uk-text-nowrap">
        {{item_name}}
    </td>
    <td class="uk-text-nowrap "><label>Purchase</label><input class="md-input uk-form-width-small uk-text-center purchase" type="text" value="{{purchase}}"> </td>
    <td class="uk-text-nowrap "><label>Price</label><input class="md-input uk-form-width-small uk-text-center price" type="text" value="{{price}}" ></td>
    <td class="uk-text-nowrap "><label>Sale</label><input class="md-input uk-form-width-small uk-text-center sale" type="text" value="{{sale}}" ></td>
    <td class="uk-text-nowrap "><label>Quantity</label><input class="md-input uk-form-width-small uk-text-center quantity" type="text" value="{{quantity}}"></td>
    <td><button class=" {{#if status_color}} md-btn-flat  {{/if}} md-btn md-btn-wave waves-effect waves-button add-to-store-button " data-id="{{user_id}}">{{status}}</button></td>
</tr>
{{/each}}
{{#if rows}}

{{else}}

<tr id="row_id_{{id}}" data-id="{{id}}">
    <td colspan="7">
        No data found
    </td>


</tr>

{{/if}}