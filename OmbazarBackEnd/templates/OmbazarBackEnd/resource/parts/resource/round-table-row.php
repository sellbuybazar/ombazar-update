<?php require_once "support.php"?>
{{#each rows}}
<tr id="row_id_{{id}}" data-id="{{id}}">
    <td><input type="checkbox" data-md-icheck class="ts_checkbox"></td>
    <td>
        {{val}}
    </td>

    <td class="active-colum">
        {{#if active}}
            <span class="uk-badge uk-badge-success">Active</span>
        {{else}}
            <span class="uk-badge uk-badge-danger">Deactive</span>
        {{/if}}
    </td>
    <td>
        {{time}}

    </td>
    <td class="uk-text-center icon-relative">
        <div class="round-manage-area">
            <input type="checkbox" data-switchery data-switchery-color="#1e88e5" {{#if active}}checked{{/if}} id="switch_demo_large" value="{{type}}" data-name="{{val}}">
            <label for="switch_demo_large" class="inline-label">Label</label>
        </div>

    </td>
</tr>
{{/each}}
<script>
    altair_forms.switches()
</script>