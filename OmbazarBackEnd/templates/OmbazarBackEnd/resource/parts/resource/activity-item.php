<?php require_once "support.php"?>
{{#each rows}}
<div class="timeline_item " data-id="{{id}}">
    <div class="timeline_icon timeline_icon_success"><i class="material-icons">&#xE85D;</i></div>
    <div class="timeline_date">
        {{time}}
    </div>
    <div class="timeline_content">
        <div class="timeline_content">{{activity}}
            <div class="timeline_content_addon">
                <div>
                    <ul class="md-list md-list-addon">
                        <li>
                            <div class="md-list-addon-element">
                                <img class="md-user-image md-list-addon-avatar" src="{{image_path}}" alt=""/>
                            </div>
                            <div class="md-list-content">
                                <span class="md-list-heading">{{user_name}}</span>
                                <span class="uk-text-small uk-text-muted">{{user_type}}</span>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

    </div>
</div>
{{/each}}