<?php require_once "support.php"?>
{{#each rows}}
<div data-product-name="{{item_name}}" class="row_id_{{id}} products-item" data-id="{{id}}" data-item-id="{{item_id}}" data-object="">
    <div class="md-card md-card-hover-img">
        <div class="md-card-head uk-text-center uk-position-relative">
            <div class="uk-badge uk-badge-danger uk-position-absolute uk-position-top-left uk-margin-left uk-margin-top">{{sale}}</div>
            <img class="md-card-head-img" src="{{image_path}}" alt=""/>
        </div>
        <div class="md-card-content">
            <h4 class="heading_c uk-margin-bottom">
                {{item_name}}
                <span class="sub-heading">Price:
                    {{#if sale}}
                    <del>{{price}} {{currency_symbol}}</del> {{sale}} {{currency_symbol}}
                    {{else}}
                    {{price}} {{currency_symbol}}
                    {{/if}}
                </span>
            </h4>
            <button class="md-btn md-btn-flat-primary md-btn-block">Add to cart</button>
        </div>
        <div class="cart-item-overlay-info">
            <div class="activated-item hidden">
                <p class="item-total-amount"><span></span> tk</p>
                <button class="md-fab cart-item-left"><i class="material-icons">remove</i></button>
                <span class="item-quantity"></span>
                <button class="md-fab cart-item-add"><i class="material-icons">add</i></button>
                <p>In Cart</p>
            </div>
            <div class="deactivated-item">
                <p>Add to cart</p>
            </div>
            <div class="details">
                <a href="{{product_details_path}}" class="md-btn no_load md-btn-flat-primary md-btn-block"><i class="material-icons md-24">info</i> Details</a>
            </div>
        </div>
    </div>

</div>
{{/each}}