<?php require_once "helper.php";?>

<div class="uk-modal-dialog">
    <div class="uk-modal-header">
        <h3 class="uk-modal-title">Prize edit</h3>
    </div>
    <?php
    $retrive=new retrive();
    $prize_info=$retrive->prize_info($_REQUEST['id']);
    foreach($prize_info as $info){
    ?>
    <form method="" class="prize-edit-form" id="form">
        <div class="uk-grid form_section " id="d_form_row" data-uk-grid-margin>
            <div class="uk-width-medium-1-2">
                <label>Prize name</label>
                <input type="text" class="md-input" name="prize_name" value="<?php echo $info['prize_name']?>">
            </div>
            <input type="hidden" value="<?php echo $_REQUEST['id'];?>" name="prize_id">
            <div class="uk-width-medium-1-2">
                <div class="uk-input-group">
                    <label>Unlock amount</label>
                    <input type="text" class="md-input" name="unlock_amount" value="<?php echo $info['unlock_amount']?>">

                </div>
            </div>

        </div>

        <div class="uk-grid">
            <div class="uk-width-large-1-2">
                <button class="md-btn md-btn-primary" button-name="save_and_new">Save</button>
                <button type="button" class="md-btn md-btn-flat uk-modal-close">Close</button>
            </div>
        </div>
    </form>
    <?php  } ?>
</div>

<script>
    altair_form_validation.init();
</script>

