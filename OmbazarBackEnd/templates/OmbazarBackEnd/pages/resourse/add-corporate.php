    <div id="page_content">
        <div id="page_content_inner">
            <form method="post" class="corporate-add" id="form" action="add-item">
                <div class="uk-grid " data-uk-grid-margin data-uk-grid-match >
                    <div class="uk-width-large-1-11">
                        <div class="md-card user_content">
                            <h4 class="heading_c uk-margin-bottom ">Add new corporate</h4>
                            <div class="uk-grid" data-uk-grid-margin>
                                <div class="uk-width-large-1-2 parsley-row">
                                    <div class="uk-grid "  data-uk-grid-margin>
                                        <div class="uk-width-large-1-1 parsley-row">
                                            <label>Company name</label>
                                            <input class="md-input item-name-field" type="text" name="name" required>
                                        </div>
                                        <div class="uk-width-large-1-1 parsley-row">
                                            <span>Tags</span>
                                            <input class=" item-tag-field corporate-tags-selectize" type="text" name="tags" value="" >
                                        </div>
                                        <div class="uk-width-large-1-1 parsley-row">
                                            <label>Phone</label>
                                            <input class="md-input item-phone-field" type="text" name="phone" value="" >
                                        </div>
                                        <div class="uk-width-large-1-1 parsley-row">
                                            <label>Email</label>
                                            <input class="md-input item-email-field" type="email" name="email" value="" >
                                        </div>
                                        <div class="uk-width-large-1-1 parsley-row">
                                            <label>Location</label>
                                            <input class="md-input item-location-field" type="text" name="location" value="" >
                                        </div>

                                        <div class="uk-width-large-1-1 parsley-row">
                                            <label>Address</label>
                                            <textarea cols="30" rows="2" class="md-input"  name="address"></textarea>
                                        </div>
                                        <div class="uk-width-large-1-1 parsley-row">
                                            <div class="uk-grid form_section" id="d_form_row" data-uk-grid-margin>
                                                <div class="uk-width-medium-1-1">
                                                    <div class="uk-input-group">
                                                        <label>Note</label>
                                                        <input type="text" class="md-input" name="notes[]">
                                                        <span class="uk-input-group-addon">
                                                            <a href="#" class="spbtnSectionClone" data-section-clone="#d_form_row"><i class="material-icons md-24">&#xE146;</i></a>
                                                        </span>
                                                    </div>
                                                </div>

                                            </div>

                                        </div>
                                    </div>
                                </div>

                                <div class="uk-width-large-1-2 uk-margin-top">
                                    <h3 class="heading_a uk-margin-small-bottom">Images</h3>
                                    <div class="uk-grid"  data-uk-grid-margin>
                                        <div class="uk-width-large-1-1 parsley-row">
                                            <input type="file" accept="image/*" multiple class="image_uploader" name="images[]">
<!--                                            <input type="file" accept="image/*" multiple name="images[]">-->
                                        </div>
                                    </div>

                                </div>

                            </div>
                            <div class="uk-grid">
                                <div class="uk-width-large-1-2">
                                    <button class="md-btn md-btn-primary" button-name="save_and_new">Save & New</button>
                                    <button class="md-btn md-btn-success" button-name="save">Save</button>
                                    <button type="reset" class="md-btn md-btn-info">Cancel</button>
                                </div>

                            </div>


                        </div>
                    </div>

                </div>
            </form>
        </div>
    </div>
