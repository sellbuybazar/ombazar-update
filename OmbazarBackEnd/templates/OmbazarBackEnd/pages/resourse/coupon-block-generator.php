<div id="page_content">
    <div id="page_content_inner">
        <form method="" class="coupon-generate" id="form">
            <div class="uk-grid " data-uk-grid-margin data-uk-grid-match >
                <div class="uk-width-large-1-11">
                    <div class="md-card user_content">
                        <h4 class="heading_c uk-margin-bottom ">Coupon block generator </h4>
                        <div class="uk-grid">
                            <div class="uk-width-large-1-2 uk-margin-bottom parsley-row">
                                <label>Amount</label>
                                <input class="md-input" type="text" name="amount" required>
                            </div>
                            <div class="uk-width-large-1-2 uk-margin-bottom parsley-row">
                                <label>Amount</label>
                                <input class="md-input" type="text" name="amount" required>
                            </div>

                        </div>
                        <div class="uk-grid">
                            <div class="uk-width-large-1-2">
                                <button class="md-btn md-btn-primary" button-name="save_and_new">generate</button>
                                <button type="reset" class="md-btn md-btn-info">Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </form>
    </div>
</div>
