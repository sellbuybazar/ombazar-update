
<div id="page_content">
    <div id="page_content_inner">
        <div class="md-card">
            <div class="md-card-content">
            <form class="item-river-filter">


                <div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-medium-3-10">
                        <label>Product name</label>
                        <input type="text" class="md-input" value="" name="product_search_name">
                    </div>
                    <div class="uk-width-medium-1-10">
                        <label>Min price</label>
                        <input type="text" class="md-input" value="" name="product_search_min_price">
                    </div>
                    <div class="uk-width-medium-1-10">
                        <label>Max price</label>
                        <input type="text" class="md-input" value="" name="product_search_max_price">
                    </div>

                    <div class="uk-width-medium-3-10">
                        <select class="categories" name="search_category">
                            <option value="">Select category</option>
                        </select>
                    </div>
                    <div class="uk-width-medium-2-10 uk-text-center">
                        <button class="uk-margin-small-top md-btn md-btn-primary md-btn-wave-light waves-effect waves-button waves-light">Filter</button>
                    </div>
                </div>
            </form>
            </div>
        </div>

        <div class="md-card">
            <div class="md-card-content">
                <div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-1-1">
                        <div class="uk-overflow-container">
                            <table class="uk-table uk-table-align-vertical item-river-table admin-uk-table" table-data-type="item-river" limit="3">
                                <thead>
                                <tr>
                                    <th>Image</th>
                                    <th>Product Name</th>
                                    <th>Purchase</th>
                                    <th>Price</th>
                                    <th>Sale</th>
                                    <th>Quantity</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>


                                </tbody>
                            </table>
                        </div>
                        <ul class="uk-pagination uk-margin-medium-top uk-margin-medium-bottom">
                            <li class="uk-disabled"><a href="#" class="previous-button"><i class="uk-icon-angle-double-left"></i> Previous</a> </li>

                            <li class="uk-text-center"><form class="page-go-form"><label>Page number</label> <input class=" md-input uk-form-width-small uk-text-center selectize-input go-page-number" type="text" value="1" name="page_number"> </form> </li>

                            <li class=""><a href="#" class="next-button">Next <i class="uk-icon-angle-double-right"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
