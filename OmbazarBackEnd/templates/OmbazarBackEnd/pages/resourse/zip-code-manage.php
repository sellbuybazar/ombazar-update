<div id="page_content">
    <div id="page_content_inner">
        <div class="uk-grid " data-uk-grid-margin data-uk-grid-match >
            <div class="uk-width-large-1-11">
                <div class="md-card user_content">
                    <h4 class="heading_c uk-margin-bottom ">Zip code manage</h4>
                    <form class="report-generate" id="form">
                        <div class="uk-grid date-range " data-uk-grid-margin>
                            <div class="uk-width-medium-1-2 parsley-row">
                                <label>Country name</label>
                                <input class="md-input start-date" type="text"   name="start_date" value="">
                            </div>
                            <div class="uk-width-medium-1-2 parsley-row">
                                <label>Currency symbol</label>
                                <input class="md-input end-date" type="text"  name="end_date"  value="">
                            </div>
                        </div>
                        <div class="uk-grid" data-uk-grid-margin>
                            <div class="uk-width-large-1-1 report_type parsley-row ">
                                <label>Services</label>
                                <select class="multiple-select"  name="type" required multiple>
                                    <option value="">Report type</option>
                                    <option value="1">Service 1</option>
                                    <option value="2">Service 2</option>
                                    <option value="3">Service 3</option>
                                </select>
                            </div>
                            <div class="uk-width-medium-1-1 parsley-row">
                                <label>Zip codes</label>
                                <input class="tags" type="text" name="zipcodes" required placeholder="Zip codes separated by coma ">
                            </div>
                        </div>
                        <div class="uk-grid">
                            <div class="uk-width-large-1-2">
                                <button class="md-btn md-btn-primary">Save</button>
                            </div>

                        </div>
                    </form>
                    <hr class="md-hr"/>
                    <div class="uk-margin-top">
                        <a href="#" class=" " id="printTable" data-uk-tooltip="{pos:'top'}" title="Print">
                            <i class="material-icons md-icon">print</i>
                        </a>
                        <div class="uk-button-dropdown" data-uk-dropdown="{mode:'click'}" style="margin-top:-8px">
                            <a class="" href="#" data-uk-tooltip="{pos:'top'}" title="Colum select"><i class="material-icons md-icon ">reorder</i></a>
                            <div class="uk-dropdown">
                                <ul class="uk-nav uk-nav-dropdown" id="columnSelector"></ul>
                            </div>
                        </div>
                    </div>
                    <div class="uk-overflow-container uk-margin-bottom">
                        <table class="uk-table uk-table-align-vertical uk-table-nowrap tablesorter tablesorter-altair" id="ts_pager_filter">
                            <thead>
                            <tr>
                                <th data-name="Select" data-priority="5"><input type="checkbox" class="ts_checkbox_all"></th>
                                <th data-priority="critical">Country name</th>
                                <th data-priority="2">Currency symbol</th>
                                <th data-priority="3">Action</th>

                            </tr>
                            </thead>
                            <tfoot>
                            <tr>
                                <th></th>
                                <th>Country name</th>
                                <th>Currency symbol</th>
                                <th>Action</th>
                            </tr>
                            </tfoot>
                            <tbody>
                                <tr>
                                    <td><input type="checkbox" data-md-icheck class="ts_checkbox"></td>
                                    <td></td>
                                    <td></td>
                                    <td>
                                        <a href="#"  data-uk-modal="{target:'#group_modal'}" class="md-btn md-btn-wave waves-effect waves-button md-btn-small" data-id="<?php echo $result['id']?>"> Zip groups</a>
                                        <div class="uk-button-dropdown " data-uk-tooltip="{pos:'top'}" title="Edit or delete"  data-uk-dropdown="{pos:'left-top',mode:'click'}" style="margin-top:0px">
                                            <span ><i class="md-icon material-icons" >&#xE5D4;</i></span>
                                            <div class="uk-dropdown uk-dropdown-small">
                                                <ul class="uk-nav uk-text-left">

                                                    <li><a href="#"  data-uk-modal="{target:'#country_modal'}" class="uk-text-info" data-id="<?php echo $result['id']?>"><i class="material-icons">cancel</i> Edit</a></li>

                                                </ul>
                                            </div>
                                        </div>
                                    </td>
                                </tr>

                            </tbody>
                        </table>
                    </div>
                    <ul class="uk-pagination ts_pager">
                        <li data-uk-tooltip title="Select Page">
                            <select class="ts_gotoPage ts_selectize"></select>
                        </li>
                        <li class="first"><a href="javascript:void(0)"><i class="uk-icon-angle-double-left"></i></a></li>
                        <li class="prev"><a href="javascript:void(0)"><i class="uk-icon-angle-left"></i></a></li>
                        <li><span class="pagedisplay"></span></li>
                        <li class="next"><a href="javascript:void(0)"><i class="uk-icon-angle-right"></i></a></li>
                        <li class="last"><a href="javascript:void(0)"><i class="uk-icon-angle-double-right"></i></a></li>
                        <li data-uk-tooltip title="Page Size">
                            <select class="pagesize ts_selectize">
                                <option value="5">5</option>
                                <option value="10">10</option>
                                <option value="25">25</option>
                                <option value="50">50</option>
                            </select>
                        </li>
                    </ul>

                </div>
            </div>

        </div>
    </div>
</div>

<div class="uk-modal" id="country_modal">
    <div class="uk-modal-dialog">
        <div class="uk-modal-header">
            <h3 class="uk-modal-title">Country edit <i class="material-icons" data-uk-tooltip="{pos:'top'}" title="headline tooltip">&#xE8FD;</i></h3>
        </div>
        <div class="uk-grid date-range " data-uk-grid-margin>
            <div class="uk-width-medium-1-2 parsley-row">
                <label>Country name</label>
                <input class="md-input start-date" type="text"   name="start_date" value="">
            </div>
            <div class="uk-width-medium-1-2 parsley-row">
                <label>Currency symbol</label>
                <input class="md-input end-date" type="text"  name="end_date"  value="">
            </div>
        </div>
        <div class="uk-grid" data-uk-grid-margin>
            <div class="uk-width-large-1-1 report_type parsley-row ">
                <label>Services</label>
                <select class="multiple-select"  name="type" required multiple>
                    <option value="">Report type</option>
                    <option value="1">Service 1</option>
                    <option value="2">Service 2</option>
                    <option value="3">Service 3</option>
                </select>
            </div>
            <div class="uk-width-medium-1-1 parsley-row">
                <label>Zip codes</label>
                <input class="tags" type="text" name="zipcodes" required placeholder="Zip codes separated by coma ">
            </div>
        </div>

        <div class="uk-modal-footer uk-text-right">
            <button type="button" class="md-btn md-btn-flat uk-modal-close">Close</button>
            <button class="md-btn md-btn-primary">Save</button>
        </div>
    </div>
</div>
<div class="uk-modal" id="group_modal">
    <div class="uk-modal-dialog">
        <div class="uk-modal-header">
            <h3 class="uk-modal-title">Zip code groups <i class="material-icons" data-uk-tooltip="{pos:'top'}" title="headline tooltip">&#xE8FD;</i></h3>
        </div>

        <div class="uk-grid date-range " data-uk-grid-margin>
            <div class="uk-width-medium-7-10 parsley-row">

                <select data-md-selectize  name="type" required>
                    <option value="">Group name</option>
                    <option value="1">Service 1</option>
                    <option value="2">Service 2</option>
                    <option value="3">Service 3</option>
                </select>
            </div>
            <div class="uk-width-medium-3-10 parsley-row">
                <button class="md-btn md-btn-success  md-btn-small">Add new</button>
            </div>
        </div>
        <div class="uk-grid " data-uk-grid-margin>
            <div class="uk-width-medium-1-1 parsley-row">
                <label>Zip codes</label>
                <input class="tags" type="text" name="zipcodes" required placeholder="Zip codes separated by coma ">
            </div>
        </div>

        <div class="uk-grid " data-uk-grid-margin>
            <div class="uk-width-medium-1-1 parsley-row">
                <label>Group name</label>
                <input class="md-input" type="text" name="group_name" required>
            </div>
            <div class="uk-width-medium-1-1 parsley-row uk-margin-top">
                <label>Zip codes</label>
                <input class="tags" type="text" name="zipcodes" required placeholder="Zip codes separated by coma ">
            </div>

        </div>

        <div class="uk-modal-footer uk-text-right">
            <button type="button" class="md-btn md-btn-flat uk-modal-close">Close</button>
            <button class="md-btn md-btn-primary">Save</button>
        </div>

    </div>
</div>