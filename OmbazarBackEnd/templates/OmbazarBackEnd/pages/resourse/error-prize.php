
<div id="page_content">
    <div id="page_content_inner">
        <div class="uk-grid prize-card-container">
            <div class="uk-width-medium-1-10"></div>
            <div class="uk-width-medium-8-10 prize-card relative">

                <div class="md-card">
                    <div class="md-card-content">
                        <div class="uk-grid">
                            <div class="uk-width-medium-3-10">
                                <img src="<?php echo SITE_URL;?>assets/img/default/sad-kids-collection-002.jpg" class="prize-company-logo"  style="height: 300px;">

                            </div>
                            <div class="uk-width-medium-7-10">
                                <h1 style="margin:60px 0px">Not found any prize this information!</h1>

                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="uk-width-medium-1-10"></div>
        </div>
    </div>
</div>
