    <?php
        $data=$retrive->category_info($_REQUEST['category']);
    ?>
    <div id="page_content">
        <div id="page_content_inner">
            <?php foreach($data as $result){?>
            <form method="post" class="category-edit" id="form">
                <div class="uk-grid " data-uk-grid-margin data-uk-grid-match >
                    <div class="uk-width-large-1-11">
                        <div class="md-card user_content">
                            <h4 class="heading_c uk-margin-bottom ">Edit category</h4>
                            <input type="hidden" value="<?php echo $_REQUEST['category'];?>" name="category_id">
                            <div class="uk-grid" data-uk-grid-margin>
                                <div class="uk-width-large-1-2 parsley-row">
                                    <div class="uk-grid "  data-uk-grid-margin>
                                        <div class="uk-width-large-1-1 parsley-row">
                                            <label>Category name</label>
                                            <input class="md-input" type="text" name="category_name" value="<?php echo $result['category_name']?>" required>
                                        </div>
                                        <div class="uk-width-large-1-1 parsley-row uk-margin-top">
                                            <label>Description</label>
                                            <textarea class="md-input" name="description" ><?php echo $result['description']?></textarea>
                                        </div>
                                        <div class="uk-width-large-1-1">
                                            <?php
                                                $category_specifications=$retrive->category_specificatons($_REQUEST['category']);
                                                $i=0;
                                                foreach ($category_specifications as $specification){

                                            ?>
                                            <div class="uk-grid form_section" id="d_form_row" data-uk-grid-margin>
                                                <div class="uk-width-medium-1-2">
                                                        <label>Specification name</label>
                                                        <input type="text" class="md-input" name="specification_name[]" value="<?php echo $specification['type']?>">

                                                </div>
                                                <div class="uk-width-medium-1-2">
                                                    <div class="uk-input-group">
                                                        <input class="tags md-input" id="tags" type="text" name="specification_tags[]"  placeholder="Separated by coma " value='<?php echo $specification['val']?>'>

                                                        <span class="uk-input-group-addon">
                                                            <?php if($i==0){?>
                                                            <a href="#" class="spbtnSectionClone" data-section-clone="#d_form_row"><i class="material-icons md-24">&#xE146;</i></a>
                                                            <?php } ?>
                                                        </span>

                                                    </div>
                                                </div>

                                            </div>
                                            <?php $i++; } ?>
                                            <?php
                                                if(!$category_specifications){?>
                                                    <div class="uk-grid form_section" id="d_form_row">
                                                        <div class="uk-width-medium-1-2">
                                                            <div class="uk-input-group">
                                                                <label>Specification name</label>
                                                                <input type="text" class="md-input" name="specification_name[]">
                                                            </div>
                                                        </div>
                                                        <div class="uk-width-medium-1-2">
                                                            <div class="uk-input-group">
                                                                <input class="tags md-input" id="tags" type="text" name="specification_tags[]"  placeholder="Separated by coma ">
                                                                <span class="uk-input-group-addon">
                                                            <a href="#" class="spbtnSectionClone" data-section-clone="#d_form_row"><i class="material-icons md-24">&#xE146;</i></a>
                                                        </span>
                                                            </div>
                                                        </div>

                                                    </div>
                                            <?php    }
                                            ?>
                                        </div>
                                    </div>
                                </div>


                                <div class="uk-width-large-1-2 uk-margin-top">
                                    <div class="uk-grid" uk-grid-margin>
                                        <label for="">Category image</label>
                                        <div class="uk-width-large-1-1 ">
                                            <input type="file" id="input-file-a" class="dropify " name="image" data-default-file="<?php echo SITE_URL;?>assets/img/images/<?php echo $result['image']?>">
                                        </div>
                                    </div>
                                    <div class="access_area uk-margin-bottom uk-margin-top">
                                        <label for="">Access</label>
                                        <?php include_once("Src/inc/category-tree.php");?>
                                    </div>

                                </div>

                            </div>
                            <div class="uk-grid">
                                <div class="uk-width-large-1-2">
                                    <button class="md-btn md-btn-primary" button-name="save">Save</button>
                                    <button type="reset" class="md-btn md-btn-info">Cancel</button>
                                </div>

                            </div>


                        </div>
                    </div>

                </div>
            </form>
            <?php } ?>
        </div>
    </div>
