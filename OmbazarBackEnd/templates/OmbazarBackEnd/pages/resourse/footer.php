    
    <form id="form">

    </form>
      <form id="customer-form">

    </form>
      <form id="vendor-form">

    </form>
    <!-- google web fonts -->
<!--    <script src='https://www.google.com/recaptcha/api.js'></script>-->
    <script src="<?php echo SITE_URL; ?>assets/js/define.js"></script>


    <!-- common functions -->
    <script src="<?php echo SITE_URL; ?>assets/js/common.min.js"></script>
    <script type='text/javascript' src='<?php echo SITE_URL;?>assets/js/functions.js'></script>
    <!-- uikit functions -->
    <script src="<?php echo SITE_URL; ?>assets/js/uikit_custom.js"></script>
    <!-- altair common functions/helpers -->
    <script src="<?php echo SITE_URL; ?>assets/js/altair_admin_common.min.js"></script>
    <script src="<?php echo SITE_URL; ?>assets/js/custom/uikit_fileinput.min.js"></script>
    <!-- tablesorter -->
    <script src="<?php echo SITE_URL; ?>bower_components/tablesorter/dist/js/jquery.tablesorter.min.js"></script>
    <script src="<?php echo SITE_URL; ?>bower_components/tablesorter/dist/js/jquery.tablesorter.widgets.min.js"></script>
    <script src="<?php echo SITE_URL; ?>bower_components/tablesorter/dist/js/widgets/widget-alignChar.min.js"></script>
    <script src="<?php echo SITE_URL; ?>bower_components/tablesorter/dist/js/widgets/widget-columnSelector.min.js"></script>
    <script src="<?php echo SITE_URL; ?>bower_components/tablesorter/dist/js/widgets/widget-print.min.js"></script>
    <script src="<?php echo SITE_URL; ?>bower_components/tablesorter/dist/js/extras/jquery.tablesorter.pager.min.js"></script>

    <!-- ionrangeslider -->
    <script src="<?php echo SITE_URL; ?>bower_components/ion.rangeslider/js/ion.rangeSlider.min.js"></script>
    <!--  tablesorter functions -->
    <script src="<?php echo SITE_URL; ?>assets/js/pages/plugins_tablesorter.min.js"></script>
    <script src="<?php echo SITE_URL; ?>bower_components/jquery-ui/jquery-ui.min.js"></script>
    <script src="<?php echo SITE_URL; ?>bower_components/jquery.fancytree/dist/jquery.fancytree-all.min.js"></script>

    <!--  tree functions -->
    <script src="<?php echo SITE_URL; ?>assets/js/pages/plugins_tree.min.js"></script>
    <!-- htmleditor (codeMirror) -->
    <script src="<?php echo SITE_URL; ?>assets/js/uikit_htmleditor_custom.min.js"></script>
    <!-- inputmask-->
    <script src="<?php echo SITE_URL; ?>bower_components/jquery.inputmask/dist/jquery.inputmask.bundle.js"></script>

    <!--  forms advanced functions -->
    <script src="<?php echo SITE_URL; ?>assets/js/pages/forms_advanced.min.js"></script>
    <!-- handlebars.js -->
    <script src="<?php echo SITE_URL; ?>bower_components/handlebars/handlebars.min.js"></script>
    <script src="<?php echo SITE_URL; ?>assets/js/custom/handlebars_helpers.min.js"></script>
    <script>

    // load parsley config (altair_admin_common.js)
    altair_forms.parsley_validation_config();
    </script>
    <script src="<?php echo SITE_URL; ?>bower_components/parsleyjs/dist/parsley.min.js"></script>
    <script src="<?php echo SITE_URL; ?>assets/js/pages/forms_validation.js"></script>
<!--    When connection is offline-->
<!--    <script src="--><?php //echo SITE_URL; ?><!--assets/js/offline.js"></script>-->
    <?php if(isset($default_doc)):?>
    <!--  invoices functions -->
    <script class="default-doc-js" src="<?php echo SITE_URL; ?>assets/js/pages/page_<?php echo $default_doc?>.js"></script>
    <?php endif ?>
    <div class="exchange-doc-js">

    </div>
    <script src="<?php echo SITE_URL; ?>assets/js/plugins/light-autocomplete-master/light-autocomplete.js"></script>



    <!--  dropify -->
    <script src="<?php echo SITE_URL; ?>assets/js/custom/dropify/dist/js/dropify.min.js"></script>

    <!--  form file input functions -->
    <script src="<?php echo SITE_URL; ?>assets/js/pages/forms_file_input.min.js"></script>
    <script src="<?php echo SITE_URL; ?>assets/js/plugins/fancybox-master/dist/jquery.fancybox.min.js"></script>
    <script src="<?php echo SITE_URL; ?>assets/js/custom/wizard_steps.min.js"></script>
    <script src="<?php echo SITE_URL; ?>assets/js/pages/forms_wizard.js"></script>
    <script src="<?php echo SITE_URL; ?>assets/js/pages/forms_wizard.min.js"></script>
    <script src="<?php echo SITE_URL; ?>assets/js/pages/page_user_edit.min.js"></script>
    <script src="<?php echo SITE_URL; ?>assets/js/pages/forms_file_upload.js"></script>
    <script src="<?php echo SITE_URL; ?>assets/js/pages/page_mailbox.min.js"></script>
    <script src="<?php echo SITE_URL; ?>bower_components/push.js/push.min.js"></script>
    <script src="<?php echo SITE_URL; ?>assets/js/plugins/jQuery-Inplace-WYSIWYG-Rich-Text-Editor-notebook/src/js/jquery.notebook.js"></script>

    <script src="<?php echo SITE_URL; ?>assets/js/plugins/Drag-And-Drop-File-Uploader-With-Preview-Imageuploadify/imageuploadify.js"></script>
    <script src="<?php echo SITE_URL; ?>assets/js/plugins/Tiny-Text-Field-Based-Tags/dist/jQuery.tagify.js"></script>
    <script src="<?php echo SITE_URL; ?>assets/js/plugins/js-cookie-master/src/js.cookie.js"></script>

    <script src="<?php echo SITE_URL; ?>assets/js/plugins/jQuery-Plugin-To-Print-Specified-Elements-Of-Webpage-printThis/lib/jquery.printThis.js"></script>
    <script src="<?php echo SITE_URL; ?>assets/js/plugins/clipboard.js-master/dist/clipboard.min.js"></script>
    <script src="<?php echo SITE_URL; ?>assets/js/custom.js"></script>
    <?php
        if($page_name=="pos"){
    ?>
    <div id="style_switcher">
        <div id="style_switcher_toggle"><i class="material-icons">&#xE8B8;</i></div>
        <div class="uk-margin-medium-bottom">
            <?php if($page_name=="pos"){?>
            <h4 class="heading_c uk-margin-bottom">Categories</h4>
                <?php require_once "Src/inc/category-tree.php"?>
            <?php } ?>
        </div>

    </div>
    <?php } ?>
    <script>
        $(function() {
            var $switcher = $('#style_switcher'),
                $switcher_toggle = $('#style_switcher_toggle'),

                $html = $('html'),
                $body = $('body');

            $switcher_toggle.click(function(e) {
                e.preventDefault();
                $switcher.toggleClass('switcher_active');
            });

        });
    </script>
    <script src="<?php echo SITE_URL; ?>assets/js/plugins/ckeditor/ckeditor.js"></script>
    <?php
        if(isset($_REQUEST['admin'])){?>
         <script>
             access_menu_selection("<?php echo $_REQUEST['admin']?>");
         </script>
    <?php    } ?>
    <?php
        if(isset($_REQUEST['category'])){?>
         <script>
             category_tree_parent("<?php echo $_REQUEST['category']?>");
         </script>
    <?php    } ?>
    <?php
        if(isset($_REQUEST['vendor']) and $id_type_name=="vendor"){?>
         <script>
             access_stores("<?php echo $_REQUEST['vendor']?>");
             access_sub_vendor_menus("<?php echo $_REQUEST['vendor']?>");
         </script>
    <?php    } ?>


</body>
</html>
