    <div id="page_content">
        <div id="page_content_inner">
            <form method="post" class="item-add" id="form" action="add-item">
                <div class="uk-grid " data-uk-grid-margin data-uk-grid-match >
                    <div class="uk-width-large-1-11">
                        <div class="md-card user_content">
                            <h4 class="heading_c uk-margin-bottom ">Add new item</h4>
                            <div class="uk-grid" data-uk-grid-margin>
                                <div class="uk-width-large-1-2 parsley-row">
                                    <div class="uk-grid "  data-uk-grid-margin>
                                        <div class="uk-width-large-1-1 parsley-row">
                                            <select class="categories" name="category_id" required>
                                                <option value="">Select category</option>
                                            </select>
                                        </div>
                                        <div class="uk-width-large-1-1 parsley-row">
                                            <label>Item name</label>
                                            <input class="md-input item-name-field" type="text" name="item_name" required>
                                        </div>

                                        <div class="uk-width-large-1-1">
                                            <div class="bar-code-form uk-grid" data-uk-grid-margin>
                                                <div class="uk-width-medium-8-10 parsley-row">
                                                    <label>Barcode</label>
                                                    <input class="md-input bar-code-field" type="text" name="bar_code" autofocus required="">
                                                </div>
                                                <div class="uk-width-medium-2-10 parsley-row">
                                                    <button data-uk-modal="{target:'#bar-code-generator'}" type="button" class="md-btn md-btn-default barcode-generator" data-uk-tooltip="{pos:'top'}" title="Barcode generator">Barcode</button>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="uk-width-large-1-1 bar-code-img-preview">

                                        </div>

                                        <div class="uk-width-large-1-1 parsley-row">
                                            <label>Purchase</label>
                                            <input class="md-input item-purchase-field" type="text" name="purchase" value="0" required>
                                        </div>
                                        <div class="uk-width-large-1-1 parsley-row">
                                            <label>Price</label>
                                            <input class="md-input item-price-field" type="text" name="price" value="0" required>
                                        </div>
                                        <div class="uk-width-large-1-1 parsley-row">
                                            <label>Sale</label>
                                            <input class="md-input item-sale-field" type="text" name="sale" value="0" required>
                                        </div>
                                        <div class="uk-width-large-1-1 parsley-row">
                                            <label>Vat</label>
                                            <input class="md-input item-vat-field" type="text" name="vat" value="0" required>
                                        </div>

                                        <div class="uk-width-large-1-1 parsley-row">
                                            <label>Short description</label>
                                            <textarea cols="30" rows="2" class="md-input"  name="short_description"></textarea>
                                        </div>
                                        <div class="uk-width-large-1-1 parsley-row">
                                            <label>Description</label>
                                            <textarea cols="30" rows="4" class="md-input" name="description"></textarea>
                                        </div>
                                        <div class="uk-width-large-1-1 parsley-row">
                                            <h4 class="heading_c uk-margin-bottom ">Specifications</h4>
                                            <div class="uk-grid form_section specification-row" id="d_form_row" data-uk-grid-margin>
                                                <div class="uk-width-medium-1-2">
                                                    <select name="specification_name[]" class="specification_selectize">
                                                        <option value="">Select specification</option>
                                                    </select>

                                                </div>
                                                <div class="uk-width-medium-1-2">
                                                    <div class="uk-input-group">
                                                        <select name="specification_tag[0][]" class="specification_tags" multiple>
                                                            <option value="">Select tags</option>
                                                        </select>
                                                        <span class="uk-input-group-addon">
                                                            <a href="#" class="spbtnSectionClone" data-section-clone="#d_form_row"><i class="material-icons md-24">&#xE146;</i></a>
                                                        </span>
                                                    </div>
                                                </div>

                                            </div>

                                        </div>
                                    </div>
                                </div>


                                <div class="uk-width-large-1-2 uk-margin-top">
                                    <h3 class="heading_a uk-margin-small-bottom">Images</h3>
                                    <div class="uk-grid"  data-uk-grid-margin>
                                        <div class="uk-width-large-1-1 parsley-row">
                                            <input type="file" accept="image/*" multiple class="image_uploader" name="images[]">
<!--                                            <input type="file" accept="image/*" multiple name="images[]">-->
                                        </div>
                                    </div>

                                </div>

                            </div>
                            <div class="uk-grid">
                                <div class="uk-width-large-1-2">
                                    <button class="md-btn md-btn-primary" button-name="save_and_new">Save & New</button>
                                    <button class="md-btn md-btn-success" button-name="save">Save</button>
                                    <button type="reset" class="md-btn md-btn-info">Cancel</button>
                                </div>

                            </div>


                        </div>
                    </div>

                </div>
            </form>
        </div>
    </div>

    <div class="uk-modal" id="bar-code-generator">

    </div>