
<?php
    if($id_type_name!="vendor" and $id_type_name!="sub_vendor" and  !isset($_SESSION['vendor_id'])){
        $location=SITE_URL."views/vendor-list/?message=Please select a vendor first";
        header("location:$location");
    }
    if(!isset($_SESSION['store_id'])) {
        $last_used_store = $retrive->get_setting("last_update", $common->user_id, "used_store", "store_id");
        if ($last_used_store) {
            $store_id = $last_used_store[0]['val'];
            if ($id_type_name == "cs" or $id_type_name == "admin" or $id_type_name == "vendor") {
                foreach ($last_used_store as $store) {
                    $_SESSION['store_id'] = $store_id;
                    $location = SITE_URL . "views/home";
                    header("location:$location");
                }
            } elseif ($id_type_name == "sub_vendor") {
                $available_access = $retrive->get_setting("access_menu", $common->user_id, "sub_vendor_access_store", "", 1);
                foreach ($available_access as $item) {
                    $this_store_id = $item['type'];
                    if ($this_store_id == $store_id) {
                        $_SESSION['store_id'] = $store_id;
                        $location = SITE_URL . "views/home";
                        header("location:$location");
                        break;
                    }

                }
            }
            echo $store_id;
        }
    }
?>
    <div id="page_content">
        <div id="page_content_inner">
            <h4>Store list</h4>

            <div class="md-card">
                <div class="md-card-content">
                    <div class="uk-margin-bottom">
                        <a href="#" class=" " id="printTable" data-uk-tooltip="{pos:'top'}" title="Print">
                            <i class="material-icons md-icon">print</i>
                        </a>
                        <div class="uk-button-dropdown" data-uk-dropdown="{mode:'click'}" style="margin-top:-8px">
                            <a class="" href="#" data-uk-tooltip="{pos:'top'}" title="Colum select"><i class="material-icons md-icon ">reorder</i></a>
                            <div class="uk-dropdown">
                                <ul class="uk-nav uk-nav-dropdown" id="columnSelector"></ul>
                            </div>
                        </div>
                        <a href="<?php echo SITE_URL?>views/add-store" class="" data-uk-tooltip="{cls:'uk-tooltip-small',pos:'bottom'}" title="Add store"><i class="material-icons md-icon md-color-green-300">add_circle_outline</i></a>


                    </div>
                    <div class="uk-overflow-container uk-margin-bottom">
                        <table data-type="store" limit="3" class="uk-table uk-table-align-vertical uk-table-nowrap tablesorter tablesorter-altair" id="ts_pager_filter">
                            <thead>
                            <tr>
                                <th data-name="Select" data-priority="5"><input type="checkbox" class="ts_checkbox_all"></th>
                                <th class="filter-false remove sorter-false" data-priority="6">Image</th>
                                <th data-priority="critical">Name</th>
                                <th data-priority="critical">Active</th>
                                <th data-priority="1">Post</th>
                                <th data-priority="4">Entry date</th>
                                <th class="filter-false remove sorter-false uk-text-center" data-priority="1">Manage</th>
                            </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th></th>
                                    <th>Image</th>
                                    <th>Name</th>
                                    <th>Active</th>
                                    <th>Post</th>
                                    <th>Entry date</th>
                                    <th class="uk-text-center">Manage</th>
                                </tr>
                            </tfoot>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                    <ul class="uk-pagination ts_pager">
                        <li data-uk-tooltip title="Select Page">
                            <select class="ts_gotoPage ts_selectize"></select>
                        </li>
                        <li class="first"><a href="javascript:void(0)"><i class="uk-icon-angle-double-left"></i></a></li>
                        <li class="prev"><a href="javascript:void(0)"><i class="uk-icon-angle-left"></i></a></li>
                        <li><span class="pagedisplay"></span></li>
                        <li class="next"><a href="javascript:void(0)"><i class="uk-icon-angle-right"></i></a></li>
                        <li class="last"><a href="javascript:void(0)"><i class="uk-icon-angle-double-right"></i></a></li>
                        <li data-uk-tooltip title="Page Size">
                            <select class="pagesize ts_selectize">
                                <option value="5">5</option>
                                <option value="10">10</option>
                                <option value="25">25</option>
                                <option value="50">50</option>
                            </select>
                        </li>
                    </ul>

                    <button  class="load-more-button md-btn md-btn-primary md-btn-mini md-btn-wave-light md-btn-icon waves-effect waves-button waves-light">Load <span></span> more </button>

                </div>
            </div>
        </div>
    </div>
