    <?php
        $user_id=$_REQUEST['cs'];
        $controller_info=$retrive->controller_info($user_id);
    ?>
    <div id="page_content">
        <div id="page_content_inner">
            <?php
                foreach ($controller_info as $result){
            ?>
            <div class="uk-form-stacked" id="user_edit_form">
                <div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-large-1-1">
                        <h4>Edit CS</h4>
                        <form method="post" class="controller-edit" id="form" form_type="cs">
                            <div class="md-card">
                                <div class="user_heading" data-uk-sticky="{ top: 48, media: 960 }" style="background:black">
                                    <div class="user_heading_avatar fileinput fileinput-new" data-provides="fileinput">
                                        <div class="fileinput-new thumbnail">
                                            <img src="<?php echo SITE_URL?>assets/img/images/<?php echo $result['image']?>" alt="user avatar" name="image">
                                        </div>
                                        <div class="fileinput-preview fileinput-exists thumbnail"></div>
                                        <div class="user_avatar_controls">
                                        <span class="btn-file">
                                            <span class="fileinput-new"><i class="material-icons">&#xE2C6;</i></span>
                                            <span class="fileinput-exists"><i class="material-icons">&#xE86A;</i></span>
                                            <input type="file" name="image" id="image" class="image">
                                        </span>
                                            <a href="#" class="btn-file fileinput-exists" data-dismiss="fileinput"><i class="material-icons">&#xE5CD;</i></a>
                                        </div>
                                    </div>
                                    <div class="user_heading_content">
                                        <h2 class="heading_b"><span class="uk-text-truncate" id="user_edit_uname"><?php echo $result['name']?></span><span class="sub-heading" id="user_edit_position"><?php echo $result['mobile']?></span></h2>
                                    </div>
                                    <div class="md-fab-wrapper">
                                        <div class="md-fab md-fab-toolbar md-fab-small md-fab-accent">
                                            <i class="material-icons">&#xE8BE;</i>
                                            <div class="md-fab-toolbar-actions">
                                                <button type="reset" id="user_edit_print" data-uk-tooltip="{cls:'uk-tooltip-small',pos:'bottom'}" title="Cancel"><i class="material-icons">&#xE14C;</i></button>
                                                <button type="submit" id="user_edit_save" data-uk-tooltip="{cls:'uk-tooltip-small',pos:'bottom'}"  button-name="save" title="Save"><i class="material-icons md-color-white">&#xE161;</i></button>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" name="user_id" value="<?php echo $user_id;?>">
                                <div class="user_content">
                                    <ul id="user_edit_tabs" class="uk-tab" data-uk-tab="{connect:'#user_edit_tabs_content'}">
                                        <li class="uk-active"></li>

                                    </ul>
                                    <ul id="user_edit_tabs_content" class="uk-switcher uk-margin">
                                        <li>
                                            <div class="uk-margin-top uk-margin-bottom">
                                                <h3 class="full_width_in_card heading_c">
                                                    General info
                                                </h3>
                                                <div class="uk-grid" data-uk-grid-margin>
                                                    <div class="uk-width-medium-1-2 parsley-row">
                                                        <label for="user_edit_uname_control">Name</label>
                                                        <input class="md-input" type="text" id="user_edit_uname_control" name="name" required>
                                                    </div>
                                                    <div class="uk-width-medium-1-2 ">

                                                        <div class="full-mobile-number uk-grid" data-uk-grid-margin>
                                                            <div class="uk-width-medium-4-10 parsley-row">
                                                                <select class="country-code-selector" selected-id="<?php echo $result['calling_code']?>" name="calling_code" required="">
                                                                    <option value="">Select code</option>
                                                                </select>
                                                            </div>
                                                            <div class="uk-width-medium-6-10 parsley-row">
                                                                <label >Mobile</label>
                                                                <input value="<?php echo $result['mobile']?>" class="md-input mobile-number-with-country-code" type="text" name="mobile"  id="user_edit_position_control" required>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="uk-grid" data-uk-grid-margin>
                                                    <div class="uk-width-1-2  parsley-row">
                                                        <label>Email</label>
                                                        <input type="text" class="md-input" name="email" value="<?php echo $result['email']?>" required>
                                                    </div>
                                                    <div class="uk-width-medium-1-2 parsley-row">
                                                        <label >Password</label>
                                                        <input class="md-input" type="text" name="password" >
                                                    </div>
                                                    <div class="uk-width-medium-1-2 parsley-row">
                                                    </div>
                                                    <div class="uk-width-medium-1-2 parsley-row">
                                                        <label for="">Active</label>
                                                        <input type="checkbox" data-switchery <?php if($result['active']==1){echo "checked";} ?> value="1" name="active"/>
                                                    </div>
                                                </div>

                                                <h3 class="full_width_in_card heading_c">
                                                    Contact info
                                                </h3>
                                                <div class="uk-grid" data-uk-grid-margin>
                                                    <div class="uk-width-1-2  parsley-row">
                                                        <label>Address line 1</label>
                                                        <input type="text" class="md-input" name="address_line_1" value="<?php echo $result['address_line_1']?>" required>
                                                    </div>
                                                    <div class="uk-width-1-2  parsley-row">
                                                        <label>Address line 2</label>
                                                        <input type="text" class="md-input" name="address_line_2" value="<?php echo $result['address_line_2']?>" >
                                                    </div>
                                                </div>
                                                <div class="uk-grid" data-uk-grid-margin>
                                                    <div class="uk-width-1-2  parsley-row">
                                                        <label>Post code</label>
                                                        <input type="text" class="md-input" name="post_code" value="<?php echo $result['post_code']?>" required>
                                                    </div>
                                                    <div class="uk-width-1-2 own-country-name  parsley-row">
                                                        <label>Country</label>
                                                        <input type="text" class="md-input own-country-name"  value="" disabled>
                                                    </div>
                                                </div>

                                                <div class="uk-grid">
                                                    <div class="uk-width-large-1-2">
                                                        <button class="md-btn md-btn-success" button-name="save">Save</button>
                                                        <button type="reset" class="md-btn md-btn-info">Cancel</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <?php } ?>
        </div>
    </div>
