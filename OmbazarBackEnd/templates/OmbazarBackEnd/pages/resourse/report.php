<?php
    $report_id=$_REQUEST['report'];
    $report_info=$retrive->report_info($report_id);
    foreach($report_info as $info){
        $date_range=$info['date_range'];
        $optional_type=$info['optional_type'];
        $report_type=$info['type'];
        $explode=explode("to",$date_range);
        $start_date=$explode[0];
        $end_date=$explode[1];
?>
<form class="report-info-form">
    <input type="hidden" name="start_date" value="<?php echo $start_date ?>">
    <input type="hidden" name="end_date" value="<?php echo $end_date ?>">
    <input type="hidden" name="type" value="<?php echo $report_type ?>">
    <input type="hidden" name="option" value="<?php echo $optional_type ?>">
    <?php
        if($optional_type!="All Activities"){
    ?>
        <input type="hidden" name="user_id" value="<?php echo $optional_type ?>">
    <?php } ?>
</form>

<?php

    if($report_type=="Income"){
        require_once("Src/inc/reports/income-report.php");
    }
    elseif($report_type=="Activities"){
        require_once("Src/inc/reports/activities.php");
    }
    elseif($report_type=="Expense"){
        require_once("Src/inc/reports/expense-report.php");
    }
    elseif($report_type=="Income by Customer"){
        require_once("Src/inc/reports/income-by-customer.php");
    }
    elseif($report_type=="Expense by Vendor"){
        require_once("Src/inc/reports/expense-by-vendor.php");
    }
    elseif($report_type=="Sales Tax"){
        require_once("Src/inc/reports/sales-tax-report.php");
    }

    elseif($report_type=="Bank Cash Balance"){
        require_once("Src/inc/reports/bank-cash-balance-report.php");
    }

    elseif($report_type=="Stock Balance"){
        require_once("Src/inc/reports/stock-balance-report.php");
    }
    elseif($report_type=="Short List"){
        require_once("Src/inc/reports/short-list-report.php");
    }
    elseif($report_type=="Income Statement"){
        require_once("Src/inc/reports/income-statement-report.php");
    }
    elseif($report_type=="Balance Sheet"){
        require_once("Src/inc/reports/balance-sheet-report.php");
    }

    elseif($report_type=="Transactions"){
        require_once("Src/inc/reports/transactions.php");
    }
    elseif($report_type=="Customer from Zero"){
        require_once("Src/inc/reports/from-zero-report.php");
    }
    elseif($report_type=="Vendor from Zero"){
        require_once("Src/inc/reports/from-zero-report.php");
    }
    elseif($report_type=="Return"){
        require_once("Src/inc/reports/return-report.php");
    }
    elseif($report_type=="Customer report"){
        require_once("Src/inc/reports/customer-vendor-report.php");
    }
    elseif($report_type=="Vendor report"){
        require_once("Src/inc/reports/customer-vendor-report.php");
    }
    elseif($report_type=="Item statement"){
        require_once("Src/inc/reports/item-statement-report.php");
    }
    elseif($report_type=="Transport statement"){
        require_once("Src/inc/reports/transport-statement-report.php");
    }
    elseif($report_type=="Cost statement"){
        require_once("Src/inc/reports/cost-statement-report.php");
    }



?>
<?php } ?>