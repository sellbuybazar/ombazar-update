    <div id="page_content">
        <div id="page_content_inner">
            <div class="uk-form-stacked" id="user_edit_form">
                <div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-large-1-1">
                        <h4>Add new patient</h4>
                        <div class="md-card">
                            <div class="user_heading" data-uk-sticky="{ top: 48, media: 960 }">
                                <form class="file-form" name="file_form" id="">
                                <div class="user_heading_avatar fileinput fileinput-new" data-provides="fileinput">
                                    <div class="fileinput-new thumbnail">
                                        <img src="<?php echo SITE_URL?>assets/img/default/profile.png" alt="user avatar"/>
                                    </div>
                                    <div class="fileinput-preview fileinput-exists thumbnail"></div>
                                    <div class="user_avatar_controls">
                                        <span class="btn-file">
                                            <span class="fileinput-new"><i class="material-icons">&#xE2C6;</i></span>
                                            <span class="fileinput-exists"><i class="material-icons">&#xE86A;</i></span>
                                            <input type="file" name="image" id="image" class="image">
                                        </span>
                                        <a href="#" class="btn-file fileinput-exists" data-dismiss="fileinput"><i class="material-icons">&#xE5CD;</i></a>
                                    </div>
                                </div>
                                </form>
                                <div class="user_heading_content">
                                    <h2 class="heading_b"><span class="uk-text-truncate" id="user_edit_uname">User name</span><span class="sub-heading" id="user_edit_position">Patient name</span></h2>
                                </div>
                                <div class="md-fab-wrapper">
                                    <div class="md-fab md-fab-toolbar md-fab-small md-fab-accent">
                                        <i class="material-icons">&#xE8BE;</i>
                                        <div class="md-fab-toolbar-actions">
                                            <button type="reset" id="user_edit_print" data-uk-tooltip="{cls:'uk-tooltip-small',pos:'bottom'}" title="Cancel"><i class="material-icons">&#xE14C;</i></button>
                                            <button type="submit" id="user_edit_save" data-uk-tooltip="{cls:'uk-tooltip-small',pos:'bottom'}"  button-name="save" title="Save"><i class="material-icons md-color-white">&#xE161;</i></button>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="user_content">
                                <ul id="user_edit_tabs" class="uk-tab" data-uk-tab="{connect:'#user_edit_tabs_content'}">
                                    <li class="uk-active"><a href="#">patient</a></li>

                                </ul>
                                <ul id="user_edit_tabs_content" class="uk-switcher uk-margin">
                                    <li>
                                        <form method="post" class="customer-add" id="customer-form">
                                            <div class="uk-margin-top uk-margin-bottom">
                                                <h3 class="full_width_in_card heading_c">
                                                    General info
                                                </h3>
                                                <div class="uk-grid" data-uk-grid-margin>
                                                    <div class="uk-width-medium-1-2 parsley-row">
                                                        <label for="user_edit_uname_control">Name</label>
                                                        <input class="md-input" type="text" id="user_edit_uname_control" name="name" required>
                                                    </div>
                                                    <div class="uk-width-medium-1-2 parsley-row">
                                                        <select data-md-selectize name="gender">
                                                            <option value="">Select gender</option>
                                                            <option value="Male">Male</option>
                                                            <option value="Female">Female</option>
                                                            <option value="Common">Common</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="uk-grid" data-uk-grid-margin>
                                                    <div class="uk-width-medium-1-2 parsley-row">
                                                        <label >Birth date</label>
                                                        <input class="md-input" data-uk-datepicker="{format:'DD.MM.YYYY'}" type="text" name="birth_date" required id="user_edit_position_control">
                                                    </div>
                                                    <div class="uk-width-medium-1-2 parsley-row">
                                                        <label >Location</label>
                                                        <input class="md-input" type="text" name="location" required>
                                                    </div>

                                                </div>

                                                <h3 class="full_width_in_card heading_c">
                                                    Contact info
                                                </h3>
                                                <div class="uk-grid">
                                                    <div class="uk-width-1-1">
                                                        <div class="uk-grid uk-grid-width-1-1 uk-grid-width-large-1-2" data-uk-grid-margin>
                                                            <div>
                                                                <div class="uk-input-group parsley-row">
                                                                    <span class="uk-input-group-addon">
                                                                        <i class="md-list-addon-icon material-icons">&#xE0CD;</i>
                                                                    </span>
                                                                    <label>Phone Number</label>
                                                                    <input type="text" class="md-input" name="phone" value="" required>
                                                                </div>
                                                            </div>
                                                            <div>
                                                                <div class="uk-input-group parsley-row">
                                                                    <span class="uk-input-group-addon">
                                                                        <i class="md-list-addon-icon material-icons">&#xE158;</i>
                                                                    </span>
                                                                    <label>Email</label>
                                                                    <input type="text" class="md-input" name="email" value="" >
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="uk-grid">
                                                    <div class="uk-width-large-1-2">
                                                        <button class="md-btn md-btn-primary" button-name="save_and_new">Save & New</button>
                                                        <button class="md-btn md-btn-success" button-name="save">Save</button>
                                                        <button type="reset" class="md-btn md-btn-info">Cancel</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </li>



                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
