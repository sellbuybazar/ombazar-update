
    <div id="page_content">
        <div id="page_content_inner">
            <?php
                $item_id=$_REQUEST['item'];
                $info=$retrive->item_info($item_id);
                foreach($info as $result){
            ?>
            <form method="post" class="item-edit" id="form">
                <div class="uk-grid " data-uk-grid-margin data-uk-grid-match >
                    <div class="uk-width-large-1-11">
                        <div class="md-card user_content">
                            <h4 class="heading_c uk-margin-bottom ">Edit item</h4>
                            <input type="hidden" name="item_id" value="<?php echo $item_id;?>">
                            <div class="uk-grid" data-uk-grid-margin>
                                <div class="uk-width-large-1-2 parsley-row">
                                    <div class="uk-grid "  data-uk-grid-margin>
                                        <div class="uk-width-large-1-1 parsley-row">
                                            <select class="categories" name="category_id" selected-id="<?php echo $result['category_id']?>" required>
                                                <option value="">Select category</option>
                                            </select>
                                        </div>
                                        <div class="uk-width-large-1-1 parsley-row">
                                            <label>Item name</label>
                                            <input class="md-input item-name-field" type="text" name="item_name" value="<?php echo $result['item_name']?>" required>
                                        </div>
                                        <div class="uk-width-large-1-1 ">

                                            <div class="uk-width-medium-1-1 parsley-row">
                                                <label>Barcode</label>
                                                <?php
                                                $bar_code=$result['bar_code'];
                                                ?>
                                                <input class="md-input bar-code-field" type="text" <?php if(!$result['bar_code']==""){?> disabled <?php } else{?>  name="bar_code"  <?php }?>  autofocus value='<?php echo $bar_code?>' required  >

                                            </div>
                                            <?php if($result['bar_code']!=""){?>
                                                <input type="hidden" name="bar_code" value='<?php echo $bar_code?>'>
                                            <?php } ?>

                                        </div>

                                        <div class="uk-width-large-1-1 parsley-row">
                                            <label>Purchase</label>
                                            <input class="md-input item-purchase-field" type="text" name="purchase" value="<?php echo $result['purchase']?>" required >
                                        </div>
                                        <div class="uk-width-large-1-1 parsley-row">
                                            <label>Price</label>
                                            <input class="md-input item-price-field" type="text" name="price" value="<?php echo $result['price']?>" required>
                                        </div>
                                        <div class="uk-width-large-1-1 parsley-row">
                                            <label>Sale</label>
                                            <input class="md-input item-sale-field" type="text" name="sale" value="<?php echo $result['sale']?>" >
                                        </div>
                                        <div class="uk-width-large-1-1 parsley-row">
                                            <label>Vat</label>
                                            <input class="md-input item-vat-field" type="text" name="vat" value="<?php echo $result['vat']?>" >
                                        </div>
                                        <div class="uk-width-large-1-1 parsley-row">
                                            <label>Short description</label>
                                            <textarea cols="30" rows="2" class="md-input"  name="short_description"><?php echo $result['short_description']?></textarea>
                                        </div>
                                        <div class="uk-width-large-1-1 parsley-row">
                                            <label>Description</label>
                                            <textarea cols="30" rows="4" class="md-input" name="description"><?php echo $result['description'];?></textarea>
                                        </div>
                                        <div class="uk-width-large-1-1 parsley-row">
                                            <h4 class="heading_c uk-margin-bottom ">Specifications
                                                <input type="checkbox" class="exin_stat" name="specification_available" value="1">
                                            </h4>
                                            <?php
                                                $item_specifications=$retrive->item_specificatons($item_id);
                                                if($item_specifications){
                                                    $i=0;
                                                    foreach($item_specifications as $specification){
                                            ?>
                                            <div class="uk-grid form_section specification-row exin_range hidden" id="d_form_row" data-uk-grid-margin>
                                                <div class="uk-width-medium-1-2">
                                                    <select disabled="" class="specification_selectize" select-id="<?php echo $specification['type']?>">
                                                        <?php
                                                            $specification_id=$specification['type'];
                                                            $specification_selected_tags=$specification['val'];
                                                            $specification_name_id=$specification['id'];
                                                            $info=$retrive->info_by_table_colum("settings","id",$specification_id);
                                                            $sp_name=$info[0]['type'];
                                                            $sp_tags=$info[0]['val'];
                                                        ?>
                                                        <option value=""  selected><?php echo $sp_name;?></option>
                                                    </select>
                                                    <input type="hidden" class="hidden_specification_selectize" name="specification_name[]" value="<?php echo $specification_id?>">

                                                </div>
                                                <div class="uk-width-medium-1-2">
                                                    <div class="uk-input-group">
                                                        <select name="specification_tag[<?php echo $i;?>][]" class="specification_tags" multiple>
                                                            <option value="">Select tags</option>
                                                            <!-- this below selected tags-->
                                                            <?php
                                                            $sp_tags_divide=explode(",",$specification_selected_tags);
                                                            foreach ($sp_tags_divide as $tag){
                                                                ?>
                                                                <option selected value=<?php echo $tag;?>><?php echo $tag;?></option>
                                                            <?php } ?>
                                                            <!-- this below for available tags-->
                                                            <?php
                                                                $sp_tags_divide=explode(",",$sp_tags);
                                                                foreach ($sp_tags_divide as $tag){
                                                            ?>
                                                            <option  value=<?php echo $tag;?>><?php echo $tag;?></option>
                                                            <?php } ?>
                                                        </select>
                                                        <span class="uk-input-group-addon">
                                                            <?php if($i!=0){?>
                                                            <a href="#" class="btnSectionRemove"><i class="material-icons md-24">&#xE872;</i></a>
                                                            <?php }else{?>
                                                            <a href="#" class="spbtnSectionClone" data-section-clone="#d_form_row"><i class="material-icons md-24">&#xE146;</i></a>
                                                            <?php } ?>
                                                        </span>
                                                    </div>
                                                </div>

                                            </div>
                                            <?php $i++; }} else{?>
                                            <div class="uk-grid form_section specification-row exin_range hidden" id="d_form_row" data-uk-grid-margin>
                                                <div class="uk-width-medium-1-2">
                                                    <select name="specification_name[]" class="specification_selectize" selected-id="">
                                                        <option value="">Select specification</option>
                                                    </select>

                                                </div>
                                                <div class="uk-width-medium-1-2">
                                                    <div class="uk-input-group">
                                                        <select name="specification_tag[0][]" class="specification_tags" multiple>
                                                            <option value="">Select tags</option>
                                                        </select>
                                                        <span class="uk-input-group-addon">
                                                            <a href="#" class="spbtnSectionClone" data-section-clone="#d_form_row"><i class="material-icons md-24">&#xE146;</i></a>
                                                        </span>
                                                    </div>
                                                </div>

                                            </div>
                                            <?php } ?>

                                        </div>
                                    </div>
                                </div>


                                <div class="uk-width-large-1-2 uk-margin-top">

                                    <div class="gallery_grid uk-grid-width-medium-1-4 uk-grid-width-large-1-4" data-uk-grid="{gutter: 16}">
                                        <?php
                                            $images=$retrive->item_images($item_id);
                                            foreach($images as $image_info){
                                        ?>
                                        <div id="delete_id_<?php echo $image_info['id']?>">
                                            <div class="md-card md-card-hover">
                                                <div class="gallery_grid_item edit-galary-grid-item md-card-content">
                                                    <a href="<?php echo SITE_URL;?>assets/img/images/<?php echo $image_info['image'];?>" data-uk-lightbox="{group:'gallery'}">
                                                        <img src="<?php echo SITE_URL;?>assets/img/images/<?php echo $image_info['image'];?>" alt="">
                                                        <i class="md-icon material-icons data-delete-button" data-delete-table="images" data-id="<?php echo $image_info['id']?>">delete_forever</i>
                                                    </a>

                                                </div>
                                            </div>
                                        </div>
                                        <?php } ?>
                                    </div>
                                    <h3 class="heading_a uk-margin-small-bottom">Images</h3>
                                    <div class="uk-grid"  data-uk-grid-margin>
                                        <div class="uk-width-large-1-1 parsley-row">
                                            <input type="file" accept="image/*" multiple class="image_uploader" name="images[]">
<!--                                            <input type="file" accept="image/*" multiple name="images[]">-->
                                        </div>
                                    </div>

                                </div>

                            </div>
                            <div class="uk-grid">
                                <div class="uk-width-large-1-2">
                                    <button class="md-btn md-btn-primary" button-name="save_and_new">Save</button>
                                    <button type="reset" class="md-btn md-btn-info">Cancel</button>
                                </div>

                            </div>


                        </div>
                    </div>

                </div>
            </form>
            <?php } ?>
        </div>
    </div>
