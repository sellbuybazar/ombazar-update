    <div id="page_content">
        <div id="page_content_inner">
            <?php
                $id=$_REQUEST['id'];
                $info=$retrive->corporate_info($id);
//                print_r($info);
                foreach($info as $result){
            ?>
            <form method="post" class="corporate-edit" id="form">
                <input  type="hidden" name="id" value="<?php echo $id ?>">
                <div class="uk-grid " data-uk-grid-margin data-uk-grid-match >
                    <div class="uk-width-large-1-11">
                        <div class="md-card user_content">
                            <h4 class="heading_c uk-margin-bottom ">Add new corporate</h4>
                            <div class="uk-grid" data-uk-grid-margin>
                                <div class="uk-width-large-1-2 parsley-row">
                                    <div class="uk-grid "  data-uk-grid-margin>
                                        <div class="uk-width-large-1-1 parsley-row">
                                            <label>Company name</label>
                                            <input class="md-input item-name-field" type="text" name="name" required value="<?php echo $result['name']?>">
                                        </div>
                                        <div class="uk-width-large-1-1 parsley-row">
                                            <span>Tags</span>
                                            <input class=" item-tag-field corporate-tags-selectize" type="text" name="tags"  value="<?php echo $result['tags']?>" required>
                                        </div>
                                        <div class="uk-width-large-1-1 parsley-row">
                                            <label>Phone</label>
                                            <input class="md-input item-phone-field" type="text" name="phone" value="<?php echo $result['phone']?>" required>
                                        </div>
                                        <div class="uk-width-large-1-1 parsley-row">
                                            <label>Email</label>
                                            <input class="md-input item-email-field" type="email" name="email"  value="<?php echo $result['email']?>" required>
                                        </div>
                                        <div class="uk-width-large-1-1 parsley-row">
                                            <label>Location</label>
                                            <input class="md-input item-location-field" type="text" name="location"  value="<?php echo $result['location']?>" required>
                                        </div>

                                        <div class="uk-width-large-1-1 parsley-row">
                                            <label>Address</label>
                                            <textarea cols="30" rows="2" class="md-input" name="address"><?php echo $result['address']?></textarea>
                                        </div>
                                        <div class="uk-width-large-1-1 parsley-row">
                                            <?php
                                                $notes=$result['notes'];
                                                if($notes){
                                                    $i=0;
                                                    foreach($notes as $note){
                                            ?>
                                            <div class="uk-grid form_section" id="d_form_row" data-uk-grid-margin>
                                                <div class="uk-width-medium-1-1">
                                                    <div class="uk-input-group">
                                                        <label>Note</label>
                                                        <input type="text" class="md-input" name="notes[]" value="<?php echo $note['val']?>">
                                                        <input type="hidden" name="before_note[]" value="<?php echo $note['type']?>">

                                                        <span class="uk-input-group-addon">
                                                            <?php if($i==0){?>
                                                            <a href="#" class="spbtnSectionClone" data-section-clone="#d_form_row"><i class="material-icons md-24">&#xE146;</i></a>
                                                            <?php } ?>
                                                        </span>

                                                    </div>
                                                </div>

                                            </div>
                                            <?php $i++; } }else{?>
                                                    <div class="uk-grid form_section" id="d_form_row" data-uk-grid-margin>
                                                        <div class="uk-width-medium-1-1">
                                                            <div class="uk-input-group">
                                                                <label>Note</label>
                                                                <input type="text" class="md-input" name="notes[]">
                                                                <input type="hidden" name="before_note[]" value="">
                                                                <span class="uk-input-group-addon">
                                                            <a href="#" class="spbtnSectionClone" data-section-clone="#d_form_row"><i class="material-icons md-24">&#xE146;</i></a>
                                                        </span>
                                                            </div>
                                                        </div>

                                                    </div>
                                            <?php } ?>

                                        </div>
                                    </div>
                                </div>

                                <div class="uk-width-large-1-2 uk-margin-top">
                                    <div class="gallery_grid uk-grid-width-medium-1-4 uk-grid-width-large-1-4" data-uk-grid="{gutter: 16}">
                                        <?php
                                        $images=$result['images'];
                                        foreach($images as $image_info){
                                            ?>
                                            <div id="delete_id_<?php echo $image_info['id']?>">
                                                <div class="md-card md-card-hover">
                                                    <div class="gallery_grid_item edit-galary-grid-item md-card-content">
                                                        <a href="<?php echo SITE_URL;?>assets/img/images/<?php echo $image_info['image'];?>" data-uk-lightbox="{group:'gallery'}">
                                                            <img src="<?php echo SITE_URL;?>assets/img/images/<?php echo $image_info['image'];?>" alt="">
                                                            <i class="md-icon material-icons data-delete-button" data-delete-table="images" data-id="<?php echo $image_info['id']?>">delete_forever</i>
                                                        </a>

                                                    </div>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    </div>
                                    <h3 class="heading_a uk-margin-small-bottom">Images</h3>
                                    <div class="uk-grid"  data-uk-grid-margin>
                                        <div class="uk-width-large-1-1 parsley-row">
                                            <input type="file" accept="image/*" multiple class="image_uploader" name="images[]">
<!--                                            <input type="file" accept="image/*" multiple name="images[]">-->
                                        </div>
                                    </div>

                                </div>

                            </div>
                            <div class="uk-grid">
                                <div class="uk-width-large-1-2">
                                    <button class="md-btn md-btn-primary" button-name="save_and_new">Save</button>
                                    <button type="reset" class="md-btn md-btn-info">Cancel</button>
                                </div>

                            </div>


                        </div>
                    </div>

                </div>
            </form>
            <?php } ?>
        </div>
    </div>
