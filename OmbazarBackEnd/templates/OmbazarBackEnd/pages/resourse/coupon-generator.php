<div id="page_content">
    <div id="page_content_inner">
        <div class="md-card">
            <form method="" class="coupon-generate" id="form">
                <div class="uk-grid " data-uk-grid-margin data-uk-grid-match >
                    <div class="uk-width-large-1-11">
                        <div class="md-card user_content">
                            <h4 class="heading_c uk-margin-bottom ">Coupon generator </h4>
                            <div class="uk-grid">
                                <div class="uk-width-large-1-2 uk-margin-bottom parsley-row">
                                    <label>Amount</label>
                                    <input class="md-input" type="text" name="amount" required>
                                </div>
                                <div class="uk-width-large-1-2 uk-margin-bottom parsley-row">
                                    <select class="" data-md-selectize name="type" required>
                                        <option value="">Select type</option>
                                        <option value="Print">Print</option>
                                        <option value="Digital">Digital</option>
                                    </select>

                                </div>

                            </div>
                            <div class="uk-grid">
                                <div class="uk-width-large-1-2">
                                    <button class="md-btn md-btn-primary" button-name="save_and_new">generate</button>
                                    <button type="reset" class="md-btn md-btn-info">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </form>
        </div>

        <div class="md-card">
            <div class="md-card-content">
                <div class="uk-margin-bottom">
                    <a href="#" class="area-print-button"  data-uk-tooltip="{pos:'top'}" title="Print">
                        <i class="material-icons md-icon">print</i>
                    </a>
                    <div class="uk-button-dropdown" data-uk-dropdown="{mode:'click'}" style="margin-top:-8px">
                        <a class="" href="#" data-uk-tooltip="{pos:'top'}" title="Colum select"><i class="material-icons md-icon ">reorder</i></a>
                        <div class="uk-dropdown">
                            <ul class="uk-nav uk-nav-dropdown" id="columnSelector"></ul>
                        </div>
                    </div>

                </div>
                <div class="uk-overflow-container uk-margin-bottom print-visible">
                    <table data-type="coupon" limit="100" class=" uk-table uk-table-align-vertical uk-table-nowrap tablesorter tablesorter-altair" id="ts_pager_filter">
                        <thead class="hidden-print">
                        <tr>
                            <th data-name="Select" data-priority="5"><input type="checkbox" class="ts_checkbox_all"></th>
                            <th data-priority="1">Coupon</th>
                            <th data-priority="2">Type</th>
                            <th data-priority="2">Batch</th>
                            <th data-priority="4">Entry date</th>
                        </tr>
                        </thead>
                        <tfoot class="hidden-print">
                        <tr>
                            <th></th>
                            <th>Coupon</th>
                            <th>Type</th>
                            <th>Batch</th>
                            <th>Entry date</th>
                        </tr>
                        </tfoot>
                        <tbody>

                        </tbody>
                    </table>
                </div>
                <ul class="uk-pagination ts_pager">
                    <li data-uk-tooltip title="Select Page">
                        <select class="ts_gotoPage ts_selectize"></select>
                    </li>
                    <li class="first"><a href="javascript:void(0)"><i class="uk-icon-angle-double-left"></i></a></li>
                    <li class="prev"><a href="javascript:void(0)"><i class="uk-icon-angle-left"></i></a></li>
                    <li><span class="pagedisplay"></span></li>
                    <li class="next"><a href="javascript:void(0)"><i class="uk-icon-angle-right"></i></a></li>
                    <li class="last"><a href="javascript:void(0)"><i class="uk-icon-angle-double-right"></i></a></li>
                    <li data-uk-tooltip title="Page Size">
                        <select class="pagesize ts_selectize">
                            <option value="5">5</option>
                            <option value="10">10</option>
                            <option value="25">25</option>
                            <option value="50">50</option>
                        </select>
                    </li>
                </ul>

                <button  class="load-more-button md-btn md-btn-primary md-btn-mini md-btn-wave-light md-btn-icon waves-effect waves-button waves-light">Load <span></span> more </button>

            </div>
        </div>
    </div>
</div>
