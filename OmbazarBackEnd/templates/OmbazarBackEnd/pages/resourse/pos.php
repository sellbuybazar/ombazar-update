    <div id="page_content">
        <div id="page_content_inner">
            <div class="uk-grid" data-uk-grid-margin>
                <div class="uk-width-medium-1-3 ">
                    <div class="pos-selected-items-part pos" data-uk-sticky="{ top: 60, media: 960 }">
                        <div class="md-card">
                            <div class="md-card-content">
                                <form class="pos-search-form">
                                    <div class="uk-grid" data-uk-grid-margin>
                                        <div class="uk-width-medium-2-10">
                                            <div  class="add-pos-user md-fab  uk-button-dropdown" data-uk-dropdown="{pos:'right-top',mode:'click'}">
                                                <span><i class="material-icons">person_add</i></span>
                                                <div class="uk-dropdown">
                                                    <div class="uk-grid">
                                                        <div class="uk-width-medium-1-1">
                                                            <select class="pos-user-search" name="pos_user">
                                                                <option value="">Search user</option>
                                                            </select>

                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="uk-width-medium-6-10 relative">
                                            <ul class="uk-tab pos-input-tabs" data-uk-tab="{connect:'#pos_input'}">
                                                <li><i class="material-icons">keyboard_arrow_left</i></li>
                                                <li><i class="material-icons">keyboard_arrow_right</i></li>
                                            </ul>
                                            <ul id="pos_input" class="uk-switcher">
                                                <li>
                                                    <label for="product_bar_code_name">Bar code</label>
                                                    <input type="text" class="md-input" id="product_bar_code_name" name="bar_code">
                                                </li>
                                                <li>
                                                    <label for="product_search_name">Item Name</label>
                                                    <input type="text" class="md-input" id="product_search_name" name="product_search_name">
                                                </li>
                                            </ul>

                                        </div>
                                        <div class="uk-width-medium-2-10 uk-text-center">
                                            <button  class="search-pos-item md-fab" >
                                                <i class="material-icons">search</i>
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="md-card-content  pos-content">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-1-1">
                                        <table class="uk-table uk-margin-bottom-remove">
                                            <thead>
                                            <tr>
                                                <th>Item Name</th>
                                                <th>Price</th>
                                                <th>Qnt</th>
                                                <th></th>
                                            </tr>
                                            </thead>
                                        </table>

                                        <!-- hidden content for cart element manage -->
                                        <script class="cart-item-template" type="text/x-handlebars-template">
                                            {{#each rows}}
                                            {{#if item_id}}
                                            <tr class="cart-item" data-id="{{item_id}}" >
                                                <td class=" ">
                                                    <img class="img_thumb" src="{{image_path}}" alt="">
                                                    <div class="inline-block"  data-uk-tooltip="{pos:'top'}" title="{{item_name}}Point : {{point}}">
                                                        {{item_name}}
                                                        {{#if sale}}
                                                        <span class="block"><del class="">{{price}}</del> <?php echo $retrive->currency_symbol();?></span>
                                                        <span class="">{{sale}}</span> <?php echo $retrive->currency_symbol();?>
                                                        {{else}}
                                                        <span class="">{{price}}</span> <?php echo $retrive->currency_symbol();?>
                                                        {{/if}}
                                                    </div>
                                                </td>
                                                <td class="" >
                                                    {{#if sale}}
                                                    <span class="uk-block"><del class=" del-cart-line-total">{{equation price '*' quantity}}</del> <?php echo $retrive->currency_symbol();?></span>
                                                    <span class="cart-line-total">{{equation sale '*' quantity}}</span> <?php echo $retrive->currency_symbol();?>
                                                    {{else}}
                                                    <span class="cart-line-total">{{equation price '*' quantity}}</span> <?php echo $retrive->currency_symbol();?>
                                                    {{/if}}
                                                </td>
                                                <td>
                                                    <div class="quantity-manager">
                                                        <input type="number" min="0" class="quantity hover" value="{{quantity}}">
                                                    </div>
                                                </td>
                                                <td class=" cart-line-total-field"><a href="#" class="cart-delete-button"><i class="material-icons " style="color:md-color-cyan-700">close</i></a>
                                                </td>
                                            </tr>
                                            {{/if}}
                                            {{/each}}
                                        </script>

                                        <div class="uk-overflow-container">
                                            <div class="cart-message"></div>
                                            <table class="uk-table cart-table">
                                                <tbody>



                                                </tbody>
                                            </table>
                                        </div>

                                    </div>
                                </div>
                                <div class="calculation uk-margin-top">
                                    <div class="uk-grid" data-uk-grid-margin>
                                        <div class="uk-width-medium-3-10">
                                            <label>Discount</label>
                                            <input type="text" class="md-input cart-discount-field">
                                        </div>
                                        <div class="uk-width-medium-3-10">
                                            <label>Change</label>
                                            <input type="text" class="md-input cart-change-field">
                                        </div>

                                        <div class="uk-width-medium-4-10 uk-text-right">
                                            <div class="sub-total">
                                                Sub total= <span class="cart-sub-total"></span>
                                            </div>
                                            <div class="vat-total">
                                                Vat= <span></span>
                                            </div>
                                            <div class="discount">
                                                Discount= <span class="cart-discount"></span>
                                            </div>

                                            <div class="total">
                                                Total= <span class="cart-total"></span>
                                            </div>
                                        </div>
                                    </div>


                                </div>
                                <hr class="hr">
<!--                                <div class="change-element">-->
<!--                                    <div class="inline"><span class="amount">600</span> =</div>-->
<!--                                    <div class="inline"><span class="amount">500</span>×<span class="amount-quantity">1</span> </div> ,-->
<!--                                    <div class="inline"><span class="amount">100</span>×<span class="amount-quantity">2</span> </div> ,-->
<!--                                    <div class="inline"><span class="amount">100</span>×<span class="amount-quantity">10</span> </div> ,-->
<!--                                    <div class="inline"><span class="amount">50</span>×<span class="amount-quantity">30</span> </div> ,-->
<!--                                    <div class="inline"><span class="amount">10</span>×<span class="amount-quantity">1</span> </div>-->
<!--                                </div>-->


                                <div class="procceed-buttons uk-text-center">
                                    <button href="#" class="md-btn md-btn-flat md-btn-wave waves-effect waves-button">Cancel</button>
                                    <button class="md-btn md-btn-success uk-margin-small-top pos-payment-button">Payment</button>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="uk-width-medium-2-3">
                    <div class="pos-items pos uk-grid-width-small-1-2 uk-grid-width-medium-1-3 uk-grid-width-large-1-4 hierarchical_show" data-uk-grid="{gutter: 20, controls: '#products_sort'}">


                    </div>
                </div>
            </div>


        </div>
    </div>