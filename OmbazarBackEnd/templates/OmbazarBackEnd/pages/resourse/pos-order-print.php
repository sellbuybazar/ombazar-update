    <div id="page_content">
        <div id="page_content_inner">
            <div class="uk-grid" data-uk-grid-margin>
                <div class="uk-width-medium-3-10 "></div>
                <div class="uk-width-medium-4-10 ">
                    <?php
                        $order_id=$_REQUEST['order'];
                        $order_info=$retrive->order_info($order_id);
                        foreach ($order_info as $result){
                    ?>
                    <div class="md-card pos-print">
                        <div class="md-card-content">
                            <?php
                            $store_info=$result['stores_info'];
                            foreach ($store_info as $store){
                            ?>
                            <p class="uk-text-center">
                                <img src="<?php echo $retrive->store_logo();?>">

                                <span class="uk-block"><?php echo $store['address_line_1'];?></span>
                                <span class="uk-block"><?php echo $store['address_line_2'];?></span>

                            </p>
                            <p class="uk-text-center pos-border-heading">
                                <span><?php echo $store['store_name'];?></span>
                            </p>
                            <?php } ?>
                            <table>
                                <tr>
                                    <th>Bill no</th>
                                    <th>:</th>
                                    <th><?php echo $result['order_id'];?></th>
                                </tr>
                                <tr>
                                    <td>Date</td>
                                    <td>:</td>
                                    <td>
                                        <?php
                                            echo $common->simple_date($result['time'],"d/m/Y H:i:s a")
                                        ?>
                                    </td>
                                </tr>
                            </table>
                            <?php
                                $customer_info=$result['user_info'];
                                foreach ($customer_info as $customer){
                            ?>
                            <table>
                                <tr>
                                    <td>Customer id</td>
                                    <td>:</td>
                                    <td><?php echo $customer['user_id'];?></td>
                                </tr>
                                <tr>
                                    <td>Customer name</td>
                                    <td>:</td>
                                    <td><?php echo $customer['name'];?></td>
                                </tr>

                            </table>
                            <?php } ?>
                            <table  width="100%">
                                <tr class="uk-text-left">
                                    <th>Sl</th>
                                    <th>Item</th>
                                    <th>Price</th>
                                    <th>Quantity</th>
                                    <th>Total</th>
                                </tr>
                                <?php
                                    $i=1;
                                    $order_items=$result['orders'];
                                    foreach($order_items as $item){
                                ?>
                                    <tr>
                                        <td>
                                            <?php echo $i;?>
                                        </td>
                                        <td colspan="3">
                                            <?php echo $item['name'];?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>
                                            <?php echo $item['item_code']?>
                                        </td>
                                        <td>
                                            <?php echo $item['price']?>
                                        </td>
                                        <td>
                                            <?php echo $item['quantity']?>
                                        </td>
                                        <td>
                                            <?php echo $item['sub_total']?>
                                        </td>

                                    </tr>
                                <?php $i++; } ?>
                            </table>
                            <p class="uk-text-center pos-border-heading">
                                <span>Summary</span>
                            </p>
                            <table width="100%">
                                <tr>
                                    <th>Sub  total</th>
                                    <th>:</th>
                                    <th class="uk-text-left"><?php echo $result['sub_total']?></th>
                                </tr>
                                <tr>
                                    <th>Vat</th>
                                    <th>:</th>
                                    <th class="uk-text-left"><?php echo $result['vat_total']?></th>
                                </tr>
                                <tr>
                                    <th>Discount</th>
                                    <th>:</th>
                                    <th class="uk-text-left"><?php echo $result['discount']?></th>
                                </tr>
                                <tr>
                                    <th>Payable amount</th>
                                    <th>:</th>
                                    <th class="uk-text-left"><?php echo $result['grand_total']?></th>
                                </tr>

                            </table>
                        </div>
                    </div>
                    <?php } ?>
                </div>
                <div class="uk-width-medium-3-10"></div>
            </div>


        </div>
    </div>