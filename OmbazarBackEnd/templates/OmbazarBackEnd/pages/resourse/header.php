<!doctype html>
<!--[if lte IE 9]> <html class="lte-ie9" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> <html lang="en"> <!--<![endif]-->

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Remove Tap Highlight on Windows Phone IE -->
    <meta name="msapplication-tap-highlight" content="no"/>

    <link rel="icon" type="image/png" href="assets/img/favicon-16x16.png" sizes="16x16">
    <link rel="icon" type="image/png" href="assets/img/favicon-32x32.png" sizes="32x32">

    <title>Ombazar</title>


    <!-- uikit -->

    <link rel="stylesheet" href="<?php echo SITE_URL; ?>bower_components/uikit/css/uikit.almost-flat.min.css" media="all">
    <link rel="stylesheet" href="<?php echo SITE_URL; ?>assets/skins/jquery.fancytree/ui.fancytree.min.css" media="all">
    <!-- dropify -->
    <link rel="stylesheet" href="<?php echo SITE_URL; ?>assets/skins/dropify/css/dropify.css">
    <link rel="stylesheet" href="<?php echo SITE_URL; ?>assets/js/plugins/fancybox-master/dist/jquery.fancybox.min.css">
    <link rel="stylesheet" href="<?php echo SITE_URL; ?>assets/js/plugins/jQuery-Inplace-WYSIWYG-Rich-Text-Editor-notebook/src/js/jquery.notebook.css">
    <!-- htmleditor (codeMirror) -->
    <link rel="stylesheet" href="<?php echo SITE_URL; ?>bower_components/codemirror/lib/codemirror.css">
    <!-- flag icons -->
    <link rel="stylesheet" href="<?php echo SITE_URL; ?>assets/icons/flags/flags.min.css" media="all">

    <!-- style switcher -->
    <link rel="stylesheet" href="<?php echo SITE_URL; ?>assets/css/style_switcher.min.css" media="all">

    <!-- altair admin -->
    <link rel="stylesheet" href="<?php echo SITE_URL; ?>assets/css/main.min.css" media="all">
    <link rel="stylesheet" href="<?php echo SITE_URL; ?>assets/css/error_page.min.css" media="all">

    <!-- themes -->
    <link rel="stylesheet" href="<?php echo SITE_URL; ?>assets/css/themes/themes_combined.min.css" media="all">
    <link rel="stylesheet" href="<?php echo SITE_URL; ?>assets/js/plugins/font-awesome-4.7.0/css/font-awesome.min.css" media="all">


    <link rel="stylesheet" href="<?php echo SITE_URL?>assets/css/login_page.min.css" />
    <link rel="stylesheet" href="<?php echo SITE_URL?>assets/js/plugins/light-autocomplete-master/css/style.css" />
    <link rel="stylesheet" href="<?php echo SITE_URL; ?>assets/js/plugins/Drag-And-Drop-File-Uploader-With-Preview-Imageuploadify/dist/imageuploadify.min.css" media="all">
    <link rel="stylesheet" href="<?php echo SITE_URL; ?>assets/js/plugins/Tiny-Text-Field-Based-Tags/dist/tagify.css" media="all">
    <link rel="stylesheet" href="<?php echo SITE_URL; ?>assets/css/custom.css" media="all">


    <!-- matchMedia polyfill for testing media queries in JS -->
    <!--[if lte IE 9]>
        <script type="text/javascript" src="bower_components/matchMedia/matchMedia.js"></script>
        <script type="text/javascript" src="bower_components/matchMedia/matchMedia.addListener.js"></script>
        <link rel="stylesheet" href="assets/css/ie.css" media="all">
    <![endif]-->
</head>
<?php
if(!isset($_SESSION['user_id']) && $page_name!="login"){
    $url=SITE_URL."views/login";
    header("location:$url");
}
elseif(isset($_SESSION['user_id']) && $page_name=="login"){
    $url=SITE_URL."views/home";
    header("location:$url");
}

else{

}
?>
<body class=" sidebar_main_open sidebar_main_swipe ">
    <!-- main header -->
    <?php
        if(isset($_SESSION['user_id'])){
            $user_id=$_SESSION['user_id'];
            $id_type_name=$common->id_type_name($user_id);
            $logger_info=$retrive->controller_info($user_id);
    ?>
    <header id="header_main">
        <div class="header_main_content">
            <nav class="uk-navbar">

                <!-- main sidebar switch -->
                <a href="#" id="sidebar_main_toggle" class="sSwitch sSwitch_left">
                    <span class="sSwitchIcon"></span>
                </a>

                <!-- secondary sidebar switch -->
                <a href="#" id="sidebar_secondary_toggle" class="sSwitch sSwitch_right sidebar_secondary_check">
                    <span class="sSwitchIcon"></span>
                </a>

                <div class="uk-navbar-flip">
                    <ul class="uk-navbar-nav user_actions">
                        <li><a href="#" id="full_screen_toggle" class="user_action_icon uk-visible-large"><i class="material-icons md-24 md-light">&#xE5D0;</i></a></li>

<!--                        <li><a href="--><?php//// echo $help_link;?><!--" id="help_button" class="user_action_icon" target="_blank"><i class="material-icons md-24 md-light" >help_outline</i></a></li>-->
                        <?php
                            if( (isset($_SESSION['store_id']) or isset($_SESSION['vendor_id'])) and ($id_type_name!="vendor" or $id_type_name!="sub_user" or $id_type_name!="user") ){
                        ?>
                        <li>
                        <a href="<?php echo SITE_URL;?>views/power-undo" id="full_screen_toggle" class="user_action_icon ">
                            <i class="material-icons md-24 md-light">undo</i>
                            <?php if($id_type_name=="cs"){?>
                            Return cs
                            <?php } elseif($id_type_name=="admin"){?>
                                Return admin
                            <?php }elseif($id_type_name=="vendor"){ ?>
                                Return vendor
                            <?php } ?>
                        </a>
                        </li>
                        <?php } ?>

                        <li data-uk-dropdown="{mode:'click',pos:'bottom-right'}">

                            <a href="#" class="user_action_image"><img class="md-user-image" src="<?php echo SITE_URL;?>assets/img/images/<?php echo $logger_info[0]['image'];?>" alt=""/></a>
                            <?php// } ?>
                            <div class="uk-dropdown uk-dropdown-small">
                                <ul class="uk-nav js-uk-prevent">

<!--                                    <li><a href="--><?php ////echo SITE_URL;?><!--views/profile/?user_id=--><?php//// echo $_SESSION['user_id']?><!--">My profile</a></li>-->
<!--                                    <li><a href="page_settings.html">Settings</a></li>-->
                                    <li><a href="<?php echo SITE_URL;?>views/logout">Logout</a></li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>

    </header><!-- main header end -->
    <!-- main sidebar -->
    <aside id="sidebar_main">

        <div class="sidebar_main_header">
            <div class="sidebar_logo">

                <a href="<?php echo SITE_URL;?>views/home" class="sSidebar_hide sidebar_logo_large">
                    <img class="logo_regular" src="<?php echo SITE_URL;?>assets/img/default/om-bazar-logo.png" alt="" height="" width=""/>
                    <img class="logo_light" src="<?php echo SITE_URL;?>assets/img/default/om-bazar-logo.png" alt="" height="" width=""/>
                </a>
                <a href="<?php echo SITE_URL;?>views/home" class="sSidebar_show sidebar_logo_small">
                    <img class="logo_regular" src="<?php echo SITE_URL;?>assets/img/default/om-bazar-logo.png" alt="" height="" width=""/>
                    <img class="logo_light" src="<?php echo SITE_URL;?>assets/img/default/om-bazar-logo.png" alt="" height="" width=""/>
                </a>
            </div>
            <?php
                if(isset($_SESSION['vendor_id']) or $id_type_name=="vendor" or $id_type_name=="sub_vendor"){
            ?>
            <div class="sidebar_actions">
                <select class="store_switcher" selected-id="<?php if(isset($_SESSION['store_id'])) {echo $common->store_id;}?>">
                    <option value="" selected>select a store</option>

                </select>

            </div>
            <?php } ?>
        </div>

        <div class="menu_section">
            <?php include_once("Src/inc/menu.php")?>
        </div>
    </aside><!-- main sidebar end -->
    <?php } ?>