<div id="page_content">
    <div id="page_content_inner">

            <div class="uk-grid " data-uk-grid-margin data-uk-grid-match >
                <div class="uk-width-large-1-11">
                    <div class="md-card user_content">
                        <form method="" class="prize-form" id="form">
                            <h4 class="heading_c uk-margin-bottom ">Coupon prize manager </h4>
                            <?php
                                $data=$retrive->prize_info(1);
                                foreach($data as $info){
                            ?>
                            <div class="uk-grid">
                                <div class="uk-width-large-1-2 uk-margin-bottom parsley-row">
                                    <label>Common Prize name</label>
                                    <input class="md-input" type="text" name="common_name" required value="<?php echo $info['prize_name']?>">
                                </div>
<!--                                <div class="uk-width-large-1-2 uk-margin-bottom parsley-row">-->
<!--                                    <label>Amount</label>-->
<!--                                    <input class="md-input" type="text" name="common_amount" required value="--><?php //echo $info['prize_amount']?><!--">-->
<!--                                </div>-->
                                <input  type="hidden" name="common_amount"  value="<?php echo $info['prize_amount']?>">


                            </div>
                            <?php } ?>
                            <div class="uk-grid form_section " id="d_form_row" data-uk-grid-margin>
                                <div class="uk-width-medium-1-2">
                                    <label>Prize name</label>
                                    <input type="text" class="md-input" name="prize_name[]">
                                </div>
                                <div class="uk-width-medium-1-2">
                                    <div class="uk-input-group">
                                        <label>Unlock amount</label>
                                        <input type="text" class="md-input" name="unlock_amount[]">

                                        <span class="uk-input-group-addon">
                                            <a href="#" class="spbtnSectionClone" data-section-clone="#d_form_row"><i class="material-icons md-24">&#xE146;</i></a>
                                        </span>
                                    </div>
                                </div>

                            </div>

                            <div class="uk-grid">
                                <div class="uk-width-large-1-2">
                                    <button class="md-btn md-btn-primary" button-name="save_and_new">Save</button>
                                    <button type="reset" class="md-btn md-btn-info">Cancel</button>
                                </div>
                            </div>
                        </form>
                    </div>

                    <div class="md-card">
                        <div class="md-card-content">
                            <div class="uk-margin-bottom">
                                <a href="#" class=" " id="printTable" data-uk-tooltip="{pos:'top'}" title="Print">
                                    <i class="material-icons md-icon">print</i>
                                </a>
                                <div class="uk-button-dropdown" data-uk-dropdown="{mode:'click'}" style="margin-top:-8px">
                                    <a class="" href="#" data-uk-tooltip="{pos:'top'}" title="Colum select"><i class="material-icons md-icon ">reorder</i></a>
                                    <div class="uk-dropdown">
                                        <ul class="uk-nav uk-nav-dropdown" id="columnSelector"></ul>
                                    </div>
                                </div>
                                <a href="<?php echo SITE_URL?>views/add-store" class="" data-uk-tooltip="{cls:'uk-tooltip-small',pos:'bottom'}" title="Add store"><i class="material-icons md-icon md-color-green-300">add_circle_outline</i></a>


                            </div>
                            <div class="uk-overflow-container uk-margin-bottom">
                                <table data-type="prize" limit="100" class="uk-table uk-table-align-vertical uk-table-nowrap tablesorter tablesorter-altair" id="ts_pager_filter">
                                    <thead>
                                    <tr>
                                        <th data-name="Select" data-priority="5"><input type="checkbox" class="ts_checkbox_all" name="f"></th>
                                        <th data-priority="critical">Prize name</th>
                                        <th data-priority="critical">Unlock amount</th>
                                        <th class="filter-false remove sorter-false uk-text-center" data-priority="1">Manage</th>
                                    </tr>
                                    </thead>
                                    <tfoot>
                                    <tr>
                                        <th></th>
                                        <th>Prize name</th>
                                        <th>Unlock amount</th>
                                        <th class="uk-text-center">Manage</th>
                                    </tr>
                                    </tfoot>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                            <ul class="uk-pagination ts_pager">
                                <li data-uk-tooltip title="Select Page">
                                    <select class="ts_gotoPage ts_selectize"></select>
                                </li>
                                <li class="first"><a href="javascript:void(0)"><i class="uk-icon-angle-double-left"></i></a></li>
                                <li class="prev"><a href="javascript:void(0)"><i class="uk-icon-angle-left"></i></a></li>
                                <li><span class="pagedisplay"></span></li>
                                <li class="next"><a href="javascript:void(0)"><i class="uk-icon-angle-right"></i></a></li>
                                <li class="last"><a href="javascript:void(0)"><i class="uk-icon-angle-double-right"></i></a></li>
                                <li data-uk-tooltip title="Page Size">
                                    <select class="pagesize ts_selectize">
                                        <option value="5">5</option>
                                        <option value="10">10</option>
                                        <option value="25">25</option>
                                        <option value="50">50</option>
                                    </select>
                                </li>
                            </ul>

                            <button  class="load-more-button md-btn md-btn-primary md-btn-mini md-btn-wave-light md-btn-icon waves-effect waves-button waves-light">Load <span></span> more </button>

                        </div>
                    </div>


                </div>

            </div>

    </div>
</div>

<div class="uk-modal" id="prize-edit">

</div>