from django.apps import AppConfig


class OmbazarbackendConfig(AppConfig):
    name = 'OmbazarBackEnd'
