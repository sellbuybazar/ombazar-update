import re
import json
from . models import *
from . helper import *
from .common import Common


class ControllerAction:
    def __init__(self, request):
        self.request = request
        self.common = Common()
        self.model = Controller()

    def data_validation(self):
        errors = {}
        messages = {}
        return_object = {
            "messages": messages,
            "errors": errors
        }
        try:
            common = self.common
            request = self.request
            # all values from form
            this_request = request.POST
            name = this_request['name']
            calling_code = this_request['calling_code']
            mobile = this_request['mobile']
            email = this_request['email']
            password = this_request['password']
            address_line_1 = this_request['address_line_1']
            address_line_2 = this_request['address_line_2']
            post_code = this_request['post_code']
            try:
                nationality = this_request['nationality']
                nationality = json.loads(nationality)
            except Exception:
                nationality = {}

            try:
                this_request['active']
            except Exception:
                active = 0
            else:
                active = 1

            # checking this country code format is correct
            try:
                calling_code = int(calling_code)
            except ValueError:
                messages['invalid_country'] = "Select a valid Country code"
            # checking this mobile number requirement is correct
            try:
                mobile = int(mobile)
            except ValueError:
                messages['invalid_mobile'] = "Enter a valid mobile number"
            else:
                length = len(str(mobile))
                if length < 10:
                    messages['mobile_length'] = "Enter at least 10 digit of your mobile number"
                else:
                    # Checking existence
                    exist_query = {
                        "mobile": str(mobile),
                        "calling_code": str(calling_code)
                    }
                    try:
                        edit_action = this_request['edit']
                        user_id = this_request['user_id']
                        exist_query["user_id"] = {"$ne": user_id}
                    except Exception:
                        pass
                    exist_mobile = common.exist("controllers", exist_query)
                    if exist_mobile:
                        messages['duplicate_mobile'] = "This mobile number is already used!"

            try:
                before_password = this_request['before_password']
            except:
                # password minimum 8 character
                length = len(password)
                if length < 8:
                    messages['password_length'] = "Password must be 8 digit"
                else:
                    password = common.md5(password)
            else:
                if not password:
                    password = before_password
                else:
                    # password minimum 8 character
                    length = len(password)
                    if length < 8:
                        messages['password_length'] = "Password must be 8 digit"
                    else:
                        password = common.md5(password)

            try:
                re_expression = '^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)'
                re_expression += '|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$'
                email_validation = re.match(re_expression, email)
            except Exception as e:
                errors['email_regular_expression'] = "Regular expression method error"
                errors['email_regex_exception'] = str(e)
            else:
                if not email_validation:
                    messages['invalid_email'] = "Enter a valid email address"

            # checking this post code format is correct
            try:
                post_code = int(post_code)
            except Exception:
                messages['invalid_post_code'] = "Enter a valid Post code"
            try:
                access_menus = request.POST.getlist('menus[]')
            except Exception:
                access_menus = []

        except Exception as e:
            errors['data_validate_exception'] = str(e)
        else:
            request_data = {
                "calling_code": str(calling_code),
                "mobile": str(mobile),
                "password": password,
                "active": active,
                "archive": 0,
                "time": time(),
                "name": name,
                "email": email,
                "address_line_1": address_line_1,
                "address_line_2": address_line_2,
                "image": '/media/default/profile.png',
                "access_menus": access_menus,
                "nationality": nationality,
                "post_code": str(post_code)
            }
            # access store for employee
            try:
                access_stores = request.POST.getlist('stores[]')
            except Exception:
                access_stores = []
            else:
                request_data['stores'] = access_stores

            try:
                before_image = this_request['before_image']
                request_data['image'] = before_image
            except Exception:
                pass
            try:
                referer_mobile = this_request['referer_mobile']
                if referer_mobile:
                    try:
                        referer_mobile = int(referer_mobile)
                    except:
                        messages['invalid_referer'] = "Enter a valid referer mobile number"
                    else:
                        request_data['referer_mobile'] = str(referer_mobile)
            except Exception:
                pass
            
            return_object['request_data'] = request_data
        return return_object

    def compare(self, request_data):
        model = self.model
        errors = {}
        messages = {}
        return_object = {
            "messages": messages,
            "errors": errors
        }
        # compare the all data with model design
        try:
            model_compare = model.compare(request_data)
        except Exception:
            errors['model_compare_exception'] = "Compare this model is failed store"
        else:
            if not model_compare:
                errors['model_compare_errors'] = "Model design not matched in (Controller)"
        return return_object


class StoreAction:
    def __init__(self, request):
        self.request = request
        self.common = Common()
        self.model = Store()

    def data_validation(self):
        errors = {}
        messages = {}
        return_object = {
            "messages": messages,
            "errors": errors
        }
        try:
            common = self.common
            request = self.request
            # all values from form
            this_request = request.POST
            store_name = this_request['store_name']
            address_line_1 = this_request['address_line_1']
            address_line_2 = this_request['address_line_2']
            post_code = this_request['post_code']
            country = this_request['country']
            # errors['test_err'] = "Not see this"
            try:
                this_request['active']
                active = 1
            except Exception:
                active = 0

            try:
                post_code = int(post_code)

            except ValueError:
                messages['invalid_post_code'] = "Enter a valid post code"
            try:
                country = int(country)
            except ValueError:
                messages['invalid_country'] = "Enter a valid country"

        except Exception as e:
            errors['data_validate_exception'] = str(e)
        else:
            request_data = {
                "store_name": store_name,
                "address_line_1": address_line_1,
                "address_line_2": address_line_2,
                "post_code": post_code,
                "country": country,
                "active": active,
                "archive": 0,
                "image": '/media/default/logo.png',
                "time": time(),
            }
            try:
                before_image = this_request['before_image']
                request_data['image'] = before_image
            except Exception:
                pass

            return_object['request_data'] = request_data
        return return_object

    def compare(self, request_data):
        model = self.model
        errors = {}
        messages = {}
        return_object = {
            "messages": messages,
            "errors": errors
        }
        # compare the all data with model design
        try:
            model_compare = model.compare(request_data)
        except Exception:
            errors['model_compare_exception'] = "Compare this model is failed store"
        else:
            if not model_compare:
                errors['model_compare_errors'] = "Model design not matched in (Store)"
        return return_object


class CategoryAction:
    def __init__(self, request):
        self.request = request
        self.common = Common()
        self.model = Category()

    def data_validation(self):
        errors = {}
        messages = {}
        return_object = {
            "messages": messages,
            "errors": errors
        }
        try:
            common = self.common
            request = self.request
            try:
                user_id = request.session['logged_user_id']

            except Exception as e:
                errors['user_undefined'] = "Unlogged"
                messages['user_undefined'] = "Login  first"
                errors['user_exist_exception'] = str(e)

            this_request = request.POST
            category_name = this_request['category_name']
            description = this_request['description']

            parent_id = '0'
            try:
                parent_id = this_request['parent_id']
            except Exception:
                pass
            # try:
            #     this_request['active']
            #     active = 1
            # except Exception:
            #     active = 0
            active = 1

        except Exception as e:
            errors['data_validate_exception'] = str(e)
        else:
            request_data = {
                "parent_id": parent_id,
                "category_name": category_name,
                "description": description,
                "active": active,
                "archive": 0,
                "image": '/media/default/no-image.png',
                "time": time(),
            }
            try:
                before_image = this_request['before_image']
                request_data['image'] = before_image
            except Exception:
                pass

            specifications = this_request.getlist("specification_name[]")

            tags = this_request.getlist("specification_tags[]")
            i = 0
            if specifications:
                specifications_list = {}
                for sp in specifications:
                    specifications_list[sp] = tags[i]
                    i += 1
                if specifications_list:
                    request_data['specifications'] = specifications_list

            return_object['request_data'] = request_data
        return return_object

    def compare(self, request_data):
        model = self.model
        errors = {}
        messages = {}
        return_object = {
            "messages": messages,
            "errors": errors
        }
        # compare the all data with model design
        try:
            model_compare = model.compare(request_data)
        except Exception:
            errors['model_compare_exception'] = "Compare this model is failed Category"
        else:
            if not model_compare:
                errors['model_compare_errors'] = "Model design not matched in (Category)"
        return return_object


class ItemAction:
    def __init__(self, request):
        self.request = request
        self.common = Common()
        self.model = Item()

    def data_validation(self):
        errors = {}
        messages = {}
        return_object = {
            "messages": messages,
            "errors": errors
        }
        quantity = 0
        try:
            common = self.common
            request = self.request
            try:
                user_id = request.session['logged_user_id']
            except Exception as e:
                errors['user_undefined'] = "Unlogged"
                messages['user_undefined'] = "Login  first"
                errors['user_exist_exception'] = str(e)

            this_request = request.POST
            item_name = this_request['item_name']
            category_id = this_request['category_id']
            description = this_request['description']
            bar_code = this_request['bar_code']
            purchase = this_request['purchase']
            price = this_request['price']
            sale = this_request['sale']
            vat = this_request['vat']
            short_description = this_request['short_description']
            # when action from vendor
            try:
                quantity = this_request['quantity']
            except:
                pass
            else:
                try:
                    quantity = int(quantity)
                except:
                    messages['invalid_quantity'] = "Enter a valid numeric quantity"

            # try:
            #     this_request['active']
            #     active = 1
            # except Exception:
            #     active = 0
            active = 1

            try:
                purchase = float(purchase)
            except ValueError:
                messages['purchase'] = "Enter a valid purchase amount"
            try:
                price = float(price)
                if price < 1:
                    messages['zero_amount'] = "Zero price not allow"
            except ValueError:
                messages['price'] = "Enter a valid price amount"
            try:
                sale = float(sale)
            except ValueError:
                messages['sale'] = "Enter a valid sale amount"
            try:
                vat = float(vat)
            except ValueError:
                messages['vat'] = "Enter a valid vat amount"
            if not category_id:
                messages['category'] = "Select a category"

        except Exception as e:
            errors['data_validate_exception'] = str(e)
        else:
            request_data = {
                "item_name": item_name,
                "category_id": category_id,
                "description": description,
                "short_description": short_description,
                "bar_code": bar_code,
                "purchase": purchase,
                "active": active,
                "price": price,
                "sale": sale,
                "vat": vat,
                "archive": 0,
                "images": ['/media/default/item.png'],
                "time": time(),
            }

            try:
                before_bar_code = json.loads(this_request['before_bar_code'])
                request_data['before_bar_code'] = before_bar_code
            except Exception:
                request_data['before_bar_code'] = {}
            request_data['quantity'] = quantity
            specifications = this_request.getlist("specification_name[]")
            tags = this_request.getlist("tags[]")
            i = 0
            if specifications:
                specifications_list = {}
                for sp in specifications:
                    if sp:
                        specifications_list[sp] = tags[i]
                        i += 1
                if specifications_list:
                    request_data['specifications'] = specifications_list

            return_object['request_data'] = request_data
        return return_object

    def compare(self, request_data):
        model = self.model
        errors = {}
        messages = {}
        return_object = {
            "messages": messages,
            "errors": errors
        }
        # compare the all data with model design
        try:
            model_compare = model.compare(request_data)
        except Exception:
            errors['model_compare_exception'] = "Compare this model is failed Item"
        else:
            if not model_compare:
                errors['model_compare_errors'] = "Model design not matched in (Item)"
        return return_object


class PrizeAction:
    def __init__(self, request):
        self.request = request
        self.common = Common()
        self.model = Prize()

    def data_validation(self):
        errors = {}
        messages = {}
        return_object = {
            "messages": messages,
            "errors": errors
        }
        try:
            request = self.request
            # all values from form
            this_request = request.POST
            prize_name = this_request['prize_name']
            unlock_amount = this_request['unlock_amount']
            try:
                unlock_amount = int(unlock_amount)
            except ValueError:
                messages['invalid_amount'] = "Enter a valid numeric amount"

        except Exception as e:
            errors['data_validate_exception'] = str(e)
        else:
            request_data = {
                "prize_name": prize_name,
                "unlock_amount": unlock_amount,
                "active": 1,
                "archive": 0,
                "time": time(),
            }
            return_object['request_data'] = request_data
        return return_object

    def compare(self, request_data):
        model = self.model
        errors = {}
        messages = {}
        return_object = {
            "messages": messages,
            "errors": errors
        }
        # compare the all data with model design
        try:
            model_compare = model.compare(request_data)
        except Exception:
            errors['model_compare_exception'] = "Compare this model is failed prize"
        else:
            if not model_compare:
                errors['model_compare_errors'] = "Model design not matched in (Prize)"
        return return_object


class CorporateAction:
    def __init__(self, request):
        self.request = request
        self.common = Common()
        self.model = Prize()

    def data_validation(self):
        errors = {}
        messages = {}
        return_object = {
            "messages": messages,
            "errors": errors
        }
        try:
            request = self.request
            # all values from form
            this_request = request.POST
            company_name = this_request['company_name']
            tags = this_request['tags']
            phone = this_request['phone']
            email = this_request['email']
            location = this_request['location']
            address = this_request['address']

        except Exception as e:
            errors['data_validate_exception'] = str(e)
        else:
            request_data = {
                "company_name": company_name,
                "tags": tags,
                "phone": phone,
                "email": email,
                "location": location,
                "address": address,
                "notes": [],
                "images": ['/media/default/logo.png'],
                "active": 1,
                "archive": 0,
                "time": time(),
            }
            note_list = request.POST.getlist('notes[]')
            notes = []
            if note_list:
                for note in note_list:
                    if note:
                        note_ob = {
                            "content": note,
                            "time": time()
                        }
                        notes.append(note_ob)

                request_data['notes'] = notes
            return_object['request_data'] = request_data
        return return_object

    def compare(self, request_data):
        model = self.model
        errors = {}
        messages = {}
        return_object = {
            "messages": messages,
            "errors": errors
        }
        # compare the all data with model design
        try:
            model_compare = model.compare(request_data)
        except Exception:
            errors['model_compare_exception'] = "Compare this model is failed corporate"
        else:
            if not model_compare:
                errors['model_compare_errors'] = "Model design not matched in (Corporate)"
        return return_object


class StoreItemAction:
    def __init__(self, request):
        self.request = request
        self.common = Common()
        self.model = StoreItem()

    def data_validation(self):
        errors = {}
        messages = {}
        return_object = {
            "messages": messages,
            "errors": errors
        }
        try:
            vendor_id = store_id = item_id = ""
            common = self.common
            request = self.request
            # Check vendor and store id authentication
            try:
                logged_user_id = request.session['logged_user_id']
                id_type_name = common.id_type_name(logged_user_id)
                if id_type_name == "vendor":
                    vendor_id = logged_user_id
                elif id_type_name == "admin" or id_type_name == "employee":
                    try:
                        vendor_id = request.session['vendor_id']
                    except Exception:
                        messages['undefined_vendor'] = "Select a vendor first"

            except Exception:
                messages['login_required'] = "Login first"
            try:
                store_id = request.session['store_id']
            except Exception:
                messages['undefined_store'] = "Select a store first"
            try:
                item_id = request.POST['item_id']
            except Exception:
                messages['undefined_item'] = "Something went this wrong item info"

            request = self.request
            # all values from form
            this_request = request.POST
            purchase = this_request['purchase']
            price = this_request['price']
            sale = this_request['sale']
            quantity = this_request['quantity']

            try:
                purchase = float(purchase)
            except ValueError:
                messages['invalid_purchase'] = "Enter a valid numeric Purchase amount"
            try:
                price = float(price)
                if price < 1:
                    messages['zero_amount'] = "Zero price not allow"
            except ValueError:
                messages['invalid_price'] = "Enter a valid numeric Price amount"
            try:
                sale = float(sale)
            except ValueError:
                messages['invalid_sale'] = "Enter a valid numeric Sale amount"
            try:
                quantity = int(quantity)
            except ValueError:
                messages['invalid_quantity'] = "Enter a valid numeric Quantity amount"
            before_quantity = quantity
        except Exception as e:
            errors['data_validate_exception'] = str(e)
        else:
            request_data = {
                "vendor_id": vendor_id,
                "store_id": store_id,
                "item_id": item_id,
                "purchase": purchase,
                "price": price,
                "sale": sale,
                "quantity": quantity,
                "time": time(),
                "active": 1,
                "archive": 0,
                "item_code": '',
                "before_quantity": before_quantity,
            }
            try:
                point = common.point_maker(request_data, 1)
                request_data['point'] = point
            except:
                errors['point_rule_undefined'] = 'Point rule not set'

            return_object['request_data'] = request_data
        return return_object

    def compare(self, request_data):
        model = self.model
        errors = {}
        messages = {}
        return_object = {
            "messages": messages,
            "errors": errors
        }
        # compare the all data with model design
        try:
            model_compare = model.compare(request_data)
        except Exception:
            errors['model_compare_exception'] = "Compare this model is failed Store item"
        else:
            if not model_compare:
                errors['model_compare_errors'] = "Model design not matched in (Store item)"
        return return_object


class OrderAction:
    def __init__(self, request):
        self.request = request
        self.common = Common()
        self.model = Order()

    def data_validation(self):
        errors = {}
        messages = {}
        return_object = {
            "messages": messages,
            "errors": errors
        }
        try:
            store_id = item_id = data = order_type = ""
            common = self.common
            request = self.request

            this_request = request.POST
            customer_id = ""
            try:
                customer_id = this_request['customer_id']
            except:
                pass
            discount = 0
            try:
                discount = float(this_request['discount'])
            except ValueError:
                messages['invalid_discount'] = "Enter a valid discount amount"

            # If order   from Pos
            if "order_type" in this_request:
                order_type = this_request['order_type']
                if order_type == "pos":
                    # Check vendor and store id authentication
                    try:
                        logged_user_id = request.session['logged_user_id']
                        id_type_name = common.id_type_name(logged_user_id)
                        if id_type_name == "vendor":
                            vendor_id = logged_user_id
                        elif id_type_name == "admin" or id_type_name == "employee":
                            try:
                                vendor_id = request.session['vendor_id']
                            except Exception:
                                messages['undefined_vendor'] = "Select a vendor first"

                    except Exception:
                        messages['login_required'] = "Login first"
                    try:
                        store_id = request.session['store_id']
                    except Exception:
                        messages['undefined_store'] = "Select a store first"
                elif order_type == "web":
                    try:
                        logged_user_id = request.session['logged_user_id']
                    except Exception:
                        messages['login_required'] = "Login  first for order"
            else:
                errors['order_type_invalid'] = "Order type not found"

        except Exception as e:
            errors['data_validate_exception'] = str(e)
        else:
            request_data = {
                "active": 1,
                "archive": 0,
                "time": time(),
                "status": "Pending",
                "customer_id": customer_id,
                "discount": discount,
                "order_type": order_type,
            }
            if order_type == "pos":
                request_data['status'] = "Completed"
            try:
                discount = this_request['discount']
                if discount:
                    try:
                        discount = float(discount)
                    except Exception:
                        messages['discount_invalid'] = "Discount not valid"
            except:
                pass

            return_object['request_data'] = request_data
        return return_object

    def compare(self, request_data):
        model = self.model
        errors = {}
        messages = {}
        return_object = {
            "messages": messages,
            "errors": errors
        }
        # compare the all data with model design
        try:
            model_compare = model.compare(request_data)
        except Exception:
            errors['model_compare_exception'] = "Compare this model is failed Order"
        else:
            if not model_compare:
                errors['model_compare_errors'] = "Model design not matched in (Order)"
        return return_object

