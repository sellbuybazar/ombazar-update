import re
import json
import random
from .common import Common
from .retrieve import Retrieve
from .document_actions import *
from .models import *
from .helper import *
from openpyxl import load_workbook
from io import BytesIO


class Other:
    def __init__(self):
        self.common = Common()
        self.retrieve = Retrieve()
        self.db = self.common.db

    def activate(self, request, activate_id):
        errors = {}
        messages = {}
        return_object = {
            "status": 0,
            "error": errors,
            "message": messages
        }
        try:
            logged_user_id = ''
            try:
                logged_user_id = request.session['logged_user_id']
            except:
                messages['login_required'] = "Login first"
            id_info = {}
            id_type_name = self.common.id_type_name(activate_id)
            if id_type_name == 'vendor':
                request.session['vendor_id'] = activate_id
                # last activities
                last_activities = self.common.last_activities(request)
                return_object.update(last_activities)
                try:
                    del request.session['store_id']
                except:
                    pass

                id_info = self.common.collection_data_one("controllers", {"user_id": activate_id})
            elif id_type_name == 'store':
                try:
                    logged_user_id = request.session['logged_user_id']
                except:
                    errors['login_required'] = "Login first"
                else:
                    logged_id_type_name = self.common.id_type_name(logged_user_id)

                    if logged_id_type_name != "vendor":
                        try:
                            vendor_id = request.session['vendor_id']
                        except Exception as e:
                            messages['undefined_vendor'] = "Chose a vendor first!"
                            return return_object
                    request.session['store_id'] = activate_id
                    self.common.assign_last_update("last_store", logged_user_id, {"value": activate_id})
                    id_info = self.common.collection_data_one("stores", {"store_id": activate_id})

            elif id_type_name == "employee":
                request.session['employee_id'] = activate_id
                id_info = self.common.collection_data_one("controllers", {"user_id": activate_id})
            if id_type_name == "vendor" or id_type_name == "employee":
                stores = id_info['stores']
                store_details = self.common.collection_data("stores", {"store_id": {"$in": stores}},
                                                     {"store_name": 1, "store_id": 1})
                if store_details['find_data']:
                    stores = store_details['find_data']
                    return_object['stores'] = stores

        except Exception as e:
            errors['activate_exception'] = str(e)
        else:
            # First check any error available
            if not return_object['error'] and not return_object['message']:
                return_object['status'] = 1
                return_object['id_type'] = id_type_name
                return_object['id_info'] = id_info
                return_object['activate_id'] = activate_id

        return_object['basic_info'] = self.retrieve.basic_information(request)
        return return_object