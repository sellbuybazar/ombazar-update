from django.urls import path, re_path
from . import views
from .helper import *
urlpatterns = [
    path('', views.index, name="index"),
    path(''+'install/', views.install, name="install"),
    path('home/', views.index, name="index"),
    path('index/', views.index, name="index"),
    path('add-admin/', views.add_admin, name="add_admin"),
    path('request/', views.request, name="request"),
    re_path(r'^.+?$', views.index, name="all_page"),
]
