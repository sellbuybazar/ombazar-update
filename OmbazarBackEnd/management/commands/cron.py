from django.core.management.base import BaseCommand, CommandError
from OmbazarBackEnd .passed import Passed
import schedule
import time


class Command(BaseCommand):
    help = 'Closes the specified poll for voting'

    def add_arguments(self, parser):
        pass

    def handle(self, *args, **options):
        print(time.strftime("%I:%M  %p tis time"))
        passed = Passed()
        job = passed.point_distributor
        schedule.every().day.at("23:59").do(job)
        # schedule.every().day.at("03:19").do(job)
        # schedule.every(10).seconds.do(job)

        while True:
            schedule.run_pending()
            time.sleep(1)
