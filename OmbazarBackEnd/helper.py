import datetime
import os
import uuid
import json
import random
import math
import re
from .models import *
from bson import ObjectId
from pymongo import UpdateOne, UpdateMany ,InsertOne
from django.conf import settings
from socket import gethostname, gethostbyname


def time():
    return datetime.datetime.now()


def time_folder():
    return time().strftime("%Y/%m/%d/")


def time_unique_id():
    return time().strftime("%y%m%d%H%M%S%f%Z")


unique_id = uuid.uuid4()


def ip():
    return gethostbyname(gethostname())


def after_minute(minute):
    after_minutes = time() + datetime.timedelta(minutes=minute)
    return after_minutes


def after_time(day=0, hour=0, minute=0, custom_time=0):
    if not custom_time:
        custom_time = time()
    after_minutes = custom_time + datetime.timedelta(minutes=minute, hours=hour, days=day)
    return after_minutes


base_bir = settings.BASE_PATH
os = os
ObjectId = ObjectId


class Decoder(json.JSONDecoder):
    def decode(self, s):
        result = super().decode(s)  # result = super(Decoder, self).decode(s) for Python 2.x
        return self._decode(result)

    def _decode(self, o):
        if isinstance(o, str):
            try:
                return float(o)
            except ValueError:
                return o
        elif isinstance(o, dict):
            return {k: self._decode(v) for k, v in o.items()}
        elif isinstance(o, list):
            return [self._decode(v) for v in o]
        else:
            return o
